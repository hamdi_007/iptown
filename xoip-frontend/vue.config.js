module.exports = {
  devServer: {
    port: process.env.VUE_APP_PORT,
    disableHostCheck: true,
    watchOptions: {
      poll: true
    }
  },
  transpileDependencies: ['vuetify'],
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = process.env.VUE_APP_NAME
        return args
      })
  }
}
