import BaseCheckbox from './inputs/BaseCheckbox.vue'
import Radio from './inputs/BaseRadio.vue'
import BaseInput from './inputs/BaseInput.vue'

import BaseDropdown from './BaseDropdown.vue'
import Table from './Table.vue'

import Card from '../../../../iptown/xoproject/xoip/xoip-frontend/src/components/Cards/Card.vue'
import ChartCard from '../../../../iptown/xoproject/xoip/xoip-frontend/src/components/Cards/ChartCard.vue'
import StatsCard from '../../../../iptown/xoproject/xoip/xoip-frontend/src/components/Cards/StatsCard.vue'

import SidebarPlugin from './sidebarplugin'

const components = {
  BaseCheckbox,
  Radio,
  BaseInput,
  Card,
  ChartCard,
  StatsCard,
  Table,
  BaseDropdown,
  SidebarPlugin
}

export default components
