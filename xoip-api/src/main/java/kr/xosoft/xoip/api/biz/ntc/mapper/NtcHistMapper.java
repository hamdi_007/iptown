package kr.xosoft.xoip.api.biz.ntc.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.ntc.vo.NtcHistVo;

/**
 * 공지사항이력 매퍼 
 * @author hypark
 *
 */
@Repository
public interface NtcHistMapper {

    /**
     * 공지사항이력 등록 
     */
    public void insertNtcHistAsSelectMaster(NtcHistVo ntcHist);    
    
}
