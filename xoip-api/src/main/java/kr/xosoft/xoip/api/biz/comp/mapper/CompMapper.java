package kr.xosoft.xoip.api.biz.comp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.comp.vo.CompVo;

/**
 * 회사 매퍼 
 * @author likejy
 *
 */
@Repository
public interface CompMapper {

    /**
     * 회사 등록
     * @param comp
     * @return
     */
    public int insertComp(CompVo comp);
    
    /**
     * 회사 수정 by PK
     * @param comp
     * @return
     */
    public int updateCompByKey(CompVo comp);
        
    /**
     * 회사 삭제 by PK
     * @param comp
     * @return
     */
    public int deleteCompByKey(CompVo comp);
    
    /**
     * 회사 조회 by PK
     * @param siteCd
     * @param compId
     * @return
     */
    public CompVo findCompByKey(@Param("siteCd") String siteCd, @Param("compId") String compId);
    
    /**
     * 회사 조회 by 회사코드 
     * @param siteCd
     * @param compCd
     * @return
     */
    public CompVo findCompByCompCd(@Param("siteCd") String siteCd, @Param("compCd") String compCd);
    
    /**
     * 회사 조회 by 사업자등록번호 
     * @param siteCd
     * @param bizno
     * @return
     */
    public CompVo findCompByBizno(@Param("siteCd") String siteCd, @Param("bizno") String bizno);
    
    /**
     * 라이선서 회사 조회 
     * @param siteCd
     * @return
     */
    public CompVo findLicrComp(@Param("siteCd") String siteCd);
    
    /**
     * 라이선시 회사 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countLiceCompListByMap(Map<String, Object> map);
    
    /**
     * 라이선시 회사 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<CompVo> findLiceCompListByMap(Map<String, Object> map);
        
}
