package kr.xosoft.xoip.api.biz.email.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.support.BizSupport;
import kr.xosoft.xoip.api.biz.email.mapper.EmailFormMapper;
import kr.xosoft.xoip.api.biz.email.mapper.EmailTrnsHistMapper;
import kr.xosoft.xoip.api.biz.email.vo.EmailFormVo;
import kr.xosoft.xoip.api.biz.email.vo.EmailReqFormVo;
import kr.xosoft.xoip.api.biz.email.vo.EmailTrnsHistVo;
import kr.xosoft.xoip.api.biz.email.vo.EmailTrnsVo;
import kr.xosoft.xoip.api.biz.site.service.SiteService;
import kr.xosoft.xoip.api.biz.site.vo.SiteSmtpVo;

/**
 * 이메일 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class EmailServiceImpl implements EmailService {

    @Value("${api.xoip.webUrl}")
    private String webUrl;    
    
    @Autowired
    private SiteService siteService;
    
    @Autowired
    private SmtpService smtpService;
    
    @Autowired
    private EmailFormMapper emailFormMapper;
    
    @Autowired
    private EmailTrnsHistMapper emailTrnsHistMapper;
    
    @Override
    public void requestForSendingEmail(EmailReqFormVo emailReqForm) {
        
        List<String> recpList = emailReqForm.getRecpList();
        Verifier.notNull(recpList, "M1004", "word.recpList");
        Verifier.gt(recpList.size(), 0, "M1004", "word.recpList");
        
        Map<String, Object> varMap = emailReqForm.getVarMap();
        if (varMap == null) {
            varMap = new HashMap<String, Object>();
        }
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 이메일 사용 언어에 맞는 사이트 SMTP 정보 조회 
        // IMPORTANT : CommonData 의 lang 값을 사용하면 안된다 
        SiteSmtpVo siteSmtp = siteService.findLocalizedSiteSmtp(emailReqForm.getTmplLang());
        Verifier.notNull(siteSmtp, "M1003", "word.siteSmtp");
        
        // SMTP사용여부가 "N"인 경우는 이메일을 발송하지 않는다. 
        if ("N".equals(siteSmtp.getSmtpUseYn())) {
            return;
        }
        
        // 이메일 사용 언어에 맞는 이메일폼 조회
        // IMPORTANT : CommonData 의 lang 값을 사용하면 안된다
        EmailFormVo emailForm = emailFormMapper.findCurrentLocalizedEmailFormByKey(commonData.getSiteCd(), emailReqForm.getFormCd(), emailReqForm.getTmplLang());
        Verifier.notNull(emailForm, "M1003", "word.emailForm");
        varMap.put("SERVICE_NAME", emailForm.getSiteNm());
        
        // 이메일폼 제목, 내용 변수 치환 
        Iterator<String> keys = varMap.keySet().iterator();
        String subject = emailForm.getSbjTmpl();
        String contents = emailForm.getCntsTmpl();
        while(keys.hasNext()) {
            String key = keys.next();
            Object obj = varMap.get(key);
            if (!(obj instanceof String)) {
                continue;
            }
            String asis = "#{" + key + "}";
            String tobe = (String)obj;
            subject = StringUtils.replace(subject, asis, tobe);
            contents = StringUtils.replace(contents, asis, tobe);
        }
        
        // 제목, 내용 변수 치환 상태 점검  
        if (subject.indexOf("#{") >= 0 || contents.indexOf("#{") >= 0) {
            Verifier.fail("M1200");
        }        
        if (siteSmtp.getHeadTmpl() != null) {
            String siteWebUrl = BizSupport.parseSiteWebUrl(commonData.getSiteCd(), webUrl);
            String headTmpl = siteSmtp.getHeadTmpl();
            headTmpl = StringUtils.replace(headTmpl, "#{WEB_URL}", siteWebUrl);
            headTmpl = StringUtils.replace(headTmpl, "#{SITE_CD}", commonData.getSiteCd());
            contents = headTmpl + contents;
        }
        if (siteSmtp.getFootTmpl() != null) {
            contents = contents + siteSmtp.getFootTmpl();
        }
        
        // 이메일전송 VO 설정 
        // IMPORTANT : EmailTrnsVo 는 비동기 스레드에서 별도로 사용되므로, siteCd 가 "명시적"으로 할당되어 있어야 한다.  
        EmailTrnsVo emailTrns = new EmailTrnsVo(siteSmtp);
        emailTrns.setSiteCd(commonData.getSiteCd()); 
        emailTrns.setFormCd(emailForm.getFormCd());
        emailTrns.setFormNo(emailForm.getFormNo());
        emailTrns.setTrnsReqDt(new Date());
        emailTrns.setTrnsReqCompId(commonData.getCompId());
        emailTrns.setTrnsReqrId(commonData.getUsrId());
        emailTrns.setRecpList(recpList);
        emailTrns.setTmplVarFtxt(varMap.toString());
        emailTrns.setEmailSbj(subject);
        emailTrns.setEmailCnts(contents);
        
        // 이메일 전송 (비동기) 
        smtpService.sendEmail(emailTrns);
    }

    @Transactional( propagation = Propagation.REQUIRES_NEW )
    @Override
    public EmailTrnsHistVo registEmailTrnsHist(EmailTrnsHistVo emailTrnsHist) {
        
        emailTrnsHistMapper.insertEmailTrnsHist(emailTrnsHist);
        Verifier.notNull(emailTrnsHist.getEmailTrnsHistSeqno(), "M1000", "word.emailTrnsHist");
        
        return emailTrnsHist;                
    }
    
}
