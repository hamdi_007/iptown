package kr.xosoft.xoip.api.biz.faq.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.faq.vo.FaqFileVo;

/**
 * FAQ파일 매퍼 
 * @author hypark
 *
 */
@Repository
public interface FaqFileMapper {

    /**
     * FAQ파일 등록
     * @param faqFile
     * @return
     */
    public int insertFaqFile(FaqFileVo faqFile);
        
    /**
     * FAQ파일 삭제 by PK
     * @param faqFile
     * @return
     */
    public int deleteFaqFileByKey(FaqFileVo faqFile);
    
    /**
     * FAQ파일 조회 by PK
     * @param siteCd
     * @param faqNo
     * @param faqFileNo
     * @return
     */
    public FaqFileVo findFaqFileByKey(@Param("siteCd") String siteCd, @Param("faqNo") Integer faqNo, @Param("faqFileNo") Integer faqFileNo);
    
    /**
     * FAQ파일 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countFaqFileListByMap(Map<String, Object> map);
    
    /**
     * FAQ파일 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<FaqFileVo> findFaqFileListByMap(Map<String, Object> map);    
    
}
