package kr.xosoft.xoip.api.common.parser;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import kr.xosoft.xoip.api.common.support.AuthSupport;

/**
 * 로그인 사용자가 라이선서인 경우에만 문자열 값을 직렬화    
 * @author likejy
 *
 */
public class AvailableLicensorStringSerializer extends StdSerializer<String> {

    private static final long serialVersionUID = 1L;
    
    public AvailableLicensorStringSerializer() {
        this(null);
    }
    
    public AvailableLicensorStringSerializer(Class<String> t) {
        super(t);
    }

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (AuthSupport.isLicensor()) {
            gen.writeString(value);                    
        } else {
            gen.writeNull();
        }
    }
    
}
