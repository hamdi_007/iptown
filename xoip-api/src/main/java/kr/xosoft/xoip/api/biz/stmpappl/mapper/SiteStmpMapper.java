package kr.xosoft.xoip.api.biz.stmpappl.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplVo;

/**
 * 증지신청 매퍼 
 * @author likejy
 *
 */
@Repository
public interface SiteStmpMapper {

    /**
     * 사이트증지 조회 by 프로젝트코드 
     * @param siteCd
     * @Param projCd
     * @return
     */
    public StmpApplVo findSiteStmpByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
            
}
