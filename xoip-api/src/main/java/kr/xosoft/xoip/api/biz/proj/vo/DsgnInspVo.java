package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 디자인검수 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class DsgnInspVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=UpdatingGroup.class )        
    private String projCd;

    /**
     * 검수번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private Integer inspNo;

    /**
     * 검수단계코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )            
    private String inspStgCd;

    /**
     * 검수단계코드명
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String inspStgCdNm;
    
    /**
     * 다음검수단계코드
     */
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )
    private String nxtInspStgCd;
    
    /**
     * 다음검수단계코드명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String nxtInspStgCdNm;

    /**
     * 검수상태코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )    
    private String inspStsCd;
    
    /**
     * 검수상태코드명
     */
    @JsonProperty( access=Access.READ_ONLY )    
    private String inspStsCdNm;

    /**
     * 검수신청일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date inspApplDt;

    /**
     * 검수신청자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String inspApplrId;

    /**
     * 검수신청내용
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String inspApplCnts;

    /**
     * 검수일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date inspDt;

    /**
     * 검수자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String insprId;
    
    /**
     * 검수자명
     */
    @JsonProperty( access=Access.READ_ONLY )    
    private String insprNm;

    /**
     * 검수의견내용
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String inspOpinCnts;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
}
