package kr.xosoft.xoip.api.biz.com.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.com.mapper.ComCdMapper;
import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import kr.xosoft.xoip.api.biz.com.vo.SiteComCdVo;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;

/**
 * 공통코드 서비스 Impl
 * @author likejy
 *
 */
@Service
@Transactional
public class ComCdServiceImpl implements ComCdService {
    
    @Autowired
    ComCdMapper comCdMapper;
    
    @Autowired
    private ComCdService selfService;
    
    @PostConstruct
    public void postConstruct() {
        // 빈 초기화 시점에 공통코드 캐싱 
        selfService.findAllSiteComCdList();
    }    
    
    /**
     * 모든 사이트 공통코드 목록 조회 
     */
    @Cacheable(value = "comCdList")
    @Override
    public Map<String, SiteComCdVo> findAllSiteComCdList() {
        
        // 공통코드 목록 조회 
        List<ComCdVo> list = comCdMapper.findAllComCdList();
        
        // 사이트별, 상위코드별로 공통코드 할당 
        Map<String, SiteComCdVo> rootMap = new HashMap<String, SiteComCdVo>();
        SiteComCdVo siteComCdVo = null;
        
        String currSiteCd = null;
        String currTopComCd = null;
        List<ComCdVo> currComCdVoList = null;
        for (ComCdVo comCdVo : list) {
            if (currSiteCd == null || !comCdVo.getTopSiteCd().equals(currSiteCd)) {
                currSiteCd = comCdVo.getTopSiteCd();
                siteComCdVo = new SiteComCdVo();
                rootMap.put(currSiteCd, siteComCdVo);
            }
            if (currTopComCd == null || !comCdVo.getTopComCd().equals(currTopComCd)) {
                currTopComCd = comCdVo.getTopComCd();
                currComCdVoList = new ArrayList<ComCdVo>();
                siteComCdVo.addComCdListByTopComCd(currTopComCd, currComCdVoList);
            }
            siteComCdVo.setComCd(comCdVo.getComCd(), comCdVo);
            currComCdVoList.add(comCdVo);
        }
        
        return rootMap;
    }
    
    /**
     * 공통코드명 조회 by 공통코드 
     */
    public String findComCdNmByComCd(String comCd) {
        
        if (comCd == null) {
            return null;
        }
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        SiteComCdVo siteComCdVo = selfService.findAllSiteComCdList().get(siteCd);
        if (siteComCdVo == null) {
            return null;
        }
        
        ComCdVo comCdVo = siteComCdVo.getComCd(comCd);
        if (comCdVo == null) {
            return null;
        } else {
            return comCdVo.getComCdNm();
        }
    }

    /**
     * 공통코드 목록 조회 by 상위코드  
     */
    public List<ComCdVo> findComCdListByTopComCd(String topComCd, boolean isOnlyAvailable) {
        
        if (topComCd == null) {
            return new ArrayList<ComCdVo>();
        }
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        SiteComCdVo siteComCdVo = selfService.findAllSiteComCdList().get(siteCd);
        if (siteComCdVo == null) {
            return new ArrayList<ComCdVo>();
        }
        
        List<ComCdVo> allList = siteComCdVo.getComCdListByTopComCd(topComCd); 
        if (isOnlyAvailable) {
            List<ComCdVo> availableList = new ArrayList<ComCdVo>();
            for (ComCdVo comCd : allList) {
                if ("Y".equals(comCd.getUseYn())) {
                    availableList.add(comCd);
                }
            }
            return availableList;
        } else {
            return allList;
        }
    }
    
	/**
	 * 공통코드 목록 조회 by 컴마로 구분된 상위코드 문자열   
	 */
	@Override
	public Map<String, Object> findComCdListByCommaSeperatedTopComCd(String commaSeperatedTopComCd, boolean isOnlyAvailable) {
	    
	    if (commaSeperatedTopComCd == null) {
	        return new HashMap<String, Object>();
	    }
	    
	    String siteCd = CommonDataHolder.getCommonData().getSiteCd();
	    SiteComCdVo siteComCdVo = selfService.findAllSiteComCdList().get(siteCd);
	    if (siteComCdVo == null) {
	        return new HashMap<String, Object>();
	    }
	    
	    String[] topComCds = StringUtils.split(commaSeperatedTopComCd, ",");
	    Map<String, Object> map = new HashMap<String, Object>();
	    for (String topComCd : topComCds) {
	        List<ComCdVo> allList = siteComCdVo.getComCdListByTopComCd(topComCd);
	        if (isOnlyAvailable) {
	            List<ComCdVo> availableList = new ArrayList<ComCdVo>();
	            for (ComCdVo comCd : allList) {
	                if ("Y".equals(comCd.getUseYn())) {
	                    availableList.add(comCd);
	                }
	            }
	            map.put(topComCd, availableList);
	        } else {
	            map.put(topComCd, allList);
	        }
	    }
	    return map;
	}
	
}
