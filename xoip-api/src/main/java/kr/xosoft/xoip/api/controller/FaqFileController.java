package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.faq.service.FaqFileService;
import kr.xosoft.xoip.api.biz.faq.vo.FaqFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.FaqFileWrapper;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * FAQ파일 Controller 
 * @author hypark
 *
 */
@RestController
public class FaqFileController {

    @Autowired
    private FaqFileService faqFileService;

    @Autowired
    private CommonFileProcessor commonFileProcessor;

    @Value("${api.file.basePath}")
    private String basePath;
    
    /**
     * FAQ파일을 등록한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping( value="/api/{version}/faq/{faqNo}/file", method=RequestMethod.POST, headers="content-type=multipart/form-data" )
    public ResponseEntity<CommonResult> registFaqFile(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo,
            @RequestParam( value="file", required=false ) MultipartFile mFile) {
        
        Verifier.notNull(mFile, "M1004", "mFile");
        FaqFileWrapper FaqFileWrapper = new FaqFileWrapper(mFile, faqNo, basePath);
        commonFileProcessor.uploadFile(FaqFileWrapper);

        FaqFileVo fetchedFaqFile = faqFileService.registFaqFile((FaqFileVo)FaqFileWrapper.getFileVo());            
        return new ResponseEntity<CommonResult>(new CommonResult("faqFile", fetchedFaqFile), HttpStatus.OK);
    }   
    
    /**
     * FAQ파일을 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping( value="/api/{version}/faq/{faqNo}/file/{faqFileNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteFaqFile(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo,
            @PathVariable("faqFileNo") final Integer faqFileNo) {
        
        faqFileService.deleteFaqFile(faqNo, faqFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * FAQ파일을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/faq/{faqNo}/file/{faqFileNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findFaqFile(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo,
            @PathVariable("faqFileNo") final Integer faqFileNo) {
        
        FaqFileVo fetchedFaqFile = faqFileService.findFaqFile(faqNo, faqFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult("faqFile", fetchedFaqFile), HttpStatus.OK);
    }
    
    /**
     * FAQ파일 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/faq/{faqNo}/file", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findFaqFilePage(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo,
            final CommonCondition condition) {
        
        CommonPage<FaqFileVo> fetchedFaqFilePage = faqFileService.findFaqFilePage(faqNo, condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("faqFilePage", fetchedFaqFilePage), HttpStatus.OK);
    }  
    
    /**
     * FAQ파일 다운로드 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/faqFile/{faqNo}/file/{faqFileNo}/download", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadFaqFile(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo,
            @PathVariable("faqFileNo") final Integer faqFileNo) {
        
        FaqFileVo fetchedFaqFile = faqFileService.findFaqFile(faqNo, faqFileNo);
        return commonFileProcessor.downloadFile(new FaqFileWrapper(fetchedFaqFile, basePath));
    }        
    
}
