package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.ip.service.IpService;
import kr.xosoft.xoip.api.biz.ip.vo.IpVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * IP Controller 
 * @author likejy
 *
 */
@RestController
public class IpController {

    @Autowired
    private IpService ipService;
    
    /**
     * IP를 등록한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ip", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registIp(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final IpVo ip) {
        
        IpVo fetchedIp = ipService.registIp(ip);            
        return new ResponseEntity<CommonResult>(new CommonResult("ip", fetchedIp), HttpStatus.OK);
    }   
    
    /**
     * IP를 수정한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ip/{ipId}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyIp(
            @PathVariable("version") final Integer version,
            @PathVariable("ipId") final String ipId,
            @Validated(UpdatingGroup.class) @RequestBody final IpVo ip) {
        
        Verifier.isEqual(ipId, ip.getIpId(), "M1004", "word.ipId");
        IpVo fetchedIp = ipService.modifyIp(ip);
        return new ResponseEntity<CommonResult>(new CommonResult("ip", fetchedIp), HttpStatus.OK);
    }        
    
    /**
     * IP를 삭제한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ip/{ipId}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteIp(
            @PathVariable("version") final Integer version,
            @PathVariable("ipId") final String ipId) {
        
        ipService.deleteIp(ipId);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * IP를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/ip/{ipId}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findIp(
            @PathVariable("version") final Integer version,
            @PathVariable("ipId") final String ipId) {
        
        IpVo fetchedIp = ipService.findIp(ipId);
        return new ResponseEntity<CommonResult>(new CommonResult("ip", fetchedIp), HttpStatus.OK);
    }
    
    /**
     * IP코드로 IP를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/ip/code", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findIpByIpCd(
            @PathVariable("version") final Integer version,
            @RequestParam( value="ipCd", required=true ) final String ipCd) {
        
        IpVo fetchedIp = ipService.findIpByIpCd(ipCd);
        return new ResponseEntity<CommonResult>(new CommonResult("ip", fetchedIp), HttpStatus.OK);
    }    
       
    /**
     * IP 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/ip", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findLiceIpPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<IpVo> fetchedIpPage = ipService.findIpPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("ipPage", fetchedIpPage), HttpStatus.OK);
    }
    
}
