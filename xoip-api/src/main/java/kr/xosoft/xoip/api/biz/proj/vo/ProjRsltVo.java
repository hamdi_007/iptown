package kr.xosoft.xoip.api.biz.proj.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트 조회결과 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjRsltVo extends ProjVo {

    private static final long serialVersionUID = 1L;

    private String ipId;
    
    private String ipCd;
    
    private String ipNm;
    
    private String compId;
    
    private String compCd;
    
    private String compNm;
    
    private String cntrId;
    
    private String cntrNo;
    
    private String cntrNm;
    
    private String cntrStrYmd;
    
    private String cntrEndYmd;
    
    private String cntrVldYmd;
    
    private String prodTypNm;
    
    private String projStgCdNm;
    
    private String projStsCdNm;
    
}
