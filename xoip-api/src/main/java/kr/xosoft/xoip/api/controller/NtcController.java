package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.ntc.service.NtcService;
import kr.xosoft.xoip.api.biz.ntc.vo.NtcVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 공지사항 Controller 
 * @author hypark
 *
 */
@RestController
public class NtcController {

    @Autowired
    private NtcService ntcService;
    
    /**
     * 공지사항을 등록한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ntc", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registNtc(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final NtcVo ntc) {
        
    	NtcVo fetchedNtc = ntcService.registNtc(ntc);
        return new ResponseEntity<CommonResult>(new CommonResult("ntc", fetchedNtc), HttpStatus.OK);
    }   
    
    /**
     * 공지사항을 수정한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ntc/{ntcNo}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyNtc(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo,
            @Validated(UpdatingGroup.class) @RequestBody final NtcVo ntc) {
        
        Verifier.isEqual(ntcNo, ntc.getNtcNo(), "M1004", "word.ntcNo");
        NtcVo fetchedNtc = ntcService.modifyNtc(ntc);
        return new ResponseEntity<CommonResult>(new CommonResult("ntc", fetchedNtc), HttpStatus.OK);
    }        
    
    /**
     * 공지사항을 삭제한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ntc/{ntcNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteNtc(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo) {
        
    	ntcService.deleteNtc(ntcNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 공지사항을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/ntc/{ntcNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findNtc(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo) {
        
    	NtcVo fetchedNtc = ntcService.findNtc(ntcNo);
        return new ResponseEntity<CommonResult>(new CommonResult("ntc", fetchedNtc), HttpStatus.OK);
    }
    
    /**
     * 공지사항 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/ntc", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findNtcPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<NtcVo> fetchedNtcPage = ntcService.findNtcPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("ntcPage", fetchedNtcPage), HttpStatus.OK);
    }
    
    /**
     * 라이선시용 공지사항 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/licentc", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findLiceNtcPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<NtcVo> fetchedLiceNtcPage = ntcService.findLiceNtcPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("liceNtcPage", fetchedLiceNtcPage), HttpStatus.OK);
    }  
    
}
