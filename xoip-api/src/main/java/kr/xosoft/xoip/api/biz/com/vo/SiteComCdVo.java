package kr.xosoft.xoip.api.biz.com.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 사이트 공통코드 VO
 * @author likejy
 *
 */
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class SiteComCdVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    public Map<String, ComCdVo> comCdMap;
    
    public Map<String, List<ComCdVo>> topComCdMap;
    
    public SiteComCdVo() {
        comCdMap = new HashMap<String, ComCdVo>();
        topComCdMap = new HashMap<String, List<ComCdVo>>();
    }
    
    public void setComCd(String comCd, ComCdVo comCdVo) {
        comCdMap.put(comCd, comCdVo);
    }
    
    public ComCdVo getComCd(String comCd) {
        return comCdMap.get(comCd);
    }
    
    public void addComCdListByTopComCd(String topComCd, List<ComCdVo> comCdList) {
        topComCdMap.put(topComCd, comCdList);
    }
    
    public List<ComCdVo> getComCdListByTopComCd(String topComCd) {
        return topComCdMap.get(topComCd);
    }
    
}
