package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.List;

import javax.validation.Valid;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 디자인검수+검수파일+의견파일 Union VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class DsgnInspFileUnionVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    @Valid
    private DsgnInspVo dsgnInsp;
    
    @Valid
    private List<InspFileVo> inspFileList;
    
    private List<OpinFileVo> opinFileList;
    
    public DsgnInspFileUnionVo() {
    }
    
    public DsgnInspFileUnionVo(DsgnInspVo dsgnInsp, List<InspFileVo> inspFileList) {
        this(dsgnInsp, inspFileList, null);
    }
    
    public DsgnInspFileUnionVo(DsgnInspVo dsgnInsp, List<InspFileVo> inspFileList, List<OpinFileVo> opinFileList) {
        setDsgnInsp(dsgnInsp);
        setInspFileList(inspFileList);
        setOpinFileList(opinFileList);
    }    
}
