package kr.xosoft.xoip.api.biz.ntc.service;

import kr.xosoft.xoip.api.biz.ntc.vo.NtcVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface NtcService {

    public NtcVo registNtc(NtcVo ntc);
        
    public NtcVo modifyNtc(NtcVo ntc);
    
    public void deleteNtc(Integer ntcNo);
    
    public NtcVo findNtc(Integer ntcNo);

    public CommonPage<NtcVo> findNtcPage(CommonCondition condition);    

	public CommonPage<NtcVo> findLiceNtcPage(CommonCondition condition);
  
}
