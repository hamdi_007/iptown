package kr.xosoft.xoip.api.biz.proj.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트상태변경폼 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjStsChgFormVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=UpdatingGroup.class )    
    private String projCd;
    
    /**
     * 상태사유내용
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String stsRsnCnts;
    
}
