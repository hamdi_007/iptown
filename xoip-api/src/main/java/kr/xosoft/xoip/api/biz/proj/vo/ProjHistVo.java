package kr.xosoft.xoip.api.biz.proj.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjHistVo extends ProjVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트이력일련번호 
     */
    private Integer projHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public ProjHistVo(String projCd, String histDivCd) {
        setProjCd(projCd);
        setHistDivCd(histDivCd);
    }        
    
}
