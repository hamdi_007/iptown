package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 상품지원언어 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProdSprtLangVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    private String projCd;

    /**
     * 언어코드
     */
    private String langCd;

    /**
     * 등록일시
     */
    private Date regDt;

    /**
     * 등록자ID
     */
    private String regrId;
    
    public ProdSprtLangVo() {
    }
    
    public ProdSprtLangVo(String projCd, String langCd) {
        setProjCd(projCd);
        setLangCd(langCd);
    }      
    
}
