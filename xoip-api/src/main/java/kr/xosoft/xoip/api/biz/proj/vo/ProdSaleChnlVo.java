package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트판매채널 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProdSaleChnlVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    private String projCd;

    /**
     * 판매채널코드
     */
    private String saleChnlCd;
    
    /**
     * 등록일시
     */
    private Date regDt;

    /**
     * 등록자ID
     */
    private String regrId;    

    public ProdSaleChnlVo() {
    }
    
    public ProdSaleChnlVo(String projCd, String saleChnlCd) {
        setProjCd(projCd);
        setSaleChnlCd(saleChnlCd);
    }    
}
