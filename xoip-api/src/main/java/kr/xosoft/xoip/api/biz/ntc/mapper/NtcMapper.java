package kr.xosoft.xoip.api.biz.ntc.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.ntc.vo.NtcVo;

/**
 * 공지사항 매퍼 
 * @author hypark
 *
 */
@Repository
public interface NtcMapper {

   /**
    * 공지사항 등록
    * @param ntc
    * @return
    */
   public int insertNtc(NtcVo ntc);
   
   /**
    * 공지사항 수정 by PK
    * @param ntc
    * @return
    */
   public int updateNtcByKey(NtcVo ntc);
   
   /**
    * 공지사항 조회수 증가  by PK
    * @param ntc
    * @return
    */
   public int updateNtcCntByKey(NtcVo ntc);
       
   /**
    * 공지사항 삭제 by PK
    * @param ntc
    * @return
    */
   public int deleteNtcByKey(NtcVo ntc);
   
   /**
    * 공지사항 조회 by PK
    * @param siteCd
    * @param ntcNo
    * @return
    */
   public NtcVo findNtcByKey(@Param("siteCd") String siteCd, @Param("ntcNo") Integer NtcNo);   
   
   /**
    * 공지사항 목록 건수조회 by 검색조건
    * @param map
    * @return
    */
   public int countNtcListByMap(Map<String, Object> map);
   
   /**
    * 공지사항 목록조회 by 검색조건
    * @param map
    * @return
    */
   public List<NtcVo> findNtcListByMap(Map<String, Object> map);    
   
   /**
    * 라이선시용 공지사항 목록 건수조회 by 검색조건
    * @param map
    * @return
    */
   public int countLiceNtcListByMap(Map<String, Object> map);
   
   /**
    * 라이선시용 공지사항 목록조회 by 검색조건
    * @param map
    * @return
    */
   public List<NtcVo> findLiceNtcListByMap(Map<String, Object> map);  
   
}
