package kr.xosoft.xoip.api.biz.proj.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 디자인검수이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class DsgnInspHistVo extends DsgnInspVo {

    private static final long serialVersionUID = 1L;

    /**
     * 검수이력일련번호 
     */
    private Integer inspHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public DsgnInspHistVo(String projCd, Integer inspNo, String histDivCd) {
        setProjCd(projCd);
        setInspNo(inspNo);
        setHistDivCd(histDivCd);
    }        
    
}
