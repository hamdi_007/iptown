package kr.xosoft.xoip.api.biz.proj.service;

import java.util.List;

import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;

public interface InspFileService {

    public List<InspFileVo> mergeInspFileList(String projCd, Integer inspNo, List<InspFileVo> inspFileList);

    public void modifyInspFileList(List<InspFileVo> inspFileList);
    
    public void deleteInspFile(String projCd, Integer inspNo, Integer inspFileNo);
    
    public InspFileVo findInspFile(String projCd, Integer inspNo, Integer inspFileNo);
    
    public List<InspFileVo> findInspFileList(String projCd, Integer inspNo);
    
}
