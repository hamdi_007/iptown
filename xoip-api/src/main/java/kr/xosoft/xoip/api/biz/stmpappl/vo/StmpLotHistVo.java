package kr.xosoft.xoip.api.biz.stmpappl.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 증지품목이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StmpLotHistVo extends StmpLotVo {

    private static final long serialVersionUID = 1L;

    /**
     * 증지품목이력일련번호 
     */
    private Integer stmpLotHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public StmpLotHistVo(String projCd, Integer stmpApplNo, Integer lotNo, String histDivCd) {
        setProjCd(projCd);
        setStmpApplNo(stmpApplNo);
        setLotNo(lotNo);
        setHistDivCd(histDivCd);
    }        
    
}
