package kr.xosoft.xoip.api.biz.proj.mapper;


import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.MarkUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.MarkVo;

/**
 * 마크업메타 매퍼 
 * @author likejy
 *
 */
@Repository
public interface MarkMapper {

    /**
     * 마크업메타 등록
     * @param mark
     * @return
     */
    public int insertMark(MarkVo mark);
    
    /**
     * 마크업메타 수정 by FK
     * @param mark
     * @return
     */
    public int updateMarkByKey(MarkVo mark);
    
    /**
     * 마크업메타 삭제 by FK
     * @param mark
     * @return
     */
    public int deleteMarkByKey(MarkVo mark);
    
    /**
     * 마크업메타 조회 by FK
     * @param siteCd
     * @param projCd
     * @Param inspNo
     * @Param inspFileNo
     * @return
     */
    public MarkVo findMarkByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo, @Param("inspFileNo") Integer inspFileNo);
    
    
    /**
     * 마크업유니온 조회 by FK
     * @param siteCd
     * @param projCd
     * @Param inspNo
     * @Param inspFileNo
     * @return
     */
    public MarkUnionVo findMarkUnionByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo, @Param("inspFileNo") Integer inspFileNo);
    
    
}
