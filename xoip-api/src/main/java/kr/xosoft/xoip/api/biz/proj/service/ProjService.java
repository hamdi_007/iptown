package kr.xosoft.xoip.api.biz.proj.service;

import java.util.Date;

import kr.xosoft.xoip.api.biz.proj.vo.ProjChgHistVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjDsgnInspUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjMetaVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjNmChgFormVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjRsltVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjStsChgFormVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface ProjService {

    public ProjDsgnInspUnionVo registProj(ProjVo proj);
    
    public ProjDsgnInspUnionVo modifyProjDsgnInspUnionInDraft(ProjDsgnInspUnionVo projUnion);
    
    public ProjDsgnInspUnionVo requestProjForApproval(ProjDsgnInspUnionVo projUnion);

    public ProjVo approveProj(String projCd);
    
    public ProjVo rejectProj(ProjStsChgFormVo projStsChgForm);
    
    public ProjVo cancelProj(ProjStsChgFormVo projStsChgForm);
    
    public ProjVo returnProj(String projCd, Date inspDt);
    
    public ProjVo completeProj(String projCd);
    
    public ProjVo renameProj(ProjNmChgFormVo projNmChgForm);
    
    public ProjVo changeProjForNextStage(String projCd, String tobeProjStgCd);
        
    public void deleteProj(String projCd);
    
    public ProjVo findProj(String projCd);
    
    public ProjMetaVo findLocalizedProjMeta(String projCd);
    
    public ProjDsgnInspUnionVo findProjDsgnInspUnionInDraft(String projCd);
    
    public CommonPage<ProjRsltVo> findLocalizedProjRsltPage(CommonCondition condition);
    
    public CommonPage<ProjRsltVo> findLocalizedStmpApplAvlProjRsltPage(String compId, CommonCondition condition);
    
    public CommonPage<ProjChgHistVo> findLocalizedProjChgHistPage(String projCd, CommonCondition condition);
    
    public String findProjExistYnByCntrId(String cntrId);
    
    public String findProjExistYnByTopProdTypId(String topProdTypId);
    
    public String findProjExistYnByProdTypId(String prodTypId);
    
}
