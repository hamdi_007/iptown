package kr.xosoft.xoip.api.biz.prodtyp.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 상품유형 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProdTypVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 상품유형ID 
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=16, max=16, groups=UpdatingGroup.class )
    private String prodTypId;        
    
    /**
     * 상품유형코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=2, max=6, groups=DefaultGroup.class )        
    private String prodTypCd;

    /**
     * 상품유형명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String prodTypNm;

    /**
     * 상품유형영문명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String prodTypEngNm;

    /**
     * 상품유형레벨
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer prodTypLv;

    /**
     * 상위사이트코드
     */
    private String topSiteCd;

    /**
     * 상위상품유형ID
     */
    private String topProdTypId;

    /**
     * 사용여부
     */
    @Pattern( message="chk.yn", regexp="(^[YN]{1}$)", groups=DefaultGroup.class ) 
    private String useYn;

    /**
     * 정렬순번
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer srtOrdno;

    /**
     * 상세설명
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String dtlDesc;

    /**
     * 영문상세설명
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String engDtlDesc;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;

}
