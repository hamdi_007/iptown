package kr.xosoft.xoip.api.biz.email.service;

import kr.xosoft.xoip.api.biz.email.vo.EmailReqFormVo;
import kr.xosoft.xoip.api.biz.email.vo.EmailTrnsHistVo;

public interface EmailService {

    public void requestForSendingEmail(EmailReqFormVo emailReqForm);
    
    public EmailTrnsHistVo registEmailTrnsHist(EmailTrnsHistVo emailTrnsHist);
    
}
