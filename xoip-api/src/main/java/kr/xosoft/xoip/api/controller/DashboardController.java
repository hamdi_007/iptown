package kr.xosoft.xoip.api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.dashboard.service.DashboardService;
import kr.xosoft.xoip.api.common.bean.CommonResult;


/**
 * 회사 Controller 
 * @author luna1819
 *
 */
@RestController
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;
    
    
    
    /**
     * 회사수를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/dashboard", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> countComp(
            @PathVariable("version") final Integer version) {
    	
    	// 라이선시 회사  	
        int fetchedCompNum = dashboardService.countCompNum();
        
        // 라이선시 사용자 
        int fetchedUsrNum = dashboardService.countUsrNum();
        
        // 등록된 IP         
        int fetchedIpNum = dashboardService.countIpNum();
        
        // 등록된 계약 
        int fetchedCntrNum = dashboardService.countCntrNum(); 
        
        // 전체 프로젝트
        int fetchedAllProjNum = dashboardService.countAllPorjNum();
        
        // 수정 프로젝트
        int fetchedModProjNum = dashboardService.countModProjNum();
        
        // 진행 프로젝트
        int fetchedProgProjNum = dashboardService.countProgProjNum();
        
        // 완료 프로젝트
        int fetchedCmplProjNum = dashboardService.countCmplProjNum();
        
        ArrayList<Integer> dashList = new ArrayList<Integer>();
        dashList.add(fetchedCompNum);
        dashList.add(fetchedUsrNum);
        dashList.add(fetchedIpNum);
        dashList.add(fetchedCntrNum);
        dashList.add(fetchedAllProjNum);
        dashList.add(fetchedModProjNum);
        dashList.add(fetchedProgProjNum);
        dashList.add(fetchedCmplProjNum);
        
        return new ResponseEntity<CommonResult>(new CommonResult("dashList", dashList), HttpStatus.OK);
    }    
   
}
