package kr.xosoft.xoip.api.biz.ntc.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.ntc.mapper.NtcFileMapper;
import kr.xosoft.xoip.api.biz.ntc.vo.NtcFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * 공지사항 파일 서비스 Impl
 * @author hypark
 *
 */
@Transactional
@Service
public class NtcFileServiceImpl implements NtcFileService {

    @Autowired
    private NtcFileMapper ntcFileMapper;

    /**
     * 공지사항 파일을 등록한다. 
     */
	@Override
	public NtcFileVo registNtcFile(NtcFileVo ntcFile) {
		
		// 공지사항 파일 등록 
        int cnt = ntcFileMapper.insertNtcFile(ntcFile);
        Verifier.eq(cnt, 1, "M1000", "word.ntcFile");

        return ntcFile;
	}

	/**
     * 공지사항 파일을 삭제한다. 
     */
	@Override
	public void deleteNtcFile(Integer ntcNo, Integer ntcFileNo) {
		
		// 계약파일 삭제 
		NtcFileVo ntcFile = new NtcFileVo();
		ntcFile.setNtcNo(ntcNo);
		ntcFile.setNtcFileNo(ntcFileNo);
        int cnt = ntcFileMapper.deleteNtcFileByKey(ntcFile); 
        Verifier.eq(cnt, 1, "M1002", "word.ntcFile");  
		
	}

	/**
     * 공지사항 파일을 조회한다. 
     */
	@Override
	public NtcFileVo findNtcFile(Integer ntcNo, Integer ntcFileNo) {
		
		CommonData commonData = CommonDataHolder.getCommonData();
	    return ntcFileMapper.findNtcFileByKey(commonData.getSiteCd(), ntcNo, ntcFileNo);
	}

	/**
     * 공지사항 파일 목록 페이지를 조회한다. 
     */
	@Override
	public CommonPage<NtcFileVo> findNtcFilePage(Integer ntcNo, CommonCondition condition) {
		
		Map<String, Object> map = condition.toMap();
        map.put("ntcNo", ntcNo);
        
        int comTtlCnt = ntcFileMapper.countNtcFileListByMap(map);
        List<NtcFileVo> ntcList = ntcFileMapper.findNtcFileListByMap(map);
        
        return new CommonPage<NtcFileVo>(condition, ntcList, comTtlCnt); 
	}

}
