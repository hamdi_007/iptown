package kr.xosoft.xoip.api.common.aop;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kr.xosoft.xoip.api.common.auth.DefaultUserDetails;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.support.RequestSupport;
import lombok.extern.slf4j.Slf4j;

/**
 * 요청정보 인터셉터 
 * @author likejy
 *
 */
@Component
@Slf4j
public class CommonRequestInterceptor extends HandlerInterceptorAdapter {

    /**
     * 처리 전 프로세스 
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	
    	// 시큐리티 컨텍스트에서 인증정보 가져오기 
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        DefaultUserDetails userDetails = null;
        if (authentication != null && authentication.getPrincipal() instanceof DefaultUserDetails) {
            userDetails = (DefaultUserDetails)authentication.getPrincipal();
        }

        // 공통 데이터 생성 
        CommonData commonData = null;
        if (userDetails == null) {
        	commonData = CommonData.builder()
        	        .siteCd(RequestSupport.parseSiteCd(request))  
        			.ipcAddr(RequestSupport.parseClientIpAddress(request))
        			.lang(RequestSupport.parseAcceptLanguage(request))
        			.build();
        } else {
        	commonData = CommonData.builder()
        			.usrId(userDetails.getUserId())
        			.usrNm(userDetails.getRealUserName())
        			.email(userDetails.getUsername())
        			.siteCd(userDetails.getSiteCode())
        			.compId(userDetails.getCompanyId())
        			.compNm(userDetails.getCompanyName())
        			.ipcAddr(RequestSupport.parseClientIpAddress(request))
        			.auth(userDetails.getStringAuthorities().get(0))
        			.lang(RequestSupport.parseAcceptLanguage(request))
        			.build();
        } 
        
        if (log.isDebugEnabled()) {
        	log.debug("url : [{}] {}", request.getMethod(), request.getQueryString() == null ? request.getRequestURI() : request.getRequestURI() + "?" + request.getQueryString());
        	StringBuilder sb = new StringBuilder();
        	Enumeration<String> headers = request.getHeaderNames();
        	while(headers.hasMoreElements()) {
        	    String header = headers.nextElement();
        	    sb.append(header).append(" : ").append(request.getHeader(header)).append("\n");
        	}
        	log.debug("headers : \n{}", sb.toString());
        	log.debug("{}", commonData.toString());
        }
        CommonDataHolder.setCommonData(commonData);
        return super.preHandle(request, response, handler);
    }
    
    /**
     * 처리 후 프로세스 
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        CommonDataHolder.resetCommonData();
    }    
    

    
}
