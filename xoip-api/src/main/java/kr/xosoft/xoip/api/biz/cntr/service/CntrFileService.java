package kr.xosoft.xoip.api.biz.cntr.service;

import kr.xosoft.xoip.api.biz.cntr.vo.CntrFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface CntrFileService {

    public CntrFileVo registCntrFile(CntrFileVo cntrFile);
        
    public CntrFileVo modifyCntrFile(CntrFileVo cntrFile);
    
    public void deleteCntrFile(String cntrId, Integer cntrFileNo);
    
    public CntrFileVo findCntrFile(String cntrId, Integer cntrFileNo);
    
    public CommonPage<CntrFileVo> findCntrFilePage(String cntrId, CommonCondition condition);
    
}
