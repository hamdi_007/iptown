package kr.xosoft.xoip.api.biz.stmpappl.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 증지품목 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StmpLotVo extends BaseVo {
    
    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=DefaultGroup.class )            
    private String projCd;

    /**
     * 증지신청번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private Integer stmpApplNo;

    /**
     * 품목번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private Integer lotNo;

    /**
     * 품목명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )        
    private String lotNm;

    /**
     * 소비자금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal cnsmAmt;

    /**
     * 출고금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal rlsAmt;

    /**
     * 로열티기준코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )        
    private String ryltStdCd;

    /**
     * 로열티기준코드명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String ryltStdCdNm;       
    
    /**
     * 로열티율
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal ryltRt;

    /**
     * 생산개수
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer prdcCnt;

    /**
     * 로열티금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal ryltAmt;

    /**
     * 증지종류값
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private String stmpKndVal;

    /**
     * 증지금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal stmpAmt;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
}
