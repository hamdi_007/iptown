package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.List;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트 + 메타정보 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjMetaVo extends ProjVo {

    private static final long serialVersionUID = 1L;

    /**
     * 상품판매국가코드 목록 
     */
    private List<ComCdVo> prodSaleNtnComCdList;
    
    /**
     * 상품판매채널코드 목록 
     */
    private List<ComCdVo> prodSaleChnlComCdList;
    
    /**
     * 상품지원언어코드 목록 
     */
    private List<ComCdVo> prodSprtLangComCdList;
    
    /**
     * 상품대상연령코드 목록 
     */
    private List<ComCdVo> prodTgtAgeComCdList;
    
    /**
     * 상품대상성별코드 목록 
     */
    private List<ComCdVo> prodTgtSexComCdList;       

}
