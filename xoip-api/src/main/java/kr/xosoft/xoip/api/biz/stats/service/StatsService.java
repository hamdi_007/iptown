package kr.xosoft.xoip.api.biz.stats.service;

import kr.xosoft.xoip.api.biz.stats.vo.StatsVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.file.buffer.BufferData;

public interface StatsService {

    public CommonPage<StatsVo> findStatsPage(CommonCondition condition);
    
    public BufferData makeStatsRptExcelData(CommonCondition condition);
    
}
