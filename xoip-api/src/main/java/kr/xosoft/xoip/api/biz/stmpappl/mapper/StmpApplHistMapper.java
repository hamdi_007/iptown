package kr.xosoft.xoip.api.biz.stmpappl.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplHistVo;

/**
 * 증지신청이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface StmpApplHistMapper {

    /**
     * 증지신청이력 등록 
     */
    public void insertStmpApplHistAsSelectMaster(StmpApplHistVo stmpApplHist);    
    
}
