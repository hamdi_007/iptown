package kr.xosoft.xoip.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.proj.service.ProjService;
import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjChgHistVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjDsgnInspUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjMetaVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjNmChgFormVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjRsltVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjStsChgFormVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.InspFileWrapper;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 프로젝트 Controller 
 * @author likejy
 *
 */
@RestController
public class ProjController {

    
    @Autowired
    private ProjService projService;
    
    @Autowired
    private CommonFileProcessor commonFileProcessor;
    
    @Value("${api.file.basePath}")
    private String basePath;    
    
    /**
     * 프로젝트를 등록한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registProjDsgnInspUnion(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final ProjVo proj) {
        
        ProjDsgnInspUnionVo fetchedProjUnion = projService.registProj(proj);            
        return new ResponseEntity<CommonResult>(new CommonResult("projUnion", fetchedProjUnion), HttpStatus.OK);
    }   
    
    /**
     * 프로젝트를 임시저장한다.  
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/draft", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyProjProjDsgnInspUnionInDraft(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @Validated(CreatingGroup.class) @RequestBody final ProjDsgnInspUnionVo projUnion) {
        
        Verifier.notNull(projUnion.getProj(), "M1004", "proj");
        Verifier.isEqual(projCd, projUnion.getProj().getProjCd(), "M1004", "projCd");
        ProjDsgnInspUnionVo fetchedProjUnion = projService.modifyProjDsgnInspUnionInDraft(projUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("projUnion", fetchedProjUnion), HttpStatus.OK);
    }        
    
    /**
     * 프로젝트를 승인요청한다.  
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/request", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> requestProjUnionForApproval(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @Validated(CreatingGroup.class) @RequestBody final ProjDsgnInspUnionVo projUnion) {
        
        Verifier.notNull(projUnion.getProj(), "M1004", "proj");
        Verifier.isEqual(projCd, projUnion.getProj().getProjCd(), "M1004", "projCd");
        ProjDsgnInspUnionVo fetchedProjUnion = projService.requestProjForApproval(projUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("projUnion", fetchedProjUnion), HttpStatus.OK);
    }        
    
    /**
     * 프로젝트를 승인한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/approve", method=RequestMethod.PUT )    
    public ResponseEntity<CommonResult> approveProj(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd) {
        
        ProjVo proj = projService.approveProj(projCd);
        return new ResponseEntity<CommonResult>(new CommonResult("proj", proj), HttpStatus.OK);
    }
    
    /**
     * 프로젝트를 반려한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/reject", method=RequestMethod.PUT )    
    public ResponseEntity<CommonResult> rejectProj(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @Validated(UpdatingGroup.class) @RequestBody final ProjStsChgFormVo projStsChgForm) {
        
        // 프로젝트 반려상태는 CJ E&M 요청으로 제거 
        Verifier.fail("M1004", "word.reject");
        ProjVo proj = projService.rejectProj(projStsChgForm);
        return new ResponseEntity<CommonResult>(new CommonResult("proj", proj), HttpStatus.OK);
    }    
    
    /**
     * 프로젝트를 취소한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/cancel", method=RequestMethod.PUT )    
    public ResponseEntity<CommonResult> cancelProj(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @Validated(UpdatingGroup.class) @RequestBody final ProjStsChgFormVo projStsChgForm) {
        
        ProjVo proj = projService.cancelProj(projStsChgForm);
        return new ResponseEntity<CommonResult>(new CommonResult("proj", proj), HttpStatus.OK);
    }        
    
    /**
     * 프로젝트명을 변경한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/rename", method=RequestMethod.PUT )    
    public ResponseEntity<CommonResult> renameProj(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @Validated(UpdatingGroup.class) @RequestBody final ProjNmChgFormVo projNmChgForm) {
        
        ProjVo proj = projService.renameProj(projNmChgForm);
        return new ResponseEntity<CommonResult>(new CommonResult("proj", proj), HttpStatus.OK);
    }     
    
    /**
     * 프로젝트를 삭제한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteProj(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd) {
        
        projService.deleteProj(projCd);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 프로젝트를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findProj(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd) {
        
        ProjMetaVo fetchedProjMeta = projService.findLocalizedProjMeta(projCd);
        return new ResponseEntity<CommonResult>(new CommonResult("projMeta", fetchedProjMeta), HttpStatus.OK);
    }
    
    /**
     * 프로젝트 및 최초 작성 디자인검수를 조회한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/draft", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findProjDsgnInspUnion(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd) {
        
        ProjDsgnInspUnionVo fetchedProjUnion = projService.findProjDsgnInspUnionInDraft(projCd);
        
        // 썸네일 데이터 조회 
        List<InspFileVo> inspFileList = fetchedProjUnion.getInspFileList();
        List<String> tumbDataList = new ArrayList<String>();
        for (InspFileVo inspFile : inspFileList) {
            tumbDataList.add(commonFileProcessor.thumbSmallToBase64String(new InspFileWrapper(inspFile, basePath)));
        }
        
        CommonResult result = new CommonResult("projUnion", fetchedProjUnion);
        result.putObject("tumbDataList", tumbDataList);
        return new ResponseEntity<CommonResult>(result, HttpStatus.OK);
    }    
           
    /**
     * 프로젝트 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findProjPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<ProjRsltVo> fetchedProjRsltPage = projService.findLocalizedProjRsltPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("projPage", fetchedProjRsltPage), HttpStatus.OK);
    }
    
    /**
     * 증지신청이 가능한 프로젝트 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/projects/complete", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findStmpApplAvlProjPage(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            final CommonCondition condition) {

        CommonPage<ProjRsltVo> fetchedProjRsltPage = projService.findLocalizedStmpApplAvlProjRsltPage(compId, condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("projPage", fetchedProjRsltPage), HttpStatus.OK);
    }    
    
    /**
     * 프로젝트변경이력 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/history", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findProjChgHistPage(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            final CommonCondition condition) {
        
        CommonPage<ProjChgHistVo> fetchedProjChgHistPage = projService.findLocalizedProjChgHistPage(projCd, condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("projChgHistPage", fetchedProjChgHistPage), HttpStatus.OK);
    }    
    
}
