package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdSprtLangVo;

/**
 * 상품지원언어 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProdSprtLangMapper {

    /**
     * 상품지원언어 등록
     * @param prodSprtLang
     * @return
     */
    public int insertProdSprtLang(ProdSprtLangVo prodSprtLang);
    
    /**
     * 상품지원언어 삭제 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */
    public int deleteProdSprtLangByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
        
    /**
     * 언어코드값 목록조회 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */    
    public List<String> findLangCdListByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);

    /**
     * 언어코드 목록조회 by 프로젝트코드
     * @param siteCd
     * @param projCd
     * @return
     */
    public List<ComCdVo> findLocalizedComCdListForLangCdByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("comLang") String comLang);
}
