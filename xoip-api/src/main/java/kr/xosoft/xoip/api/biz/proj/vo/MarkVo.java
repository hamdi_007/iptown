package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 마크업데이터 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class MarkVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    
    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=DefaultGroup.class )     
    private String projCd;

    /**
     * 검수번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer inspNo;

    /**
     * 검수파일번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer inspFileNo;
    
    /**
     * 마크업번호
     */
    private Integer markNo;
    
    /**
     * 마크업메타
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private String markMeta;
    
    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
}
