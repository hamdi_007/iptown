package kr.xosoft.xoip.api.biz.cntr.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.parser.AvailableLicensorStringSerializer;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 계약 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class CntrVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    
    /**
     * 계약ID 
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=16, max=16, groups=UpdatingGroup.class )
    private String cntrId;
    
    /**
     * 계약번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=50, groups=DefaultGroup.class )    
    private String cntrNo;

    /**
     * 계약명
     */
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String cntrNm;

    /**
     * 회사ID
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=10, max=10, groups=DefaultGroup.class )    
    private String compId;
    
    /**
     * 회사코드 
     */
    private String compCd;
    
    /**
     * 회사명 
     */
    private String compNm;
    
    /**
     * IPID
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=10, max=10, groups=DefaultGroup.class )    
    private String ipId;

    /**
     * IP코드 
     */
    private String ipCd;
    
    /**
     * IP명
     */
    private String ipNm;

    /**
     * 계약년월일
     */
    @Pattern( message="chk.ymd", regexp="(^[0-9]{8}$)", groups=DefaultGroup.class )
    private String cntrYmd;

    /**
     * 계약시작년월일
     */
    @Pattern( message="chk.ymd", regexp="(^[0-9]{8}$)", groups=DefaultGroup.class )
    private String cntrStrYmd;

    /**
     * 계약종료년월일
     */
    @Pattern( message="chk.ymd", regexp="(^[0-9]{8}$)", groups=DefaultGroup.class )
    private String cntrEndYmd;

    /**
     * 계약유효년월일
     */
    @Pattern( message="chk.ymd", regexp="(^[0-9]{8}$)", groups=DefaultGroup.class )
    private String cntrVldYmd;

    /**
     * 비고
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    @JsonSerialize( using = AvailableLicensorStringSerializer.class )
    private String rmk;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
}
