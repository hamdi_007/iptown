package kr.xosoft.xoip.api.biz.proj.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspHistVo;

/**
 * 디자인검수이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface DsgnInspHistMapper {

    /**
     * 디자인검수이력 등록 
     */
    public void insertDsgnInspHistAsSelectMaster(DsgnInspHistVo dsgnInspHist);    
    
}
