package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.stmpappl.service.StmpApplService;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonResult;

/**
 * 홈 Controller 
 * @author likejy
 *
 */
@RestController
public class HomeConroller {
	
	@Autowired
    private StmpApplService stmpApplService;
	
    
    @RequestMapping( value="/**", method = RequestMethod.OPTIONS )
    public ResponseEntity<Void> handleOptions() {
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/*/test", method = RequestMethod.GET )
    public ResponseEntity<Void> test() {
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
}
