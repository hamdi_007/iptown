package kr.xosoft.xoip.api.biz.usr.vo;

import javax.validation.constraints.NotNull;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 토큰 폼 VO
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class TokenFormVo extends BaseVo {
    
    private static final long serialVersionUID = 1L;

    /**
     * 갱신토큰 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private String rfshToken;
    
}
