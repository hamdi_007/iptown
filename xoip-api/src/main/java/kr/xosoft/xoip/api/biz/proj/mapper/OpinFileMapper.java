package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.OpinFileVo;

/**
 * 의견파일 매퍼 
 * @author likejy
 *
 */
@Repository
public interface OpinFileMapper {

    /**
     * 의견파일 등록
     * @param opinFile
     * @return
     */
    public int insertOpinFile(OpinFileVo opinFile);
    
    /**
     * 의견파일 삭제 by PK
     * @param opinFile
     * @return
     */
    public int deleteOpinFileByKey(OpinFileVo opinFile);
    
    /**
     * 의견파일 조회 by PK
     * @param siteCd
     * @param projCd
     * @Param inspNo
     * @Param inspFileNo
     * @param opinFileNo
     * @return
     */
    public OpinFileVo findOpinFileByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo, @Param("inspFileNo") Integer inspFileNo, @Param("opinFileNo") Integer opinFileNo);
    
    /**
     * 의견파일 목록조회
     * @param siteCd
     * @param projCd
     * @param inspNo
     * @Param inspFileNo
     * @return
     */
    public List<OpinFileVo> findOpinFileList(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo);    
    
}
