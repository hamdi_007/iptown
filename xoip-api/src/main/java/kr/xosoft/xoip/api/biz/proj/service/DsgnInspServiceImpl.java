package kr.xosoft.xoip.api.biz.proj.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.proj.mapper.DsgnInspHistMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.DsgnInspMapper;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspFileUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspHistVo;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspVo;
import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;
import kr.xosoft.xoip.api.biz.proj.vo.OpinFileVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjDsgnInspUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.support.AuthSupport;

/**
 * 디자인검수 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class DsgnInspServiceImpl implements DsgnInspService {

    @Autowired
    private ProjService projService;
    
    @Autowired
    private InspFileService inspFileService;
    
    @Autowired
    private OpinFileService opinFileService;
    
    @Autowired
    private DsgnInspMapper dsgnInspMapper;
    
    @Autowired
    private DsgnInspHistMapper dsgnInspHistMapper;
        
    /**
     * 디자인검수를 등록한다. 
     */
    @Override
    public DsgnInspFileUnionVo registDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        List<InspFileVo> inspFileList = dsgnInspUnion.getInspFileList();
        
        // 작성중 혹은 승인요청중인 디자인 검수건이 존재하면 새로운 디자인 검수건을 등록할 수 없다. 
        int onGoingCnt = dsgnInspMapper.countOnGoingDsgnInspByProjCd(dsgnInsp.getSiteCd(), dsgnInsp.getProjCd());
        Verifier.eq(onGoingCnt, 0, "M7007");
        
        // 디자인검수 등록 
        int cnt = dsgnInspMapper.insertDsgnInsp(dsgnInsp);
        Verifier.eq(cnt, 1, "M1000", "word.dsgnInsp");

        // 디자인검수 이력 등록 
        DsgnInspHistVo dsgnInspHist = new DsgnInspHistVo(dsgnInsp.getProjCd(), dsgnInsp.getInspNo(), "HD0100");
        dsgnInspHistMapper.insertDsgnInspHistAsSelectMaster(dsgnInspHist);
        Verifier.notNull(dsgnInspHist.getInspHistSeqno(), "M1000", "word.dsgnInspHist");

        // 검수파일 병합
        if (inspFileList != null) {
            for (InspFileVo inspFile : inspFileList) {
                inspFile.setInspNo(dsgnInsp.getInspNo());
            }
        }
        inspFileService.mergeInspFileList(dsgnInsp.getProjCd(), dsgnInsp.getInspNo(), inspFileList);            
        
        return dsgnInspUnion;
    }
    
    /**
     * 디자인검수를 변경한다. 
     */
    private DsgnInspVo modifyDsgnInspWithoutVerification(DsgnInspVo dsgnInsp) {

        // 디자인검수 수정 
        int cnt = dsgnInspMapper.updateDsgnInspByKey(dsgnInsp);
        Verifier.eq(cnt, 1, "M1001", "word.dsgnInsp");

        // 디자인검수 이력 등록 
        DsgnInspHistVo dsgnInspHist = new DsgnInspHistVo(dsgnInsp.getProjCd(), dsgnInsp.getInspNo(), "HD0200");
        dsgnInspHistMapper.insertDsgnInspHistAsSelectMaster(dsgnInspHist);
        Verifier.notNull(dsgnInspHist.getInspHistSeqno(), "M1000", "word.dsgnInspHist");
        
        return dsgnInsp;                
    }

    /**
     * 디자인검수를 임시저장한다. 
     */
    @Override
    public DsgnInspFileUnionVo modifyDsgnInspFileUnionInDraft(DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        List<InspFileVo> inspFileList = dsgnInspUnion.getInspFileList();
        List<OpinFileVo> opinFileList = dsgnInspUnion.getOpinFileList();
        
        // 디자인검수 조회 
        DsgnInspVo fetchedDsgnInsp = findLocalizedDsgnInsp(dsgnInsp.getProjCd(), dsgnInsp.getInspNo());
        Verifier.notNull(fetchedDsgnInsp, "M1003", "word.dsgnInsp");
        
        // 디자인검수 수정 
        fetchedDsgnInsp.setInspStgCd(dsgnInsp.getInspStgCd());
        fetchedDsgnInsp.setNxtInspStgCd(dsgnInsp.getNxtInspStgCd());
        fetchedDsgnInsp.setInspStsCd(dsgnInsp.getInspStsCd());
        fetchedDsgnInsp.setInspApplCnts(dsgnInsp.getInspApplCnts());
        fetchedDsgnInsp.setInspOpinCnts(dsgnInsp.getInspOpinCnts());
        modifyDsgnInspWithoutVerification(fetchedDsgnInsp);
        
        // 검수파일 병합 or 수정
        if (AuthSupport.isLicensee()) {
            // 라이선시가 임시저장시 검수파일 목록의 등록/수정/삭제가 가능하다.   
            inspFileService.mergeInspFileList(dsgnInsp.getProjCd(), dsgnInsp.getInspNo(), inspFileList);
            // 라이선시는 임시저장시 의견파일 목록은 등록/수정/삭제가 불가하다. 
        } else {
            // 라이선서는 임시저장시 검수파일 목록의 수정(검수의견 값 변경)만 가능하다. 
            inspFileService.modifyInspFileList(inspFileList);
            // 라이선서는 임시저장시 의견파일 목록의 등록/삭제가 가능하다. 
            opinFileService.registOrDeleteFileList(dsgnInsp.getProjCd(), dsgnInsp.getInspNo(), opinFileList);
        }
        
        return dsgnInspUnion;
    }
    
    /**
     * 디자인검수를 신청하면서 프로젝트단계코드를 변경한다.  
     */
    @Override
    public DsgnInspFileUnionVo requestDsgnInspFileUnionForApproval(DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        
        // 프로젝트단계 및 상태 변경 
        projService.changeProjForNextStage(dsgnInsp.getProjCd(), dsgnInsp.getInspStgCd());
        
        // 디자인검수 신청 
        return applyDsgnInspFileUnion(dsgnInspUnion);
    }
    
    /**
     * 디자인검수를 신청한다. 
     */
    @Override
    public DsgnInspFileUnionVo applyDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        dsgnInsp.setInspStsCd("IS0200"); // 승인요청
        dsgnInsp.setInspApplDt(new Date());
        dsgnInsp.setInspApplrId(commonData.getUsrId());
        
        List<InspFileVo> inspFileList = dsgnInspUnion.getInspFileList();
        
        // 디자인검수 조회 
        DsgnInspVo fetchedDsgnInsp = findLocalizedDsgnInsp(dsgnInsp.getProjCd(), dsgnInsp.getInspNo());
        Verifier.notNull(fetchedDsgnInsp, "M1003", "word.dsgnInsp");
        
        // 디자인검수 수정 
        fetchedDsgnInsp.setInspStgCd(dsgnInsp.getInspStgCd());
        fetchedDsgnInsp.setInspStsCd(dsgnInsp.getInspStsCd());
        fetchedDsgnInsp.setInspApplDt(dsgnInsp.getInspApplDt());
        fetchedDsgnInsp.setInspApplrId(dsgnInsp.getInspApplrId());
        fetchedDsgnInsp.setInspApplCnts(dsgnInsp.getInspApplCnts());
        modifyDsgnInspWithoutVerification(fetchedDsgnInsp);
        
        // 검수파일 병합
        inspFileService.mergeInspFileList(dsgnInsp.getProjCd(), dsgnInsp.getInspNo(), inspFileList);        
        
        return dsgnInspUnion;
    }
    
    /**
     * 디자인검수를 완료한다. 
     */
    private DsgnInspFileUnionVo completeDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        dsgnInsp.setInspDt(new Date());
        dsgnInsp.setInsprId(commonData.getUsrId());
        
        List<InspFileVo> inspFileList = dsgnInspUnion.getInspFileList();
        List<OpinFileVo> opinFileList = dsgnInspUnion.getOpinFileList();
        
        // 프로젝트 상태 변경  
        if ("IT0500".equals(dsgnInsp.getNxtInspStgCd())) { // 프로젝트완료
            projService.completeProj(dsgnInsp.getProjCd());
        } else {
            projService.returnProj(dsgnInsp.getProjCd(), dsgnInsp.getInspDt());
        }
        
        // 디자인검수 조회 
        DsgnInspVo fetchedDsgnInsp = findLocalizedDsgnInsp(dsgnInsp.getProjCd(), dsgnInsp.getInspNo());
        Verifier.notNull(fetchedDsgnInsp, "M1003", "word.dsgnInsp");
        
        // 디자인검수 수정 
        fetchedDsgnInsp.setNxtInspStgCd(dsgnInsp.getNxtInspStgCd());
        fetchedDsgnInsp.setInspStsCd(dsgnInsp.getInspStsCd());
        fetchedDsgnInsp.setInspDt(dsgnInsp.getInspDt());
        fetchedDsgnInsp.setInsprId(dsgnInsp.getInsprId());
        fetchedDsgnInsp.setInspOpinCnts(dsgnInsp.getInspOpinCnts());
        modifyDsgnInspWithoutVerification(fetchedDsgnInsp);
        
        // 라이선서는 검수완료시 검수파일 목록의 수정(검수의견 값 변경)만 가능하다. 
        inspFileService.modifyInspFileList(inspFileList);
        // 라이선서는 검수완료시 의견파일 목록의 등록/삭제가 가능하다. 
        opinFileService.registOrDeleteFileList(dsgnInsp.getProjCd(), dsgnInsp.getInspNo(), opinFileList);
        
        return dsgnInspUnion;
    }    
    
    /**
     * 디자인검수를 조건부 승인한다. 
     */
    @Override
    public DsgnInspFileUnionVo approveWithChangeDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        dsgnInsp.setInspStsCd("IS0310"); // 조건부승인 
        
        return completeDsgnInspFileUnion(dsgnInspUnion);
    }
    
    /**
     * 디자인검수를 승인한다. 
     */
    @Override
    public DsgnInspFileUnionVo approveDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        dsgnInsp.setInspStsCd("IS0300"); // 승인 
        
        return completeDsgnInspFileUnion(dsgnInspUnion);
    }
    
    /**
     * 디자인검수를 반려한다. 
     */
    @Override
    public DsgnInspFileUnionVo rejectDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        dsgnInsp.setInspStsCd("IS0400"); // 반려 
        
        return completeDsgnInspFileUnion(dsgnInspUnion);
    }
    
    /**
     * 디자인검수를 삭제한다. 
     */
    @Override
    public void deleteDsgnInsp(String projCd, Integer inspNo) {
        
        // 디자인검수 조회 
        DsgnInspVo dsgnInsp = findLocalizedDsgnInsp(projCd, inspNo);
        Verifier.notNull(dsgnInsp, "M1003", "word.dsgnInsp");
        
        // 작성중 프로젝트만 삭제 가능함 
        Verifier.isEqual(dsgnInsp.getInspStsCd(), "IS0100", "M7009");        
        
        // 디자인검수 삭제 
        int cnt = dsgnInspMapper.deleteDsgnInspByKey(dsgnInsp); 
        Verifier.eq(cnt, 1, "M1002", "word.dsgnInsp");
        
        // 디자인검수 이력 등록 
        DsgnInspHistVo dsgnInspHist = new DsgnInspHistVo(projCd, inspNo, "HD0300");
        dsgnInspHistMapper.insertDsgnInspHistAsSelectMaster(dsgnInspHist);
        Verifier.notNull(dsgnInspHist.getInspHistSeqno(), "M1000", "word.dsgnInspHist");         
    }
    
    /**
     * 프로젝트+디자인검수+검수파일+의견파일 목록을 조회한다. 
     */
    @Override
    public ProjDsgnInspUnionVo findProjDsgnInspUnion(String projCd, Integer inspNo) {

        // 프로젝트 조회 
        ProjVo proj = projService.findProj(projCd);
        
        // 디자인검수 조회 
        DsgnInspVo dsgnInsp = findLocalizedDsgnInsp(projCd, inspNo);
        
        // 검수파일 목록 조회 
        List<InspFileVo> inspFileList = inspFileService.findInspFileList(projCd, inspNo);
        
        // 의견파일 목록 조회 
        List<OpinFileVo> opinFileList = opinFileService.findOpinFileList(projCd, inspNo);
        
        return new ProjDsgnInspUnionVo(proj, dsgnInsp, inspFileList, opinFileList);
    }    
    
    /**
     * 디자인검수를 조회한다. 
     */
    @Override
    public DsgnInspVo findLocalizedDsgnInsp(String projCd, Integer inspNo) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return dsgnInspMapper.findLocalizedDsgnInspByKey(commonData.getSiteCd(), projCd, inspNo, commonData.getLang());
    }

    /**
     * 디자인검수목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<DsgnInspVo> findLocalizedDsgnInspPage(String projCd, CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        map.put("projCd", projCd);
        
        int comTtlCnt = dsgnInspMapper.countDsgnInspListByMap(map);
        List<DsgnInspVo> dsgnInspList = dsgnInspMapper.findLocalizedDsgnInspListByMap(map);
        
        return new CommonPage<DsgnInspVo>(condition, dsgnInspList, comTtlCnt);           
    }

    /**
     * 다음 디자인 검수 단계를 조회한다. 
     */
    @Override
    public String findInspStgCdForNextStage(String projCd) {
        
        CommonData commonData = CommonDataHolder.getCommonData(); 
        return dsgnInspMapper.findInspStgCdForNextStageByProjCd(commonData.getSiteCd(), projCd);
    }
    
}
