package kr.xosoft.xoip.api.common.exception;

/**
 * 비즈니스 예외 
 * @author likejy
 *
 */
public class BizException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private String code;
	private String[] args;
	
	/**
	 * 생성자
	 */
	public BizException() {
		super();
	}
	
	/**
	 * 생성자 
	 * @param code
	 */
	public BizException(String code) {
		super(code);
		this.code = code;
	}
	
	/**
	 * 생성자 
	 * @param code
	 * @param args
	 */
	public BizException(String code, String[] args) {
		super(code);
		this.code = code;
		this.args = args;
	}
	
	/**
	 * 생성자 
	 * @param code
	 * @param args
	 * @param cause
	 */
	public BizException(String code, String[] args, Throwable cause) {
		super(code, cause);
		this.code = code;
		this.args = args;
	}
	
	/**
	 * 생성자 
	 * @param code
	 * @param cause
	 */
	public BizException(String code, Throwable cause) {
		super(code, cause);
		this.code = code;		
	}
	
	/**
	 * 생성자 
	 * @param cause
	 */
	public BizException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * 오류코드 값을 리턴한다. 
	 * @return
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * 오류코드 값을 할당한다. 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * 오류코드를 매핑된 메시지로 변환 시 사용될 오류 인자값을 리턴한다. 
	 * @return
	 */
	public String[] getArgs() {
		return args;
	}
	
	/**
	 * 오류코드를 매핑된 메시지로 변환 시 사용될 오류 인자값을 할당한다. 
	 * @param args
	 */
	public void setArgs(String[] args) {
		this.args = args;
	}
}
