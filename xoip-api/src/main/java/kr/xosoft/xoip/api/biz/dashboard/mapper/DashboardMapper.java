package kr.xosoft.xoip.api.biz.dashboard.mapper;


import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


/**
 * 회사 매퍼 
 * @author luna1819
 *
 */
@Repository
public interface DashboardMapper {

    /**
     * 라이선시 등록 회사 수 조회
     * @param siteCd
     * @return
     */
    public int countCompCnt(@Param("siteCd") String siteCd);
    
    /**
     * 라이선시 등록 사용자 수 조회
     * @param siteCd
     * @return
     */
    public int countUsrCnt(@Param("siteCd") String siteCd);
    
    /**
     * 전체 IP 수 조회
     * @param siteCd
     * @return
     */
    public int countIpCnt(@Param("siteCd") String siteCd);
    
    /**
     * 전체 계 수 조회
     * @param siteCd
     * @return
     */
    public int countCntrCnt(@Param("siteCd") String siteCd);
    
    /**
     * 전체 프로젝트 수 조회
     * @param siteCd
     * @return
     */
    public int countAllProjCnt(@Param("siteCd") String siteCd);
    
    /**
     * 수정 프로젝트 수 조회
     * @param siteCd
     * @return
     */
    public int countModProjCnt(@Param("siteCd") String siteCd);
    
    /**
     * 진행 프로젝트 수 조회
     * @param siteCd
     * @return
     */
    public int countProgProjCnt(@Param("siteCd") String siteCd);
    
    /**
     * 완료 프로젝트 수 조회
     * @param siteCd
     * @return
     */
    public int countCmplProjCnt(@Param("siteCd") String siteCd);
    
}
