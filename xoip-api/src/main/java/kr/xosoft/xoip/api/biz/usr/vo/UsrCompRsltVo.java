package kr.xosoft.xoip.api.biz.usr.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 사용자회사 조회결과 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class UsrCompRsltVo extends UsrVo {

    private static final long serialVersionUID = 1L;
    
    /**
     * 회사ID
     */
    private String compId;    
    
    /**
     * 회사코드
     */
    private String compCd;

    /**
     * 회사명
     */
    private String compNm;
    
    /**
     * 회사구분코드
     */
    private String compDivCd;

    /**
     * 국가코드
     */
    private String ntnCd;
    
    /**
     * 국가코드명
     */
    private String ntnCdNm;

    /**
     * 사업자등록번호
     */
    private String bizno;

    /**
     * 주소
     */
    private String addr;

    /**
     * 상세주소
     */
    private String dtlAddr;

    /**
     * 우편번호
     */
    private String postNo;

    /**
     * 대표자명
     */
    private String ceoNm;

    /**
     * 대표전화번호
     */
    private String repTelno;
    
}
