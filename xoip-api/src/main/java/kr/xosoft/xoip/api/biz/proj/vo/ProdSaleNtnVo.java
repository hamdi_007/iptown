package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 상품판매국가 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProdSaleNtnVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    private String projCd;

    /**
     * 국가코드
     */
    private String ntnCd;

    /**
     * 등록일시
     */
    private Date regDt;

    /**
     * 등록자ID
     */
    private String regrId;
    
    public ProdSaleNtnVo() {
    }
    
    public ProdSaleNtnVo(String projCd, String ntnCd) {
        setProjCd(projCd);
        setNtnCd(ntnCd);
    }
    
}
