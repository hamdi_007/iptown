package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import kr.xosoft.xoip.api.biz.stmpappl.service.StmpApplFileService;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.StmpApplFileWrapper;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;

/**
 * 증지신청파일 Controller 
 * @author luna1819
 *
 */
@RestController
public class StmpApplFileController {
	
	@Autowired
	private StmpApplFileService stmpApplFileService;
	
	@Autowired
	private CommonFileProcessor commonFileProcessor;
	
	@Value("${api.file.basePath}")
	private String basePath;

	/**
     * 증지파일을 등록한다. 
     */
	@PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/file", method=RequestMethod.POST, headers="content-type=multipart/form-data" )
    public ResponseEntity<CommonResult> registStmpApplFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            @RequestParam( value="file", required=false ) MultipartFile mFile,
            @RequestParam("fileDivCd") String fileDivCd) {
        
        Verifier.notNull(mFile, "M1004", "mFile");
        Verifier.notNull(fileDivCd, "M1004", "fileDivCd");
        StmpApplFileWrapper stmpApplFileWrapper = new StmpApplFileWrapper(mFile, projCd, stmpApplNo, basePath);
        commonFileProcessor.uploadFile(stmpApplFileWrapper);
       
        StmpApplFileVo fetchedStmpApplFile = stmpApplFileService.registStmpApplFile((StmpApplFileVo)stmpApplFileWrapper.getFileVo());            
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplFile", fetchedStmpApplFile), HttpStatus.OK);
    }
	
	/**
     * 증지파일을 수정한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/file/{stmpFileNo}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyStmpApplFile(
    		@PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            @PathVariable("stmpFileNo") final Integer stmpFileNo,
            @Validated(UpdatingGroup.class) @RequestBody final StmpApplFileVo stmpApplFile) {
        
        Verifier.isEqual(projCd, stmpApplFile.getProjCd(), "M1004", "word.projCd");
        Verifier.isEqual(stmpApplNo, stmpApplFile.getStmpApplNo(), "M1004", "word.stmpApplNo");
        Verifier.isEqual(stmpFileNo, stmpApplFile.getStmpFileNo(), "M1004", "word.stmpFileNo");
        StmpApplFileVo fetchedStmpApplFile = stmpApplFileService.modifyStmpApplFile(stmpApplFile);
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplFile", fetchedStmpApplFile), HttpStatus.OK);
    }   
    
    /**
     * 계약파일을 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/file/{stmpFileNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteStmpApplFile(
    		@PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            @PathVariable("stmpFileNo") final Integer stmpFileNo) {
        
    	stmpApplFileService.deleteStmpApplFile(projCd, stmpApplNo, stmpFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
	
	/**
     * 증지파일을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/file/{stmpFileNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findStmpApplFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            @PathVariable("stmpFileNo") final Integer stmpFileNo) {
       
    	StmpApplFileVo fetchedStmpApplFile = stmpApplFileService.findStmpApplFile(projCd, stmpApplNo, stmpFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplFile", fetchedStmpApplFile), HttpStatus.OK);
    }
    
    /**
     * 증지파일 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/file", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findStmpApplFilePage(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            final CommonCondition condition) {
        
        CommonPage<StmpApplFileVo> fetchedStmpApplFilePage = stmpApplFileService.findStmpApplFilePage(projCd, stmpApplNo, condition);
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplFilePage", fetchedStmpApplFilePage), HttpStatus.OK);
    }
    
    /**
     * 증지파일 미리보기 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/file/{stmpFileNo}/preview", method=RequestMethod.GET )
    public ResponseEntity<Resource> previewStmpApplFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            @PathVariable("stmpFileNo") final Integer stmpFileNo) {
        
    	StmpApplFileVo fetchedStmpApplFile = stmpApplFileService.findStmpApplFile(projCd, stmpApplNo, stmpFileNo);
        return commonFileProcessor.previewFile(new StmpApplFileWrapper(fetchedStmpApplFile, basePath));
    }       
    
    /**
     * 증지파일 다운로드 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/file/{stmpFileNo}/download", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadCntrFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            @PathVariable("stmpFileNo") final Integer stmpFileNo) {
        
    	StmpApplFileVo fetchedStmpApplFile = stmpApplFileService.findStmpApplFile(projCd, stmpApplNo, stmpFileNo);
        return commonFileProcessor.downloadFile(new StmpApplFileWrapper(fetchedStmpApplFile, basePath));
    }   
}
