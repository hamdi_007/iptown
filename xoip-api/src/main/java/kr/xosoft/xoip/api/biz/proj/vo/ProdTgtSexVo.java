package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 상품대상성별 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProdTgtSexVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    private String projCd;

    /**
     * 성별코드
     */
    private String sexCd;

    /**
     * 등록일시
     */
    private Date regDt;

    /**
     * 등록자ID
     */
    private String regrId;
    
    public ProdTgtSexVo() {
    }
    
    public ProdTgtSexVo(String projCd, String sexCd) {
        setProjCd(projCd);
        setSexCd(sexCd);
    }         
    
}
