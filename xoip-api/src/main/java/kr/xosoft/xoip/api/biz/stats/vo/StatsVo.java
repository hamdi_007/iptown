package kr.xosoft.xoip.api.biz.stats.vo;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 통계 VO 
 * @author leejaejun
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StatsVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    
    /**
     * IP코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=3, max=3, groups=DefaultGroup.class )    
    private String ipCd;   
    
    /**
     * IP명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String ipNm;      
    
    /**
     * 회사명
     */
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String compNm;
    
    /**
     * 상품유형명
     */
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String prodTypNm;

    /**
     * 최고소비자금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private BigDecimal maxCnsmAmt;
    
    /**
     * 최저소비자금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private BigDecimal minCnsmAmt;
    
    /**
     * 평균소비자금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private BigDecimal avgCnsmAmt;
    
    /**
     * 최고출고금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private BigDecimal maxRlsAmt;
    
    /**
     * 최저출고금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private BigDecimal minRlsAmt;

    /**
     * 평균출고금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private BigDecimal avgRlsAmt;
    
    /**
     * 생산개수
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private Integer prdcCnt;
    
    /**
     * 로열티금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )    
    private BigDecimal ryltAmt;
    
}
