package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.List;

import javax.validation.Valid;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트+디자인검수+검수파일+의견파일 Union VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjDsgnInspUnionVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    @Valid
    private ProjVo proj;
    
    private DsgnInspVo dsgnInsp;
    
    private List<InspFileVo> inspFileList;
    
    private List<OpinFileVo> opinFileList;
    
    public ProjDsgnInspUnionVo() {
    }
    
    public ProjDsgnInspUnionVo(ProjVo proj, DsgnInspVo dsgnInsp, List<InspFileVo> inspFileList) {
        this(proj, dsgnInsp, inspFileList, null);
    }
    
    public ProjDsgnInspUnionVo(ProjVo proj, DsgnInspVo dsgnInsp, List<InspFileVo> inspFileList, List<OpinFileVo> opinFileList) {
        setProj(proj);
        setDsgnInsp(dsgnInsp);
        setInspFileList(inspFileList);
        setOpinFileList(opinFileList);
    }    
}
