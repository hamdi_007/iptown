package kr.xosoft.xoip.api.common.file;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.Base64;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.exception.BizException;
import lombok.extern.slf4j.Slf4j;

/**
 * 파일 업로드 처리 
 * @author likejy
 *
 */
@Component
@Slf4j
public class CommonFileProcessor {
    
    @Value("${api.file.maxUploadSize}")
    private Long maxUploadSize;    
    
    private static final Integer THUMB_MEDIUM_SIZE = 256;
    
    private static final Integer THUMB_SMALL_SIZE = 64;
    
    public void uploadFile(FileProcessable fp) {

        MultipartFile mFile = fp.getMultipartFile();
        
        Verifier.notNull(mFile, "chk.notNull", "file");
        Verifier.le(mFile.getSize(), maxUploadSize, "M1101", String.valueOf(maxUploadSize / 1024 / 1024)); 
        Verifier.le(mFile.getOriginalFilename().length(), 300, "chk.sizeMax", "fileName", "300");
        
        Verifier.not(StringUtils.isBlank(fp.getFileExtNm()), "M1005", "word.fileExtNm");
        Verifier.notEqual(fp.getFileExtNm().toLowerCase(), "ai", "M1105", "ai");
        Verifier.notEqual(fp.getFileExtNm().toLowerCase(), "psd", "M1105", "psd");
        
        try {
            
            // 디렉토리 생성 
            File fileDirInstance = new File(fp.getCanonicalDirPath());
            if (!fileDirInstance.exists()) {
                FileUtils.forceMkdir(fileDirInstance);
                fileDirInstance.setReadable(true, false);
                fileDirInstance.setWritable(true, true);
                fileDirInstance.setExecutable(true, false);                
            }
            
            // 파일 생성 
            File fileInstance = new File(fp.getCanonicalFilePath());
            fp.getMultipartFile().transferTo(fileInstance);
            fileInstance.setReadable(true, false);
            fileInstance.setWritable(true, true);
            fileInstance.setExecutable(false, false);
            
            // 썸네일 생성 
            if (fp.shouldCreateThumbnail()) {
                createThumbnail(fp); 
            }
                        
        } catch (IOException e) {
            throw new BizException("M1100", e);
        }        
    }
    
    private void createThumbnail(FileProcessable fp) {

        BufferedImage sourceImage = null;
        BufferedImage targetMediumImage = null;
        BufferedImage targetSmallImage = null;
        try {
            
            File sourceFile = new File(fp.getCanonicalFilePath());
            File targetMediumFile = new File(fp.getCanonicalTumbMdPath());
            File targetSmallFile = new File(fp.getCanonicalTumbSmPath());
            
            sourceImage = ImageIO.read(sourceFile);
            int width = sourceImage.getWidth();
            int height = sourceImage.getHeight();
            
            boolean isCopyMediumFile = false;
            boolean isCopySmallFile = false;
            Scalr.Mode mode = null;
            if (width > height) {
                mode = Scalr.Mode.FIT_TO_HEIGHT;
                isCopyMediumFile = height <= THUMB_MEDIUM_SIZE;
                isCopySmallFile = height <= THUMB_SMALL_SIZE;
            } else {
                mode = Scalr.Mode.FIT_TO_WIDTH;
                isCopyMediumFile = width <= THUMB_MEDIUM_SIZE;
                isCopySmallFile = width <= THUMB_SMALL_SIZE;                
            }
            
            // 썸네일 중 생성 
            if (isCopyMediumFile) {
                Files.copy(sourceFile.toPath(), targetMediumFile.toPath());                
            } else {
                targetMediumImage = Scalr.resize(sourceImage, Scalr.Method.QUALITY, mode, THUMB_MEDIUM_SIZE, Scalr.OP_ANTIALIAS);                
                ImageIO.write(targetMediumImage, fp.getFileExtNm(), targetMediumFile);
            }
            targetMediumFile.setReadable(true, false);
            targetMediumFile.setWritable(true, true);
            targetMediumFile.setExecutable(false, false);                        
            
            // 썸네일 소 생성 
            if (isCopySmallFile) {
                Files.copy(sourceFile.toPath(), targetSmallFile.toPath());                
            } else {
                targetSmallImage = Scalr.resize(sourceImage, Scalr.Method.QUALITY, mode, THUMB_SMALL_SIZE, Scalr.OP_ANTIALIAS);                
                ImageIO.write(targetSmallImage, fp.getFileExtNm(), targetSmallFile);
            }
            targetSmallFile.setReadable(true, false);
            targetSmallFile.setWritable(true, true);
            targetSmallFile.setExecutable(false, false);
            
        } catch (IOException e) {
            throw new BizException("M1100", e);
        } finally {
            if (sourceImage != null) { sourceImage.flush(); }
            if (targetMediumImage != null) { targetMediumImage.flush(); }
            if (targetSmallImage != null) { targetSmallImage.flush(); }
        }        
    }
    
    private String thumbToBase64String(String filePath) {
        if (filePath == null) {
            return null;
        }
        String base64String = null;
        try {
            byte[] fileData = FileUtils.readFileToByteArray(new File(filePath));
            base64String = Base64.getEncoder().encodeToString(fileData);        
        } catch (IOException e) {
            log.warn("failed to encode thumbnail : " + filePath);
        }
        return base64String;
    }    
    
    public String thumbMediumToBase64String(FileProcessable fp) {
        return thumbToBase64String(fp.getCanonicalTumbMdPath());
    }    
    
    public String thumbSmallToBase64String(FileProcessable fp) {
        return thumbToBase64String(fp.getCanonicalTumbSmPath());
    }
    
    public ResponseEntity<Resource> previewFile(FileProcessable fp) {
        
        if (!fp.isAvailableForPreview()) {
            Verifier.fail("M1103");
        }
        
        HttpHeaders headers = new HttpHeaders();

        // 미디어 타입 설정 
        MediaType mediaType = null;
        if (fp.getFileCntnTyp() == null) {
            mediaType = MediaType.parseMediaType("application/octet-stream");
        } else {
            mediaType = MediaType.parseMediaType(fp.getFileCntnTyp());
        }
        
        // 리소스 변환 및 리턴  
        try {
        
            InputStreamResource resource = new InputStreamResource(new FileInputStream(new File(fp.getCanonicalFilePath())));
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(mediaType)
                    .contentLength(fp.getFileSize())
                    .body(resource);
            
        } catch (FileNotFoundException e) {
            throw new BizException("M1102", e);
        }        
    }    
    
    public ResponseEntity<Resource> downloadFile(FileProcessable fp) {
        
        // 헤더 설정 
        // 모든브라우저에서 한글 파일명이 안깨지게 하려면, 아래와 같이 설정해야 한다.  
        String encFileNm = null;
        try {
            encFileNm = URLEncoder.encode(fp.getMngFileNm(),"UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            log.warn("failed to encode file name : {}", e.toString());
            encFileNm = fp.getMngFileNm();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + encFileNm + ";filename*=UTF-8''" + encFileNm);
        
        // 미디어 타입 설정 
        MediaType mediaType = null;
        if (fp.getFileCntnTyp() == null) {
            mediaType = MediaType.parseMediaType("application/octet-stream");
        } else {
            mediaType = MediaType.parseMediaType(fp.getFileCntnTyp());
        }
        
        // 리소스 변환 및 리턴  
        try {
        
            InputStreamResource resource = new InputStreamResource(new FileInputStream(new File(fp.getCanonicalFilePath())));
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(mediaType)
                    .contentLength(fp.getFileSize())
                    .body(resource);    
        
        } catch (FileNotFoundException e) {
            throw new BizException("M1102", e);
        }        
    }
    
}
