package kr.xosoft.xoip.api.biz.stmpappl.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 증지신청이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StmpApplHistVo extends StmpApplVo {

    private static final long serialVersionUID = 1L;

    /**
     * 증지신청이력일련번호 
     */
    private Integer stmpApplHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public StmpApplHistVo(String projCd, Integer stmpApplNo, String histDivCd) {
        setProjCd(projCd);
        setStmpApplNo(stmpApplNo);
        setHistDivCd(histDivCd);
    }        
    
}
