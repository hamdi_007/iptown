package kr.xosoft.xoip.api.common.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * UserDetails 구현체 
 * @author likejy
 *
 */
public class DefaultUserDetails implements UserDetails {

	private static final long serialVersionUID = 1L;

	/**
	 * 로그인 아이디
	 */
	private String username;

	/**
	 * 로그인 비밀번호
	 */
	private String password;

	/**
	 * 계정 유효기간이 만료되지 않았는지 여부
	 */
	private boolean accountNonExpired;

	/**
	 * 계정이 잠기지 않았는지 여부
	 */
	private boolean accountNonLocked;

	/**
	 * 비밀번호 유효기간이 만료되지 않았는지 여부
	 */
	private boolean credentialsNonExpired;

	/**
	 * 계정이 사용가능한지 여부
	 */
	private boolean enabled;

	/**
	 * 사용자 권한 목록
	 */
	private Collection<? extends GrantedAuthority> authorities;

	/**
	 * 사이트코드 
	 */
    private String siteCode;
    
    /**
     * 회사ID 
     */
    private String companyId;
    
    /**
     * 회사명 
     */
    private String companyName;
    
    /**
     * 사용자ID
     */
    private String userId;
    
    /**
     * 사용자명 
     */
    private String realUserName;
	
	private DefaultUserDetails(DefaultUserDetailsBuilder builder) {
		this.username = builder.username;
		this.password = builder.password;
		this.accountNonExpired = builder.accountNonExpired;
		this.accountNonLocked = builder.accountNonLocked;
		this.credentialsNonExpired = builder.credentialsNonExpired;
		this.enabled = builder.enabled;
		this.authorities = builder.authorities;
		this.siteCode = builder.siteCode;
		this.companyId = builder.companyId;
		this.companyName = builder.companyName;
		this.userId = builder.userId;
		this.realUserName = builder.realUserName;
	}

	public static DefaultUserDetailsBuilder builder() {
		return new DefaultUserDetailsBuilder();
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public List<String> getStringAuthorities() {
		if (authorities == null) {
			return null;
		}
		List<String> list = new ArrayList<String>();
		for (GrantedAuthority authority : authorities) {
			list.add(authority.getAuthority());
		}
		return list;
	}

	public String getCommaSeperatedAuthority() {
		if (authorities == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		int idx = 0;
		int lastIdx = authorities.size() - 1;
		for (GrantedAuthority authority : authorities) {
			sb.append(authority.getAuthority());
			if (idx < lastIdx) {
				sb.append(", ");
			}
			idx++;
		}
		return sb.toString();
	}
	
	public String getFirstStringAuthority() {
	    List<String> list = getStringAuthorities();
	    if (list == null || list.size() == 0) {
	        return null;
	    } else {
	        return list.get(0);
	    }
	}

	public String getSiteCode() {
	    return siteCode;
	}
	
	public String getCompanyId() {
	    return companyId;
	}

	public String getCompanyName() {
	    return companyName;
	}
	
	public String getUserId() {
	    return userId;
	}

	public String getRealUserName() {
	    return realUserName;
	}
	
    public static class DefaultUserDetailsBuilder {

        private String username;
        private String password;
    	private boolean accountNonExpired;
    	private boolean accountNonLocked;
    	private boolean credentialsNonExpired;
    	private boolean enabled;
        private List<GrantedAuthority> authorities;
        
        private String siteCode;
        private String companyId;
        private String companyName;
        private String userId;
        private String realUserName;        

        private DefaultUserDetailsBuilder() {
        	this.accountNonExpired = true;
        	this.accountNonLocked = true;
        	this.credentialsNonExpired = true;
        	this.enabled = true;
        }

        public DefaultUserDetailsBuilder username(String username) {
            this.username = username;
            return this;
        }

        public DefaultUserDetailsBuilder password(String password) {
        	this.password = password;
        	return this;
        }

        public DefaultUserDetailsBuilder accountNonExpired(boolean accountNonExpired) {
        	this.accountNonExpired = accountNonExpired;
        	return this;
        }

        public DefaultUserDetailsBuilder accountNonLocked(boolean accountNonLocked) {
        	this.accountNonLocked = accountNonLocked;
        	return this;
        }

        public DefaultUserDetailsBuilder credentialNonExpired(boolean credentialNonExpired) {
        	this.credentialsNonExpired = credentialNonExpired;
        	return this;
        }

        public DefaultUserDetailsBuilder enabled(boolean enabled) {
        	this.enabled = enabled;
        	return this;
        }

        public DefaultUserDetailsBuilder authorities(List<GrantedAuthority> authorities) {
            this.authorities = authorities;
            return this;
        }

        public DefaultUserDetailsBuilder commaSeperatedAuthority(String commaSeperatedAuthority) {
        	if (commaSeperatedAuthority == null) {
        		return this;
        	}
            this.authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(commaSeperatedAuthority);
            return this;
        }
        
        public DefaultUserDetailsBuilder siteCode(String siteCode) {
            this.siteCode = siteCode;
            return this;
        }
        
        public DefaultUserDetailsBuilder companyId(String companyId) {
            this.companyId = companyId;
            return this;
        }
        
        public DefaultUserDetailsBuilder companyName(String companyName) {
            this.companyName = companyName;
            return this;
        }
        
        public DefaultUserDetailsBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }
        
        public DefaultUserDetailsBuilder realUserName(String realUserName) {
            this.realUserName = realUserName;
            return this;
        }

        public DefaultUserDetails build() {
            return new DefaultUserDetails(this);
        }
    }

}
