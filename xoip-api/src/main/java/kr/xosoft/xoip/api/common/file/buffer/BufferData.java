package kr.xosoft.xoip.api.common.file.buffer;

import lombok.Data;

@Data
public class BufferData {
    
    public static final String CONTENT_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    private byte[] buffer;
    
    private String fileNm;
    
    private String fileCntnTyp;
    
    public BufferData(byte[] buffer, String fileNm) {
        this(buffer, fileNm, null);
    }
    
    public BufferData(byte[] buffer, String fileNm, String fileCntnTyp) {
        setBuffer(buffer);
        setFileNm(fileNm);
        setFileCntnTyp(fileCntnTyp);
    }
    
    public int getBufferLength() {
        if (buffer == null) {
            return 0;
        }
        return buffer.length;
    }
    
}
