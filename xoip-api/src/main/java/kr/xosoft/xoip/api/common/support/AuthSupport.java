package kr.xosoft.xoip.api.common.support;

import java.util.Map;

import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;

/**
 * 권한 서포트 
 * @author likejy
 *
 */
public class AuthSupport {

    /**
     * 로그인 사용자가 관리자인지 여부를 반환
     * @return
     */
    public static boolean isAdmin() {
        return isAdmin(CommonDataHolder.getCommonData().getAuth());
    }
    
    /**
     * 관리자 권한 여부를 반환
     * @param auth
     * @return
     */
    public static boolean isAdmin(String auth) {
        if (AuthConstant.ROLE_ADMIN.equals(auth)) {
            return true;
        } else {
            return false;
        }        
    }    
    
    /**
     * 로그인 사용자가 라이선서인지 여부를 반환
     * @return
     */
    public static boolean isLicensor() {
        return isLicensor(CommonDataHolder.getCommonData().getAuth());
    }

    /**
     * 라이선서 권한 여부를 반환
     * @param auth
     * @return
     */
    public static boolean isLicensor(String auth) {
        if (AuthConstant.ROLE_LICENSEE.equals(auth)) {
            return false;
        } else {
            return true;
        }
    }  
    
    /**
     * 로그인 사용자가 리뷰어인지 여부를 반환
     * @return
     */
    public static boolean isReviewer() {
        return isReviewer(CommonDataHolder.getCommonData().getAuth());
    }

    /**
     * 리뷰어 권한 여부를 반환
     * @param auth
     * @return
     */
    public static boolean isReviewer(String auth) {
        if (AuthConstant.ROLE_REVIEWER.equals(auth)) {
            return true;
        } else {
            return false;
        }
    }      

    /**
     * 로그인 사용자가 라이선시인지 여부를 반환
     * @return
     */
    public static boolean isLicensee() {
        return isLicensee(CommonDataHolder.getCommonData().getAuth());
    }    
    
    /**
     * 라이선시 권한 여부를 반환
     * @param auth
     * @return
     */
    public static boolean isLicensee(String auth) {
        if (AuthConstant.ROLE_LICENSEE.equals(auth)) {
            return true;
        } else {
            return false;
        }        
    }        
    
    /**
     * 로그인 사용자가 라이선시인 경우 map 에 로그인 사용자의 회사ID (comLgnCompId) 를 할당  
     * @param map
     */
    public static void assignComLgnCompIdIfLicensee(Map<String, Object> map) {
        CommonData commonData = CommonDataHolder.getCommonData();
        if (isLicensee(commonData.getAuth())) {
            map.put("comLgnCompId", commonData.getCompId());
        }
    }
    
}
