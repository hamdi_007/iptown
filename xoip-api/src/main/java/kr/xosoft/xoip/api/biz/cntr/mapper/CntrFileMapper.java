package kr.xosoft.xoip.api.biz.cntr.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.cntr.vo.CntrFileVo;

/**
 * 계약파일 매퍼 
 * @author likejy
 *
 */
@Repository
public interface CntrFileMapper {

    /**
     * 계약파일 등록
     * @param cntrFile
     * @return
     */
    public int insertCntrFile(CntrFileVo cntrFile);
    
    /**
     * 계약파일 수정 by PK
     * @param CntrFile
     * @return
     */
    public int updateCntrFileByKey(CntrFileVo cntrFile);
        
    /**
     * 계약파일 삭제 by PK
     * @param CntrFile
     * @return
     */
    public int deleteCntrFileByKey(CntrFileVo cntrFile);
    
    /**
     * 계약파일 조회 by PK
     * @param siteCd
     * @param CntrId
     * @param CntrFileNo
     * @return
     */
    public CntrFileVo findCntrFileByKey(@Param("siteCd") String siteCd, @Param("cntrId") String cntrId, @Param("cntrFileNo") Integer cntrFileNo);
    
    /**
     * 계약파일 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countCntrFileListByMap(Map<String, Object> map);
    
    /**
     * 계약파일 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<CntrFileVo> findCntrFileListByMap(Map<String, Object> map);    
    
}
