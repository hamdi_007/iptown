package kr.xosoft.xoip.api.biz.email.vo;

import java.util.List;
import java.util.Map;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 이메일 요청 폼 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class EmailReqFormVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    private String formCd;
    
    private String tmplLang;
    
    private List<String> recpList; 
    
    private Map<String, Object> varMap;
    
}
