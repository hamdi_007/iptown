package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.proj.service.InspFileService;
import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.InspFileWrapper;

/**
 * 검수파일 Controller 
 * @author likejy
 *
 */
@RestController
public class InspFileController {

    @Autowired
    private InspFileService inspFileService;

    @Autowired
    private CommonFileProcessor commonFileProcessor;

    @Value("${api.file.basePath}")
    private String basePath;
    
    /**
     * 검수파일을 등록한다. 
     * 파일을 디스크에 저장하기만 하며, 테이블에는 정보를 등록하지 않는다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/file", method=RequestMethod.POST, headers="content-type=multipart/form-data" )
    public ResponseEntity<CommonResult> registInspFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @RequestParam( value="file", required=false ) MultipartFile mFile,
            @RequestParam("compId") String compId,
            @RequestParam("inspFileDivCd") String inspFileDivCd) {
        
        Verifier.notNull(mFile, "M1004", "mFile");
        Verifier.notNull(compId, "M1004", "compId");
        Verifier.notNull(inspFileDivCd, "M1004", "inspFileDivCd");
        InspFileWrapper inspFileWrapper = new InspFileWrapper(mFile, compId, projCd, inspFileDivCd, basePath);
        commonFileProcessor.uploadFile(inspFileWrapper);
        
        InspFileVo inspFile = (InspFileVo)inspFileWrapper.getFileVo();
        String tumbData = commonFileProcessor.thumbSmallToBase64String(inspFileWrapper); 

        CommonResult result = new CommonResult("inspFile", inspFile);
        result.putObject("tumbData", tumbData);
        return new ResponseEntity<CommonResult>(result, HttpStatus.OK);
    }
    
    /**
     * 검수파일 미리보기 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/preview", method=RequestMethod.GET )
    public ResponseEntity<Resource> previewInspFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @PathVariable("inspFileNo") final Integer inspFileNo) {
        
        InspFileVo fetchedInspFile = inspFileService.findInspFile(projCd, inspNo, inspFileNo);
        return commonFileProcessor.previewFile(new InspFileWrapper(fetchedInspFile, basePath));
    }       
    
    /**
     * 검수파일 다운로드 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/download", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadInspFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @PathVariable("inspFileNo") final Integer inspFileNo) {
        
        InspFileVo fetchedInspFile = inspFileService.findInspFile(projCd, inspNo, inspFileNo);
        return commonFileProcessor.downloadFile(new InspFileWrapper(fetchedInspFile, basePath));
    }        
    
}
