package kr.xosoft.xoip.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.proj.service.DsgnInspService;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspFileUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspVo;
import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjDsgnInspUnionVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.InspFileWrapper;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * IP Controller 
 * @author likejy
 *
 */
@RestController
public class DsgnInspController {

    @Autowired
    private DsgnInspService dsngInspService;

    @Autowired
    private CommonFileProcessor commonFileProcessor;
    
    @Value("${api.file.basePath}")
    private String basePath;    
        
    /**
     * 디자인검수를 등록한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registDsgnInspUnion(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspFileUnionVo fetchedDsgnInspUnion = dsngInspService.registDsgnInspFileUnion(dsgnInspUnion);            
        return new ResponseEntity<CommonResult>(new CommonResult("dsgnInspUnion", fetchedDsgnInspUnion), HttpStatus.OK);
    }   
    
    /**
     * 디자인검수를 임시저장한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/draft", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyDsgnInspUnionInDraft(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @Validated(UpdatingGroup.class) @RequestBody final DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        Verifier.isEqual(projCd, dsgnInsp.getProjCd(), "M1004", "word.projCd");
        Verifier.eq(inspNo, dsgnInsp.getInspNo(), "M1004", "word.inspNo");
        DsgnInspFileUnionVo fetchedDsgnInspUnion = dsngInspService.modifyDsgnInspFileUnionInDraft(dsgnInspUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("dsgnInspUnion", fetchedDsgnInspUnion), HttpStatus.OK);
    }  
    
    /**
     * 디자인검수를 승인요청한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/request", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> requestDsgnInspUnionForApproval(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @Validated(UpdatingGroup.class) @RequestBody final DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        Verifier.isEqual(projCd, dsgnInsp.getProjCd(), "M1004", "word.projCd");
        Verifier.eq(inspNo, dsgnInsp.getInspNo(), "M1004", "word.inspNo");
        DsgnInspFileUnionVo fetchedDsgnInspUnion = dsngInspService.requestDsgnInspFileUnionForApproval(dsgnInspUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("dsgnInspUnion", fetchedDsgnInspUnion), HttpStatus.OK);
    }      
    
    /**
     * 디자인검수를 조건부 승인한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/change", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> approveDsgnInspUnionForChange(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @Validated(UpdatingGroup.class) @RequestBody final DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        Verifier.isEqual(projCd, dsgnInsp.getProjCd(), "M1004", "word.projCd");
        Verifier.eq(inspNo, dsgnInsp.getInspNo(), "M1004", "word.inspNo");
        DsgnInspFileUnionVo fetchedDsgnInspUnion = dsngInspService.approveWithChangeDsgnInspFileUnion(dsgnInspUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("dsgnInspUnion", fetchedDsgnInspUnion), HttpStatus.OK);
    }      
    
    /**
     * 디자인검수를 승인한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/approve", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> approveDsgnInspUnion(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @Validated(UpdatingGroup.class) @RequestBody final DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        Verifier.isEqual(projCd, dsgnInsp.getProjCd(), "M1004", "word.projCd");
        Verifier.eq(inspNo, dsgnInsp.getInspNo(), "M1004", "word.inspNo");
        DsgnInspFileUnionVo fetchedDsgnInspUnion = dsngInspService.approveDsgnInspFileUnion(dsgnInspUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("dsgnInspUnion", fetchedDsgnInspUnion), HttpStatus.OK);
    }          
    
    /**
     * 디자인검수를 반려한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/reject", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> rejectDsgnInspUnion(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @Validated(UpdatingGroup.class) @RequestBody final DsgnInspFileUnionVo dsgnInspUnion) {
        
        DsgnInspVo dsgnInsp = dsgnInspUnion.getDsgnInsp();
        Verifier.isEqual(projCd, dsgnInsp.getProjCd(), "M1004", "word.projCd");
        Verifier.eq(inspNo, dsgnInsp.getInspNo(), "M1004", "word.inspNo");
        DsgnInspFileUnionVo fetchedDsgnInspUnion = dsngInspService.rejectDsgnInspFileUnion(dsgnInspUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("dsgnInspUnion", fetchedDsgnInspUnion), HttpStatus.OK);
    }              
    
    /**
     * 디자인검수를 삭제한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteDsgnInsp(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo) {
        
        dsngInspService.deleteDsgnInsp(projCd, inspNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 디자인검수를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findProjDsgnInspUnion(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo) {
        
        ProjDsgnInspUnionVo fetchedProjUnion = dsngInspService.findProjDsgnInspUnion(projCd, inspNo);
        
        // 썸네일 데이터 조회 
        List<InspFileVo> inspFileList = fetchedProjUnion.getInspFileList();
        List<String> tumbDataList = new ArrayList<String>();
        for (InspFileVo inspFile : inspFileList) {
            tumbDataList.add(commonFileProcessor.thumbSmallToBase64String(new InspFileWrapper(inspFile, basePath)));
        }
        
        CommonResult result = new CommonResult("projUnion", fetchedProjUnion);
        result.putObject("tumbDataList", tumbDataList);        
        return new ResponseEntity<CommonResult>(result, HttpStatus.OK);
    }
    
    /**
     * 디자인검수 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findDsngInspPage(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            final CommonCondition condition) {
        
        CommonPage<DsgnInspVo> fetchedDsgnInspPage = dsngInspService.findLocalizedDsgnInspPage(projCd, condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("dsgnInspPage", fetchedDsgnInspPage), HttpStatus.OK);
    }
    
    /**
     * 다음검수단계를 조회한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/next", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findInspStgCdForNextStage(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd) {
        
        String inspStgCd = dsngInspService.findInspStgCdForNextStage(projCd);
        return new ResponseEntity<CommonResult>(new CommonResult("inspStgCd", inspStgCd), HttpStatus.OK);
    }    
    
}
