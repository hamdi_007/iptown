package kr.xosoft.xoip.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.com.service.ComCdService;
import kr.xosoft.xoip.api.common.bean.CommonResult;

/**
 * 공통코드 Controller 
 * @author likejy
 *
 */
@RestController
public class ComCdController {

    @Autowired
    private ComCdService comCdService;
    
    /**
     * 공통코드 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/codes", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findAvailableComCdList(
            @PathVariable("version") final Integer version,
            @RequestParam("comCds") final String comCds) {
        
        Map<String, Object> map = comCdService.findComCdListByCommaSeperatedTopComCd(comCds, true); 
        return new ResponseEntity<CommonResult>(new CommonResult(map), HttpStatus.OK);
    }

}
