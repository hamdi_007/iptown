package kr.xosoft.xoip.api.biz.com.service;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.xosoft.xoip.api.biz.com.vo.AddrVo;
import kr.xosoft.xoip.api.biz.com.vo.JusoCommonVo;
import kr.xosoft.xoip.api.biz.com.vo.JusoDataVo;
import kr.xosoft.xoip.api.biz.com.vo.JusoResultUnionVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.exception.BizException;
import lombok.extern.slf4j.Slf4j;

/**
 * 주소 서비스 Impl
 * @author likejy
 *
 */
@Service
@Transactional
@Slf4j
public class AddrServiceImpl implements AddrService {
    
    @Value("${api.addr.url}")
    private String addrUrl;
    
    @Value("${api.addr.confirmKey}")
    private String addrConfirmKey;
    
    @Autowired( required=false )
    RestTemplate restTemplate;
    
    @Autowired( required=false )
    ObjectMapper objectMapper;
    
    /**
     * 주소 목록 조회 by 검색조건 
     */
    @Override 
	public CommonPage<AddrVo> findAddrPageByCondition(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application","json", Charset.forName("UTF-8")));  
                
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(addrUrl)
            .queryParam("confmKey", addrConfirmKey)
            .queryParam("resultType", "json")
            .queryParam("currentPage", map.get("comPageNo"))
            .queryParam("countPerPage", map.get("comItemCntPerPage"))
            .queryParam("keyword", map.get("kwd"))
            .build(false);
            
        log.debug(builder.toUriString());
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);
        log.debug("statusCode : {}", response.getStatusCode());
        log.debug("body : {}", response.getBody());
        
        JusoResultUnionVo jusoResultUnion = null;
        try {
            jusoResultUnion = objectMapper.readValue(response.getBody(), JusoResultUnionVo.class);
        } catch (Exception e) {
            throw new BizException("T1200", e);
        }
        
        JusoCommonVo common = jusoResultUnion.getResults().getCommon();
        List<JusoDataVo> jusoList = jusoResultUnion.getResults().getJuso();
        Integer comTtlCnt = Integer.valueOf(common.getTotalCount());
        List<AddrVo> addrList = new ArrayList<AddrVo>();
        if ("0".equals(common.getErrorCode()) && jusoList != null) {
            for (JusoDataVo juso : jusoList) {
                AddrVo addr = new AddrVo();
                addr.setPostNo(juso.getZipNo());
                addr.setAddr(juso.getRoadAddr());
                addr.setDtlAddr1(juso.getRoadAddrPart1());
                addr.setDtlAddr2(juso.getRoadAddrPart2());
                addr.setJibunAddr(juso.getJibunAddr());
                addrList.add(addr);
            }
            return new CommonPage<AddrVo>(condition, addrList, comTtlCnt);
        } else {
            throw new BizException("M9001", new String[] { common.getErrorMessage() });
        }
        
	}
	
}
