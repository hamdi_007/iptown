package kr.xosoft.xoip.api.biz.stmpappl.mapper;



import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplFileVo;

/**
 * 계약파일 매퍼 
 * @author luna1819
 *
 */
@Repository
public interface StmpApplFileMapper {

    /**
     * 증지파일 등록
     * @param stmpApplFile
     * @return
     */
    public int insertStmpApplFile(StmpApplFileVo stmpApplFile);
    
    /**
     * 증지파일 수정 by PK
     * @param stmpApplFile
     * @return
     */
    public int updateStmpApplFileByKey(StmpApplFileVo stmpApplFile);
        
    /**
     * 증지파일 삭제 by PK
     * @param stmpApplFile
     * @return
     */
    public int deleteStmpApplFileByKey(StmpApplFileVo stmpApplFile);
    
    
    /**
     * 증지파일 조회 by PK
     * @param siteCd
     * @param stmpApplNo
     * @return
     */
    public StmpApplFileVo findStmpApplFileByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("stmpApplNo") Integer stmpApplNo, @Param("stmpFileNo") Integer stmpFileNo);
    
    /**
     * 증지파일 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countStmpApplFileListByMap(Map<String, Object> map);
    
    /**
     * 증지파일 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<StmpApplFileVo> findStmpApplFileListByMap(Map<String, Object> map);    
    
    
}
