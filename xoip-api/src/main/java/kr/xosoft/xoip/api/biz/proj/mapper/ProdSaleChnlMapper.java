package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdSaleChnlVo;

/**
 * 상품판매채널 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProdSaleChnlMapper {

    /**
     * 상품판매채널 등록
     * @param prodSaleChnl
     * @return
     */
    public int insertProdSaleChnl(ProdSaleChnlVo prodSaleChnl);
    
    /**
     * 상품판매채널 삭제 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */
    public int deleteProdSaleChnlByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
    
    /**
     * 판매채널코드값 목록조회 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */    
    public List<String> findSaleChnlCdListByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);

    /**
     * 판매채널코드 목록조회 by 프로젝트코드
     * @param siteCd
     * @param projCd
     * @return
     */
    public List<ComCdVo> findLocalizedComCdListForSaleChnlCdByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("comLang") String comLang);
    
}
