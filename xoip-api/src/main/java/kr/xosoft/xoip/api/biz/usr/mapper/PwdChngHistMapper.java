package kr.xosoft.xoip.api.biz.usr.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.usr.vo.PwdChngHistVo;

/**
 * 비밀번호변경이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface PwdChngHistMapper {

    /**
     * 비밀번호변경이력 등록 
     */
    public int insertPwdChngHist(PwdChngHistVo pwdChngHist);
        
}
