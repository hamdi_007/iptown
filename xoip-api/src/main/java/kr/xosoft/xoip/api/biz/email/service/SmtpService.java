package kr.xosoft.xoip.api.biz.email.service;

import kr.xosoft.xoip.api.biz.email.vo.EmailTrnsVo;

public interface SmtpService {

    public void sendEmail(EmailTrnsVo emailTrns);
    
}
