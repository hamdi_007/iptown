package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;

/**
 * 검수파일 매퍼 
 * @author likejy
 *
 */
@Repository
public interface InspFileMapper {

    /**
     * 검수파일 등록
     * @param inspFile
     * @return
     */
    public int insertInspFile(InspFileVo inspFile);
    
    /**
     * 검수파일 수정 by PK
     * @param inspFile
     * @return
     */
    public int updateInspFileByKey(InspFileVo inspFile);
        
    /**
     * 검수파일 삭제 by PK
     * @param inspFile
     * @return
     */
    public int deleteInspFileByKey(InspFileVo inspFile);
    
    /**
     * 검수파일 조회 by PK
     * @param siteCd
     * @param projCd
     * @Param inspNo
     * @param inspFileNo
     * @return
     */
    public InspFileVo findInspFileByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo, @Param("inspFileNo") Integer inspFileNo);
    
    /**
     * 검수파일 목록조회
     * @param siteCd
     * @param projCd
     * @param inspNo
     * @return
     */
    public List<InspFileVo> findInspFileList(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo);    
    
}
