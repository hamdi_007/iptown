package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.ProjChgHistVo;

/**
 * 프로젝트변경이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProjChgHistMapper {

    /**
     * 프로젝트변경이력 등록
     * @param projChgHist
     * @return
     */
    public int insertProjChgHist(ProjChgHistVo projChgHist);
    
    /**
     * 프로젝트변경이력 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countProjChgHistListByMap(Map<String, Object> map);
    
    /**
     * 프로젝트변경이력 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<ProjChgHistVo> findLocalizedProjChgHistListByMap(Map<String, Object> map);        
    
}
