package kr.xosoft.xoip.api.biz.ip.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.ip.vo.IpVo;

/**
 * IP 매퍼 
 * @author likejy
 *
 */
@Repository
public interface IpMapper {

   /**
    * IP 등록
    * @param ip
    * @return
    */
   public int insertIp(IpVo ip);
   
   /**
    * IP 수정 by PK
    * @param ip
    * @return
    */
   public int updateIpByKey(IpVo ip);
       
   /**
    * IP 삭제 by PK
    * @param ip
    * @return
    */
   public int deleteIpByKey(IpVo ip);
   
   /**
    * IP 조회 by PK
    * @param siteCd
    * @param ipId
    * @return
    */
   public IpVo findIpByKey(@Param("siteCd") String siteCd, @Param("ipId") String ipId);
   
   /**
    * IP 조회 by IP코드
    * @param siteCd
    * @param ipCd
    * @return
    */
   public IpVo findIpByIpCd(@Param("siteCd") String siteCd, @Param("ipCd") String ipCd);   
   
   /**
    * IP 목록 건수조회 by 검색조건
    * @param map
    * @return
    */
   public int countIpListByMap(Map<String, Object> map);
   
   /**
    * IP 목록조회 by 검색조건
    * @param map
    * @return
    */
   public List<IpVo> findIpListByMap(Map<String, Object> map);        
    
}
