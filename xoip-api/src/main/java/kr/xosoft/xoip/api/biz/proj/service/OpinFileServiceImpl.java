package kr.xosoft.xoip.api.biz.proj.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.proj.mapper.OpinFileMapper;
import kr.xosoft.xoip.api.biz.proj.vo.OpinFileVo;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * 의견파일 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class OpinFileServiceImpl implements OpinFileService {

    @Autowired
    private OpinFileMapper opinFileMapper;
    
    /**
     * 의견파일을 등록/삭제한다. 
     */
    @Override
    public List<OpinFileVo> registOrDeleteFileList(String projCd, Integer inspNo, List<OpinFileVo> opinFileList) {

        List<OpinFileVo> createdList = new ArrayList<OpinFileVo>();
        List<OpinFileVo> skippedList = new ArrayList<OpinFileVo>();
        List<OpinFileVo> deletedList = findOpinFileList(projCd, inspNo);
        
        // 등록/삭제대상 목록 필터링    
        if (opinFileList != null) {
            for (OpinFileVo file : opinFileList) {
                // 의견 파일번호가 없는 경우는 신규 등록 파일인 경우임 
                if (file.getOpinFileNo() == null) {
                    createdList.add(file);
                    continue;
                }
                int idx = -1;
                for (int i = 0; i < deletedList.size(); i++) {
                    if (deletedList.get(i).getOpinFileNo() == file.getOpinFileNo()) {
                        idx = i;
                        break;
                    }
                }
                if (idx >= 0) {
                    deletedList.remove(idx);
                    skippedList.add(file);
                } else {
                    createdList.add(file);
                }
            }
        } 
        
        // 의견파일 삭제 
        deleteOpinFileList(deletedList);
        
        // 신규 의견파일 등록 
        registOpinFileList(createdList);
        
        return opinFileList;
    }
    
    /**
     * 의견파일을 등록한다. 
     */
    private void registOpinFileList(List<OpinFileVo> opinFileList) {
        
        // 의견파일 등록
        if (opinFileList != null) {
            for (OpinFileVo opinFile : opinFileList) {
                int cnt = opinFileMapper.insertOpinFile(opinFile);
                Verifier.eq(cnt, 1, "M1000", "word.opinFile");
            }
        }
    }

    /**
     * 의견파일목록을 삭제한다. 
     */
    private void deleteOpinFileList(List<OpinFileVo> opinFileList) {
        
        // 의견파일 삭제
        if (opinFileList != null) {
            for (OpinFileVo opinFile : opinFileList) {
                int cnt = opinFileMapper.deleteOpinFileByKey(opinFile); 
                Verifier.eq(cnt, 1, "M1002", "word.opinFile");                        
            }
        }
    }

    /**
     * 의견파일을 삭제한다. 
     */
    @Override
    public void deleteOpinFile(String projCd, Integer inspNo, Integer inspFileNo, Integer opinFileNo){

        OpinFileVo opinFile = findOpinFile(projCd, inspNo, inspFileNo, opinFileNo);
        Verifier.notNull(opinFile, "M1003", "word.opinFile");
        
        int cnt = opinFileMapper.deleteOpinFileByKey(opinFile); 
        Verifier.eq(cnt, 1, "M1002", "word.opinFile");   
    }    

    /**
     * 의견파일을 조회한다. 
     */
    @Override
    public OpinFileVo findOpinFile(String projCd, Integer inspNo, Integer inspFileNo, Integer opinFileNo) {

        CommonData commonData = CommonDataHolder.getCommonData();
        return opinFileMapper.findOpinFileByKey(commonData.getSiteCd(), projCd, inspNo, inspFileNo, opinFileNo);
    }

    /**
     * 의견파일 목록 페이지를 조회한다. 
     */
    @Override
    public List<OpinFileVo> findOpinFileList(String projCd, Integer inspNo) {

        CommonData commonData = CommonDataHolder.getCommonData();
        return opinFileMapper.findOpinFileList(commonData.getSiteCd(), projCd, inspNo);
    }

}
