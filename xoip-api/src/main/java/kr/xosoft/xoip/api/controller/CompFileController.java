package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.comp.service.CompFileService;
import kr.xosoft.xoip.api.biz.comp.vo.CompFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.CompFileWrapper;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 회사파일 Controller 
 * @author likejy
 *
 */
@RestController
public class CompFileController {

    @Autowired
    private CompFileService compFileService;

    @Autowired
    private CommonFileProcessor commonFileProcessor;

    @Value("${api.file.basePath}")
    private String basePath;
    
    /**
     * 회사파일을 등록한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/file", method=RequestMethod.POST, headers="content-type=multipart/form-data" )
    public ResponseEntity<CommonResult> registCompFile(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            @RequestParam( value="file", required=false ) MultipartFile mFile,
            @RequestParam("compFileDivCd") String compFileDivCd) {
        
        Verifier.notNull(mFile, "M1004", "mFile");
        Verifier.notNull(compFileDivCd, "M1004", "compFileDivCd");
        CompFileWrapper compFileWrapper = new CompFileWrapper(mFile, compId, compFileDivCd, basePath);
        commonFileProcessor.uploadFile(compFileWrapper);

        CompFileVo fetchedCompFile = compFileService.registCompFile((CompFileVo)compFileWrapper.getFileVo());            
        return new ResponseEntity<CommonResult>(new CommonResult("compFile", fetchedCompFile), HttpStatus.OK);
    }   
    
    /**
     * 회사파일 정보를 수정한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/file/{compFileNo}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyCompFile(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            @PathVariable("compFileNo") final Integer compFileNo,
            @Validated(UpdatingGroup.class) @RequestBody final CompFileVo compFile) {
        
        Verifier.isEqual(compId, compFile.getCompId(), "M1004", "word.compId");
        Verifier.isEqual(compFileNo, compFile.getCompFileNo(), "M1004", "word.compFileNo");
        CompFileVo fetchedCompFile = compFileService.modifyCompFile(compFile);
        return new ResponseEntity<CommonResult>(new CommonResult("compFile", fetchedCompFile), HttpStatus.OK);
    }        
    
    /**
     * 회사파일을 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/file/{compFileNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteCompFile(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            @PathVariable("compFileNo") final Integer compFileNo) {
        
        compFileService.deleteCompFile(compId, compFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 회사파일을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/file/{compFileNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findCompFile(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            @PathVariable("compFileNo") final Integer compFileNo) {
        
        CompFileVo fetchedCompFile = compFileService.findCompFile(compId, compFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult("compFile", fetchedCompFile), HttpStatus.OK);
    }
    
    /**
     * 회사파일 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/file", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findCompFilePage(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            final CommonCondition condition) {
        
        CommonPage<CompFileVo> fetchedCompFilePage = compFileService.findCompFilePage(compId, condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("compFilePage", fetchedCompFilePage), HttpStatus.OK);
    }
    
    /**
     * 회사파일 미리보기 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/file/{compFileNo}/preview", method=RequestMethod.GET )
    public ResponseEntity<Resource> previewCompFile(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            @PathVariable("compFileNo") final Integer compFileNo) {
        
        CompFileVo fetchedCompFile = compFileService.findCompFile(compId, compFileNo);
        return commonFileProcessor.previewFile(new CompFileWrapper(fetchedCompFile, basePath));
    }    
    
    /**
     * 회사파일 다운로드 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}/file/{compFileNo}/download", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadCompFile(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            @PathVariable("compFileNo") final Integer compFileNo) {
        
        CompFileVo fetchedCompFile = compFileService.findCompFile(compId, compFileNo);
        return commonFileProcessor.downloadFile(new CompFileWrapper(fetchedCompFile, basePath));
    }        
    
}
