package kr.xosoft.xoip.api.biz.ip.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.ip.vo.IpHistVo;

/**
 * IP이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface IpHistMapper {

    /**
     * 회사이력 등록 
     */
    public void insertIpHistAsSelectMaster(IpHistVo ipHist);    
    
}
