package kr.xosoft.xoip.api.biz.com.vo;

import lombok.Data;

@Data
public class JusoCommonVo {

    /**
     * 총검색 데이터 수 
     */
    private String totalCount;
    
    /**
     * 페이지번호 
     */
    private Integer currentPage;
    
    /**
     * 페이지당 출력할 결과 ROW 수  
     */
    private Integer countPerPage;
    
    /**
     * 에러코드 
     */
    private String errorCode;
    
    /**
     * 에러메시지 
     */
    private String errorMessage;
    
}
