package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.stmpappl.service.StmpApplService;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplVo;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplUnionVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.file.buffer.BufferData;
import kr.xosoft.xoip.api.common.file.buffer.BufferDownloader;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 증지신청 Controller 
 * @author likejy
 *
 */
@RestController
public class StmpApplController {

    @Autowired
    private StmpApplService stmpApplService;
    
    @Autowired
    private BufferDownloader bufferDownloader;

    /**
     * 증지신청을 위한 폼 정보를 조회한다.  
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/init", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> initStmpApplUnion(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd) {
        
        StmpApplUnionVo fetchedStmpApplUnion = stmpApplService.initStmpApplUnion(projCd);            
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplUnion", fetchedStmpApplUnion), HttpStatus.OK);
    }       
    
    /**
     * 증지신청을 등록한다. 
     */
    @PreAuthorize("hasRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registStmpApplUnion(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final StmpApplUnionVo stmpApplUnion) {
        
        StmpApplUnionVo fetchedStmpApplUnion = stmpApplService.registStmpApplUnion(stmpApplUnion);            
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplUnion", fetchedStmpApplUnion), HttpStatus.OK);
    }   
    
    /**
     * 증지신청을 변경한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyStmpApplUnion(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo,
            @Validated(UpdatingGroup.class) @RequestBody final StmpApplUnionVo stmpApplUnion) {
        
        StmpApplUnionVo fetchedStmpApplUnion = stmpApplService.modifyStmpApplUnion(stmpApplUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplUnion", fetchedStmpApplUnion), HttpStatus.OK);
    }  
    
    /**
     * 증지신청을 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteStmpAppl(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo) {
        
        stmpApplService.deleteStmpAppl(projCd, stmpApplNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 증지신청을 확인유무를 변경한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> updateConfirmStmpAppl(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo) {
        
        stmpApplService.updateConfirmStmpAppl(projCd, stmpApplNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 증지신청을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findProjStmpApplUnion(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo) {
        
        StmpApplUnionVo fetchedStmpApplUnion = stmpApplService.findStmpApplUnionVo(projCd, stmpApplNo);
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplUnion", fetchedStmpApplUnion), HttpStatus.OK);
    }
    
    /**
     * 증지신청 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/stamps", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findDsngInspPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<StmpApplVo> fetchedStmpApplPage = stmpApplService.findStmpApplRsltPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("stmpApplPage", fetchedStmpApplPage), HttpStatus.OK);
    }

    /**
     * 생산보고서를 다운로드 한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/reports/product", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadProcRpt(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo) {
        
        BufferData bd = stmpApplService.makePrdcRptExcelData(projCd, stmpApplNo);
        return bufferDownloader.downloadBuffer(bd);
    }   
  
    
    /**
     * 증지신청서를 다운로드 한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/stamps/{stmpApplNo}/reports/application", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadStmpApplRpt(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("stmpApplNo") final Integer stmpApplNo) {
        
        BufferData bd = stmpApplService.makeStmpApplRptExcelData(projCd, stmpApplNo);
        return bufferDownloader.downloadBuffer(bd);
    }
      
}
