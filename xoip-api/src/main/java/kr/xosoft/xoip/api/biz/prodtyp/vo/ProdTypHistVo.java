package kr.xosoft.xoip.api.biz.prodtyp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 상품유형이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProdTypHistVo extends ProdTypVo {

    private static final long serialVersionUID = 1L;

    /**
     * 상품유형이력일련번호 
     */
    private Integer prodTypHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public ProdTypHistVo(String prodTypId, String histDivCd) {
        setProdTypId(prodTypId);
        setHistDivCd(histDivCd);
    }        
    
}
