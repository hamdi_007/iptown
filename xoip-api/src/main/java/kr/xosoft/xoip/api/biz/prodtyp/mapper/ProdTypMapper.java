package kr.xosoft.xoip.api.biz.prodtyp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.prodtyp.vo.ProdTypVo;

/**
 * 상품유형 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProdTypMapper {

    /**
     * 상품유형 등록
     * @param prodTyp
     * @return
     */
    public int insertProdTyp(ProdTypVo prodTyp);
    
    /**
     * 상품유형 수정 by PK
     * @param prodTyp
     * @return
     */
    public int updateProdTypByKey(ProdTypVo prodTyp);
        
    /**
     * 상품유형 삭제 by PK
     * @param prodTyp
     * @return
     */
    public int deleteProdTypByKey(ProdTypVo prodTyp);
    
    /**
     * 상품유형 조회 by PK
     * @param siteCd
     * @param prodTypId
     * @return
     */
    public ProdTypVo findProdTypByKey(@Param("siteCd") String siteCd, @Param("prodTypId") String prodTypId);
    
    /**
     * 상품유형 조회 by 상품유형코드
     * @param siteCd
     * @param prodTypCd
     * @return
     */
    public ProdTypVo findProdTypByProdTypCd(@Param("siteCd") String siteCd, @Param("prodTypCd") String prodTypCd);   
    
    /**
     * 상품유형 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countProdTypListByMap(Map<String, Object> map);
    
    /**
     * 상품유형 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<ProdTypVo> findProdTypListByMap(Map<String, Object> map);     
    
    /**
     * 상품유형 목록조회 by 상품유형레벨, 상위상품유형ID, 사용여부
     * @param siteCd
     * @param prodTypLv
     * @param topProdTypId
     * @param useYn
     * @return
     */
    public List<ProdTypVo> findProdTypListByProdTypLvAndTopProdTypId(@Param("siteCd") String siteCd, @Param("prodTypLv") Integer prodTypLv, @Param("topProdTypId") String topProdTypId, @Param("useYn") String useYn);
    
}
