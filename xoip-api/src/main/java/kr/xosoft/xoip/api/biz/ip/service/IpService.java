package kr.xosoft.xoip.api.biz.ip.service;

import kr.xosoft.xoip.api.biz.ip.vo.IpVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface IpService {

    public IpVo registIp(IpVo ip);
        
    public IpVo modifyIp(IpVo ip);
    
    public void deleteIp(String ipId);
    
    public IpVo findIp(String ipId);
    
    public IpVo findIpByIpCd(String ipCd);
    
    public CommonPage<IpVo> findIpPage(CommonCondition condition);
    
}
