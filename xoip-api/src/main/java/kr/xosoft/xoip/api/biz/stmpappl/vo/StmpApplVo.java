package kr.xosoft.xoip.api.biz.stmpappl.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 증지신청 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StmpApplVo extends BaseVo {
    
    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=UpdatingGroup.class )        
    private String projCd;
    
    /**
     * 계약ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String cntrId;
    
    /**
     * 계약번호 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String cntrNo;
    
    /**
     * 계약명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String cntrNm;
    
    /**
     * 증지신청번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private Integer stmpApplNo;

    /**
     * 증지신청일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date stmpApplDt;
    
    /**
     * 증지신청회사명
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String stmpApplCompNm;
    
    /**
     * 확인여부
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String stmpConf;

    /**
     * 증지신청자명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String stmpApplrNm;

    /**
     * 증지신청자전화번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )
    private String stmpApplrTelno;

    /**
     * 증지신청자핸드폰번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )    
    private String stmpApplrHnpno;

    /**
     * 증지신청자이메일
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    @Email( message="chk.email", groups=DefaultGroup.class )        
    private String stmpApplrEmail;
    
    /**
     * 라이선서회사명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String licrCompNm;

    /**
     * 라이선서담당자명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String licrChrgrNm;

    /**
     * 라이선서담당자전화번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )    
    private String licrChrgrTelno;

    /**
     * 라이선서담당자핸드폰번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )    
    private String licrChrgrHnpno;

    /**
     * 라이선서담당자이메일
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    @Email( message="chk.email", groups=DefaultGroup.class )            
    private String licrChrgrEmail;

    /**
     * 증지수신자명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String stmpRecpNm;

    /**
     * 증지수신자전화번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )    
    private String stmpRecpTelno;

    /**
     * 증지수신자주소
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=500, groups=DefaultGroup.class )
    private String stmpRecpAddr;

    /**
     * 증지수신자상세주소
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=500, groups=DefaultGroup.class )
    private String stmpRecpDtlAddr;

    /**
     * 증지수신자우편번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )
    private String stmpRecpPostNo;

    /**
     * 계산서이메일
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    @Email( message="chk.email", groups=DefaultGroup.class )            
    private String billEmail;

    /**
     * 증지제작회사명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String stmpMnfcCompNm;

    /**
     * 증지제작담당자명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String stmpMnfcChrgrNm;

    /**
     * 증지제작담당자전화번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )    
    private String stmpMnfcChrgrTelno;

    /**
     * 증지제작담당자핸드폰번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )    
    private String stmpMnfcChrgrHnpno;

    /**
     * 증지제작담당자이메일
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    @Email( message="chk.email", groups=DefaultGroup.class )            
    private String stmpMnfcChrgrEmail;

    /**
     * 증지제작회사계좌정보
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String stmpMnfcCompAcctInfo;

    /**
     * 통화코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )    
    private String ccCd;

    /**
     * 증지기준금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal stmpStdAmt;

    /**
     * 생산개수합계
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer prdcCntSum;

    /**
     * 로열티금액합계
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal ryltAmtSum;

    /**
     * 증지금액합계
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal stmpAmtSum;

    /**
     * 증지부가세합계
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal stmpVatSum;

    /**
     * 입금대상금액합계
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal dpsTgtAmtSum;

    /**
     * 증지신청자서명값
     */
    @NotNull( message="chk.notNull", groups=CreatingGroup.class )
    private String stmpApplrSignVal;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;    
    
}
