package kr.xosoft.xoip.api.biz.stmpappl.vo;

import java.util.List;

import javax.validation.Valid;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 디자인검수+검수파일+의견파일 Union VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StmpApplUnionVo extends BaseVo {
    
    private static final long serialVersionUID = 1L;

    @Valid
    private StmpApplVo stmpAppl;
    
    private List<StmpLotVo> stmpLotList;
    
    public StmpApplUnionVo() {
    }
    
    public StmpApplUnionVo(StmpApplVo stmpAppl, List<StmpLotVo> stmpLotList) {
        setStmpAppl(stmpAppl);
        setStmpLotList(stmpLotList);
    }
    
}
