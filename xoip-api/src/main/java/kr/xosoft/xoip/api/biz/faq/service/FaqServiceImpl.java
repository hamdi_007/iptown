package kr.xosoft.xoip.api.biz.faq.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.biz.com.service.ComCdService;
import kr.xosoft.xoip.api.biz.faq.mapper.FaqHistMapper;
import kr.xosoft.xoip.api.biz.faq.mapper.FaqMapper;
import kr.xosoft.xoip.api.biz.faq.vo.FaqHistVo;
import kr.xosoft.xoip.api.biz.faq.vo.FaqVo;

/**
 * FAQ 서비스 Impl
 * @author hypark
 *
 */
@Transactional
@Service
public class FaqServiceImpl implements FaqService {
	
    @Autowired
    private FaqMapper faqMapper;
    
    @Autowired
    private FaqHistMapper faqHistMapper;
    
    /**
     * FAQ를 등록한다. 
     */
	@Override
	public FaqVo registFaq(FaqVo faq) {

        // FAQ 등록 
        int cnt = faqMapper.insertFaq(faq);
        Verifier.eq(cnt, 1, "M1000", "word.faq");

        faq.setRegDt(faq.getModDt());
        
        // FAQ 이력 등록 
        FaqHistVo faqHist = new FaqHistVo(faq.getFaqNo(), "HD0100");
        faqHistMapper.insertFaqHistAsSelectMaster(faqHist);
        Verifier.notNull(faqHist.getFaqHistSeqno(), "M1000", "word.faqHist");
                
        return faq;
	}

	/**
     * FAQ를 수정한다. 
     */
	@Override
	public FaqVo modifyFaq(FaqVo faq) {
		// FAQ 조회 
        FaqVo fetchedFaq = findFaq(faq.getFaqNo());
        Verifier.notNull(fetchedFaq, "M1003", "word.faq");
        
        fetchedFaq.setLangVal(faq.getLangVal());
        fetchedFaq.setFaqDivCd(faq.getFaqDivCd());
        fetchedFaq.setQstn(faq.getQstn());
        fetchedFaq.setAnsr(faq.getAnsr());
        fetchedFaq.setPostYn(faq.getPostYn());
        
        // FAQ 수정 
        int cnt = faqMapper.updateFaqByKey(fetchedFaq);
        Verifier.eq(cnt, 1, "M1001", "word.faq");

        // FAQ 이력 등록 
        FaqHistVo faqHist = new FaqHistVo(faq.getFaqNo(), "HD0200");
        faqHistMapper.insertFaqHistAsSelectMaster(faqHist);
        Verifier.notNull(faqHist.getFaqHistSeqno(), "M1000", "word.faqHist");        
        
        return fetchedFaq;        
    }

	/**
     * FAQ를 삭제한다. 
     */
	@Override
	public void deleteFaq(Integer faqNo) {
		
        // FAQ 삭제 
        FaqVo faq = new FaqVo();
        faq.setFaqNo(faqNo);
        int cnt = faqMapper.deleteFaqByKey(faq); 
        Verifier.eq(cnt, 1, "M1002", "word.faq");
         
        // FAQ 이력 등록 
        FaqHistVo faqHist = new FaqHistVo(faq.getFaqNo(), "HD0300");
        faqHistMapper.insertFaqHistAsSelectMaster(faqHist);
        Verifier.notNull(faqHist.getFaqHistSeqno(), "M1000", "word.faqHist");       
        	
	}
	
	/**
     * FAQ를 조회한다. 
     */
	@Override
	public FaqVo findFaq(Integer faqNo) {
		
		String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return faqMapper.findFaqByKey(siteCd, faqNo);
		
	}
	
	/**
     * FAQ 목록 페이지를 조회한다. 
     */
	@Override
	public CommonPage<FaqVo> findFaqPage(CommonCondition condition) {
		
		Map<String, Object> map = condition.toMap();

        int comTtlCnt = faqMapper.countFaqListByMap(map);
        List<FaqVo> faqList = faqMapper.findFaqListByMap(map);
        
        return new CommonPage<FaqVo>(condition, faqList, comTtlCnt);   
	}
	
	/**
     * 라이선시용 FAQ 목록 페이지를 조회한다. 
     */
	@Override
	public CommonPage<FaqVo> findLiceFaqPage(CommonCondition condition) {
		
		Map<String, Object> map = condition.toMap();

        int comTtlCnt = faqMapper.countLiceFaqListByMap(map);
        List<FaqVo> liceFaqList = faqMapper.findLiceFaqListByMap(map);
        
        return new CommonPage<FaqVo>(condition, liceFaqList, comTtlCnt);   
	}
	
	/**
     * 라이선시용 FAQ 구분코드를  조회한다. 
     */
	@Override
	public List<FaqVo> findFaqDivCdList(CommonCondition condition) {
		String siteCd = CommonDataHolder.getCommonData().getSiteCd();
		CommonData commonData = CommonDataHolder.getCommonData();
		
		List<FaqVo> faqDivCdList = faqMapper.findFaqDivCd(siteCd, commonData.getLang());
		return faqDivCdList;
	}
	
}
