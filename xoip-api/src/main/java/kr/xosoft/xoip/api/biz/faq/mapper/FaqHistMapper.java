package kr.xosoft.xoip.api.biz.faq.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.faq.vo.FaqHistVo;

/**
 * 계약이력 매퍼 
 * @author hypark
 *
 */
@Repository
public interface FaqHistMapper {

    /**
     * 계약이력 등록 
     */
    public void insertFaqHistAsSelectMaster(FaqHistVo faqHist);    
    
}
