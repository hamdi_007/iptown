package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.comp.service.CompService;
import kr.xosoft.xoip.api.biz.comp.vo.CompVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 회사 Controller 
 * @author likejy
 *
 */
@RestController
public class CompController {

    @Autowired
    private CompService compService;
    
    /**
     * 회사를 등록한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/companies", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registLicensorComp(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final CompVo comp) {
        
        CompVo fetchedComp = compService.registComp(comp);            
        return new ResponseEntity<CommonResult>(new CommonResult("comp", fetchedComp), HttpStatus.OK);
    }   
    
    /**
     * 회사 기본 정보를 수정한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyComp(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId,
            @Validated(UpdatingGroup.class) @RequestBody final CompVo comp) {
        
        Verifier.isEqual(compId, comp.getCompId(), "M1004", "word.compId");
        CompVo fetchedComp = compService.modifyComp(comp);
        return new ResponseEntity<CommonResult>(new CommonResult("comp", fetchedComp), HttpStatus.OK);
    }        
    
    /**
     * 회사를 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/companies/{compId}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteComp(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId) {
        
        compService.deleteComp(compId);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 회사를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/{compId}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findComp(
            @PathVariable("version") final Integer version,
            @PathVariable("compId") final String compId) {
        
        CompVo fetchedComp = compService.findComp(compId);
        return new ResponseEntity<CommonResult>(new CommonResult("comp", fetchedComp), HttpStatus.OK);
    }
    
    /**
     * 회사코드로 회사를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/code", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findCompByCompCd(
            @PathVariable("version") final Integer version,
            @RequestParam( value="compCd", required=true ) final String compCd) {
        
        CompVo fetchedComp = compService.findCompByCompCd(compCd);
        return new ResponseEntity<CommonResult>(new CommonResult("comp", fetchedComp), HttpStatus.OK);
    }    
    
    /**
     * 사업자등록번호로 회사를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies/bizno", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findCompByBizno(
            @PathVariable("version") final Integer version,
            @RequestParam( value="bizno", required=true ) final String bizno) {
        
        CompVo fetchedComp = compService.findCompByBizno(bizno);
        return new ResponseEntity<CommonResult>(new CommonResult("comp", fetchedComp), HttpStatus.OK);
    }    
    
    /**
     * 라이선시 회사 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/companies", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findLiceCompPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<CompVo> fetchedCompPage = compService.findLiceCompPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("compPage", fetchedCompPage), HttpStatus.OK);
    }
    
}
