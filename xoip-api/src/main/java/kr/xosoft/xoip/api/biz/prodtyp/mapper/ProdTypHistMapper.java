package kr.xosoft.xoip.api.biz.prodtyp.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.prodtyp.vo.ProdTypHistVo;

/**
 * 상품유형이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProdTypHistMapper {

    /**
     * 상품유형이력 등록 
     */
    public void insertProdTypHistAsSelectMaster(ProdTypHistVo prodTypHist);    
    
}
