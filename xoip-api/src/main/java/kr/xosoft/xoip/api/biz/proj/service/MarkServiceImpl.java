package kr.xosoft.xoip.api.biz.proj.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.proj.mapper.MarkMapper;
import kr.xosoft.xoip.api.biz.proj.vo.MarkUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspVo;
import kr.xosoft.xoip.api.biz.proj.vo.MarkOpinVo;
import kr.xosoft.xoip.api.biz.proj.vo.MarkVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjVo;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * 마크업 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class MarkServiceImpl implements MarkService {

    @Autowired
    private MarkService markService;
    
    @Autowired
    private MarkOpinService markOpinService;
    
    @Autowired
    private MarkMapper markMapper;
    
    @Autowired
    private ProjService projService;
    
    @Autowired
    private DsgnInspService dsgnInspService;
    
    /**
     * 마크업메타와 의견을 등록한다. 
     */
    @Override
    public MarkUnionVo registMarkUnion(MarkUnionVo markUnion) {
    	
    	MarkVo mark = markUnion.getMark();

        ProjVo proj = projService.findProj(mark.getProjCd());
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 진행 상태의 프로젝트만 마크업 수정 처리가 가능함 
        String projStsCd = proj.getProjStsCd();
        System.out.println(projStsCd);
        Verifier.isEqual(projStsCd, "PS0300", "M7006");
        
        DsgnInspVo dsgnInsp = dsgnInspService.findLocalizedDsgnInsp(mark.getProjCd(), mark.getInspNo());
        Verifier.notNull(dsgnInsp, "M1003", "word.dsgnInsp");
        
        // 승인요청 상태의 디자인검수만 마크업 수정 처리가 가능함 
        String dsgnInspStsCd = dsgnInsp.getInspStsCd();
        System.out.println(dsgnInspStsCd);
        Verifier.isEqual(dsgnInspStsCd, "IS0200", "M7021");
        
        // 메타데이터가 존재하는지 확인 
        MarkVo fetchedMark = markService.findMarkByKey(mark.getProjCd(), mark.getInspNo(), mark.getInspFileNo());
        Verifier.isNull(fetchedMark, "M1000", "word.mark");
        
        // 메타데이터 등록 
        int cnt = markMapper.insertMark(mark);
        Verifier.eq(cnt, 1, "M1000", "word.mark");
        
        mark = markService.findMarkByKey(mark.getProjCd(), mark.getInspNo(), mark.getInspFileNo());
        
        List<MarkOpinVo> markOpinList = markUnion.getMarkOpinList();
        if (markOpinList != null) {
            for (MarkOpinVo opin : markOpinList) {
            	opin.setInspNo(mark.getInspNo());
            	opin.setInspFileNo(mark.getInspFileNo());
            	opin.setMarkNo(mark.getMarkNo());
            }
        }
        // 마크업의견 등록 
        markOpinService.mergeMarkOpinList(mark.getProjCd(), mark.getInspNo(), mark.getInspFileNo(), mark.getMarkNo(), markOpinList);
        
        return markUnion;
    }
    
    
    /**
     * 마크업메타와 의견을 수정한다. 
     */
	@Override
	public MarkUnionVo modifyMarkUnion(MarkUnionVo markUnion) {
		
    	MarkVo mark = markUnion.getMark();
    	List<MarkOpinVo> markOpinList = markUnion.getMarkOpinList();
    	
        ProjVo proj = projService.findProj(mark.getProjCd());
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 진행 상태의 프로젝트만 마크업 수정 처리가 가능함 
        String projStsCd = proj.getProjStsCd();
        Verifier.isEqual(projStsCd, "PS0300", "M7006");
        
        DsgnInspVo dsgnInsp = dsgnInspService.findLocalizedDsgnInsp(mark.getProjCd(), mark.getInspNo());
        Verifier.notNull(dsgnInsp, "M1003", "word.dsgnInsp");
        
        // 승인요청 상태의 디자인검수만 마크업 수정 처리가 가능함 
        String dsgnInspStsCd = dsgnInsp.getInspStsCd();
        Verifier.isEqual(dsgnInspStsCd, "IS0200", "M7021");
        
    	
    	MarkVo fetchedMark = findMarkByKey(mark.getProjCd(), mark.getInspNo(), mark.getInspFileNo());
    	Verifier.notNull(fetchedMark, "M1003", "word.mark");
    	
    	int cnt = markMapper.updateMarkByKey(mark);
        Verifier.eq(cnt, 1, "M1001", "word.mark");
        
        if (markOpinList != null) {
            for (MarkOpinVo opin : markOpinList) {
            	opin.setInspNo(mark.getInspNo());
            	opin.setInspFileNo(mark.getInspFileNo());
            	opin.setMarkNo(mark.getMarkNo());
            }
        }
        
        markOpinService.mergeMarkOpinList(mark.getProjCd(), mark.getInspNo(), mark.getInspFileNo(), mark.getMarkNo(), markOpinList);
        return markUnion;
	}
    
    /**
     * 마크업메타와 의견을 삭제한다. 
     */
	@Override
	public void deleteMarkUnion(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo) {
		
		MarkVo mark = findMarkByKey(projCd, inspNo, inspFileNo);
		Verifier.notNull(mark, "M1003", "word.mark");
    	
        ProjVo proj = projService.findProj(mark.getProjCd());
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 진행 상태의 프로젝트만 마크업 수정 처리가 가능함 
        String projStsCd = proj.getProjStsCd();
        Verifier.isEqual(projStsCd, "PS0300", "M7006");
        
        DsgnInspVo dsgnInsp = dsgnInspService.findLocalizedDsgnInsp(mark.getProjCd(), mark.getInspNo());
        Verifier.notNull(dsgnInsp, "M1003", "word.dsgnInsp");
        
        // 승인요청 상태의 디자인검수만 마크업 수정 처리가 가능함 
        String dsgnInspStsCd = dsgnInsp.getInspStsCd();
        Verifier.isEqual(dsgnInspStsCd, "IS0200", "M7021");
		
		int cnt = markMapper.deleteMarkByKey(mark); 
        Verifier.eq(cnt, 1, "M1002", "word.mark");
        
        markOpinService.deleteMarkOpinListByKey(projCd, inspNo, inspFileNo, markNo);
	}

    /**
     * 마크업메타와 의견을 조회한다. 
     */
	@Override
	public MarkUnionVo findMarkUnionByKey(String projCd, Integer inspNo, Integer inspFileNo) {
		
		MarkVo mark = findMarkByKey(projCd, inspNo, inspFileNo);
		List<MarkOpinVo> markOpinList = new ArrayList<>(); 
	
		if (null != mark) {
			markOpinList = markOpinService.findMarkOpinList(projCd, inspNo, inspFileNo, mark.getMarkNo());
		}
		
		MarkUnionVo fetchedMarkUnion = new MarkUnionVo(mark, markOpinList);
		
		return fetchedMarkUnion;
	}

    /**
     * 마크업메타를 조회한다. 
     */
	@Override
	public MarkVo findMarkByKey(String projCd, Integer inspNo, Integer inspFileNo) {
		
		String siteCd = CommonDataHolder.getCommonData().getSiteCd();
		return markMapper.findMarkByKey(siteCd, projCd, inspNo, inspFileNo);     
	}
}
