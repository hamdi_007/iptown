package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.ntc.service.NtcFileService;
import kr.xosoft.xoip.api.biz.ntc.vo.NtcFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.NtcFileWrapper;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 공지사항 파일 Controller 
 * @author hypark
 *
 */
@RestController
public class NtcFileController {

    @Autowired
    private NtcFileService ntcFileService;

    @Autowired
    private CommonFileProcessor commonFileProcessor;

    @Value("${api.file.basePath}")
    private String basePath;
    
    /**
     * 공지사항 파일을 등록한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ntc/{ntcNo}/file", method=RequestMethod.POST, headers="content-type=multipart/form-data" )
    public ResponseEntity<CommonResult> registNtcFile(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo,
            @RequestParam( value="file", required=false ) MultipartFile mFile) {
        
        Verifier.notNull(mFile, "M1004", "mFile");
        NtcFileWrapper NtcFileWrapper = new NtcFileWrapper(mFile, ntcNo, basePath);
        commonFileProcessor.uploadFile(NtcFileWrapper);

        NtcFileVo fetchedNtcFile = ntcFileService.registNtcFile((NtcFileVo)NtcFileWrapper.getFileVo());            
        return new ResponseEntity<CommonResult>(new CommonResult("ntcFile", fetchedNtcFile), HttpStatus.OK);
    }     
    
    /**
     * 공지사항 파일을 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping( value="/api/{version}/ntc/{ntcNo}/file/{ntcFileNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteNtcFile(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo,
            @PathVariable("ntcFileNo") final Integer ntcFileNo) {
        
    	ntcFileService.deleteNtcFile(ntcNo, ntcFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 공지사항 파일을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/ntc/{ntcNo}/file/{ntcFileNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findNtcFile(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo,
            @PathVariable("ntcFileNo") final Integer ntcFileNo) {
        
    	NtcFileVo fetchedNtcFile = ntcFileService.findNtcFile(ntcNo, ntcFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult("ntcFile", fetchedNtcFile), HttpStatus.OK);
    }
    
    /**
     * 공지사항 파일 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/ntc/{ntcNo}/file", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findNtcFilePage(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo,
            final CommonCondition condition) {
        
        CommonPage<NtcFileVo> fetchedNtcFilePage = ntcFileService.findNtcFilePage(ntcNo, condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("ntcFilePage", fetchedNtcFilePage), HttpStatus.OK);
    }  
    
    /**
     * 공지사항 파일 다운로드 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/ntcFile/{ntcNo}/file/{ntcFileNo}/download", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadNtcFile(
            @PathVariable("version") final Integer version,
            @PathVariable("ntcNo") final Integer ntcNo,
            @PathVariable("ntcFileNo") final Integer ntcFileNo) {
        
    	NtcFileVo fetchedNtcFile = ntcFileService.findNtcFile(ntcNo, ntcFileNo);
        return commonFileProcessor.downloadFile(new NtcFileWrapper(fetchedNtcFile, basePath));
    }        
    
}
