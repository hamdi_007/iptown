package kr.xosoft.xoip.api.biz.usr.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import kr.xosoft.xoip.api.biz.usr.vo.LgnUsrRsltVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdChngFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdJoinFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetReqFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.TokenFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrCompRsltVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface UsrService extends UserDetailsService {

    public UsrVo registLicrUsr(UsrVo usr);
    
    public UsrVo registLiceUsr(UsrVo usr);
    
    public UsrVo modifyUsrInfo(UsrVo usr);
    
    public void modifyUsrPwdWithCurrPwd(PwdChngFormVo pwdChngForm);
    
    public void modifyUsrPwdWithVrfVal(PwdRsetFormVo pwdRsetForm);
    
    public UsrVo inviteUsr(String usrId);
    
    public UsrVo joinUsr(PwdJoinFormVo pwdJoinForm);
    
//    public UsrVo withdrawUsr(String usrId);
    
    public void deleteUsr(String usrId);
    
    public UsrVo findUsr(String usrId);
    
    public UsrVo findUsrByEmail(String email);
        
    public CommonPage<UsrCompRsltVo> findUsrCompRsltPage(CommonCondition condition);
    
    public List<String> findEmailList(String hnpno, String usrNm);
    
    public List<String> findEmailListByCompId(String compId);
    
    public String findUsrIvtYn(String usrId);
    
    public LgnUsrRsltVo postLogin();
    
    public void logout();
    
    public LgnUsrRsltVo renewToken(TokenFormVo tokenForm);

    public void processPwdRsetReq(PwdRsetReqFormVo pwdRsetReqForm);
    
    public List<String> checkRequiredUsrInfo(String usrId);
    
}
