package kr.xosoft.xoip.api.biz.stmpappl.service;

import java.util.List;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpLotVo;

public interface StmpLotService {

    public List<StmpLotVo> mergeStmpLotList(String projCd, Integer stmpApplNo, List<StmpLotVo> stmpLotList);
    
    public List<StmpLotVo> findStmpLotList(String projCd, Integer stmpApplNo);
    
}
