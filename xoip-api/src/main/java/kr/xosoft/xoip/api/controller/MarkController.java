package kr.xosoft.xoip.api.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.proj.service.MarkService;
import kr.xosoft.xoip.api.biz.proj.vo.MarkUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.MarkVo;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 마크업 Controller 
 * @author likejy
 *
 */
@RestController
public class MarkController {

	@Autowired
    private MarkService markService;

    /**
     * 마크업메타와 의견을 등록한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/markup", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registMark(
    		@PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final MarkUnionVo markUnion) {
        
    	MarkUnionVo fetchedMarkUnion = markService.registMarkUnion(markUnion);
        return new ResponseEntity<CommonResult>(new CommonResult("mark", fetchedMarkUnion), HttpStatus.OK);
    }   
    
    /**
     * 마크업메타와 의견을 수정한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/markup/{markNo}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyMark(
    		@PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @PathVariable("inspFileNo") final Integer inspFileNo,
            @PathVariable("markNo") final Integer markNo,
    		@Validated(UpdatingGroup.class) @RequestBody final MarkUnionVo markUnion) {
    	
    	MarkVo mark = markUnion.getMark();
    	Verifier.isEqual(projCd, mark.getProjCd(), "M1004", "word.projCd");
        Verifier.eq(inspNo, mark.getInspNo(), "M1004", "word.inspNo");
        Verifier.eq(inspFileNo, mark.getInspFileNo(), "M1004", "word.inspFileNo");
        Verifier.eq(markNo, mark.getMarkNo(), "M1004", "word.markNo");
        
    	MarkUnionVo fetchedMarkUnion = markService.modifyMarkUnion(markUnion);
    	return new ResponseEntity<CommonResult>(new CommonResult("mark", fetchedMarkUnion), HttpStatus.OK);
    }   
    
    /**
     * 마크업메타와 의견을 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/markup/{markNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteMark(
    		@PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @PathVariable("inspFileNo") final Integer inspFileNo,
            @PathVariable("markNo") final Integer markNo) {
    	
    	markService.deleteMarkUnion(projCd, inspNo, inspFileNo, markNo);
    	return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }   
    
    /**
     * 마크업메타와 의견을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/markup", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findMark(
    		@PathVariable("version") final Integer version,
    		@PathVariable("projCd") final String projCd,
    		@PathVariable("inspNo") final Integer inspNo,
    		@PathVariable("inspFileNo") final Integer inspFileNo) {
    	
    	MarkUnionVo fetchedMarkUnion = markService.findMarkUnionByKey(projCd, inspNo, inspFileNo);
    	return new ResponseEntity<CommonResult>(new CommonResult("markUnion", fetchedMarkUnion), HttpStatus.OK);
    }
    
    /**
     * 마크업을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/markup/current", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findCurrentMark(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @PathVariable("inspFileNo") final Integer inspFileNo) {
        
        MarkVo fetchedMark = markService.findMarkByKey(projCd, inspNo, inspFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult("mark", fetchedMark), HttpStatus.OK);
    }    
    
}

