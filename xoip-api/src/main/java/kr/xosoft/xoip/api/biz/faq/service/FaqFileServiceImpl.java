package kr.xosoft.xoip.api.biz.faq.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.faq.mapper.FaqFileMapper;
import kr.xosoft.xoip.api.biz.faq.vo.FaqFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * FAQ파일 서비스 Impl
 * @author hypark
 *
 */
@Transactional
@Service
public class FaqFileServiceImpl implements FaqFileService {

    @Autowired
    private FaqFileMapper faqFileMapper;
   
    /**
     * FAQ파일을 등록한다. 
     */
	@Override
	public FaqFileVo registFaqFile(FaqFileVo faqFile) {
		
		// FAQ파일 등록 
        int cnt = faqFileMapper.insertFaqFile(faqFile);
        Verifier.eq(cnt, 1, "M1000", "word.faqFile");

        return faqFile;
	}
	
	/**
     * FAQ파일을 삭제한다. 
     */
	@Override
	public void deleteFaqFile(Integer faqNo, Integer faqFileNo) {
		
		// 계약파일 삭제 
        FaqFileVo faqFile = new FaqFileVo();
        faqFile.setFaqNo(faqNo);
        faqFile.setFaqFileNo(faqFileNo);
        int cnt = faqFileMapper.deleteFaqFileByKey(faqFile); 
        Verifier.eq(cnt, 1, "M1002", "word.faqFile");  
		
	}

	/**
     * FAQ파일을 조회한다. 
     */
	@Override
	public FaqFileVo findFaqFile(Integer faqNo, Integer faqFileNo) {

		CommonData commonData = CommonDataHolder.getCommonData();
	    return faqFileMapper.findFaqFileByKey(commonData.getSiteCd(), faqNo, faqFileNo);
	}

	/**
     * FAQ파일 목록 페이지를 조회한다. 
     */
	@Override
	public CommonPage<FaqFileVo> findFaqFilePage(Integer faqNo, CommonCondition condition) {
		
		Map<String, Object> map = condition.toMap();
        map.put("faqNo", faqNo);
        
        int comTtlCnt = faqFileMapper.countFaqFileListByMap(map);
        List<FaqFileVo> faqList = faqFileMapper.findFaqFileListByMap(map);
        
        return new CommonPage<FaqFileVo>(condition, faqList, comTtlCnt); 
	}

}
