package kr.xosoft.xoip.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.prodtyp.service.ProdTypService;
import kr.xosoft.xoip.api.biz.prodtyp.vo.ProdTypVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 상품유형 Controller 
 * @author likejy
 *
 */
@RestController
public class ProdTypController {

    @Autowired
    private ProdTypService prodTypService;
    
    /**
     * 상품유형 1단계를 등록한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/products/level1", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registProdTyp1(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final ProdTypVo prodTyp) {
        
    	ProdTypVo fetchedProdTyp = prodTypService.registProdTyp1(prodTyp);           
        return new ResponseEntity<CommonResult>(new CommonResult("prodTyp", fetchedProdTyp), HttpStatus.OK);
    }   
    
    /**
     * 상품유형 2단계를 등록한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/products/level2", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registProdTyp2(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final ProdTypVo prodTyp) {
        
    	ProdTypVo fetchedProdTyp = prodTypService.registProdTyp2(prodTyp);           
        return new ResponseEntity<CommonResult>(new CommonResult("prodTyp", fetchedProdTyp), HttpStatus.OK);
    }   
    
    /**
     * 상품유형을 수정한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/products/{prodTypId}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyProdTyp(
            @PathVariable("version") final Integer version,
            @PathVariable("prodTypId") final String prodTypId,
            @Validated(UpdatingGroup.class) @RequestBody final ProdTypVo prodTyp) {
        
        Verifier.isEqual(prodTypId, prodTyp.getProdTypId(), "M1004", "word.prodTypId");
        ProdTypVo fetchedProdTyp = prodTypService.modifyProdTyp(prodTyp);
        return new ResponseEntity<CommonResult>(new CommonResult("prodTyp", fetchedProdTyp), HttpStatus.OK);
    } 
    
    /**
     * 상품유형 1단계를 삭제한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/products/{prodTypId}/level1", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteProdTyp1(
            @PathVariable("version") final Integer version,
            @PathVariable("prodTypId") final String prodTypId) {
        
    	prodTypService.deleteProdTyp1(prodTypId);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 상품유형을 2단계를 삭제한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/products/{prodTypId}/level2", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteProdTyp2(
            @PathVariable("version") final Integer version,
            @PathVariable("prodTypId") final String prodTypId) {
        
    	prodTypService.deleteProdTyp2(prodTypId);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 상품유형을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/products/{prodTypId}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findProdTyp(
            @PathVariable("version") final Integer version,
            @PathVariable("prodTypId") final String prodTypId) {
        
    	ProdTypVo fetchedProdTyp = prodTypService.findProdTyp(prodTypId);
        return new ResponseEntity<CommonResult>(new CommonResult("prodTyp", fetchedProdTyp), HttpStatus.OK);
    }
    
    /**
     * 상품유형코드로 상품유형을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/products/code", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findProdTypByProdTypCd(
            @PathVariable("version") final Integer version,
            @RequestParam( value="prodTypCd", required=true ) final String prodTypCd) {
        
    	ProdTypVo fetchedProdTyp = prodTypService.findProdTypByProdTypCd(prodTypCd);
        return new ResponseEntity<CommonResult>(new CommonResult("prodTyp", fetchedProdTyp), HttpStatus.OK);
    }  
    
    /**
     * 상품유형 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/products/available", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findAvailableComCdList(
            @PathVariable("version") final Integer version,
            @RequestParam( value="prodTypLv", required=true ) final Integer prodTypLv,
            @RequestParam( value="topProdTypId", required=false ) final String topProdTypId) {
        
        List<ProdTypVo> prodTypList = prodTypService.findProdTypListAt(prodTypLv, topProdTypId, "Y"); 
        return new ResponseEntity<CommonResult>(new CommonResult("prodTypList", prodTypList), HttpStatus.OK);
    }
    
    /**
     * 상품유형 1단계 목록을 조회한다. (페이징 처리x)    
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/products/level1/all", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findProdTypLevel1All(
            @PathVariable("version") final Integer version) {
        
    	List<ProdTypVo> prodTypList = prodTypService.findProdTypListAt(1, null, null);
        return new ResponseEntity<CommonResult>(new CommonResult("prodTypList", prodTypList), HttpStatus.OK);
    }

    /**
     * 상품유형 1단계 목록을 조회한다. (페이징 처리o) 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/products/level1", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findProdTypPage1(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<ProdTypVo> fetchedProdTypPage = prodTypService.findProdTypPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("prodTypPage", fetchedProdTypPage), HttpStatus.OK);
    }
    
    /**
     * 상품유형 2단계 목록을 조회한다.
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/products/level2", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findProdTypPage2(
            @PathVariable("version") final Integer version,
            @RequestParam( value="prodTypLv", required=true ) final Integer prodTypLv,
            @RequestParam( value="topProdTypId", required=false ) final String topProdTypId,
            @RequestParam( value="useYn", required=false ) final String useYn) {
        
        List<ProdTypVo> prodTypList = prodTypService.findProdTypListAt(prodTypLv, topProdTypId, useYn); 
        return new ResponseEntity<CommonResult>(new CommonResult("prodTypList", prodTypList), HttpStatus.OK);
    }
   
}
