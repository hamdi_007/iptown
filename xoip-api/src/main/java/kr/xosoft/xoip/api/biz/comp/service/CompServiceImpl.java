package kr.xosoft.xoip.api.biz.comp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.cntr.service.CntrService;
import kr.xosoft.xoip.api.biz.comp.mapper.CompHistMapper;
import kr.xosoft.xoip.api.biz.comp.mapper.CompMapper;
import kr.xosoft.xoip.api.biz.comp.vo.CompHistVo;
import kr.xosoft.xoip.api.biz.comp.vo.CompVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.support.AuthSupport;

/**
 * 회사 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class CompServiceImpl implements CompService {

    @Autowired
    private CntrService cntrService;
    
    @Autowired
    private CompMapper compMapper;
    
    @Autowired
    private CompHistMapper compHistMapper;
    
    /**
     * 회사를 등록한다. 
     */
    @Override
    public CompVo registComp(CompVo comp) {
        
        // 회사코드 중복 확인 
        CompVo fetchedComp = findCompByCompCd(comp.getCompCd());
        Verifier.isNull(fetchedComp, "M3007");
        
        // 회사 등록 
        int cnt = compMapper.insertComp(comp);
        Verifier.eq(cnt, 1, "M1000", "word.comp");

        // 회사 이력 등록 
        CompHistVo compHist = new CompHistVo(comp.getCompId(), "HD0100");
        compHistMapper.insertCompHistAsSelectMaster(compHist);
        Verifier.notNull(compHist.getCompHistSeqno(), "M1000", "word.compHist");
        
        return comp;
    }

    /**
     * 회사를 수정한다. 
     */
    @Override
    public CompVo modifyComp(CompVo comp) {
        
        CommonData commonData = CommonDataHolder.getCommonData();

        // 라이선시는 본인 회사 정보만 수정 가능하다. 
        if (AuthSupport.isLicensee(commonData.getAuth())) {
            Verifier.isEqual(comp.getCompId(), commonData.getCompId(), "M3001");            
        }
        
        // 회사 조회 
        CompVo fetchedComp = findComp(comp.getCompId());
        Verifier.notNull(fetchedComp, "M1003", "word.comp");
        
        fetchedComp.setCompNm(comp.getCompNm());
        fetchedComp.setNtnCd(comp.getNtnCd());
        fetchedComp.setAddr(comp.getAddr());
        fetchedComp.setDtlAddr(comp.getDtlAddr());
        fetchedComp.setPostNo(comp.getPostNo());
        fetchedComp.setCeoNm(comp.getCeoNm());
        fetchedComp.setRepTelno(comp.getRepTelno());
        fetchedComp.setRmk(comp.getRmk());            
        
        // 회사 수정 
        int cnt = compMapper.updateCompByKey(fetchedComp);
        Verifier.eq(cnt, 1, "M1001", "word.comp");

        // 회사 이력 등록 
        CompHistVo compHist = new CompHistVo(comp.getCompId(), "HD0200");
        compHistMapper.insertCompHistAsSelectMaster(compHist);
        Verifier.notNull(compHist.getCompHistSeqno(), "M1000", "word.compHist");
        
        return comp;        
    }

    /**
     * 회사를 삭제한다. 
     */
    @Override
    public void deleteComp(String compId) {
        
        // 등록된 계약이 존재하는 경우 회사 삭제 불가 
        String existYn = cntrService.findCntrExistYnByCompId(compId);
        Verifier.isEqual(existYn, "N", "M3010");
        
        // 회사 삭제 
        CompVo comp = new CompVo();
        comp.setCompId(compId);
        int cnt = compMapper.deleteCompByKey(comp); 
        Verifier.eq(cnt, 1, "M1002", "word.comp");
        
        // 회사 이력 등록 
        CompHistVo compHist = new CompHistVo(compId, "HD0300");
        compHistMapper.insertCompHistAsSelectMaster(compHist);
        Verifier.notNull(compHist.getCompHistSeqno(), "M1000", "word.compHist");        
        
    }

    /**
     * 회사를 조회한다. 
     */
    @Override
    public CompVo findComp(String compId) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return compMapper.findCompByKey(siteCd, compId);
    }

    /**
     * 회사코드로 회사를 조회한다. 
     */
    public CompVo findCompByCompCd(String compCd) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return compMapper.findCompByCompCd(siteCd, compCd);        
    }
    
    /**
     * 사업자등록번호로 회사를 조회한다. 
     */
    @Override
    public CompVo findCompByBizno(String bizno) {
        
        // 사업자등록번호의 중복을 허용하기로 결정됨에 따라  
        // 사업자등록번호 조회 기능은 시스템에서 오류를 리턴하도록 처리 (더이상 사용하지 말것)   
        Verifier.fail("M3011");
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return compMapper.findCompByBizno(siteCd, bizno);
    }
    
    /**
     * 라이선서 회사를 조회한다. 
     */
    public CompVo findLicrComp() {

        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return compMapper.findLicrComp(siteCd);        
    }
    
    /**
     * 라이선시 회사목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<CompVo> findLiceCompPage(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        
        int comTtlCnt = compMapper.countLiceCompListByMap(map);
        List<CompVo> compList = compMapper.findLiceCompListByMap(map);
        
        return new CommonPage<CompVo>(condition, compList, comTtlCnt);           
    }

}
