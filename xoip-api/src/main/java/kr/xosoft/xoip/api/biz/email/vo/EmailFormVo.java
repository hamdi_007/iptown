package kr.xosoft.xoip.api.biz.email.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 이메일폼 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class EmailFormVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 사이트코드
     */
    private String siteCd;
    
    /**
     * 사이트명
     */
    private String siteNm;
    
    /**
     * 사이트영문명
     */
    private String siteEngNm;

    /**
     * 폼코드
     */
    private String formCd;

    /**
     * 폼번호
     */
    private Integer formNo;

    /**
     * 폼명
     */
    private String formNm;

    /**
     * 적용시작년월일
     */
    private String aplyStrYmd;

    /**
     * 적용종료년월일
     */
    private String aplyEndYmd;

    /**
     * 제목템플릿
     */
    private String sbjTmpl;

    /**
     * 내용템플릿
     */
    private String cntsTmpl;

    /**
     * 비고
     */
    private String rmk;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;

    
    
}
