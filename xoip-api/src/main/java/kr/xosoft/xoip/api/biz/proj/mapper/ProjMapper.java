package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.ProjMetaVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjRsltVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjVo;

/**
 * 프로젝트 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProjMapper {

    /**
     * 프로젝트 등록
     * @param proj
     * @return
     */
    public int insertProj(ProjVo proj);
    
    /**
     * 프로젝트 수정 by PK
     * @param proj
     * @return
     */
    public int updateProjByKey(ProjVo proj);
        
    /**
     * 프로젝트 삭제 by PK
     * @param proj
     * @return
     */
    public int deleteProjByKey(ProjVo proj);
    
    /**
     * 프로젝트 조회 by PK
     * @param siteCd
     * @param projCd
     * @return
     */
    public ProjVo findProjByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
    
    /**
     * 사용자 언어설정에 따른 프로젝트 조회 by PK
     * @param siteCd
     * @param projCd
     * @return
     */
    public ProjMetaVo findLocalizedProjMetaByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("comLang") String comLang);    
       
    /**
     * 프로젝트 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countProjRsltListByMap(Map<String, Object> map);
    
    /**
     * 사용자 언어설정에 따른 프로젝트 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<ProjRsltVo> findLocalizedProjRsltListByMap(Map<String, Object> map);        

    /**
     * 프로젝트 존재여부 조회 by 계약ID 
     * @param cntrId
     * @return
     */
    public String findProjExistYnByCntrId(@Param("siteCd") String siteCd, @Param("cntrId") String cntrId);
    
    /**
     * 프로젝트 존재여부 조회 by 상위상품유형ID 
     * @param topProdTypId
     * @return
     */
    public String findProjExistYnByTopProdTypId(@Param("siteCd") String siteCd, @Param("topProdTypId") String topProdTypId);
    
    /**
     * 프로젝트 존재여부 조회 by 상품유형ID 
     * @param prodTypId
     * @return
     */
    public String findProjExistYnByProdTypId(@Param("siteCd") String siteCd, @Param("prodTypId") String prodTypId);   
    
}
