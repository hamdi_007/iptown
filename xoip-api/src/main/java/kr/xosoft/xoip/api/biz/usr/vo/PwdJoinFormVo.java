package kr.xosoft.xoip.api.biz.usr.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 라이선시 회원가입을 위한 비밀번호 설정 폼 VO
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class PwdJoinFormVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 사용자 아이디 
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private String usrId;
    
    /**
     * 초대검증값
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private String ivtVrfVal;    
    
    /**
     * 비밀번호 
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.size", min=8, max=100, groups=UpdatingGroup.class )
    @Pattern( message="chk.pwd", regexp="(^.*(?=^.{8,15}$)(?=.*\\d)(?=.*[a-zA-Z]).*$)", groups=UpdatingGroup.class )
    @JsonProperty( access=Access.WRITE_ONLY )        
    private String pwd;
    
    /**
     * 비밀번호 확인 
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.size", min=8, max=100, groups=UpdatingGroup.class )
    @Pattern( message="chk.pwd", regexp="(^.*(?=^.{8,15}$)(?=.*\\d)(?=.*[a-zA-Z]).*$)", groups=UpdatingGroup.class )
    @JsonProperty( access=Access.WRITE_ONLY )        
    private String pwdCnfm;    
    
}
