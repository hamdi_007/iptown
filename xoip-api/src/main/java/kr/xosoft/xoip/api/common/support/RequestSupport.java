package kr.xosoft.xoip.api.common.support;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

/**
 * 요청 서포트 
 * @author likejy
 *
 */
public class RequestSupport {
    
    /**
     * 사용자 요청에서 사이트코드를 검색한다.
     * @param request
     * @return
     */
    public static String parseSiteCd(HttpServletRequest request) {
        return request.getHeader("XOIP-SiteCd");
    }

    /**
     * 사용자 요청에서 접속 IP 주소를 검색한다.
     * @param request
     * @return
     */
    public static String parseClientIpAddress(HttpServletRequest request) {
        String clientIpAddr = request.getHeader("X-Forwarded-For");
        if (clientIpAddr == null) {
            clientIpAddr = request.getHeader("Proxy-Client-IP");
        }
        if (clientIpAddr == null) {
            clientIpAddr = request.getHeader("WL-Proxy-Client-IP");
        }
        if (clientIpAddr == null) {
            clientIpAddr = request.getHeader("HTTP_CLIENT_IP");
        }
        if (clientIpAddr == null) {
            clientIpAddr = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (clientIpAddr == null) {
            clientIpAddr = request.getRemoteAddr();
        }       
        return clientIpAddr;
    }    

    /**
     * 사용자 요청에서 언어코드를 검색한다. 
     * @param request
     * @return
     */
    public static String parseAcceptLanguage(HttpServletRequest request) {
        String lang = request.getHeader("Accept-Language");
        if (StringUtils.isBlank(lang) || lang.length() < 2) {
            return "en";
        } else {
            lang = lang.substring(0, 2).toLowerCase();
            if (lang.equals("ko") || lang.equals("en")) {
                return lang;
            } else {
                return "en";
            }
        }
    }
    
}
