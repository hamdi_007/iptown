package kr.xosoft.xoip.api.biz.stmpappl.service;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface StmpApplFileService {
	
	public StmpApplFileVo registStmpApplFile(StmpApplFileVo stmpApplFile);
	
	public StmpApplFileVo modifyStmpApplFile(StmpApplFileVo stmpAppFile);
	
	public void deleteStmpApplFile(String projCd, Integer stmpApplNo, Integer stmpFilelNo);
	
	public StmpApplFileVo findStmpApplFile(String projCd, Integer stmpApplNo, Integer stmpFileNo);
    
    public CommonPage<StmpApplFileVo> findStmpApplFilePage(String projCd, Integer stmpApplNo, CommonCondition condition);
}
