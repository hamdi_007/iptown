package kr.xosoft.xoip.api.biz.cntr.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 계약이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class CntrHistVo extends CntrVo {

    private static final long serialVersionUID = 1L;

    /**
     * 계약이력일련번호 
     */
    private Integer cntrHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public CntrHistVo(String cntrId, String histDivCd) {
        setCntrId(cntrId);
        setHistDivCd(histDivCd);
    }        
    
}
