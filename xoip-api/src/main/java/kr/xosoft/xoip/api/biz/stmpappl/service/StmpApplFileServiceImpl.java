package kr.xosoft.xoip.api.biz.stmpappl.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import kr.xosoft.xoip.api.biz.stmpappl.mapper.StmpApplFileMapper;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;


/**
 * 증지신청파일 서비스 Impl
 * @author luna1819
 *
 */
@Transactional
@Service
public class StmpApplFileServiceImpl implements StmpApplFileService{
	
	@Autowired
	private StmpApplFileMapper stmpApplFileMapper;
	
	/**
     * 증지신청파일을 등록한다. 
     */
	@Override
	public StmpApplFileVo registStmpApplFile(StmpApplFileVo stmpApplFile) {
		
		// 증지신청파일 등록
		int cnt = stmpApplFileMapper.insertStmpApplFile(stmpApplFile);
		Verifier.eq(cnt, 1, "M1000", "word.stmpApplFile");
		
		return stmpApplFile;
	}
	
	/**
     * 증지신청파일을 수정한다. 
     */
	
	@Override
	public StmpApplFileVo modifyStmpApplFile(StmpApplFileVo stmpApplFile) {
		
		// 증지신청파일 조회
		StmpApplFileVo fetchedStmpApplFile = findStmpApplFile(stmpApplFile.getProjCd(), stmpApplFile.getStmpApplNo(), stmpApplFile.getStmpFileNo());
		Verifier.notNull(fetchedStmpApplFile, "M1003", "word.stmpApplFile");
		
		fetchedStmpApplFile.setMngFileNm(stmpApplFile.getMngFileNm());
		
		// 증시신청 수정
		int cnt = stmpApplFileMapper.updateStmpApplFileByKey(fetchedStmpApplFile);
		Verifier.eq(cnt, 1, "M1001", "word.stmpApplFile");
		
		return fetchedStmpApplFile;
	}
	
	/**
     * 증지신청파일을 삭제한다. 
     */
	
	@Override
	public void deleteStmpApplFile(String projCd, Integer stmpApplNo, Integer stmpFilelNo) {
		// 계약파일 삭제 
		StmpApplFileVo stmpApplFile = new StmpApplFileVo();
        stmpApplFile.setProjCd(projCd);
        stmpApplFile.setStmpApplNo(stmpApplNo);
        stmpApplFile.setStmpFileNo(stmpFilelNo);
        int cnt = stmpApplFileMapper.deleteStmpApplFileByKey(stmpApplFile);
        Verifier.eq(cnt, 1, "M1002", "word.stmpApplFile");
	
	}
	
	
	/**
     * 증지파일을 조회한다. 
     */
	@Override
	public StmpApplFileVo findStmpApplFile(String projCd, Integer stmpApplNo, Integer stmpFileNo) {
		
		CommonData commonData = CommonDataHolder.getCommonData();
		return stmpApplFileMapper.findStmpApplFileByKey(commonData.getSiteCd(), projCd, stmpApplNo, stmpFileNo);
	}
	
	/**
     * 증지파일 목록 페이지를 조회한다. 
     */
	@Override
	public CommonPage<StmpApplFileVo> findStmpApplFilePage(String projCd, Integer stmpApplNo,CommonCondition condition) {
		Map<String, Object> map = condition.toMap();
		map.put("projCd", projCd);
		map.put("stmpApplNo", stmpApplNo);
		
		int comTtlcnt = stmpApplFileMapper.countStmpApplFileListByMap(map);
		List<StmpApplFileVo> stmpApplList = stmpApplFileMapper.findStmpApplFileListByMap(map);
		
		return new CommonPage<>(condition, stmpApplList, comTtlcnt);
	}

}
