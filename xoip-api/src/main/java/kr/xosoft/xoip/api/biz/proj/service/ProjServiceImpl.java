package kr.xosoft.xoip.api.biz.proj.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.support.BizSupport;
import kr.xosoft.xoip.api.biz.cntr.service.CntrService;
import kr.xosoft.xoip.api.biz.cntr.vo.CntrVo;
import kr.xosoft.xoip.api.biz.comp.service.CompService;
import kr.xosoft.xoip.api.biz.email.service.EmailService;
import kr.xosoft.xoip.api.biz.email.vo.EmailReqFormVo;
import kr.xosoft.xoip.api.biz.prodtyp.service.ProdTypService;
import kr.xosoft.xoip.api.biz.proj.mapper.ProdSaleChnlMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.ProdSaleNtnMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.ProdSprtLangMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.ProdTgtAgeMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.ProdTgtSexMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.ProjChgHistMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.ProjHistMapper;
import kr.xosoft.xoip.api.biz.proj.mapper.ProjMapper;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspFileUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspVo;
import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdSaleChnlVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdSaleNtnVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdSprtLangVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdTgtAgeVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdTgtSexVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjChgHistVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjDsgnInspUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjHistVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjMetaVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjNmChgFormVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjRsltVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjStsChgFormVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjVo;
import kr.xosoft.xoip.api.biz.usr.service.UsrService;
import kr.xosoft.xoip.api.biz.usr.vo.UsrVo;

/**
 * 프로젝트 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class ProjServiceImpl implements ProjService {

	@Value("${api.xoip.webUrl}")
    private String webUrl;
	
    @Autowired
    private CntrService cntrService;
    
    @Autowired
    private ProdTypService prodTypService;
    
    @Autowired
    private DsgnInspService dsgnInspService;
    
    @Autowired
    private InspFileService inspFileService;
    
    @Autowired
    private CompService compService;
    
    @Autowired
    private EmailService emailService;
    
    @Autowired
    private UsrService usrService;
    
    @Autowired
    private ProjMapper projMapper;
    
    @Autowired
    private ProjChgHistMapper projChgHistMapper;
    
    @Autowired
    private ProjHistMapper projHistMapper;
    
    @Autowired
    private ProdSaleNtnMapper prodSaleNtnMapper;
    
    @Autowired
    private ProdSaleChnlMapper prodSaleChnlMapper;
    
    @Autowired
    private ProdSprtLangMapper prodSprtLangMapper;
    
    @Autowired
    private ProdTgtAgeMapper prodTgtAgeMapper;
    
    @Autowired
    private ProdTgtSexMapper prodTgtSexMapper;
    
    /**
     * 상품판매국가, 상품판매채널, 상품지원언어, 상품대상연령 목록을 등록한다. 
     */
    private void registAllRelatedMetaInfo(ProjVo proj) {
        
        List<String> prodSaleNtnCdList = proj.getProdSaleNtnCdList();
        List<String> prodSaleChnlCdList = proj.getProdSaleChnlCdList();
        List<String> prodSprtLangCdList = proj.getProdSprtLangCdList();
        List<String> prodTgtAgeCdList = proj.getProdTgtAgeCdList();
        List<String> prodTgtSexCdList = proj.getProdTgtSexCdList();
        
        // 상품판매국가정보 등록 - 삭제후 재등록 
        prodSaleNtnMapper.deleteProdSaleNtnByProjCd(proj.getSiteCd(), proj.getProjCd());
        if (prodSaleNtnCdList != null) {
            for (String code : prodSaleNtnCdList) {
                int cnt = prodSaleNtnMapper.insertProdSaleNtn(new ProdSaleNtnVo(proj.getProjCd(), code));
                Verifier.eq(cnt, 1, "M1000", "word.prodSaleNtn");
            }
        }
        
        // 상품판매채널정보 등록 - 삭제후 재등록 
        prodSaleChnlMapper.deleteProdSaleChnlByProjCd(proj.getSiteCd(), proj.getProjCd());
        if (prodSaleChnlCdList != null) {
            for (String code : prodSaleChnlCdList) {
                int cnt = prodSaleChnlMapper.insertProdSaleChnl(new ProdSaleChnlVo(proj.getProjCd(), code));
                Verifier.eq(cnt, 1, "M1000", "word.prodSaleChnl");
            }            
        }
        
        // 상품지원언어정보 등록 - 삭제후 재등록 
        prodSprtLangMapper.deleteProdSprtLangByProjCd(proj.getSiteCd(), proj.getProjCd());
        if (prodSprtLangCdList != null) {
            for (String code : prodSprtLangCdList) {
                int cnt = prodSprtLangMapper.insertProdSprtLang(new ProdSprtLangVo(proj.getProjCd(), code));
                Verifier.eq(cnt, 1, "M1000", "word.prodSprtLang");
            }            
        }
        
        // 상품대상연령정보 등록 - 삭제후 재등록 
        prodTgtAgeMapper.deleteProdTgtAgeByProjCd(proj.getSiteCd(), proj.getProjCd());
        if (prodTgtAgeCdList != null) {
            for (String code : prodTgtAgeCdList) {
                int cnt = prodTgtAgeMapper.insertProdTgtAge(new ProdTgtAgeVo(proj.getProjCd(), code));
                Verifier.eq(cnt, 1, "M1000", "word.prodTgtAge");
            }            
        }
        
        // 상품대상성별정보 등록 - 삭제후 재등록 
        prodTgtSexMapper.deleteProdTgtSexByProjCd(proj.getSiteCd(), proj.getProjCd());
        if (prodTgtSexCdList != null) {
            for (String code : prodTgtSexCdList) {
                int cnt = prodTgtSexMapper.insertProdTgtSex(new ProdTgtSexVo(proj.getProjCd(), code));
                Verifier.eq(cnt, 1, "M1000", "word.prodTgtSex");
            }            
        }        
    }

    /**
     * 프로젝트변경이력을 등록한다. 
     * @param projCd
     * @param projHistDivCd
     * @param befVal
     * @param aftVal
     * @return
     */
    private ProjChgHistVo registProjChgHist(String projCd, String projHistDivCd, String befVal, String aftVal, String histRsnCnts) {
        
        // 프로젝트 변경이력 등록  
        ProjChgHistVo projChgHist = new ProjChgHistVo();
        projChgHist.setProjCd(projCd);
        projChgHist.setProjHistDivCd(projHistDivCd);
        projChgHist.setBefVal(befVal);
        projChgHist.setAftVal(aftVal);
        projChgHist.setHistRsnCnts(histRsnCnts);
        
        int cnt = projChgHistMapper.insertProjChgHist(projChgHist);
        Verifier.eq(cnt, 1, "M1001", "word.projChgHist");
        
        return projChgHist;
    }
    
    /**
     * 프로젝트를 등록한다. 
     */
    @Override
    public ProjDsgnInspUnionVo registProj(ProjVo proj) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 유효한 계약 여부 확인 
        CntrVo cntr = cntrService.findValidCntr(proj.getCntrId());
        
        // 상품유형 사용가능여부 확인
        prodTypService.findValidProdTyp(proj.getProdTypId());
        
        // 프로젝트 등록
        proj.setCompId(cntr.getCompId()); 
        proj.setCompCd(cntr.getCompCd());
        proj.setIpId(cntr.getIpId());
        proj.setIpCd(cntr.getIpCd());
        proj.setProjStsCd("PS0100"); // 작성중 
        proj.setProjCrtDt(new Date());
        proj.setProjCrtrId(commonData.getUsrId());
        int cnt = projMapper.insertProj(proj);
        Verifier.eq(cnt, 1, "M1000", "word.proj");

        // 프로젝트 이력 등록 
        ProjHistVo projHist = new ProjHistVo(proj.getProjCd(), "HD0100");
        projHistMapper.insertProjHistAsSelectMaster(projHist);
        Verifier.notNull(projHist.getProjHistSeqno(), "M1000", "word.projHist");

        // 프로젝트 변경이력 등록
        registProjChgHist(proj.getProjCd(), "PH0100", null, proj.getProjStsCd(), null); // 프로젝트 생성 
        
        // 상품판매국가, 상품판매채널, 상품지원언어, 상품대상연령 등록 
        registAllRelatedMetaInfo(proj);
        
        // 디자인검수 등록 
        DsgnInspVo dsgnInsp = new DsgnInspVo();
        dsgnInsp.setProjCd(proj.getProjCd());
        dsgnInsp.setInspStgCd(proj.getProjStgCd());
        dsgnInsp.setInspStsCd("IS0100"); // 작성중 
        DsgnInspFileUnionVo dsgnUnion = dsgnInspService.registDsgnInspFileUnion(new DsgnInspFileUnionVo(dsgnInsp, null));
        
        // 이메일 전송요청 
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(cntr.getCompId()).getNtnCd());        
        String siteWebUrl = BizSupport.parseSiteWebUrl(commonData.getSiteCd(), webUrl);
        UsrVo usr = usrService.findUsr(commonData.getUsrId());
        List<String> recpList = new ArrayList<String>();
        recpList.add(commonData.getEmail());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("PROJECT_NAME", proj.getProjNm());
        varMap.put("PROJECT_CODE", proj.getProjCd());
        varMap.put("USER_NAME", usr.getUsrNm());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", usr.getCompNm());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(proj.getProjCrtDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0201");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);
        
        return new ProjDsgnInspUnionVo(proj, dsgnUnion.getDsgnInsp(), dsgnUnion.getInspFileList());
    }
    
    /**
     * 프로젝트를 변경한다. 
     */
    private ProjVo modifyProjWithoutVerification(ProjVo proj) {

        // 프로젝트 수정 
        int cnt = projMapper.updateProjByKey(proj);
        Verifier.eq(cnt, 1, "M1001", "word.proj");

        // 프로젝트 이력 등록 
        ProjHistVo projHist = new ProjHistVo(proj.getProjCd(), "HD0200");
        projHistMapper.insertProjHistAsSelectMaster(projHist);
        Verifier.notNull(projHist.getProjHistSeqno(), "M1000", "word.projHist");
        
        return proj;            
    }
    
    /**
     * 프로젝트를 임시저장한다. 
     */
    @Override
    public ProjDsgnInspUnionVo modifyProjDsgnInspUnionInDraft(ProjDsgnInspUnionVo projUnion) {
        
        ProjVo proj = projUnion.getProj();
        DsgnInspVo dsgnInsp = projUnion.getDsgnInsp();
        
        // 유효한 계약 여부 확인 
        cntrService.findValidCntr(proj.getCntrId());
        
        // 상품유형 사용가능여부 확인
        prodTypService.findValidProdTyp(proj.getProdTypId());
        
        // 프로젝트 조회 
        ProjVo fetchedProj = findProj(proj.getProjCd());
        Verifier.notNull(fetchedProj, "M1003", "word.proj");
        
        // 저장된 프로젝트 상태가 작성중이 아닌 경우에는 임시저장 불가 
        Verifier.isEqual(fetchedProj.getProjStsCd(), "PS0100", "M7024");
        
        fetchedProj.setProjNm(proj.getProjNm());
        fetchedProj.setProjStgCd(proj.getProjStgCd());
        fetchedProj.setProdNo(proj.getProdNo());
        fetchedProj.setTopProdTypId(proj.getTopProdTypId());
        fetchedProj.setProdTypId(proj.getProdTypId());
        fetchedProj.setRlsPlanYm(proj.getRlsPlanYm());
        fetchedProj.setEstmRlsAmt(proj.getEstmRlsAmt());
        fetchedProj.setEstmCnsmAmt(proj.getEstmCnsmAmt());
        fetchedProj.setRlsAmtRt(proj.getRlsAmtRt());
        fetchedProj.setCcCd(proj.getCcCd());
        
        // 비고 조회 권한이 없는 경우 비고 값이 null 값으로 전달됨         
        if (proj.getRmk() != null) {
            fetchedProj.setRmk(proj.getRmk());            
        }
        
        // 프로젝트 수정 
        modifyProjWithoutVerification(fetchedProj);

        // 상품판매국가, 상품판매채널, 상품지원언어, 상품대상연령 등록 
        registAllRelatedMetaInfo(proj);
        
        // 디자인검수 수정
        // 프로젝트 승인 이전까지는 프로젝트 단계가 변경 가능함
        dsgnInsp.setInspStgCd(proj.getProjStgCd());
        dsgnInspService.modifyDsgnInspFileUnionInDraft(new DsgnInspFileUnionVo(dsgnInsp, projUnion.getInspFileList()));
        
        return projUnion;        
    }
    
    /**
     * 프로젝트를 승인요청한다. 
     */
    public ProjDsgnInspUnionVo requestProjForApproval(ProjDsgnInspUnionVo projUnion) {

        CommonData commonData = CommonDataHolder.getCommonData();
        ProjVo proj = projUnion.getProj();
        DsgnInspVo dsgnInsp = projUnion.getDsgnInsp();
        
        // 유효한 계약 여부 확인 
        CntrVo cntr = cntrService.findValidCntr(proj.getCntrId());
        
        // 상품유형 사용가능여부 확인
        prodTypService.findValidProdTyp(proj.getProdTypId());
        
        // 프로젝트 조회 
        ProjVo fetchedProj = findProj(proj.getProjCd());
        Verifier.notNull(fetchedProj, "M1003", "word.proj");
        
        // 작성중 프로젝트만 승인 요청이 가능 
        String befProjStsCd = fetchedProj.getProjStsCd();
        Verifier.isEqual(befProjStsCd, "PS0100", "M7004");
        
        fetchedProj.setProjNm(proj.getProjNm());
        fetchedProj.setProjStgCd(proj.getProjStgCd());        
        fetchedProj.setProdNo(proj.getProdNo());
        fetchedProj.setTopProdTypId(proj.getTopProdTypId());
        fetchedProj.setProdTypId(proj.getProdTypId());        
        fetchedProj.setRlsPlanYm(proj.getRlsPlanYm());
        fetchedProj.setEstmRlsAmt(proj.getEstmRlsAmt());
        fetchedProj.setEstmCnsmAmt(proj.getEstmCnsmAmt());
        fetchedProj.setRlsAmtRt(proj.getRlsAmtRt());
        fetchedProj.setCcCd(proj.getCcCd());
        fetchedProj.setProjApvReqDt(new Date());
        fetchedProj.setProjApvReqrId(commonData.getUsrId());
        fetchedProj.setProjStsCd("PS0200"); // 신규  
        
        // 비고 조회 권한이 없는 경우 비고 값이 null 값으로 전달됨         
        if (proj.getRmk() != null) {
            fetchedProj.setRmk(proj.getRmk());            
        }
        
        // 프로젝트 수정 
        modifyProjWithoutVerification(fetchedProj);
        
        // 프로젝트 변경이력 등록
        registProjChgHist(fetchedProj.getProjCd(), "PH0200", befProjStsCd, fetchedProj.getProjStsCd(), null); // 프로젝트 등록
        
        // 상품판매국가, 상품판매채널, 상품지원언어, 상품대상연령 등록 
        registAllRelatedMetaInfo(proj);
        
        // 디자인검수 등록 
        // 프로젝트 승인 이전까지는 프로젝트 단계가 변경 가능함 
        dsgnInsp.setInspStgCd(proj.getProjStgCd());        
        dsgnInspService.applyDsgnInspFileUnion(new DsgnInspFileUnionVo(dsgnInsp, projUnion.getInspFileList()));
        
        // 이메일 전송요청 
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(cntr.getCompId()).getNtnCd());
        String siteWebUrl = BizSupport.parseSiteWebUrl(commonData.getSiteCd(), webUrl);
        UsrVo usr = usrService.findUsr(commonData.getUsrId());
        List<String> recpList = new ArrayList<String>();
        recpList.add(commonData.getEmail());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("PROJECT_NAME", proj.getProjNm());
        varMap.put("PROJECT_CODE", proj.getProjCd());
        varMap.put("USER_NAME", usr.getUsrNm());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", usr.getCompNm());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(fetchedProj.getProjApvReqDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0210");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);
        
        return projUnion;        
    }
    
    /**
     * 프로젝트 값 변경과 함께 프로젝트 변경이력을 등록한다. 
     * 아래의 경우에 호출되어 사용된다.
     * - 라이선서의 프로젝트 승인
     * - 라이선서/라이선시의 프로젝트 취소
     * - 라이선서의 프로젝트 완료
     * - 라이선서의 프로젝트 거절
     * - 라이선서의 프로제트 수정 
     * - 라이선시의 디자인 검수 등록시 프로젝트 단계 및 상태 변경 
     */
    private ProjVo modifyProjStatus(ProjVo proj, String projHistDivCd, String befProjStsCd, String histRsnCnts) {
        
        // 유효한 계약 여부 확인 : 프로젝트취소 (PS0700), 프로젝트반려 (PS0600) 의 경우 유효한 계약여부 확인 처리를 하지 않는다. 
        if (!"PS0700".equals(proj.getProjStsCd()) && !"PS0600".equals(proj.getProjStsCd())) {
            cntrService.findValidCntr(proj.getCntrId());            
        }
        
        // 프로젝트 수정 
        modifyProjWithoutVerification(proj);

        // 프로젝트 변경이력 등록
        registProjChgHist(proj.getProjCd(), projHistDivCd, befProjStsCd, proj.getProjStsCd(), histRsnCnts);
        
        return proj;
    }    
    
    /**
     * 프로젝트를 승인한다. 
     */
    public ProjVo approveProj(String projCd) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projCd);
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 신규 상태의 프로젝트만 승인 처리가 가능함 
        String befProjStsCd = proj.getProjStsCd();
        Verifier.isEqual(befProjStsCd, "PS0200", "M7000");
        
        // 상품유형 사용가능여부 확인
        prodTypService.findValidProdTyp(proj.getProdTypId());        
        
        // 프로젝트 상태 변경 
        proj.setProjApvDt(new Date());
        proj.setProjApvrId(commonData.getUsrId());
        proj.setProjStsCd("PS0300"); // 진행 
        
        ProjVo rtnProj = modifyProjStatus(proj, "PH0300", befProjStsCd, null); // 프로젝트 변경 (진행) 
        
        // 이메일 전송요청 
        CntrVo cntr = cntrService.findLocalizedCntr(proj.getCntrId());
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(cntr.getCompId()).getNtnCd());
        String siteWebUrl = BizSupport.parseSiteWebUrl(commonData.getSiteCd(), webUrl);
        List<String> recpList = usrService.findEmailListByCompId(cntr.getCompId());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("PROJECT_NAME", proj.getProjNm());
        varMap.put("PROJECT_CODE", proj.getProjCd());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", cntr.getCompNm());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(proj.getProjApvDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0220");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);
        
        return rtnProj;
    }
    
    /**
     * 프로젝트를 반려한다. 
     */
    public ProjVo rejectProj(ProjStsChgFormVo projStsChgForm) {
        
        // 프로젝트 반려상태는 CJ E&M 요청으로 제거 
        Verifier.fail("M1004", "word.reject");        
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projStsChgForm.getProjCd());
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 신규 상태의 프로젝트만 반려 처리가 가능함 
        String befProjStsCd = proj.getProjStsCd();
        Verifier.isEqual(befProjStsCd, "PS0200", "M7001");
        
        // 프로젝트 상태 변경 
        proj.setProjRejtDt(new Date());
        proj.setProjRejtrId(commonData.getUsrId());
        proj.setProjStsCd("PS0600"); // 반려  
        return modifyProjStatus(proj, "PH0600", befProjStsCd, projStsChgForm.getStsRsnCnts()); // 프로젝트 반려          
    }
    
    /**
     * 프로젝트를 취소한다. 
     */
    public ProjVo cancelProj(ProjStsChgFormVo projStsChgForm) {    

        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projStsChgForm.getProjCd());
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 신규, 진행, 수정 프로젝트만 취소가 가능함 
        String befProjStsCd = proj.getProjStsCd();
        if (!"PS0200".equals(befProjStsCd) && !"PS0300".equals(befProjStsCd) && !"PS0400".equals(befProjStsCd)) {
            Verifier.fail("M7002");
        }
        
        // 프로젝트 상태 변경 
        proj.setProjCnclDt(new Date());
        proj.setProjCnclrId(commonData.getUsrId());
        proj.setProjStsCd("PS0700"); // 취소 
        
        ProjVo rtnProj = modifyProjStatus(proj, "PH0700", befProjStsCd, projStsChgForm.getStsRsnCnts()); // 프로젝트 취소 
        
        // 이메일 전송요청 
        CntrVo cntr = cntrService.findLocalizedCntr(proj.getCntrId());
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(cntr.getCompId()).getNtnCd());
        String siteWebUrl = BizSupport.parseSiteWebUrl(commonData.getSiteCd(), webUrl);
        List<String> recpList = usrService.findEmailListByCompId(cntr.getCompId());
     
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("PROJECT_NAME", proj.getProjNm());
        varMap.put("PROJECT_CODE", proj.getProjCd());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", cntr.getCompNm());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(proj.getProjCnclDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0250");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);
        
        return rtnProj;       
    }
    
    /**
     * 프로젝트를 수정상태로 변경한다. 
     */
    public ProjVo returnProj(String projCd, Date inspDt) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projCd);
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 진행 프로젝트만 수정 변경이 가능함 
        String befProjStsCd = proj.getProjStsCd();
        if (!"PS0300".equals(befProjStsCd)) {
            Verifier.fail("M7006");
        }
        // 프로젝트 상태 변경 
        proj.setProjStsCd("PS0400"); // 수정
        
        ProjVo rtnProj = modifyProjStatus(proj, "PH0400", befProjStsCd, null); // 프로젝트 변경 (수정) 
        
        // 이메일 전송요청 
        CntrVo cntr = cntrService.findLocalizedCntr(proj.getCntrId());
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(cntr.getCompId()).getNtnCd());
        String siteWebUrl = BizSupport.parseSiteWebUrl(commonData.getSiteCd(), webUrl);
        List<String> recpList = usrService.findEmailListByCompId(cntr.getCompId());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("PROJECT_NAME", proj.getProjNm());
        varMap.put("PROJECT_CODE", proj.getProjCd());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", cntr.getCompNm());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(inspDt, "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0240");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);
        
        return rtnProj;                               
    }
    
    /**
     * 프로젝트를 완료한다. 
     */
    public ProjVo completeProj(String projCd) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projCd);
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 진행 프로젝트만 완료가 가능함 
        String befProjStsCd = proj.getProjStsCd();
        if (!"PS0300".equals(befProjStsCd)) {
            Verifier.fail("M7005");
        }
        
        // 프로젝트 상태 변경 
        proj.setProjCmplDt(new Date());
        proj.setProjCmplrId(commonData.getUsrId());
        proj.setProjStsCd("PS0500"); // 완료 
        
        ProjVo rtnProj = modifyProjStatus(proj, "PH0500", befProjStsCd, null); // 프로젝트 완료   
        
        // 이메일 전송요청 
        CntrVo cntr = cntrService.findLocalizedCntr(proj.getCntrId());
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(cntr.getCompId()).getNtnCd());
        String siteWebUrl = BizSupport.parseSiteWebUrl(commonData.getSiteCd(), webUrl);
        List<String> recpList = usrService.findEmailListByCompId(cntr.getCompId());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("PROJECT_NAME", proj.getProjNm());
        varMap.put("PROJECT_CODE", proj.getProjCd());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", cntr.getCompNm());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(proj.getProjCmplDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0260");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);
        
        return rtnProj;                     
    }

    /**
     * 라이선시가 디자인 검수를 신청할 때 프로젝트단계 및 상태를 변경한다.  
     */
    @Override
    public ProjVo changeProjForNextStage(String projCd, String tobeProjStgCd) {
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projCd);
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 수정 프로젝트만 프로젝트단계 및 상태 변경이 가능함  
        String befProjStsCd = proj.getProjStsCd();
        if (!"PS0400".equals(befProjStsCd)) {
            Verifier.fail("M7008");
        } 
        
        // 프로젝트 단계 및 상태 변경 
        proj.setProjStgCd(tobeProjStgCd);
        proj.setProjStsCd("PS0300"); // 진행  
        return modifyProjStatus(proj, "PH0300", befProjStsCd, null); // 프로젝트 변경 (진행)          
        
    }
    
    /**
     * 프로젝트명을 변경한다. 
     */
    @Override
    public ProjVo renameProj(ProjNmChgFormVo projNmChgForm) {
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projNmChgForm.getProjCd());
        Verifier.notNull(proj, "M1003", "word.proj");
                
        // 프로젝트 이름 변경
        String befProjNm = proj.getProjNm();
        proj.setProjNm(projNmChgForm.getProjNm());
        
        // 프로젝트 수정 
        modifyProjWithoutVerification(proj);

        // 프로젝트 변경이력 등록
        registProjChgHist(proj.getProjCd(), "PH0800", befProjNm, projNmChgForm.getProjNm(), projNmChgForm.getHistRsnCnts()); // 프로젝트 이름 변경 
        
        return proj;        
        
    }    
    
    /**
     * 프로젝트를  삭제한다. 
     */
    @Override
    public void deleteProj(String projCd) {

        // 프로젝트 조회 
        ProjVo proj = findProj(projCd);
        Verifier.notNull(proj, "M1003", "word.proj");
        
        // 작성중 프로젝트만 삭제 가능함 
        Verifier.isEqual(proj.getProjStsCd(), "PS0100", "M7003");
        
        // 프로젝트 삭제 
        int cnt = projMapper.deleteProjByKey(proj); 
        Verifier.eq(cnt, 1, "M1002", "word.proj");
        
        // 프로젝트 이력 등록 
        ProjHistVo projHist = new ProjHistVo(projCd, "HD0300");
        projHistMapper.insertProjHistAsSelectMaster(projHist);
        Verifier.notNull(projHist.getProjHistSeqno(), "M1000", "word.projHist");        
    }

    /**
     * 프로젝트를 조회한다. 
     */
    @Override
    public ProjVo findProj(String projId) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return projMapper.findProjByKey(siteCd, projId);
    }
    
    /**
     * 메타 목록 정보를 포함한 프로젝트를 조회한다. 
     */
    public ProjMetaVo findLocalizedProjMeta(String projCd) {
        
        // 프로젝트 조회 
        CommonData commonData = CommonDataHolder.getCommonData();
        ProjMetaVo proj = projMapper.findLocalizedProjMetaByKey(commonData.getSiteCd(), projCd, commonData.getLang());
        
        // 판매국가, 판매채널, 지원언어, 대상연령, 대상성별 코드 목록 조회 및 할당 
        proj.setProdSaleNtnComCdList(prodSaleNtnMapper.findLocalizedComCdListForNtnCdByProjCd(proj.getSiteCd(), projCd, commonData.getLang()));
        proj.setProdSaleChnlComCdList(prodSaleChnlMapper.findLocalizedComCdListForSaleChnlCdByProjCd(proj.getSiteCd(), projCd, commonData.getLang()));
        proj.setProdSprtLangComCdList(prodSprtLangMapper.findLocalizedComCdListForLangCdByProjCd(proj.getSiteCd(), projCd, commonData.getLang()));
        proj.setProdTgtAgeComCdList(prodTgtAgeMapper.findLocalizedComCdListForAgeCdByProjCd(proj.getSiteCd(), projCd, commonData.getLang()));
        proj.setProdTgtSexComCdList(prodTgtSexMapper.findLocalizedComCdListForSexCdByProjCd(proj.getSiteCd(), projCd, commonData.getLang()));
        
        return proj;
    }
    
    /**
     * 프로젝트 + 최초 작성 디자인검수를 조회한다. 
     */
    @Override
    public ProjDsgnInspUnionVo findProjDsgnInspUnionInDraft(String projCd) {
        
        // 프로젝트 조회 
        ProjVo proj = findProj(projCd);
        
        // 판매국가, 판매채널, 지원언어, 대상연령, 대상성별 코드 목록 조회 및 할당 
        proj.setProdSaleNtnCdList(prodSaleNtnMapper.findNtnCdListByProjCd(proj.getSiteCd(), projCd));
        proj.setProdSaleChnlCdList(prodSaleChnlMapper.findSaleChnlCdListByProjCd(proj.getSiteCd(), projCd));
        proj.setProdSprtLangCdList(prodSprtLangMapper.findLangCdListByProjCd(proj.getSiteCd(), projCd));
        proj.setProdTgtAgeCdList(prodTgtAgeMapper.findAgeCdListByProjCd(proj.getSiteCd(), projCd));
        proj.setProdTgtSexCdList(prodTgtSexMapper.findSexCdListByProjCd(proj.getSiteCd(), projCd));
        
        // 디자인검수 조회 
        DsgnInspVo dsgnInsp = dsgnInspService.findLocalizedDsgnInsp(projCd, 1);
        
        // 검수파일 조회
        List<InspFileVo> inspFileList = inspFileService.findInspFileList(projCd, dsgnInsp.getInspNo());
        
        return new ProjDsgnInspUnionVo(proj, dsgnInsp, inspFileList);
    }
    
    /**
     * 프로젝트목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<ProjRsltVo> findLocalizedProjRsltPage(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        
        int comTtlCnt = projMapper.countProjRsltListByMap(map);
        List<ProjRsltVo> projList = projMapper.findLocalizedProjRsltListByMap(map);
        
        return new CommonPage<ProjRsltVo>(condition, projList, comTtlCnt);           
    }
    
    /**
     * 증지신청이 가능한 프로젝트 목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<ProjRsltVo> findLocalizedStmpApplAvlProjRsltPage(String compId, CommonCondition condition) {
        
        Map<String, Object> map = condition.toMap();
        map.put("compId", compId);
        
        int comTtlCnt = projMapper.countProjRsltListByMap(map);
        List<ProjRsltVo> projList = projMapper.findLocalizedProjRsltListByMap(map);
        
        return new CommonPage<ProjRsltVo>(condition, projList, comTtlCnt);                   
    }

    /**
     * 프로젝트변경이력 페이지를 조회한다. 
     */
    @Override
    public CommonPage<ProjChgHistVo> findLocalizedProjChgHistPage(String projCd, CommonCondition condition) {
        
        Map<String, Object> map = condition.toMap();
        map.put("projCd", projCd);
        
        int comTtlCnt = projChgHistMapper.countProjChgHistListByMap(map);
        List<ProjChgHistVo> projList = projChgHistMapper.findLocalizedProjChgHistListByMap(map);
        
        return new CommonPage<ProjChgHistVo>(condition, projList, comTtlCnt);                   
    }
    
    /**
     * 계약ID로 프로젝트 존재여부를 조회한다. 
     */
    @Override
    public String findProjExistYnByCntrId(String cntrId) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return projMapper.findProjExistYnByCntrId(commonData.getSiteCd(), cntrId);
    }
    
    /**
     * 상위상품유형ID로 프로젝트 존재여부를 조회한다. 
     */
    public String findProjExistYnByTopProdTypId(String topProdTypId) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return projMapper.findProjExistYnByTopProdTypId(commonData.getSiteCd(), topProdTypId);
    }
    
    /**
     * 상품유형ID로 프로젝트 존재여부를 조회한다. 
     */
    public String findProjExistYnByProdTypId(String prodTypId) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return projMapper.findProjExistYnByProdTypId(commonData.getSiteCd(), prodTypId);
    }
    
}
