package kr.xosoft.xoip.api.biz.comp.service;

import kr.xosoft.xoip.api.biz.comp.vo.CompVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface CompService {

    public CompVo registComp(CompVo comp);
        
    public CompVo modifyComp(CompVo comp);
    
    public void deleteComp(String compId);
    
    public CompVo findComp(String compId);
    
    public CompVo findCompByCompCd(String compCd);
    
    public CompVo findCompByBizno(String bizno);
    
    public CompVo findLicrComp();
    
    public CommonPage<CompVo> findLiceCompPage(CommonCondition condition);
    
}
