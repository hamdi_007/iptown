package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.cntr.service.CntrService;
import kr.xosoft.xoip.api.biz.cntr.vo.CntrVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 계약 Controller 
 * @author likejy
 *
 */
@RestController
public class CntrController {

    @Autowired
    private CntrService cntrService;
    
    /**
     * 계약을 등록한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/contracts", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registCntr(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final CntrVo cntr) {
        
        CntrVo fetchedCntr = cntrService.registCntr(cntr);            
        return new ResponseEntity<CommonResult>(new CommonResult("cntr", fetchedCntr), HttpStatus.OK);
    }   
    
    /**
     * 계약을 수정한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyCntr(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            @Validated(UpdatingGroup.class) @RequestBody final CntrVo cntr) {
        
        Verifier.isEqual(cntrId, cntr.getCntrId(), "M1004", "word.cntrId");
        CntrVo fetchedCntr = cntrService.modifyCntr(cntr);
        return new ResponseEntity<CommonResult>(new CommonResult("cntr", fetchedCntr), HttpStatus.OK);
    }        
    
    /**
     * 계약을 삭제한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteCntr(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId) {
        
        cntrService.deleteCntr(cntrId);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 계약을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findCntr(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId) {
        
        CntrVo fetchedCntr = cntrService.findLocalizedCntr(cntrId);
        return new ResponseEntity<CommonResult>(new CommonResult("cntr", fetchedCntr), HttpStatus.OK);
    }
    
    /**
     * 계약번호로 계약을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/no", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findCntrByCntrNo(
            @PathVariable("version") final Integer version,
            @RequestParam( value="cntrNo", required=true ) final String cntrNo) {
        
        CntrVo fetchedCntr = cntrService.findCntrByCntrNo(cntrNo);
        return new ResponseEntity<CommonResult>(new CommonResult("cntr", fetchedCntr), HttpStatus.OK);
    }    
       
    /**
     * 계약 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findLiceCntrPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<CntrVo> fetchedCntrPage = cntrService.findLocalizedCntrPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("cntrPage", fetchedCntrPage), HttpStatus.OK);
    }
    
}
