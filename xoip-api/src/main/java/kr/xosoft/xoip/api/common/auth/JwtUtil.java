package kr.xosoft.xoip.api.common.auth;

import java.util.Calendar;
import java.util.Date;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;

/**
 * Jwt 유틸 
 * @author likejy
 *
 */
@Component
@Slf4j
public class JwtUtil {

    @Value("${api.jwt.secret}")
    private String secret;
    
    @Value("${api.jwt.accessTokenExpiredSeconds}")
    private Integer accessTokenExpiredSeconds;
    
    @Value("${api.jwt.refreshTokenExpiredSeconds}")
    private Integer refreshTokenExpiredSeconds;
    
    /**
     * 토큰정보를 파싱하여 UserDetails 구현 인스턴스로 반환한다.  
     * @param token
     * @return
     */
    public UserDetails parseToken(String token) {
    	
    	Claims claims = Jwts.parserBuilder()
    			.setSigningKey(secret)
    			.build()
    			.parseClaimsJws(token)
    			.getBody();
    	    			
    	DefaultUserDetails user = DefaultUserDetails.builder()
    			.userId(claims.getSubject())
    			.username(claims.get("username") == null ? null : String.valueOf(claims.get("username")))
    			.realUserName(claims.get("realUserName") == null ? null : String.valueOf(claims.get("realUserName")))
    			.siteCode(claims.get("siteCode") == null ? null : String.valueOf(claims.get("siteCode")))
    			.companyId(claims.get("companyId") == null ? null : String.valueOf(claims.get("companyId")))
    			.companyName(claims.get("companyNaame") == null ? null : String.valueOf(claims.get("companyName")))
    			.commaSeperatedAuthority(claims.get("commaSeperatedAuthority") == null ? null : String.valueOf(claims.get("commaSeperatedAuthority")))
    			.build();
    	
    	return user;    	
    }
    
    /**
     * 엑세스 토큰을 생성한다. 
     * @param userDetails
     * @return
     */
    public String generateToken(UserDetails userDetails) {
    	
    	DefaultUserDetails user = (DefaultUserDetails)userDetails;
        
        Date issueDate = new Date();
        Date expiredDate = dateAfterSeconds(issueDate, accessTokenExpiredSeconds);
        
        Claims claims = Jwts.claims().setSubject(user.getUserId());
        claims.setIssuedAt(issueDate);
        claims.setExpiration(expiredDate);
        claims.put("username", user.getUsername());
        claims.put("realUserName", user.getRealUserName());
        claims.put("siteCode", user.getSiteCode());
        claims.put("companyId", user.getCompanyId());
        claims.put("companyName", user.getCompanyName());
        claims.put("commaSeperatedAuthority", user.getCommaSeperatedAuthority());
        
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        SecretKey key = Keys.hmacShaKeyFor(keyBytes);
        
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
        
        if (log.isDebugEnabled()) {        	
        	log.debug("access token : {}", token);
        	log.debug("access token will be expired at {}", expiredDate);
        }
        
        return token;
    }
    
    /**
     * 갱신 토큰을 생성한다. 
     * @param userDetails
     * @return
     */
    public String generateRefreshToken(UserDetails userDetails) {
    
        DefaultUserDetails user = (DefaultUserDetails)userDetails;
        
        Date issueDate = new Date();
        Date expiredDate = dateAfterSeconds(issueDate, refreshTokenExpiredSeconds);
        
        Claims claims = Jwts.claims().setSubject(user.getUserId());
        claims.setIssuedAt(issueDate);
        claims.setExpiration(expiredDate);
        claims.put("username", user.getUsername());
        
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        SecretKey key = Keys.hmacShaKeyFor(keyBytes);
        
        String token = Jwts.builder()
        		.setClaims(claims)
        		.signWith(key, SignatureAlgorithm.HS256)
        		.compact();
        
        if (log.isDebugEnabled()) {
        	log.debug("refresh token : {}", token);
        	log.debug("refresh token will be expired at {}", expiredDate);
        }
        
        return token;
        
    }
    
    /**
     * 기준일자에 입력 초값을 더한후 결과 값을 반환한다. 
     * @param date
     * @param seconds
     * @return
     */
    private Date dateAfterSeconds(Date date, Integer seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }
    
}
