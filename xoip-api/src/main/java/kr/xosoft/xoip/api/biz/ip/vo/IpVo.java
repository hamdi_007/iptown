package kr.xosoft.xoip.api.biz.ip.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * IP VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class IpVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * IPID
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=10, max=10, groups=UpdatingGroup.class )
    private String ipId;        
    
    /**
     * IP코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=3, max=3, groups=DefaultGroup.class )    
    private String ipCd;

    /**
     * IP명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String ipNm;

    /**
     * IP영문명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String ipEngNm;

    /**
     * 사용여부
     */
    @Pattern( message="chk.yn", regexp="(^[YN]{1}$)", groups=DefaultGroup.class )        
    private String useYn;

    /**
     * 정렬순번
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer srtOrdno;

    /**
     * 상세설명
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String dtlDesc;

    /**
     * 영문상세설명
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String engDtlDesc;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
}
