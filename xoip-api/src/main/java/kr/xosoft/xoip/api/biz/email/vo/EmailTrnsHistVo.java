package kr.xosoft.xoip.api.biz.email.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 이메일전송이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class EmailTrnsHistVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 이메일전송이력일련번호
     */
    private Integer emailTrnsHistSeqno;

    /**
     * 사이트코드 
     * IMPORTANT: 이메일전송이력VO 는 비동기 스레드 에서 실행되므로, 전역설정값인 siteCd 가 없으므로 
     * BaseVO 의 setSiteCd(), getSiteCd() 를 이용하는 대신 별도의 셋터 및 게터가 생성 및 설정 되어야 한다. 
     */
    private String siteCd;
    
    /**
     * 폼코드
     */
    private String formCd;

    /**
     * 폼번호
     */
    private Integer formNo;

    /**
     * 전송상태코드
     */
    private String trnsStsCd;

    /**
     * 전송요청일시
     */
    private Date trnsReqDt;

    /**
     * 전송종료일시
     */
    private Date trnsEndDt;

    /**
     * 전송요청회사ID
     */
    private String trnsReqCompId;

    /**
     * 전송요청자ID
     */
    private String trnsReqrId;

    /**
     * 전송자이메일
     */
    private String trnsrEmail;

    /**
     * 전송자명
     */
    private String trnsrNm;

    /**
     * 전체수신자
     */
    private String ttlRecp;

    /**
     * 템플릿변수전문
     */
    private String tmplVarFtxt;
    
    /**
     * 오류내용 
     */
    private String errCnts;
    
    /**
     * 생성자 
     * @param emailTrns
     */
    public EmailTrnsHistVo(EmailTrnsVo emailTrns) {
        
        setSiteCd(emailTrns.getSiteCd());
        setFormCd(emailTrns.getFormCd());
        setFormNo(emailTrns.getFormNo());
        setTrnsReqDt(emailTrns.getTrnsReqDt());
        setTrnsReqCompId(emailTrns.getTrnsReqCompId());
        setTrnsReqrId(emailTrns.getTrnsReqrId());
        setTrnsrEmail(emailTrns.getTrnsrEmail());
        setTrnsrNm(emailTrns.getTrnsrNm());
        setTtlRecp(String.join(", ", emailTrns.getRecpList()));
        setTmplVarFtxt(emailTrns.getTmplVarFtxt());                
    }
    
}
