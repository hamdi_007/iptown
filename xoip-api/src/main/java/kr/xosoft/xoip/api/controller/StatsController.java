package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.stats.service.StatsService;
import kr.xosoft.xoip.api.biz.stats.vo.StatsVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.file.buffer.BufferData;
import kr.xosoft.xoip.api.common.file.buffer.BufferDownloader;

/**
 * 통계 Controller 
 * @author leejaejun
 *
 */
@RestController
public class StatsController {

    @Autowired
    private StatsService statsService;
    
    @Autowired
    private BufferDownloader bufferDownloader;
    
    /**
     * 통계 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/statistics", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findStatsList(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<StatsVo> fetchedStatsPage = statsService.findStatsPage(condition);
        return new ResponseEntity<CommonResult>(new CommonResult("statsPage", fetchedStatsPage), HttpStatus.OK);
    }
    
    /**
     * 통계 목록을 엑셀 시트로 다운로드 한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/statistics/reports", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadStatsRpt(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        BufferData bd = statsService.makeStatsRptExcelData(condition);
        return bufferDownloader.downloadBuffer(bd);
    }   
    
}
