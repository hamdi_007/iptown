package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.List;

import javax.validation.Valid;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 검수파일 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class MarkUnionVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    
    @Valid
    private MarkVo mark;
    
    @Valid
    private List<MarkOpinVo> markOpinList;
    
    public MarkUnionVo() {
    }
    
    public MarkUnionVo(MarkVo mark, List<MarkOpinVo> markOpinList) {
    	setMark(mark);
    	setMarkOpinList(markOpinList);
    }
}
