package kr.xosoft.xoip.api.biz.prodtyp.service;

import java.util.List;

import kr.xosoft.xoip.api.biz.prodtyp.vo.ProdTypVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface ProdTypService {

    public ProdTypVo registProdTyp1(ProdTypVo prodTyp);
    
    public ProdTypVo registProdTyp2(ProdTypVo prodTyp);
    
    public ProdTypVo modifyProdTyp(ProdTypVo prodTyp);
     
    public void deleteProdTyp1(String prodTypId);
    
    public void deleteProdTyp2(String prodTypId);
    
    public ProdTypVo findProdTyp(String prodTypId);
    
    public ProdTypVo findProdTypByProdTypCd(String prodTypCd);
    
    public ProdTypVo findValidProdTyp(String prodTypId);
        
    public CommonPage<ProdTypVo> findProdTypPage(CommonCondition condition);
    
    public List<ProdTypVo> findProdTypListAt(Integer prodTypLv, String topProdTypId, String useYn);
    
}
