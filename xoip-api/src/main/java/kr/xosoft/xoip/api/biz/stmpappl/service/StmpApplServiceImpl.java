package kr.xosoft.xoip.api.biz.stmpappl.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.proj.service.ProjService;
import kr.xosoft.xoip.api.biz.proj.vo.ProjRsltVo;
import kr.xosoft.xoip.api.biz.stmpappl.mapper.SiteStmpMapper;
import kr.xosoft.xoip.api.biz.stmpappl.mapper.StmpApplHistMapper;
import kr.xosoft.xoip.api.biz.stmpappl.mapper.StmpApplMapper;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplHistVo;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplUnionVo;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplVo;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpLotVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.MessageBean;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.exception.BizException;
import kr.xosoft.xoip.api.common.file.buffer.BufferData;
import lombok.extern.slf4j.Slf4j;

/**
 * 증지신청 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
@Slf4j
public class StmpApplServiceImpl implements StmpApplService {

    private static final String PRDC_RPT_FILE_NM = "prdc_rpt.xlsx";
    
    private static final String STMP_APPL_RPT_FILE_NM = "stmp_appl_rpt.xlsx";
    
    @Value("${api.template.basePath}")
    private String templateBasePath;
    
    @Autowired
    private ProjService projService;
    
    @Autowired
    private StmpLotService stmpLotService;
    
    @Autowired
    private StmpApplMapper stmpApplMapper;
    
    @Autowired
    private StmpApplHistMapper stmpApplHistMapper;
    
    @Autowired
    private SiteStmpMapper siteStmpMapper;
    
    @Autowired
    private MessageBean messageBean;        
    
    /**
     * 증지 신청을 위한 폼을 조회한다. 
     */
    public StmpApplUnionVo initStmpApplUnion(String projCd) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
                
        // 증지신청이 가능한 프로젝트 여부 확인 
        CommonCondition condition = new CommonCondition();
        condition.setComParamKeys(new String[] { "projCd", "stmpApplAvlYn" });
        condition.setComParamVals(new String[] { projCd, "Y" });
        CommonPage<ProjRsltVo> projPage = projService.findLocalizedProjRsltPage(condition);
        Verifier.eq(projPage.getComTtlCnt(), 1, "M7010");    

        // IMPORTANT: 
        // 증지신청시 사용되는 라이선서 담당자 정보, 증지 인쇄 업체 담당자 정보는 별도의 DB 모델링 없이 프로그램내에 하드코딩 하기로 협의 
        // 그럼에도 불구하고, 추후에 값 변경시 편의를 위해 임시 테이블(site_stmp)을 생성하여 해당 정보를 조회하도록 처리 하였음   
        StmpApplVo stmpAppl = siteStmpMapper.findSiteStmpByProjCd(commonData.getSiteCd(), projCd);
        
        return new StmpApplUnionVo(stmpAppl, new ArrayList<StmpLotVo>());
    }
    
    /**
     * 증지 신청을 한다. 
     */
    @Override
    public StmpApplUnionVo registStmpApplUnion(StmpApplUnionVo stmpApplUnion) {

        StmpApplVo stmpAppl = stmpApplUnion.getStmpAppl();
        List<StmpLotVo> stmpLotList = stmpApplUnion.getStmpLotList();
        
        // 증지신청이 가능한 프로젝트 여부 확인 
        CommonCondition condition = new CommonCondition();
        condition.setComParamKeys(new String[] { "projCd", "stmpApplAvlYn" });
        condition.setComParamVals(new String[] { stmpAppl.getProjCd(), "Y" });
        CommonPage<ProjRsltVo> projPage = projService.findLocalizedProjRsltPage(condition);
        Verifier.eq(projPage.getComTtlCnt(), 1, "M7010");
        
        // 증지품목이 최소 1건, 최대 20건 등록되어야 함 
        Verifier.notNull(stmpLotList, "M7011");
        Verifier.ge(stmpLotList.size(), 1, "M7011");
        Verifier.le(stmpLotList.size(), 20, "M7020");

        // 증지신청 등록 
        stmpAppl.setStmpApplDt(new Date());
        int cnt = stmpApplMapper.insertStmpAppl(stmpAppl);
        Verifier.eq(cnt, 1, "M1000", "word.stmpAppl");

        // 증지신청 이력 등록 
        StmpApplHistVo stmpApplHist = new StmpApplHistVo(stmpAppl.getProjCd(), stmpAppl.getStmpApplNo(), "HD0100");
        stmpApplHistMapper.insertStmpApplHistAsSelectMaster(stmpApplHist);
        Verifier.notNull(stmpApplHist.getStmpApplHistSeqno(), "M1000", "word.stmpApplHist");

        // 증지품목별 증지신청번호 할당  
        // 입력값 검증
        for (StmpLotVo stmpLot : stmpLotList) {
            stmpLot.setProjCd(stmpAppl.getProjCd());
            stmpLot.setStmpApplNo(stmpAppl.getStmpApplNo());
        }
        checkValidData(stmpAppl, stmpLotList);
        
        // 증지품목 등록 
        stmpLotService.mergeStmpLotList(stmpAppl.getProjCd(), stmpAppl.getStmpApplNo(), stmpLotList);            
        
        return stmpApplUnion;        
    }

    /**
     * 증지신청건을 수정한다. 
     */
    @Override
    public StmpApplUnionVo modifyStmpApplUnion(StmpApplUnionVo stmpApplUnion) {
        
        StmpApplVo stmpAppl = stmpApplUnion.getStmpAppl();
        List<StmpLotVo> stmpLotList = stmpApplUnion.getStmpLotList();
        
        // 증지신청건 수정이 가능한 프로젝트 여부 확인 
        CommonCondition condition = new CommonCondition();
        condition.setComParamKeys(new String[] { "projCd", "stmpApplAvlYn" });
        condition.setComParamVals(new String[] { stmpAppl.getProjCd(), "Y" });
        CommonPage<ProjRsltVo> projPage = projService.findLocalizedProjRsltPage(condition);
        Verifier.eq(projPage.getComTtlCnt(), 1, "M7019");
        
        // 증지품목이 최소 1건, 최대 20건 등록되어야 함
        Verifier.notNull(stmpLotList, "M7011");
        Verifier.ge(stmpLotList.size(), 1, "M7011");     
        Verifier.le(stmpLotList.size(), 20, "M7020");        
        
        // 증지품목별 증지신청번호 할당  
        // 입력값 검증
        for (StmpLotVo stmpLot : stmpLotList) {
            stmpLot.setProjCd(stmpAppl.getProjCd());
            stmpLot.setStmpApplNo(stmpAppl.getStmpApplNo());
        }        
        checkValidData(stmpAppl, stmpLotList);
        
        // 증지신청 수정 
        int cnt = stmpApplMapper.updateStmpApplByKey(stmpAppl);
        Verifier.eq(cnt, 1, "M1001", "word.stmpAppl");

        // 디자인검수 이력 등록 
        StmpApplHistVo stmpApplHist = new StmpApplHistVo(stmpAppl.getProjCd(), stmpAppl.getStmpApplNo(), "HD0200");
        stmpApplHistMapper.insertStmpApplHistAsSelectMaster(stmpApplHist);
        Verifier.notNull(stmpApplHist.getStmpApplHistSeqno(), "M1000", "word.stmpApplHist");
        
        // 증지품목 변경  
        stmpLotService.mergeStmpLotList(stmpAppl.getProjCd(), stmpAppl.getStmpApplNo(), stmpLotList);            
        
        return null;
    }
    
    /**
     * 증지신청폼 입력값 검증 
     * @param stmpAppl
     * @param stmpLotList
     */
    private void checkValidData(StmpApplVo stmpAppl, List<StmpLotVo> stmpLotList) {
        
        // 증지신청폼의 필드합계와 증지품목폼의 필드합계 값 정합성 검증 
        Integer prdcCntSum = 0; // 생산개수합계 
        BigDecimal ryltAmtSum = new BigDecimal(0); // 로열티금액합계
        BigDecimal stmpAmtSum = new BigDecimal(0); // 증지금액합계         
        for (StmpLotVo stmpLot : stmpLotList) {
            
            // 프로젝트 코드 확인 
            Verifier.isEqual(stmpLot.getProjCd(), stmpAppl.getProjCd(), "M1004", "word.projCd");

            // 생산개수합계  
            prdcCntSum += stmpLot.getPrdcCnt();
            
            // 로열티금액 
            BigDecimal ryltTgtAmt = null;
            if ("RS0100".equals(stmpLot.getRyltStdCd())) { // 소비자가 
                ryltTgtAmt = stmpLot.getCnsmAmt();
            } else if ("RS0200".equals(stmpLot.getRyltStdCd())) { // 출고가 
                ryltTgtAmt = stmpLot.getRlsAmt();
            } else {
                Verifier.fail("M1004", "word.ryltStdCd");
            }
            BigDecimal ryltRtRslt = stmpLot.getRyltRt().divide(new BigDecimal(100), 4, RoundingMode.FLOOR);
            BigDecimal ryltAmt = ryltTgtAmt.multiply(ryltRtRslt).multiply(new BigDecimal(stmpLot.getPrdcCnt())).setScale(0, RoundingMode.HALF_UP);
            Verifier.eq(ryltAmt, stmpLot.getRyltAmt(), "M7012", stmpLot.getLotNm());
            ryltAmtSum = ryltAmtSum.add(stmpLot.getRyltAmt());
            
            // 증지금액 
            BigDecimal stmpAmt = stmpAppl.getStmpStdAmt().multiply(new BigDecimal(stmpLot.getPrdcCnt())).setScale(0, RoundingMode.HALF_UP);
            Verifier.eq(stmpAmt, stmpLot.getStmpAmt(), "M7013", stmpLot.getLotNm());
            stmpAmtSum = stmpAmtSum.add(stmpLot.getStmpAmt());
        }
        
        BigDecimal stmpVatSum = stmpAmtSum.multiply(new BigDecimal("0.1")).setScale(0, RoundingMode.HALF_UP); // 증지부가세합계 = ROUND(증지금액합계 * 0.1, 0)
        BigDecimal dpsTgtAmtSum = stmpAmtSum.add(stmpVatSum); // 입금대상금액합계 = 증지금액합계 + 증지부가세합계
        
        Verifier.eq(prdcCntSum, stmpAppl.getPrdcCntSum(), "M7014");
        Verifier.eq(ryltAmtSum, stmpAppl.getRyltAmtSum(), "M7015");
        Verifier.eq(stmpAmtSum, stmpAppl.getStmpAmtSum(), "M7016");
        Verifier.eq(stmpVatSum, stmpAppl.getStmpVatSum(), "M7017");
        Verifier.eq(dpsTgtAmtSum, stmpAppl.getDpsTgtAmtSum(), "M7018");        
        
    }

    /**
     * 증지신청을 삭제한다. 
     */
    @Override
    public void deleteStmpAppl(String projCd, Integer stmpApplNo) {
        
        // 증지신청 삭제 
        StmpApplVo stmpAppl = new StmpApplVo();
        stmpAppl.setProjCd(projCd);
        stmpAppl.setStmpApplNo(stmpApplNo);
        int cnt = stmpApplMapper.deleteStmpApplByKey(stmpAppl); 
        Verifier.eq(cnt, 1, "M1002", "word.stmpAppl");
        
        // 증지신청 이력 등록 
        StmpApplHistVo stmpApplHist = new StmpApplHistVo(projCd, stmpApplNo, "HD0300");
        stmpApplHistMapper.insertStmpApplHistAsSelectMaster(stmpApplHist);
        Verifier.notNull(stmpApplHist.getStmpApplHistSeqno(), "M1000", "word.stmpApplHist");  
    }
    
    /**
     * 증지신청을 확인유무를 변경한다.
     */
    @Override
    public void updateConfirmStmpAppl(String projCd, Integer stmpApplNo) {
    	
    	// 증지신청 확인유무 변
        StmpApplVo stmpAppl = new StmpApplVo();
        stmpAppl.setProjCd(projCd);
        stmpAppl.setStmpApplNo(stmpApplNo);
        int cnt = stmpApplMapper.confirmStateStmpApplByKey(stmpAppl);
        Verifier.eq(cnt, 1, "M1002", "word.stmpAppl");
        
        // 증지신청 이력 등록 
        StmpApplHistVo stmpApplHist = new StmpApplHistVo(projCd, stmpApplNo, "HD0300");
        stmpApplHistMapper.insertStmpApplHistAsSelectMaster(stmpApplHist);
        Verifier.notNull(stmpApplHist.getStmpApplHistSeqno(), "M1000", "word.stmpApplHist"); 
   	
    }

    /**
     * 증지신청건을 조회한다. 
     */
    public StmpApplVo findStmpApplVo(String projCd, Integer stmpApplNo) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return stmpApplMapper.findStmpApplByKey(commonData.getSiteCd(), projCd, stmpApplNo);        
    }
    
    /**
     * 증지신청+증지품목 목록을 조회한다. 
     */
    @Override
    public StmpApplUnionVo findStmpApplUnionVo(String projCd, Integer stmpApplNo) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        StmpApplVo stmpAppl = stmpApplMapper.findStmpApplWithMetaByKey(commonData.getSiteCd(), projCd, stmpApplNo);
        List<StmpLotVo> stmpLotList = stmpLotService.findStmpLotList(projCd, stmpApplNo);
        
        return new StmpApplUnionVo(stmpAppl, stmpLotList);
    }

    /**
     * 증지신청 목록을 조회한다. 
     */
    @Override
    public CommonPage<StmpApplVo> findStmpApplRsltPage(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        
        int comTtlCnt = stmpApplMapper.countStmpApplListByMap(map);
        List<StmpApplVo> stmpApplList = stmpApplMapper.findStmpApplListByMap(map);
        
        return new CommonPage<StmpApplVo>(condition, stmpApplList, comTtlCnt);                   
    }

    /**
     * 생산보고서 엑셀 데이터를 생성한다.  
     */
    @Override
    public BufferData makePrdcRptExcelData(String projCd, Integer stmpApplNo) {

        StmpApplUnionVo stmpApplUnion = findStmpApplUnionVo(projCd, stmpApplNo);
        StmpApplVo stmpAppl = stmpApplUnion.getStmpAppl();
        List<StmpLotVo> stmpLotList = stmpApplUnion.getStmpLotList();
        
        FileInputStream fis = null;
        ByteArrayOutputStream bos = null;
        XSSFWorkbook workbook = null;
        try {
            
            File file = new File(getTemplateFilePath(PRDC_RPT_FILE_NM));
            fis = new FileInputStream(file);
            workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowIdx = 0;
            int cellIdx = 0;
            
            // 발신정보
            rowIdx = 3;
            cellIdx = 2;
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplCompNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrTelno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrHnpno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrEmail());
            
            // 수신정보 
            rowIdx = 3;
            cellIdx = 7;
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getLicrCompNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getLicrChrgrNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getLicrChrgrTelno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getLicrChrgrHnpno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getLicrChrgrEmail());
            
            // 신청일, 계약명 
            rowIdx = 9;
            cellIdx = 2;
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(DateFormatUtils.format(stmpAppl.getStmpApplDt(), "yyyy-MM-dd"));
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(StringUtils.defaultString(stmpAppl.getCntrNm(), stmpAppl.getCntrNo()));
            
            // 생산내역
            rowIdx = 15;
            for (StmpLotVo stmpLot : stmpLotList) {
                cellIdx = 2;
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getLotNm());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getCnsmAmt().intValue());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getRlsAmt().intValue());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getRyltStdCdNm());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getRyltRt().setScale(2).toString());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getPrdcCnt().intValue());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getRyltAmt().intValue());
                rowIdx++;
            }
            
            // 합계 
            rowIdx = 35;
            cellIdx = 7;
            sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpAppl.getPrdcCntSum());
            sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpAppl.getRyltAmtSum().intValue());
            
            // 담당자 서명 
            rowIdx = 38;
            cellIdx = 2;
            sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpAppl.getStmpApplrNm());

            // dataUrl 값을 byte 로 변환 
            String stmpApplrSignVal = stmpApplMapper.findStmpApplrSignValByKey(stmpAppl.getSiteCd(), projCd, stmpApplNo);
            String encodingPrefix = "base64,";
            int contentStartIndex = stmpApplrSignVal.indexOf(encodingPrefix) + encodingPrefix.length();
            byte[] bytes = Base64.decodeBase64(stmpApplrSignVal.substring(contentStartIndex));
            
            // 서명 이미지 할당 
            rowIdx = 38;
            cellIdx = 3;
            int pictureIdx = workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_PNG);
            CreationHelper helper = workbook.getCreationHelper();
            XSSFDrawing drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = helper.createClientAnchor();
            anchor.setRow1(rowIdx++); 
            anchor.setCol1(cellIdx++);
            anchor.setRow2(rowIdx);
            anchor.setCol2(cellIdx);
            drawing.createPicture(anchor, pictureIdx);
            
            // BufferData 생성 및 리턴 
            String fileNm = messageBean.getMessage("word.prdcRptFileNmPrefix") + stmpAppl.getProjCd() + "-" + stmpAppl.getStmpApplNo() + ".xlsx";
            bos = new ByteArrayOutputStream();
            workbook.write(bos);
            return new BufferData(bos.toByteArray(), fileNm, BufferData.CONTENT_TYPE_XLSX);
            
        } catch (FileNotFoundException e) {
            throw new BizException("M7022", e);
        } catch (IOException e) {
            throw new BizException("M7023", e);
        } finally {
            try { if (workbook != null) workbook.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
            try { if (fis != null) fis.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
            try { if (bos != null) bos.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
        }
    }
    
    /**
     * 증지신청서 엑셀 데이터를 생성한다.  
     */
    @Override
    public BufferData makeStmpApplRptExcelData(String projCd, Integer stmpApplNo) {

        StmpApplUnionVo stmpApplUnion = findStmpApplUnionVo(projCd, stmpApplNo);
        StmpApplVo stmpAppl = stmpApplUnion.getStmpAppl();
        List<StmpLotVo> stmpLotList = stmpApplUnion.getStmpLotList();
        
        FileInputStream fis = null;
        ByteArrayOutputStream bos = null;
        XSSFWorkbook workbook = null;
        try {
            
            File file = new File(getTemplateFilePath(STMP_APPL_RPT_FILE_NM));
            fis = new FileInputStream(file);
            workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowIdx = 0;
            int cellIdx = 0;
            
            // 발신정보
            rowIdx = 3;
            cellIdx = 2;
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplCompNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrTelno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrHnpno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpApplrEmail());
            
            // 수신정보 (증지제작회사) 
            rowIdx = 3;
            cellIdx = 6;
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpMnfcCompNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpMnfcChrgrNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpMnfcChrgrTelno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpMnfcChrgrHnpno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpMnfcChrgrEmail());
            
            // 신청일, 계약명, 수령자정보 
            rowIdx = 9;
            cellIdx = 2;
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(DateFormatUtils.format(stmpAppl.getStmpApplDt(), "yyyy-MM-dd"));
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(StringUtils.defaultString(stmpAppl.getCntrNm(), stmpAppl.getCntrNo()));
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpRecpNm());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpRecpAddr() + " " + stmpAppl.getStmpRecpDtlAddr());
            
            // 증지단가, 연락처, 계산서이메일 
            rowIdx = 10;
            cellIdx = 6;
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpStdAmt().setScale(1).toString() + "원/장 (VAT별도)");
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getStmpRecpTelno());
            sheet.getRow(rowIdx++).getCell(cellIdx).setCellValue(stmpAppl.getBillEmail());
            
            // 입금처 
            rowIdx = 13;
            cellIdx = 2;
            sheet.getRow(rowIdx).getCell(cellIdx).setCellValue(stmpAppl.getStmpMnfcCompAcctInfo());
            
            // 입금액 
            rowIdx = 13;
            cellIdx = 6;
            sheet.getRow(rowIdx).getCell(cellIdx).setCellValue(stmpAppl.getDpsTgtAmtSum().intValue());
            
            // 신청내역
            rowIdx = 18;
            for (StmpLotVo stmpLot : stmpLotList) {
                cellIdx = 2;
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getLotNm());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getCnsmAmt().intValue());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpAppl.getStmpStdAmt().setScale(1).toString());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getStmpKndVal());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getPrdcCnt().intValue());
                sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpLot.getStmpAmt().intValue());
                rowIdx++;
            }
            
            // 합계 
            rowIdx = 38;
            cellIdx = 6;
            sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpAppl.getPrdcCntSum());
            sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpAppl.getStmpAmtSum().intValue());
            
            // 담당자 서명 
            rowIdx = 41;
            cellIdx = 2;
            sheet.getRow(rowIdx).getCell(cellIdx++).setCellValue(stmpAppl.getStmpApplrNm());

            // dataUrl 값을 byte 로 변환 
            String stmpApplrSignVal = stmpApplMapper.findStmpApplrSignValByKey(stmpAppl.getSiteCd(), projCd, stmpApplNo);
            String encodingPrefix = "base64,";
            int contentStartIndex = stmpApplrSignVal.indexOf(encodingPrefix) + encodingPrefix.length();
            byte[] bytes = Base64.decodeBase64(stmpApplrSignVal.substring(contentStartIndex));
            
            // 서명 이미지 할당 
            rowIdx = 41;
            cellIdx = 3;
            int pictureIdx = workbook.addPicture(bytes, XSSFWorkbook.PICTURE_TYPE_PNG);
            CreationHelper helper = workbook.getCreationHelper();
            XSSFDrawing drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = helper.createClientAnchor();
            anchor.setRow1(rowIdx++); 
            anchor.setCol1(cellIdx++);
            anchor.setRow2(rowIdx);
            anchor.setCol2(cellIdx);
            drawing.createPicture(anchor, pictureIdx);
            
            // BufferData 생성 및 리턴 
            String fileNm = messageBean.getMessage("word.stmpApplRptFileNmPrefix") + stmpAppl.getProjCd() + "-" + stmpAppl.getStmpApplNo() + ".xlsx";
            bos = new ByteArrayOutputStream();
            workbook.write(bos);
            return new BufferData(bos.toByteArray(), fileNm, BufferData.CONTENT_TYPE_XLSX);
            
        } catch (FileNotFoundException e) {
            throw new BizException("M7022", e);
        } catch (IOException e) {
            throw new BizException("M7023", e);
        } finally {
            try { if (workbook != null) workbook.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
            try { if (fis != null) fis.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
            try { if (bos != null) bos.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
        }
    }    

    private String getTemplateFilePath(String fileNm) {
        CommonData commonData = CommonDataHolder.getCommonData();
        String templateFilePath = FilenameUtils.concat(templateBasePath, commonData.getSiteCd());
        templateFilePath = FilenameUtils.concat(templateFilePath, fileNm);
        return templateFilePath;
    }
    
}
