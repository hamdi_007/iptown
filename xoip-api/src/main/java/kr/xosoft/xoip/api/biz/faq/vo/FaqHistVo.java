package kr.xosoft.xoip.api.biz.faq.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * FAQ이력 VO 
 * @author hypark
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class FaqHistVo extends FaqVo {

    private static final long serialVersionUID = 1L;

    /**
     * FAQ 일련번호
     */
    private Integer faqHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public FaqHistVo(Integer faqNo, String histDivCd) {
		setFaqNo(faqNo);
		setHistDivCd(histDivCd);
	}
    
}
