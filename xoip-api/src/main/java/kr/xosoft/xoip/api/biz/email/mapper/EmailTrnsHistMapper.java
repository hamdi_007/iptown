package kr.xosoft.xoip.api.biz.email.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.email.vo.EmailTrnsHistVo;

/**
 * 이메일 전송 이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface EmailTrnsHistMapper {

    /**
     * 이메일 전송 이력 등록 
     * @param emailTrnsHist
     */
    public void insertEmailTrnsHist(EmailTrnsHistVo emailTrnsHist);
    
}
