package kr.xosoft.xoip.api.biz.comp.vo;

import kr.xosoft.xoip.api.biz.comp.vo.CompHistVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 회사이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class CompHistVo extends CompVo {

    private static final long serialVersionUID = 1L;

    /**
     * 회사이력일련번호 
     */
    private Integer compHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public CompHistVo(String compId, String histDivCd) {
        setCompId(compId);
        setHistDivCd(histDivCd);
    }    
    
}
