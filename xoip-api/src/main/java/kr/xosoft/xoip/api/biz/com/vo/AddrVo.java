package kr.xosoft.xoip.api.biz.com.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 주소 VO
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class AddrVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    private String postNo;
    
    private String addr;
    
    private String dtlAddr1;
    
    private String dtlAddr2;
    
    private String jibunAddr;
    
}
