package kr.xosoft.xoip.api.biz.site.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.site.vo.SiteSmtpVo;

/**
 * 사이트 SMTP 매퍼 
 * @author likejy
 *
 */
@Repository
public interface SiteSmtpMapper {

    public SiteSmtpVo findLocalizedSiteSmtpByKey(@Param("siteCd") String siteCd, @Param("tmplLang") String tmplLang);
    
}
