package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.cntr.service.CntrFileService;
import kr.xosoft.xoip.api.biz.cntr.vo.CntrFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.CntrFileWrapper;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 계약파일 Controller 
 * @author likejy
 *
 */
@RestController
public class CntrFileController {

    @Autowired
    private CntrFileService cntrFileService;

    @Autowired
    private CommonFileProcessor commonFileProcessor;

    @Value("${api.file.basePath}")
    private String basePath;
    
    /**
     * 계약파일을 등록한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}/file", method=RequestMethod.POST, headers="content-type=multipart/form-data" )
    public ResponseEntity<CommonResult> registCntrFile(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            @RequestParam( value="file", required=false ) MultipartFile mFile,
            @RequestParam("cntrFileDivCd") String cntrFileDivCd) {
        
        Verifier.notNull(mFile, "M1004", "mFile");
        Verifier.notNull(cntrFileDivCd, "M1004", "cntrFileDivCd");
        CntrFileWrapper cntrFileWrapper = new CntrFileWrapper(mFile, cntrId, cntrFileDivCd, basePath);
        commonFileProcessor.uploadFile(cntrFileWrapper);

        CntrFileVo fetchedCntrFile = cntrFileService.registCntrFile((CntrFileVo)cntrFileWrapper.getFileVo());            
        return new ResponseEntity<CommonResult>(new CommonResult("cntrFile", fetchedCntrFile), HttpStatus.OK);
    }   
    
    /**
     * 계약파일 정보를 수정한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}/file/{cntrFileNo}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyCntrFile(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            @PathVariable("cntrFileNo") final Integer cntrFileNo,
            @Validated(UpdatingGroup.class) @RequestBody final CntrFileVo cntrFile) {
        
        Verifier.isEqual(cntrId, cntrFile.getCntrId(), "M1004", "word.cntrId");
        Verifier.isEqual(cntrFileNo, cntrFile.getCntrFileNo(), "M1004", "word.cntrFileNo");
        CntrFileVo fetchedCntrFile = cntrFileService.modifyCntrFile(cntrFile);
        return new ResponseEntity<CommonResult>(new CommonResult("cntrFile", fetchedCntrFile), HttpStatus.OK);
    }        
    
    /**
     * 계약파일을 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}/file/{cntrFileNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteCntrFile(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            @PathVariable("cntrFileNo") final Integer cntrFileNo) {
        
        cntrFileService.deleteCntrFile(cntrId, cntrFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 계약파일을 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}/file/{cntrFileNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findCntrFile(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            @PathVariable("cntrFileNo") final Integer cntrFileNo) {
        
        CntrFileVo fetchedCntrFile = cntrFileService.findCntrFile(cntrId, cntrFileNo);
        return new ResponseEntity<CommonResult>(new CommonResult("cntrFile", fetchedCntrFile), HttpStatus.OK);
    }
    
    /**
     * 계약파일 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}/file", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findCntrFilePage(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            final CommonCondition condition) {
        
        CommonPage<CntrFileVo> fetchedCntrFilePage = cntrFileService.findCntrFilePage(cntrId, condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("cntrFilePage", fetchedCntrFilePage), HttpStatus.OK);
    }
    
    /**
     * 계약파일 미리보기 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}/file/{cntrFileNo}/preview", method=RequestMethod.GET )
    public ResponseEntity<Resource> previewCntrFile(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            @PathVariable("cntrFileNo") final Integer cntrFileNo) {
        
        CntrFileVo fetchedCntrFile = cntrFileService.findCntrFile(cntrId, cntrFileNo);
        return commonFileProcessor.previewFile(new CntrFileWrapper(fetchedCntrFile, basePath));
    }       
    
    /**
     * 계약파일 다운로드 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/contracts/{cntrId}/file/{cntrFileNo}/download", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadCntrFile(
            @PathVariable("version") final Integer version,
            @PathVariable("cntrId") final String cntrId,
            @PathVariable("cntrFileNo") final Integer cntrFileNo) {
        
        CntrFileVo fetchedCntrFile = cntrFileService.findCntrFile(cntrId, cntrFileNo);
        return commonFileProcessor.downloadFile(new CntrFileWrapper(fetchedCntrFile, basePath));
    }        
    
}
