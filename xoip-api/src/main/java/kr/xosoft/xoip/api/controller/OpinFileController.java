package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.proj.service.OpinFileService;
import kr.xosoft.xoip.api.biz.proj.vo.OpinFileVo;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.file.CommonFileProcessor;
import kr.xosoft.xoip.api.common.file.OpinFileWrapper;

/**
 * 의견파일 Controller 
 * @author likejy
 *
 */
@RestController
public class OpinFileController {

    @Autowired
    private OpinFileService opinFileService;

    @Autowired
    private CommonFileProcessor commonFileProcessor;

    @Value("${api.file.basePath}")
    private String basePath;
    
    /**
     * 의견파일을 등록한다. 
     * 파일을 디스크에 저장하기만 하며, 테이블에는 정보를 등록하지 않는다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/opinion", method=RequestMethod.POST, headers="content-type=multipart/form-data" )
    public ResponseEntity<CommonResult> registOpinFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @PathVariable("inspFileNo") final Integer inspFileNo,
            @RequestParam( value="file", required=false ) MultipartFile mFile,
            @RequestParam("compId") String compId) {
        
        Verifier.notNull(mFile, "M1004", "mFile");
        Verifier.notNull(compId, "M1004", "compId");
        OpinFileWrapper opinFileWrapper = new OpinFileWrapper(mFile, compId, projCd, inspNo, inspFileNo, basePath);
        commonFileProcessor.uploadFile(opinFileWrapper);
        
        return new ResponseEntity<CommonResult>(new CommonResult("opinFile", opinFileWrapper.getFileVo()), HttpStatus.OK);
    }
    
    /**
     * 의견파일 다운로드 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/projects/{projCd}/inspection/{inspNo}/file/{inspFileNo}/opinion/{opinFileNo}/download", method=RequestMethod.GET )
    public ResponseEntity<Resource> downloadOpinFile(
            @PathVariable("version") final Integer version,
            @PathVariable("projCd") final String projCd,
            @PathVariable("inspNo") final Integer inspNo,
            @PathVariable("inspFileNo") final Integer inspFileNo,
            @PathVariable("opinFileNo") final Integer opinFileNo) {
        
        OpinFileVo fetchedOpinFile = opinFileService.findOpinFile(projCd, inspNo, inspFileNo, opinFileNo);
        return commonFileProcessor.downloadFile(new OpinFileWrapper(fetchedOpinFile, basePath));
    }        
    
}
