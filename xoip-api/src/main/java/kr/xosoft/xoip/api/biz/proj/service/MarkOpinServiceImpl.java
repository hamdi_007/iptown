package kr.xosoft.xoip.api.biz.proj.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.proj.mapper.MarkOpinMapper;
import kr.xosoft.xoip.api.biz.proj.vo.MarkOpinVo;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * 마크업의견 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class MarkOpinServiceImpl implements MarkOpinService {

    @Autowired
    private MarkOpinMapper markOpinMapper;
    
	@Override
	public List<MarkOpinVo> mergeMarkOpinList(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo,
			List<MarkOpinVo> markOpinList) {
		
        List<MarkOpinVo> createdList = new ArrayList<MarkOpinVo>();
        List<MarkOpinVo> updatedList = new ArrayList<MarkOpinVo>();
        List<MarkOpinVo> deletedList = findMarkOpinList(projCd, inspNo, inspFileNo, markNo);
        
        if (markOpinList != null) {
            for (MarkOpinVo opin : markOpinList) {
                // 마크업의견 번호가 없는 경우는 신규 등록인 경우임 
                if (opin.getMarkOpinNo() == null) {
                    createdList.add(opin);
                    continue;
                }
                int idx = -1;
                for (int i = 0; i < deletedList.size(); i++) {
                    if (deletedList.get(i).getMarkOpinNo() == opin.getMarkOpinNo()) {
                        idx = i;
                        break;
                    }
                }
                if (idx >= 0) {
                    deletedList.remove(idx);
                    updatedList.add(opin);
                } else {
                    createdList.add(opin);
                }
            }
        } 
        
        // 마크업의견 삭제 
        deleteMarkOpinList(deletedList);
        
        // 저장된 마크업의견 값 수정
        modifyMarkOpinList(updatedList);
        
        // 신규 마크업의견 등록 
        registMarkOpinList(createdList);
        
        return markOpinList;
    }
	
	
    /**
     * 마크업의견을 등록한다. 
     */
    private void registMarkOpinList(List<MarkOpinVo> markOpinList) {
        // 검수파일 등록
        if (markOpinList != null) {
            for (MarkOpinVo opin : markOpinList) {
                int cnt = markOpinMapper.insertMarkOpin(opin);
                Verifier.eq(cnt, 1, "M1000", "word.markOpin");
            }
        }
    }

    /**
     * 마크업의견을 수정한다. 
     */
    public void modifyMarkOpinList(List<MarkOpinVo> markOpinList) {
        
        // 검수파일 수정 
        if (markOpinList != null) {
            for (MarkOpinVo opin : markOpinList) {
                int cnt = markOpinMapper.updateMarkOpinByKey(opin);
                Verifier.eq(cnt, 1, "M1001", "word.markOpin");
            }
        }
    }
    
    /**
     * 마크업의견목록을 삭제한다. 
     */
    public void deleteMarkOpinList(List<MarkOpinVo> markOpinList) {
        
        // 마크업의견을 삭제
        if (markOpinList != null) {
            for (MarkOpinVo opin : markOpinList) {
                int cnt = markOpinMapper.deleteMarkOpinByKey(opin); 
                Verifier.eq(cnt, 1, "M1002", "word.markOpin");
            }
        }
    }
    
    /**
     * 마크업의견목록을 삭제한다. 
     */
    public void deleteMarkOpinListByKey(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo) {
    	
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	MarkOpinVo markOpin = new MarkOpinVo();
    	markOpin.setSiteCd(siteCd);
    	markOpin.setProjCd(projCd);
    	markOpin.setInspNo(inspNo);
    	markOpin.setInspFileNo(inspFileNo);
    	markOpin.setMarkNo(markNo);
    	markOpinMapper.deleteMarkOpinListByKey(markOpin);
    }

	
    /**
     * 마크업의견목록을 조회한다. 
     */
	@Override
	public List<MarkOpinVo> findMarkOpinList(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo) {
		
		String siteCd = CommonDataHolder.getCommonData().getSiteCd();
		return markOpinMapper.findMarkOpinListByKey(siteCd, projCd, inspNo, inspFileNo, markNo);
	}

}
