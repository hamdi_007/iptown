package kr.xosoft.xoip.api.common.bean;

/**
 * 공통데이터 저장소
 * @author likejy
 *
 */
public class CommonDataHolder {

    private static final ThreadLocal<CommonData> context = new ThreadLocal<CommonData>();

    private CommonDataHolder() {
    }

    /**
     * 공통데이터를 로컬 스레드에서 제거한다.
     */
    public static void resetCommonData() {
        context.remove();
    }

    /**
     * 로컬 스레드에 공통데이터를 할당한다.
     */
    public static void setCommonData(CommonData commonData) {
        if (commonData == null) {
            resetCommonData();
        } else {
            context.set(commonData);
        }
    }

    /**
     * 로컬 스레드에서 공통데이터를 반환한다.
     */
    public static CommonData getCommonData() {
        return context.get();
    }

}
