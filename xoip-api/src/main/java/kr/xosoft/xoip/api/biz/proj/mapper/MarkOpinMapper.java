package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.MarkOpinVo;

/**
 * 마크업의견 매퍼 
 * @author likejy
 *
 */
@Repository
public interface MarkOpinMapper {

    /**
     * 마크업의견 등록
     * @param markOpin
     * @return
     */
    public int insertMarkOpin(MarkOpinVo markOpin);
    
    /**
     * 마크업의견 수정
     * @param markOpin
     * @return
     */
    public int updateMarkOpinByKey(MarkOpinVo markOpin);    
    
    /**
     * 마크업의견 삭제
     * @param markOpin
     * @return
     */
    public int deleteMarkOpinByKey(MarkOpinVo markOpin);
    
    /**
     * 마크업의견목록 삭제
     * @param markOpin
     * @return
     */
    public void deleteMarkOpinListByKey(MarkOpinVo markOpin);
    
    /**
     * 마크업의견 목록조회
     * @param siteCd
     * @param projCd
     * @param inspNo
     * @Param inspFileNo
     * @Param markNo
     * @return
     */
    public List<MarkOpinVo> findMarkOpinListByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo, @Param("inspFileNo") Integer inspFileNo, @Param("markNo") Integer markNo);    
    
}
