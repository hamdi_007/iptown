package kr.xosoft.xoip.api.biz.cntr.service;

import kr.xosoft.xoip.api.biz.cntr.vo.CntrVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface CntrService {

    public CntrVo registCntr(CntrVo cntr);
        
    public CntrVo modifyCntr(CntrVo cntr);
    
    public void deleteCntr(String cntrId);
    
    public CntrVo findLocalizedCntr(String cntrId);
    
    public CntrVo findValidCntr(String cntrId);
    
    public CntrVo findCntrByCntrNo(String cntrNo);
    
    public CommonPage<CntrVo> findLocalizedCntrPage(CommonCondition condition);    
    
    public String findCntrExistYnByCompId(String compId);
    
    public String findCntrExistYnByIpId(String ipId);
    
}
