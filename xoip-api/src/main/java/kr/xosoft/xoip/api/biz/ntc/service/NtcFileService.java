package kr.xosoft.xoip.api.biz.ntc.service;

import kr.xosoft.xoip.api.biz.ntc.vo.NtcFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface NtcFileService {

    public NtcFileVo registNtcFile(NtcFileVo ntcFile);
    
    public void deleteNtcFile(Integer ntcNo, Integer ntcFileNo);
    
    public NtcFileVo findNtcFile(Integer ntcNo, Integer ntcFileNo);
    
    public CommonPage<NtcFileVo> findNtcFilePage(Integer ntcNo, CommonCondition condition);
    
}
