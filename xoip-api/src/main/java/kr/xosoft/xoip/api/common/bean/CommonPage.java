package kr.xosoft.xoip.api.common.bean;

import java.util.List;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 공통페이징 
 * @author likejy
 *
 */
@Data
@Slf4j
public class CommonPage<T> {
	
    /**
     * 정렬칼럼 
     */
    private String comSrtBy;
    
    /**
     * 역정렬여부 
     */
    private Boolean comSrtDesc;
    
	/**
	 * 전체 (아이템) 개수 
	 */
	private Integer comTtlCnt;

	/**
	 * 페이지당 아이템 개수  
	 */
	private Integer comItemCntPerPage;
	
	/**
	 * 페이지 번호 
	 */
	private Integer comPageNo;

	/**
	 * (전체) 페이지 개수 
	 */
	private Integer comPageCnt;
		
	/**
	 * 결과 (로우) 목록 
	 */
	private List<T> list;
	
	/**
	 * 생성자 
	 * @param condition
	 * @param list
	 * @param comTtlCnt
	 */
	public CommonPage(CommonCondition condition, List<T> list, Integer comTtlCnt) {
		
		this.list = list;
		Verifier.notNull(list, "T9100");
				
		this.comTtlCnt = comTtlCnt;
		Verifier.notNull(comTtlCnt, "T9103");

		Verifier.notNull(condition, "T9105");
		
        this.comSrtBy = condition.getComSrtBy();
        this.comSrtDesc = condition.getComSrtDesc();
		
		this.comItemCntPerPage = condition.getComItemCntPerPage();
		Verifier.notNull(comItemCntPerPage, "T9101");
		Verifier.gt(comItemCntPerPage, 0, "T9101");
		
		this.comPageNo = condition.getComPageNo();
		Verifier.notNull(comPageNo, "T9102");
		Verifier.gt(comPageNo, 0, "T9102");

		try {
			this.comPageCnt = (int)Math.ceil((double)comTtlCnt / (double)comItemCntPerPage);
		} catch (Exception e) {
			Verifier.fail("T9104");
		} finally {
		    if (this.comPageCnt == 0) {
		        this.comPageCnt = 1;
		    }
		}
		
		if (log.isDebugEnabled()) {
			log.debug("commonPage : {}", this);
		}
	}
}
