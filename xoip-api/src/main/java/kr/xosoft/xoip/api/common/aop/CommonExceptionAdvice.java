package kr.xosoft.xoip.api.common.aop;

import javax.servlet.ServletException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import io.jsonwebtoken.ExpiredJwtException;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.MessageBean;
import kr.xosoft.xoip.api.common.exception.BizException;
import lombok.extern.slf4j.Slf4j;

/**
 * Controller 예외 처리 
 * @author likejy
 *
 */
@ControllerAdvice
@Slf4j
public class CommonExceptionAdvice {

    @Autowired
    private MessageBean messageBean;
    
    /**
     * MethodArgumentNotValidException 을 처리한다. (400)
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<CommonResult> handleException(MethodArgumentNotValidException e) {
        
        BindingResult bindingResult = e.getBindingResult();
        String messageCode = null;
        String[] messageArguments = null;
        if (bindingResult != null) {
            FieldError err = bindingResult.getFieldError();
            if (err != null) {
                messageCode = err.getDefaultMessage();
                Object[] objs = err.getArguments();
                messageArguments = new String[objs.length];
                int idx = 0;
                for (Object o : objs) {
                    if (idx == 0) {
                        messageArguments[idx] = err.getField();        
                    } else {
                        messageArguments[idx] = String.valueOf(o);    
                    }
                    idx++;
                }               
            }
        }    
                
        return commonResultFromCode(HttpStatus.BAD_REQUEST, messageCode, messageArguments, e);        
    }
    
    
    /**
     * MissingServletRequestParameterException 을 처리한다. (400)
     * @param e
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<CommonResult> handleException(MissingServletRequestParameterException e) {
        return commonResultFromCode(HttpStatus.BAD_REQUEST, "E4000", e);
    }
    
    /**
     * ServletException 을 처리한다. (400)
     * @param e
     * @return
     */
    public ResponseEntity<CommonResult> handleException(ServletException e) {
        return commonResultFromCode(HttpStatus.BAD_REQUEST, "E4000", e);     
    }
    
    /**
     * ExpiredJwtException 을 처리한다. (401)
     * @param e
     * @return
     */
    @ExceptionHandler(ExpiredJwtException.class)    
    public ResponseEntity<CommonResult> handleException(ExpiredJwtException e) {
        return commonResultFromCode(HttpStatus.UNAUTHORIZED, "E4013", e);
    }
    
    /**
     * AccessDeniedException 을 처리한다. (403)
     * @param e
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<CommonResult> handleException(AccessDeniedException e) {
        return commonResultFromCode(HttpStatus.FORBIDDEN, "E4030", e);
    }       
    
    /**
     * HttpRequestMethodNotSupportedException 을 처리한다. (404)
     * @param e
     * @return
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<CommonResult> handleException(HttpRequestMethodNotSupportedException e) {
        return commonResultFromCode(HttpStatus.NOT_FOUND, "E4040", e);
    }    
    
    /**
     * MaxUploadSizeExceededException 을 처리한다.(413)
     * @param e
     * @return
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<CommonResult> handleException(MaxUploadSizeExceededException e) {
        return commonResultFromCode(HttpStatus.PAYLOAD_TOO_LARGE, "E4130", e);
    }
    
    /**
     * HttpMediaTypeNotSupportedException 을 처리한다. (415)
     * @param e
     * @return
     */
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<CommonResult> handleException(HttpMediaTypeNotSupportedException e) {
        return commonResultFromCode(HttpStatus.PAYLOAD_TOO_LARGE, "E4150", e);
    }
    
    /**
     * DuplicateKeyException 을 처리한다. (422)
     * @param e
     * @return
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity<CommonResult> handleException(DuplicateKeyException e) {
        return commonResultFromCode(HttpStatus.UNPROCESSABLE_ENTITY, "M1006", null, e);
    }
    
    /**
     * BizException 을 처리한다. (422)
     * @param e
     * @return
     */
    @ExceptionHandler(BizException.class)
    public ResponseEntity<CommonResult> handleException(BizException e) {
        
        String messageCode = e.getCode();
        String[] messageArguments = e.getArgs();
        
        if (messageArguments != null) {
            for (int i = 0; i < messageArguments.length; i++) {
                if (!messageArguments[i].startsWith("word.")) {
                    continue;
                } else {
                    messageArguments[i] = messageBean.getMessage(messageArguments[i]);          
                }
            }
        }

        return commonResultFromCode(HttpStatus.UNPROCESSABLE_ENTITY, messageCode, messageArguments, e);        
    }        
    
    /**
     * Exception 을 처리한다. (500)
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<CommonResult> handleException(Exception e) {
        return commonResultFromCode(HttpStatus.INTERNAL_SERVER_ERROR, "E5000", e);
    }  

    /**
     * 오류코드로 CommonResult 인스턴스를 생성 후 리턴
     * @param status
     * @param errorCode
     * @param e
     * @return
     */
    private ResponseEntity<CommonResult> commonResultFromCode(HttpStatus status, String errorCode, Exception e) {

        // 로깅
        if (log.isDebugEnabled()) {
            log.debug("{}\n{}", e.toString(), ExceptionUtils.getStackTrace(e));
        } else if (log.isInfoEnabled()) {
            log.info(e.toString());
        }
        
        CommonResult result = new CommonResult();
        result.setErrorCode(errorCode);
        result.setErrorMessage(messageBean.getMessage(result.getErrorCode()));      
        return new ResponseEntity<CommonResult>(result, status);
    }
    
    /**
     * 오류코드로 CommonResult 인스턴스를 생성 후 리턴
     * @param status
     * @param errorCode
     * @param messageArguments
     * @param e
     * @return
     */
    private ResponseEntity<CommonResult> commonResultFromCode(HttpStatus status, String errorCode, String[] messageArguments, Exception e) {

        // 로깅
        if (log.isDebugEnabled()) {
            log.debug("{}\n{}", e.toString(), ExceptionUtils.getStackTrace(e));
        } else if (log.isInfoEnabled()) {
            log.info(e.toString());
        }
        
        CommonResult result = new CommonResult();
        result.setErrorCode(errorCode);
        result.setErrorMessage(messageBean.getMessage(result.getErrorCode(), messageArguments));      
        return new ResponseEntity<CommonResult>(result, status);
    }    
     
}
