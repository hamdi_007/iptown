package kr.xosoft.xoip.api.biz.faq.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.faq.vo.FaqFileVo;
import kr.xosoft.xoip.api.biz.faq.vo.FaqVo;

/**
 * FAQ 매퍼 
 * @author hypark
 *
 */
@Repository
public interface FaqMapper {

   /**
    * FAQ 등록
    * @param faq
    * @return
    */
   public int insertFaq(FaqVo faq);
   
   /**
    * FAQ 수정 by PK
    * @param faq
    * @return
    */
   public int updateFaqByKey(FaqVo faq);
       
   /**
    * FAQ 삭제 by PK
    * @param faq
    * @return
    */
   public int deleteFaqByKey(FaqVo faq);
   
   /**
    * FAQ 구분코드 조회 by PK
    * @param siteCd
 * @param faq 
    * @return
    */
   public List<FaqVo> findFaqDivCd(@Param("siteCd") String siteCd, @Param("comLang") String comLang);   
   
   /**
    * FAQ 조회 by PK
    * @param siteCd
    * @param faqNo
    * @return
    */
   public FaqVo findFaqByKey(@Param("siteCd") String siteCd, @Param("faqNo") Integer faqNo);   
   
   /**
    * FAQ 목록 건수조회 by 검색조건
    * @param map
    * @return
    */
   public int countFaqListByMap(Map<String, Object> map);
   
   /**
    * FAQ 목록조회 by 검색조건
    * @param map
    * @return
    */
   public List<FaqVo> findFaqListByMap(Map<String, Object> map);    
   
   /**
    * 라이선시용 FAQ 목록 건수조회 by 검색조건
    * @param map
    * @return
    */
   public int countLiceFaqListByMap(Map<String, Object> map);
   
   /**
    * 라이선시용 FAQ 목록조회 by 검색조건
    * @param map
    * @return
    */
   public List<FaqVo> findLiceFaqListByMap(Map<String, Object> map);  
   
}
