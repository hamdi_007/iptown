package kr.xosoft.xoip.api.common.auth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.xosoft.xoip.api.biz.usr.vo.LgnFormVo;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.MessageBean;
import kr.xosoft.xoip.api.common.support.RequestSupport;
import lombok.extern.slf4j.Slf4j;

/**
 * JWT 로그인 필터 
 * @author likejy
 *
 */
@Slf4j
public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private MessageBean messageBean;
    
    /**
     * 생성자 
     * @param authenticationManager
     */
    public JwtLoginFilter(AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher("/api/*/auth/login", "POST"));
        setAuthenticationManager(authenticationManager);
    }

    /**
     * 로그인 처리 
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        
        LgnFormVo lgnForm = null;
        try {
            lgnForm = objectMapper.readValue(request.getInputStream(), LgnFormVo.class);
        } catch (Exception e) {
            lgnForm = new LgnFormVo();
        }
        log.debug("lgnForm : {}", lgnForm);
        

        CommonData commonData = CommonData.builder()
                .siteCd(RequestSupport.parseSiteCd(request))  
                .ipcAddr(RequestSupport.parseClientIpAddress(request))
                .build();        
        CommonDataHolder.setCommonData(commonData);

        Authentication auth = null;
        try {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(lgnForm.getEmail(), lgnForm.getPwd());
            auth = getAuthenticationManager().authenticate(authenticationToken);
        } finally {
            CommonDataHolder.resetCommonData();
        }

        return auth;        
    }

    /**
     * 로그인 성공 
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {

        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
    }
    
    /**
     * 로그인 실패 
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        
        SecurityContextHolder.clearContext();
        if (log.isInfoEnabled()) {
            log.info("Authentication request failed: {}", failed.toString());
        }

        CommonResult result = new CommonResult();
        
        if (failed instanceof AuthenticationServiceException) {
            result.setErrorCode("E5000");
            result.setErrorMessage(messageBean.getMessage(result.getErrorCode()));
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());            
        } else {
            result.setErrorCode("E4010");
            result.setErrorMessage(messageBean.getMessage(result.getErrorCode()));
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(result));
    }    
}
