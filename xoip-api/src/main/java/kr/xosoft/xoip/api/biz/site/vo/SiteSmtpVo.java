package kr.xosoft.xoip.api.biz.site.vo;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 사이트 SMTP VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class SiteSmtpVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * SMTP프로토콜
     */
    private String smtpPrtc;

    /**
     * 서버호스트
     */
    private String svrHost;

    /**
     * 포트번호
     */
    private Integer portNo;

    /**
     * SMTP사용자ID
     */
    private String smtpUsrId;

    /**
     * SMTP사용자비밀번호
     */
    private String smtpUsrPwd;

    /**
     * 전송자이메일
     */
    private String trnsrEmail;

    /**
     * 전송자명
     */
    private String trnsrNm;

    /**
     * SMTP사용여부
     */
    private String smtpUseYn;

    /**
     * 인증여부
     */
    private String atrzYn;

    /**
     * TLS사용여부
     */
    private String tlsUseYn;

    /**
     * SSL사용여부
     */
    private String sslUseYn;

    /**
     * 상단템플릿
     */
    private String headTmpl;

    /**
     * 하단템플릿
     */
    private String footTmpl;    
    
}
