package kr.xosoft.xoip.api.biz.proj.service;


import java.util.List;

import kr.xosoft.xoip.api.biz.proj.vo.MarkOpinVo;

public interface MarkOpinService {

    public List<MarkOpinVo> mergeMarkOpinList(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo, List<MarkOpinVo> markupOpinList);
    
    public void deleteMarkOpinListByKey(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo);

    public List<MarkOpinVo> findMarkOpinList(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo);
    
}
