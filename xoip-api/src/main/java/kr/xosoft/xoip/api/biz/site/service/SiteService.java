package kr.xosoft.xoip.api.biz.site.service;

import kr.xosoft.xoip.api.biz.site.vo.SiteSmtpVo;

public interface SiteService {

    public SiteSmtpVo findLocalizedSiteSmtp(String tmplLang);
    
}
