package kr.xosoft.xoip.api.biz.dashboard.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.dashboard.mapper.DashboardMapper;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;


/**
 * 대시보드 서비스 Impl
 * @author luna1819
 *
 */
@Transactional
@Service
public class DashboardServiceImpl implements DashboardService {

    
    @Autowired
    private DashboardMapper dashboardMapper;
    
    @Override
    public int countCompNum() {
    	
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	return dashboardMapper.countCompCnt(siteCd);
    }
    
    @Override
    public int countUsrNum() {
    	
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	return dashboardMapper.countUsrCnt(siteCd);
    }
    
    @Override
    public int countIpNum() {
    	
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	return dashboardMapper.countIpCnt(siteCd);
    }
    
    @Override
    public int countCntrNum() {
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	return dashboardMapper.countCntrCnt(siteCd);
    }
    
    @Override
    public int countAllPorjNum() {
    
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	return dashboardMapper.countAllProjCnt(siteCd);
    }
    
    @Override
    public int countModProjNum() {
    	
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	return dashboardMapper.countModProjCnt(siteCd);
    }
    
    @Override
    public int countProgProjNum() {
    	
    	String siteCd = CommonDataHolder.getCommonData().getSiteCd();
    	return dashboardMapper.countProgProjCnt(siteCd);
    }
    
   @Override
    public int countCmplProjNum() {
	   
	   String siteCd = CommonDataHolder.getCommonData().getSiteCd();
	   return dashboardMapper.countCmplProjCnt(siteCd);
    } 
    
       

}
