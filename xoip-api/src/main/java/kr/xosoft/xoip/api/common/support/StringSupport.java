package kr.xosoft.xoip.api.common.support;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.time.DateUtils;

import kr.xosoft.xoip.api.common.exception.BizException;
import lombok.extern.slf4j.Slf4j;

/**
 * 문자열 서포트 
 * @author likejy
 *
 */
@Slf4j
public class StringSupport {

    /**
     * 카멜케이스 문자열을 언더바 문자열로 변환
     * @param camelCase
     * @return
     */
    public static final String camelCaseToUnderScore(String camelCase) {
        if (camelCase == null) {
            return null;
        }
        return camelCase.replaceAll("([A-Z0-9]+)","\\_$1").toLowerCase(); 
    }
    
    /**
     * 랜덤한 len 길이의 문자열을 생성하여 리턴
     * @param len
     * @return
     */
    public static final String randomString(int len) {
        Random rand = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            int index = rand.nextInt(3);
            switch (index) {
                case 0:
                    sb.append((char)(rand.nextInt(26) + 97));
                    break;
                case 1:
                    sb.append((char)(rand.nextInt(26) + 65));
                    break;
                case 2: 
                    sb.append(rand.nextInt(10));
                    break;
            }
        }
        return sb.toString();
    }
    
    /**
     * URL 인코딩
     * @param value
     * @return
     */
    public static final String encodeValue(String value) {
        String encoded = null;
        try {
            encoded = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            log.warn("{} : failed to encode value ({})", e.toString(), value);
        }
        return encoded;
    }

    /**
     * URL 결합
     * @param v1
     * @param v2
     * @return
     */
    public static final String joinUrl(String v1, String v2) {
        if (v1 == null || v2 == null) {
            return null;
        }
        String rtn = null;
        if (v1.endsWith("/")) {
            if (v2.startsWith("/")) {
                rtn = v1 + v2.substring(1);
            } else {
                rtn = v1 + v2;
            }
        } else {
            if (v2.startsWith("/")) {
                rtn = v1 + v2;
            } else {
                rtn = v1 + "/" + v2;
            }
        }
        return rtn;
    }
    
    public static final Date parseDate(String str, String pattern) {
        try {
            return DateUtils.parseDate(str, pattern);
        } catch (ParseException e) {
            throw new BizException("M1300");
        }
    }
    
}
