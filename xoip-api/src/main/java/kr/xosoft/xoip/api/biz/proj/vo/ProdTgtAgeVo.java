package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 상품대상연령 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProdTgtAgeVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    private String projCd;

    /**
     * 연령코드
     */
    private String ageCd;

    /**
     * 등록일시
     */
    private Date regDt;

    /**
     * 등록자ID
     */
    private String regrId;
    
    public ProdTgtAgeVo() {
    }
    
    public ProdTgtAgeVo(String projCd, String ageCd) {
        setProjCd(projCd);
        setAgeCd(ageCd);
    }         
    
}
