package kr.xosoft.xoip.api.biz.prodtyp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.biz.prodtyp.mapper.ProdTypHistMapper;
import kr.xosoft.xoip.api.biz.prodtyp.mapper.ProdTypMapper;
import kr.xosoft.xoip.api.biz.prodtyp.vo.ProdTypHistVo;
import kr.xosoft.xoip.api.biz.prodtyp.vo.ProdTypVo;
import kr.xosoft.xoip.api.biz.proj.service.ProjService;

/**
 * 상품유형 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class ProdTypServiceImpl implements ProdTypService {

	@Autowired
    private ProjService projService;
	
    @Autowired
    private ProdTypMapper prodTypMapper;
    
    @Autowired
    private ProdTypHistMapper prodTypHistMapper;
    
    /**
     * 상품유형 1단계를 등록한다. 
     */
    @Override
    public ProdTypVo registProdTyp1(ProdTypVo prodTyp) {
        
    	// 상품유형 1단계 코드 중복 확인
    	ProdTypVo fetchedProdTyp = findProdTypByProdTypCd(prodTyp.getProdTypCd());
        Verifier.isNull(fetchedProdTyp, "M6000");
    	
        // 상품유형 1단계 등록 
        int cnt = prodTypMapper.insertProdTyp(prodTyp);
        Verifier.eq(cnt, 1, "M1000", "word.prodTyp");

        // 상품유형 1단계 이력 등록 
        ProdTypHistVo prodTypHist = new ProdTypHistVo(prodTyp.getProdTypId(), "HD0100");
        prodTypHistMapper.insertProdTypHistAsSelectMaster(prodTypHist);
        Verifier.notNull(prodTypHist.getProdTypHistSeqno(), "M1000", "word.prodTypHist");
        
        return prodTyp;
    }
    
    /**
     * 상품유형 2단계를 등록한다. 
     */
    @Override
    public ProdTypVo registProdTyp2(ProdTypVo prodTyp) {
        
    	// 상품유형 2단계 코드 중복 확인
    	ProdTypVo fetchedProdTypLv2 = findProdTypByProdTypCd(prodTyp.getProdTypCd());
        Verifier.isNull(fetchedProdTypLv2, "M6001");
    	
        // 상품유형 1단계 코드 유무 확인
        ProdTypVo fetchedProdTypLv1 = findProdTyp(prodTyp.getTopProdTypId());
        Verifier.notNull(fetchedProdTypLv1, "M6002", "word.prodTypeCd1");
        
        // 상품유형 2단계 코드 앞 두글자와 상품유형 1단계 코드가 동일한지 확인
        Verifier.isEqual(prodTyp.getProdTypCd().substring(0, 2), fetchedProdTypLv1.getProdTypCd(), "M6005");
        
        // 상품유형 2단계 등록 
        int cnt = prodTypMapper.insertProdTyp(prodTyp);
        Verifier.eq(cnt, 1, "M1000", "word.prodTyp");

        // 상품유형 2단계 이력 등록 
        ProdTypHistVo prodTypHist = new ProdTypHistVo(prodTyp.getProdTypId(), "HD0100");
        prodTypHistMapper.insertProdTypHistAsSelectMaster(prodTypHist);
        Verifier.notNull(prodTypHist.getProdTypHistSeqno(), "M1000", "word.prodTypHist");
        
        return prodTyp;
    }
    
    /**
     * 상품유형을 수정한다. 
     */
    @Override
    public ProdTypVo modifyProdTyp(ProdTypVo prodTyp) {
        
        // 상품유형 조회 
        ProdTypVo fetchedProdTyp = findProdTyp(prodTyp.getProdTypId());
        Verifier.notNull(fetchedProdTyp, "M1003", "word.prodTyp");
        
        fetchedProdTyp.setProdTypNm(prodTyp.getProdTypNm());
        fetchedProdTyp.setProdTypEngNm(prodTyp.getProdTypEngNm());
        fetchedProdTyp.setUseYn(prodTyp.getUseYn());
        fetchedProdTyp.setSrtOrdno(prodTyp.getSrtOrdno());
        fetchedProdTyp.setDtlDesc(prodTyp.getDtlDesc());
        fetchedProdTyp.setEngDtlDesc(prodTyp.getEngDtlDesc());
        
        // 상품유형 수정 
        int cnt = prodTypMapper.updateProdTypByKey(fetchedProdTyp);
        Verifier.eq(cnt, 1, "M1001", "word.prodTyp");

        // 상품유형 이력 등록 
        ProdTypHistVo prodTypHist = new ProdTypHistVo(prodTyp.getProdTypId(), "HD0200");
        prodTypHistMapper.insertProdTypHistAsSelectMaster(prodTypHist);
        Verifier.notNull(prodTypHist.getProdTypHistSeqno(), "M1000", "word.prodTypHist");
        
        return prodTyp;        
    }

    /**
     * 상품유형 1단계를 삭제한다. 
     */
    @Override
    public void deleteProdTyp1(String prodTypId) {
        
    	// 상품유형 1단계가 프로젝트에 한 건이라도 사용된 경우 상품유형 1단계 삭제 불가 
        String existYn = projService.findProjExistYnByTopProdTypId(prodTypId);
        Verifier.isEqual(existYn, "N", "M6003");
    	
        // 상품유형 삭제 
        ProdTypVo prodTyp = new ProdTypVo();
        prodTyp.setProdTypId(prodTypId);
        int cnt = prodTypMapper.deleteProdTypByKey(prodTyp); 
        Verifier.eq(cnt, 1, "M1002", "word.prodTyp");
        
        // 상품유형 이력 등록 
        ProdTypHistVo prodTypHist = new ProdTypHistVo(prodTypId, "HD0300");
        prodTypHistMapper.insertProdTypHistAsSelectMaster(prodTypHist);
        Verifier.notNull(prodTypHist.getProdTypHistSeqno(), "M1000", "word.prodTypHist");        
        
    }
    
    /**
     * 상품유형 2단계를 삭제한다. 
     */
    @Override
    public void deleteProdTyp2(String prodTypId) {
        
    	// 상품유형 2단계가 프로젝트에 한 건이라도 사용된 경우 상품유형 2단계 삭제 불가 
        String existYn = projService.findProjExistYnByProdTypId(prodTypId);
        Verifier.isEqual(existYn, "N", "M6004");
    	
        // 상품유형 삭제 
        ProdTypVo prodTyp = new ProdTypVo();
        prodTyp.setProdTypId(prodTypId);
        int cnt = prodTypMapper.deleteProdTypByKey(prodTyp); 
        Verifier.eq(cnt, 1, "M1002", "word.prodTyp");
        
        // 상품유형 이력 등록 
        ProdTypHistVo prodTypHist = new ProdTypHistVo(prodTypId, "HD0300");
        prodTypHistMapper.insertProdTypHistAsSelectMaster(prodTypHist);
        Verifier.notNull(prodTypHist.getProdTypHistSeqno(), "M1000", "word.prodTypHist");        
        
    }

    /**
     * 상품유형을 조회한다. 
     */
    @Override
    public ProdTypVo findProdTyp(String prodTypId) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return prodTypMapper.findProdTypByKey(siteCd, prodTypId);
    }
    
    /**
     * 상품유형코드로 상품유형을 조회한다. 
     */
    public ProdTypVo findProdTypByProdTypCd(String prodTypCd) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return prodTypMapper.findProdTypByProdTypCd(siteCd, prodTypCd);       
    }
    
    /**
     * 현재 시점에 유효한 상품유형을 조회한다. 
     */
    public ProdTypVo findValidProdTyp(String prodTypId) {
        
        ProdTypVo prodTyp = findProdTyp(prodTypId);
        Verifier.notNull(prodTyp, "M1003", "word.prodTyp");
        Verifier.isEqual(prodTyp.getUseYn(), "Y", "M1007", "word.prodTyp");      
        
        if (prodTyp.getTopProdTypId() != null) {
            ProdTypVo topProdTyp = findProdTyp(prodTyp.getTopProdTypId());
            Verifier.isEqual(topProdTyp.getUseYn(), "Y", "M1007", "word.prodTyp");                  
        }
        
        return prodTyp;
    }
    
    /**
     * 상품유형목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<ProdTypVo> findProdTypPage(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        
        int comTtlCnt = prodTypMapper.countProdTypListByMap(map);
        List<ProdTypVo> prodTypList = prodTypMapper.findProdTypListByMap(map);
        
        return new CommonPage<ProdTypVo>(condition, prodTypList, comTtlCnt);           
    }

    /**
     * 상품유형 목록을 조회한다. 
     */
    @Override
    public List<ProdTypVo> findProdTypListAt(Integer prodTypLv, String topProdTypId, String useYn) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return prodTypMapper.findProdTypListByProdTypLvAndTopProdTypId(siteCd, prodTypLv, topProdTypId, useYn);
    }
    
}
