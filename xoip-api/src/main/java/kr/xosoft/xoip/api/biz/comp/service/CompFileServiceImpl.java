package kr.xosoft.xoip.api.biz.comp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.comp.mapper.CompFileMapper;
import kr.xosoft.xoip.api.biz.comp.vo.CompFileVo;
import kr.xosoft.xoip.api.biz.comp.vo.CompVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.support.AuthSupport;

/**
 * 회사파일 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class CompFileServiceImpl implements CompFileService {

    @Autowired
    private CompService compService;
    
    @Autowired
    private CompFileMapper compFileMapper;
    
    /**
     * 회사파일을 등록한다. 
     */
    @Override
    public CompFileVo registCompFile(CompFileVo compFile) {

        CommonData commonData = CommonDataHolder.getCommonData();

        // 라이선시는 본인 회사 정보만 등록 가능하다. 
        if (AuthSupport.isLicensee(commonData.getAuth())) {
            Verifier.isEqual(compFile.getCompId(), commonData.getCompId(), "M3003");            
        }
        
        // 회사가 존재하는지 확인 
        CompVo comp = compService.findComp(compFile.getCompId());
        Verifier.notNull(comp, "M1004", "word.compId");
        
        // 회사파일 등록 
        int cnt = compFileMapper.insertCompFile(compFile);
        Verifier.eq(cnt, 1, "M1000", "word.compFile");

        return compFile;
    }

    /**
     * 회사파일을 수정한다. 
     */
    @Override
    public CompFileVo modifyCompFile(CompFileVo compFile) {
        
        CommonData commonData = CommonDataHolder.getCommonData();

        // 라이선시는 본인 회사파일만 수정 가능하다. 
        if (AuthSupport.isLicensee(commonData.getAuth())) {
            Verifier.isEqual(compFile.getCompId(), commonData.getCompId(), "M3004");            
        }
        
        // 회사파일 조회 
        CompFileVo fetchedCompFile = findCompFile(compFile.getCompId(), compFile.getCompFileNo());
        Verifier.notNull(fetchedCompFile, "M1003", "word.compFile");
        
        fetchedCompFile.setCompFileDivCd(compFile.getCompFileDivCd());
        fetchedCompFile.setMngFileNm(compFile.getMngFileNm());
        
        // 회사파일 수정 
        int cnt = compFileMapper.updateCompFileByKey(fetchedCompFile);
        Verifier.eq(cnt, 1, "M1001", "word.compFile");

        return fetchedCompFile;        
    }

    /**
     * 회사파일을 삭제한다. 
     */
    @Override
    public void deleteCompFile(String compId, Integer compFileNo) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 라이선시는 본인 회사파일만 삭제 가능하다. 
        if (AuthSupport.isLicensee(commonData.getAuth())) {
            Verifier.isEqual(compId, commonData.getCompId(), "M3005");            
        }
        
        // 회사파일 삭제 
        CompFileVo compFile = new CompFileVo();
        compFile.setCompId(compId);
        compFile.setCompFileNo(compFileNo);
        int cnt = compFileMapper.deleteCompFileByKey(compFile); 
        Verifier.eq(cnt, 1, "M1002", "word.compFile");        
    }

    /**
     * 회사파일을 조회한다. 
     */
    @Override
    public CompFileVo findCompFile(String compId, Integer compFileNo) {

        CommonData commonData = CommonDataHolder.getCommonData();

        // 라이선시는 본인 회사파일만 조회 가능하다. 
        if (AuthSupport.isLicensee(commonData.getAuth())) {
            Verifier.isEqual(compId, commonData.getCompId(), "M3006");            
        }
        
        return compFileMapper.findCompFileByKey(commonData.getSiteCd(), compId, compFileNo);
    }

    /**
     * 회사파일 목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<CompFileVo> findCompFilePage(String compId, CommonCondition condition) {

        CommonData commonData = CommonDataHolder.getCommonData();

        // 라이선시는 본인 회사파일만 조회 가능하다. 
        if (AuthSupport.isLicensee(commonData.getAuth())) {
            Verifier.isEqual(compId, commonData.getCompId(), "M3006");            
        }        
        
        Map<String, Object> map = condition.toMap();
        map.put("compId", compId);
        
        int comTtlCnt = compFileMapper.countCompFileListByMap(map);
        List<CompFileVo> compList = compFileMapper.findCompFileListByMap(map);
        
        return new CommonPage<CompFileVo>(condition, compList, comTtlCnt);           
    }

}
