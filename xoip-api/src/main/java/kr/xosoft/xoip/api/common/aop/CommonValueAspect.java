package kr.xosoft.xoip.api.common.aop;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;

/**
 * 매퍼 호출 어스펙트  
 * @author likejy
 *
 */
@Aspect
@Component
public class CommonValueAspect {
    
    private static final String SYSTEM_USR_ID = "SYSTEM"; 
    
    @Pointcut("execution(* kr.xosoft.xoip.api.biz.**.mapper.*Mapper.insert*(..))")
    public void insertQuery() {}
    
    @Pointcut("execution(* kr.xosoft.xoip.api.biz.**.mapper.*Mapper.update*(..))")
    public void updateQuery() {}
    
    @Pointcut("execution(* kr.xosoft.xoip.api.biz.**.mapper.*Mapper.delete*(..))")
    public void deleteQuery() {}
        
    @Before("insertQuery() || updateQuery() || deleteQuery()")
    public void assignModDtAndModUsrId(JoinPoint jp) {
        CommonData commonData = CommonDataHolder.getCommonData();
        Object[] objs = jp.getArgs();
        for (Object o : objs) {
            if (o instanceof BaseVo) {
            	BaseVo base = (BaseVo)o;
            	base.setModDt(new Date());
                if (commonData == null || commonData.getUsrId() == null) {
                    base.setModrId(SYSTEM_USR_ID);
                } else {
                    base.setModrId(commonData.getUsrId());
                }
            }
        }
    }
        
}
