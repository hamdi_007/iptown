package kr.xosoft.xoip.api.biz.site.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.biz.site.mapper.SiteSmtpMapper;
import kr.xosoft.xoip.api.biz.site.vo.SiteSmtpVo;

/**
 * 사이트 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class SiteServiceImpl implements SiteService {

    @Autowired
    private SiteSmtpMapper siteSmtpMapper;
    
    /**
     * 템플릿 언어에 맞는 사이트 SMTP 정보를 조회한다.
     * IMPORTANT : 
     * 이메일 전송의 경우 시스템을 사용하는 사용자의 언어코드를 사용하는 대신 
     * 소속 회사의 국가코드에 기반한 언어코드를 사용해야 한다. (CommonData 의 lang 값을 사용하면 안됨)     
     * @return
     */
    @Override
    public SiteSmtpVo findLocalizedSiteSmtp(String tmplLang) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return siteSmtpMapper.findLocalizedSiteSmtpByKey(commonData.getSiteCd(), tmplLang);
    }
    
}
