package kr.xosoft.xoip.api.biz.stmpappl.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.stmpappl.mapper.StmpLotHistMapper;
import kr.xosoft.xoip.api.biz.stmpappl.mapper.StmpLotMapper;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpLotHistVo;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpLotVo;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * 증지품목 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class StmpLotServiceImpl implements StmpLotService {

    @Autowired
    private StmpLotMapper stmpLotMapper;
    
    @Autowired
    private StmpLotHistMapper stmpLotHistMapper;
    
    /**
     * 증지품목목록을 등록/수정/삭제한다. 
     */
    @Override
    public List<StmpLotVo> mergeStmpLotList(String projCd, Integer stmpApplNo, List<StmpLotVo> stmpLotList) {

        List<StmpLotVo> createdList = new ArrayList<StmpLotVo>();
        List<StmpLotVo> updatedList = new ArrayList<StmpLotVo>();
        List<StmpLotVo> deletedList = findStmpLotList(projCd, stmpApplNo);
        
        // 등록/수정/삭제대상 목록 필터링    
        if (stmpLotList != null) {
            for (StmpLotVo stmpLot : stmpLotList) {
                // 품목번호가 없는 경우는 신규 등록 품목인 경우임 
                if (stmpLot.getLotNo() == null) {
                    createdList.add(stmpLot);
                    continue;
                }
                int idx = -1;
                for (int i = 0; i < deletedList.size(); i++) {
                    if (deletedList.get(i).getLotNo() == stmpLot.getLotNo()) {
                        idx = i;
                        break;
                    }
                }
                if (idx >= 0) {
                    deletedList.remove(idx);
                    updatedList.add(stmpLot);
                } else {
                    createdList.add(stmpLot);
                }
            }
        } 
        
        // 증지품목 삭제 
        deleteStmpLotList(deletedList);
        
        // 저장된 증지품목 값 수정
        modifyStmpLotList(updatedList);
        
        // 신규 증지품목 등록 
        registStmpLotList(createdList);
        
        return stmpLotList;        
    }

    /**
     * 증지품목 목록을 등록한다. 
     * @param stmpLotList
     */
    private void registStmpLotList(List<StmpLotVo> stmpLotList) {

        if (stmpLotList == null) {
            return;
        }
        for (StmpLotVo stmpLot : stmpLotList) {
            
            // 증지품목 등록
            int cnt = stmpLotMapper.insertStmpLot(stmpLot);
            Verifier.eq(cnt, 1, "M1000", "word.stmpLot");
            
            // 증지품목 이력 등록 
            StmpLotHistVo stmpLotHist = new StmpLotHistVo(stmpLot.getProjCd(), stmpLot.getStmpApplNo(), stmpLot.getLotNo(), "HD0100");
            stmpLotHistMapper.insertStmpLotHistAsSelectMaster(stmpLotHist);
            Verifier.notNull(stmpLotHist.getStmpLotHistSeqno(), "M1000", "word.stmpLotHist");                
        }
    }
    
    /**
     * 증지품목 목록을 수정한다. 
     * @param stmpLotList
     */
    private void modifyStmpLotList(List<StmpLotVo> stmpLotList) {
        
        if (stmpLotList == null) {
            return;
        }
        for (StmpLotVo stmpLot : stmpLotList) {
            
            // 증지품목 수정 
            int cnt = stmpLotMapper.updateStmpLotByKey(stmpLot);
            Verifier.eq(cnt, 1, "M1000", "word.stmpLot");
            
            // 증지품목 이력 등록 
            StmpLotHistVo stmpLotHist = new StmpLotHistVo(stmpLot.getProjCd(), stmpLot.getStmpApplNo(), stmpLot.getLotNo(), "HD0200");
            stmpLotHistMapper.insertStmpLotHistAsSelectMaster(stmpLotHist);
            Verifier.notNull(stmpLotHist.getStmpLotHistSeqno(), "M1000", "word.stmpLotHist");                                
        }
    }
    
    /**
     * 증지품목 목록을 삭제한다. 
     * @param stmpLotList
     */
    private void deleteStmpLotList(List<StmpLotVo> stmpLotList) {
        
        if (stmpLotList == null) {
            return;
        }
        for (StmpLotVo stmpLot : stmpLotList) {
            
            // 증지품목 삭제 
            int cnt = stmpLotMapper.deleteStmpLotByKey(stmpLot);
            Verifier.eq(cnt, 1, "M1000", "word.stmpLot");
            
            // 증지품목 이력 등록 
            StmpLotHistVo stmpLotHist = new StmpLotHistVo(stmpLot.getProjCd(), stmpLot.getStmpApplNo(), stmpLot.getLotNo(), "HD0300");
            stmpLotHistMapper.insertStmpLotHistAsSelectMaster(stmpLotHist);
            Verifier.notNull(stmpLotHist.getStmpLotHistSeqno(), "M1000", "word.stmpLotHist");                                                
        }
    }
    
    /**
     * 증지품목 목록을 조회한다. 
     */
    @Override
    public List<StmpLotVo> findStmpLotList(String projCd, Integer stmpApplNo) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return stmpLotMapper.findStmpLotListByStmpApplKey(commonData.getSiteCd(), projCd, stmpApplNo);
    }
}
