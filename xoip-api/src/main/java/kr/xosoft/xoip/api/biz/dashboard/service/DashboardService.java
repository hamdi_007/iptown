package kr.xosoft.xoip.api.biz.dashboard.service;


public interface DashboardService {
    
    public int countCompNum();
    
    public int countUsrNum();
    
    public int countIpNum();
    
    public int countCntrNum();
    
    public int countAllPorjNum();
    
    public int countModProjNum();
    
    public int countProgProjNum();
    
    public int countCmplProjNum();
    
    
}
