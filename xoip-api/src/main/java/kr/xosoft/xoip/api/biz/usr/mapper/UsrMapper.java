package kr.xosoft.xoip.api.biz.usr.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.usr.vo.UsrCompRsltVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrVo;

/**
 * 사용자 매퍼 
 * @author likejy
 *
 */
@Repository
public interface UsrMapper {

    /**
     * 사용자 등록
     * @param usr
     * @return
     */
    public int insertUsr(UsrVo usr);
    
    /**
     * 사용자 수정 by PK
     * @param usr
     * @return
     */
    public int updateUsrByKey(UsrVo usr);
    
    /**
     * 비밀번호 변경 by PK 
     * @param usr
     * @return
     */
    public int updatePwdByKey(UsrVo usr);
    
    /**
     * 사용자 삭제 by PK
     * @param usr
     * @return
     */
    public int deleteUsrByKey(UsrVo usr);
    
    /**
     * 사용자 조회 by PK
     * @param siteCd
     * @param usrId
     * @return
     */
    public UsrVo findUsrByKey(@Param("siteCd") String siteCd, @Param("usrId") String usrId);
    
    /**
     * 사용자 조회 by email
     * @param siteCd
     * @param email
     * @return
     */
    public UsrVo findUsrByEmail(@Param("siteCd") String siteCd, @Param("email") String email);

    /**
     * 사용자 및 회사 조회 by 검색조건 
     * @param siteCd
     * @param usrId
     * @return
     */
    public UsrCompRsltVo findUsrCompRsltByKey(@Param("siteCd") String siteCd, @Param("usrId") String usrId);
    
    /**
     * 사용자 및 회사 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countUsrCompRsltListByMap(Map<String, Object> map);
    
    /**
     * 사용자 및 회사 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<UsrCompRsltVo> findUsrCompRsltListByMap(Map<String, Object> map);
    
    /**
     * 사용자 상태값이 정상인 이메일 조회 by 핸드폰번호, 사용자명
     * @param bizno
     * @param usrNm
     * @return
     */
    public List<String> findEmailListInNormalByHnpnoAndUsrNm(@Param("siteCd") String siteCd, @Param("hnpno") String hnpno, @Param("usrNm") String usrNm);
    
    /**
     * 사용자 상태값이 정상인 이메일 조회 by 회사Id
     * @param compId
     * @return
     */
    public List<String> findEmailListInNormalByCompId(@Param("siteCd") String siteCd, @Param("compId") String compId);
    
}
