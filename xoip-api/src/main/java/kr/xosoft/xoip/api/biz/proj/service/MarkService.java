package kr.xosoft.xoip.api.biz.proj.service;


import kr.xosoft.xoip.api.biz.proj.vo.MarkUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.MarkVo;

public interface MarkService {

    public MarkUnionVo registMarkUnion(MarkUnionVo markUnion);
    
    public MarkUnionVo modifyMarkUnion(MarkUnionVo markUnion);
    
    public void deleteMarkUnion(String projCd, Integer inspNo, Integer inspFileNo, Integer markNo);

    public MarkVo findMarkByKey(String projCd, Integer inspNo, Integer inspFileNo);
    
    public MarkUnionVo findMarkUnionByKey(String projCd, Integer inspNo, Integer inspFileNo);
    
}
