package kr.xosoft.xoip.api.common.support;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import kr.xosoft.xoip.api.common.bean.Verifier;

public class BizSupport {

    /**
     * 회사 국가코드 값에 해당하는 이메일 언어코드를 조회 
     * @param ntnCd
     * @return
     */
    public static String emailTemplateLangFromCompNtnCd(String ntnCd) {
        if (ntnCd == null) {
            return "en";
        } else if ("SNKR00".equals(ntnCd)) {
            return "ko";
        } else {
            return "en";
        }
    }
    
    /**
     * 프로퍼티 파일의 webUrl 은 사이트코드^URL 형태로 입력되며, 다건인 경우 "," 로 구분되어 입력된다.  
     * @param siteCd
     * @param webUrl
     * @return
     */
    public static String parseSiteWebUrl(String siteCd, String webUrl) {
        String[] arr1 = StringUtils.split(webUrl, ',');
        if (arr1 == null || arr1.length == 0) {
            Verifier.fail("M9002", new String[] {"webUrl"});
        }        
        Map<String, String> map = new HashMap<String, String>();
        for (String val : arr1) {
            String[] arr2 = StringUtils.split(val, '^');
            if (arr2.length >= 2) {
                map.put(StringUtils.trim(arr2[0]), StringUtils.trim(arr2[1]));                    
            }
        }            
        String siteWebUrl = map.get(siteCd);
        if (siteWebUrl == null) {
            Verifier.fail("M9002", new String[] {"webUrl"});
        }
        return siteWebUrl;
    }
    
}
