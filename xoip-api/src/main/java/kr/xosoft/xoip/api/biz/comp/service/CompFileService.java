package kr.xosoft.xoip.api.biz.comp.service;

import kr.xosoft.xoip.api.biz.comp.vo.CompFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface CompFileService {

    public CompFileVo registCompFile(CompFileVo compFile);
        
    public CompFileVo modifyCompFile(CompFileVo compFile);
    
    public void deleteCompFile(String compId, Integer compFileNo);
    
    public CompFileVo findCompFile(String compId, Integer compFileNo);
    
    public CommonPage<CompFileVo> findCompFilePage(String compId, CommonCondition condition);
    
}
