package kr.xosoft.xoip.api.biz.com.vo;

import lombok.Data;

@Data
public class JusoResultUnionVo {

    private JusoResultVo results;
    
}
