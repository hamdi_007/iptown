package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트변경이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjChgHistVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    private String projCd;

    /**
     * 프로젝트이력번호
     */
    private Integer projHistNo;

    /**
     * 프로젝트이력구분코드
     */
    private String projHistDivCd;

    /**
     * 프로젝트이력구분코드명  
     */
    private String projHistDivCdNm;
    
    /**
     * 이전값
     */
    private String befVal;

    /**
     * 이후값
     */
    private String aftVal;
    
    /**
     * 이력사유내용
     */
    private String histRsnCnts;
    
    /**
     * 등록일 
     */
    private Date regDt;
    
    /**
     * 등록자ID
     */
    private String regrId;
    
    /**
     * 등록자명
     */
    private String regrNm;
    
}
