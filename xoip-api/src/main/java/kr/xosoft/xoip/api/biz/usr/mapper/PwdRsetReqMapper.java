package kr.xosoft.xoip.api.biz.usr.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetReqVo;

/**
 * 비밀번호 재설정 요청 매퍼 
 * @author likejy
 *
 */
@Repository
public interface PwdRsetReqMapper {

    /**
     * 비밀번호 재설정 요청 등록 
     * @param pwdRsetReq
     * @return
     */
    public int insertPwdRsetReq(PwdRsetReqVo pwdRsetReq);    
    
    /**
     * 비밀번호 재설정 요청 수정 
     * @param pwdRsetReq
     * @return
     */
    public int updatePwdRsetReqByKey(PwdRsetReqVo pwdRsetReq);
    
    /**
     * 비밀번호 재설정 요청 조회 by PK 
     * @param siteCd
     * @param usrId
     * @param rsetReqNo
     * @return
     */
    public PwdRsetReqVo findPwdRsetReqByKey(@Param("siteCd") String siteCd, @Param("usrId") String usrId, @Param("rsetReqNo") Integer rsetReqNo);
    
}
