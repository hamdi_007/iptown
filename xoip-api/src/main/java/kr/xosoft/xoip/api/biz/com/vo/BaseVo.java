package kr.xosoft.xoip.api.biz.com.vo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.Verifier;
import lombok.Data;

/**
 * 기본 VO 
 * @author likejy
 *
 */
@Data
public class BaseVo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 언어코드 
     */
    @JsonIgnore
    private String comLang;
    
    /**
     * 사이트코드 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String siteCd;

    /**
     * 수정일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonInclude( JsonInclude.Include.NON_NULL )
    @JsonFormat( shape=JsonFormat.Shape.NUMBER )
    private Date modDt;
    
    /**
     * 수정자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonInclude( JsonInclude.Include.NON_NULL )
    private String modrId;
    
    /**
     * 수정자명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonInclude( JsonInclude.Include.NON_NULL )    
    private String modrNm;
    
    /**
     * 사이트코드 할당 
     * @param siteCd
     * @return
     */
    public void setSiteCd(String siteCd) {
        String comSiteCd = getSiteCd();
        if (siteCd == null || !siteCd.equals(comSiteCd)) {
            Verifier.fail("M9000", new String[] { siteCd, comSiteCd });
        }
        this.siteCd = siteCd;
    }
    
    /**
     * 사이트코드 조회 
     * @return
     */
    public String getSiteCd() {
        if (siteCd == null) {
            CommonData commonData = CommonDataHolder.getCommonData();
            if (commonData == null) {
                return null;
            }
            return commonData.getSiteCd();            
        } else {
            return siteCd;
        }
    }
    
    /**
     * 언어코드 조회 
     * @return
     */
    public String getComLang() {
        if (comLang == null) {
            CommonData commonData = CommonDataHolder.getCommonData();
            if (commonData == null) {
                this.comLang = "en";
            } else {
                this.comLang = commonData.getLang();                
            }
        } 
        return comLang;
    }
    
}
