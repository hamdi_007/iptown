package kr.xosoft.xoip.api.biz.stmpappl.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplVo;

/**
 * 증지신청 매퍼 
 * @author likejy
 *
 */
@Repository
public interface StmpApplMapper {

    /** 
     * 증지신청 등록
     * @param stmpAppl
     * @return
     */
    public int insertStmpAppl(StmpApplVo stmpAppl);
    
    /**
     * 증지신청 수정 by PK
     * @param stmpAppl
     * @return
     */
    public int updateStmpApplByKey(StmpApplVo stmpAppl);
    
    /**
     * 증지신청 확인유무 변경 by PK
     * @param stmpAppl
     * @return
     */
    public int confirmStateStmpApplByKey(StmpApplVo stmpAppl);
        
    /**
     * 증지신청 삭제 by PK
     * @param stmpAppl
     * @return
     */
    public int deleteStmpApplByKey(StmpApplVo stmpAppl);
    
    /**
     * 증지신청 조회 by PK
     * @param siteCd
     * @param projCd
     * @param stmpApplNo
     * @return
     */
    public StmpApplVo findStmpApplByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("stmpApplNo") Integer stmpApplNo);
    
    /**
     * 증지신청+부가정보 조회 by PK 
     * @param siteCd
     * @param projCd
     * @param stmpApplNo
     * @return
     */
    public StmpApplVo findStmpApplWithMetaByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("stmpApplNo") Integer stmpApplNo);

    /**
     * 증지신청 서명값 조회 
     * @param siteCd
     * @param projCd
     * @param stmpApplNo
     * @return
     */
    public String findStmpApplrSignValByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("stmpApplNo") Integer stmpApplNo);
    
    /**
     * 증지신청 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countStmpApplListByMap(Map<String, Object> map);
    
    /**
     * 증지신청 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<StmpApplVo> findStmpApplListByMap(Map<String, Object> map);
    
    
    
    
   
        
}
