package kr.xosoft.xoip.api.biz.usr.vo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 비밀번호 재설정 요청폼 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class PwdRsetReqFormVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 핸드폰번호 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )
    private String hnpno;
    
    /**
     * 이메일
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    @Email( message="chk.email", groups=DefaultGroup.class )    
    private String email;

    /**
     * 사용자명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String usrNm;    
    
}
