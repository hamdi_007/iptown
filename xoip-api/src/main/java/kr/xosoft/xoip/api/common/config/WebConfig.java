package kr.xosoft.xoip.api.common.config;

import java.util.Collections;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import kr.xosoft.xoip.api.common.aop.CommonRequestInterceptor;
import kr.xosoft.xoip.api.common.aop.RestTemplateInterceptor;

/**
 * 웹 설정 
 * @author likejy
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan( basePackages = { "kr.xosoft.xoip.api.controller" },
                useDefaultFilters = false,
                includeFilters = @Filter({ org.springframework.web.bind.annotation.RestController.class }))
@EnableGlobalMethodSecurity( prePostEnabled=true )
@EnableCaching
public class WebConfig extends WebMvcConfigurerAdapter {

    @Value("${api.file.maxUploadSize}")
    private Long maxUploadSize;

    @Value("${api.file.maxInMemorySize}")
    private Integer maxInMemorySize;
    
    @Value("${api.rest.maxConnTotal}")
    private Integer restMaxConnTotal;
    
    @Value("${api.rest.maxConnPerRoute}")
    private Integer restMaxConnPerRoute;
    
    @Value("${api.rest.connTimeout}")
    private Integer restConnTimeout;
    
    @Value("${api.rest.readTimeout}")
    private Integer restReadTimeout;    

    @Autowired
    CommonRequestInterceptor commonRequestInterceptor;
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")
            .addResourceLocations("classpath:/resources/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(commonRequestInterceptor);
    }       
    
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer){
        configurer.enable();
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        
        registry.addMapping("/**")
            .allowedOrigins("*")
            .allowedMethods("OPTIONS", "GET", "POST", "PUT", "DELETE")
            .allowedHeaders("*")
            .allowCredentials(true)
            .exposedHeaders(HttpHeaders.CONTENT_DISPOSITION)
            .maxAge(36000L);
    }

    @Bean
    public MultipartResolver multipartResolver() {
        
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(maxUploadSize);
        multipartResolver.setMaxInMemorySize(maxInMemorySize);
        return multipartResolver;
    }

    @Bean
    public RestTemplate restTemplate() {
        
        HttpClient client = HttpClientBuilder.create()
                .setMaxConnTotal(restMaxConnTotal)
                .setMaxConnPerRoute(restMaxConnPerRoute)
                .build();
        
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setHttpClient(client);
        factory.setConnectTimeout(restConnTimeout);
        factory.setReadTimeout(restReadTimeout);
        
        RestTemplate template = new RestTemplate(new BufferingClientHttpRequestFactory(factory));
        template.setInterceptors(Collections.singletonList(new RestTemplateInterceptor()));
        return template;
    }   
    
    @Bean
    public CacheManager cacheManager() {
        return new EhCacheCacheManager(ehCacheCacheManager().getObject());
    }
    
    @Bean
    public EhCacheManagerFactoryBean ehCacheCacheManager() {
        EhCacheManagerFactoryBean factory = new EhCacheManagerFactoryBean();
        factory.setConfigLocation(new ClassPathResource("ehcache.xml"));
        factory.setShared(true);
        return factory;
    }
}
