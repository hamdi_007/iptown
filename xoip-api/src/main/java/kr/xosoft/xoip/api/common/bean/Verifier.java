package kr.xosoft.xoip.api.common.bean;

import java.math.BigDecimal;
import java.util.Date;

import kr.xosoft.xoip.api.common.exception.VerificationException;

/**
 * 검증기 
 * @author likejy
 *
 */
public class Verifier {

	/**
	 * 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다.  
	 * @param messageCode
	 * @param fields
	 */
	private static void throwException(String messageCode, String[] fields) {
		if (fields == null) {
			throw new VerificationException(messageCode);
		} else {
			throw new VerificationException(messageCode, fields);
		}
	}
	
	/**
	 * 입력된 코드 및 필드 값을 갖는 예외를 항상 발생시킨다. 
	 * @param messageCode
	 * @param fields
	 */
	public static void fail(String messageCode, String ...fields) {
		throwException(messageCode, fields);
	}
	
	/**
	 * 입력조건이 false 이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param object
	 * @param messageCode
	 * @param fields
	 */
	public static void is(Boolean object, String messageCode, String ...fields) {
		if (object == null || !object.booleanValue()) {
			throwException(messageCode, fields);
		}
	}
	
	/**
	 * 입력조건이 true 이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param object
	 * @param messageCode
	 * @param fields
	 */
	public static void not(Boolean object, String messageCode, String ...fields) {
		if (object == null || object.booleanValue()) {
			throwException(messageCode, fields);
		}
	}	
	
	/**
	 * 입력된 객체가 null 값이 아니면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param object
	 * @param messageCode
	 * @param fields
	 */
	public static void isNull(Object object, String messageCode, String ... fields) {
		if (object != null) {
			throwException(messageCode, fields);
		}		
	}

	/**
	 * 입력된 객체가 null 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param object
	 * @param messageCode
	 * @param fields
	 */
	public static void notNull(Object object, String messageCode, String ... fields) {
		if (object == null) {
			throwException(messageCode, fields);
		}
	}
	
	/**
	 * 입력된 객체가 null 이 아니고 숫자값이 아니면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param object
	 * @param messageCode
	 * @param fields
	 */
	public static void isNullOrParsableNumeric(Object object, String messageCode, String ... fields) {
	    if (object == null) {
	        return;
	    }
	    if (object instanceof Number) {
	        return;
	    } else if (object instanceof String) {
	        String value = (String)object;
	        int length = value.length();
	        if (length == 0) {
	            throwException(messageCode, fields);
	        }
	        int i = 0;
	        if (value.charAt(0) == '-') {
	            if (length == 1) {
	                throwException(messageCode, fields);
	            }
	            i = 1;
	        }
	        for (; i < length; i++) {
	            char c = value.charAt(i);
	            if (c < '0' || c > '9') {
                    throwException(messageCode, fields);
	            }
	        }
	    } else {
	        throwException(messageCode, fields);
	    }
	}
	
	/**
	 * object1 과 object2의 equals 메소드 리턴값이 false 이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다.  
	 * @param object1
	 * @param object2
	 * @param messageCode
	 * @param fields
	 */
	public static void isEqual(Object object1, Object object2, String messageCode, String ... fields) {
		if (object1 == null && object2 != null) {
			throwException(messageCode, fields);
		} else if (object1 != null && object2 == null) {
			throwException(messageCode, fields);
		} else if (object1 != null && object2 != null) {
			if (!object1.equals(object2)) {
				throwException(messageCode, fields);
			}
		}
	}
	
	/**
	 * object1 과 object2의 equals 메소드 리턴값이 true 이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param object1
	 * @param object2
	 * @param messageCode
	 * @param fields
	 */
	public static void notEqual(Object object1, Object object2, String messageCode, String ... fields) {
		if (object1 == null && object2 == null) {
			throwException(messageCode, fields);
		} else if (object1 != null && object2 != null) {
			if (object1.equals(object2)) {
				throwException(messageCode, fields);
			}
		}		
	}
	
	/**
	 * Integer value1 과 value2 의 값이 다르면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다.  
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void eq(Integer value1, Integer value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.intValue() != value2.intValue()) {
			throwException(messageCode, fields);
		}		
	}
	
	/**
	 * Integer value1 이 value2 보다 작거나 같은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void gt(Integer value1, Integer value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.intValue() <= value2.intValue()) {
			throwException(messageCode, fields);
		}
	}
	
	/**
	 * Integer value1 이 value2 보다 작은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void ge(Integer value1, Integer value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.intValue() < value2.intValue()) {
			throwException(messageCode, fields);
		}		
	}
	
	/**
	 * Integer value1 이 value2 보다 크거나 같은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void lt(Integer value1, Integer value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.intValue() >= value2.intValue()) {
			throwException(messageCode, fields);
		}		
	}
	
	/**
	 * Integer value1 이 value2 보다 큰 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void le(Integer value1, Integer value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.intValue() > value2.intValue()) {
			throwException(messageCode, fields);
		}		
	}	
	
	/**
	 * Long value1 과 value2 의 값이 다르면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다.  
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void eq(Long value1, Long value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.longValue() != value2.longValue()) {
			throwException(messageCode, fields);
		}		
	}	
	
	/**
	 * Long value1 이 value2 보다 작거나 같은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */	
	public static void gt(Long value1, Long value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.longValue() <= value2.longValue()) {
			throwException(messageCode, fields);
		}
	}
	
	/**
	 * Long value1 이 value2 보다 작은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */	
	public static void ge(Long value1, Long value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.longValue() < value2.longValue()) {
			throwException(messageCode, fields);
		}		
	}
	
	/**
	 * Long value1 이 value2 보다 크거나 같은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */	
	public static void lt(Long value1, Long value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.longValue() >= value2.longValue()) {
			throwException(messageCode, fields);
		}		
	}
	
	/**
	 * Long value1 이 value2 보다 큰 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */	
	public static void le(Long value1, Long value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		if (value1.longValue() > value2.longValue()) {
			throwException(messageCode, fields);
		}		
	}	
	
	/**
	 * BigDecimal value1 과 value2 의 값이 다르면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다.  
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void eq(BigDecimal value1, BigDecimal value2, String messageCode, String ... fields) {
	    if (value1 == null || value2 == null) {
	        throwException(messageCode, fields);
	    }
	    if (value1.compareTo(value2) != 0) {
	        throwException(messageCode, fields);
	    }       
	}   

	/**
	 * BigDecimal value1 이 value2 보다 작거나 같은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */ 
	public static void gt(BigDecimal value1, BigDecimal value2, String messageCode, String ... fields) {
	    if (value1 == null || value2 == null) {
	        throwException(messageCode, fields);
	    }
	    if (value1.compareTo(value2) <= 0) {
	        throwException(messageCode, fields);
	    }
	}

	/**
	 * BigDecimal value1 이 value2 보다 작은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */ 
	public static void ge(BigDecimal value1, BigDecimal value2, String messageCode, String ... fields) {
	    if (value1 == null || value2 == null) {
	        throwException(messageCode, fields);
	    }
	    if (value1.compareTo(value2) < 0) {
	        throwException(messageCode, fields);
	    }       
	}

	/**
	 * BigDecimal value1 이 value2 보다 크거나 같은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */ 
	public static void lt(BigDecimal value1, BigDecimal value2, String messageCode, String ... fields) {
	    if (value1 == null || value2 == null) {
	        throwException(messageCode, fields);
	    }
	    if (value1.compareTo(value2) >= 0) {
	        throwException(messageCode, fields);
	    }       
	}

	/**
	 * BigDecimal value1 이 value2 보다 큰 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */ 
	public static void le(BigDecimal value1, BigDecimal value2, String messageCode, String ... fields) {
	    if (value1 == null || value2 == null) {
	        throwException(messageCode, fields);
	    }
	    if (value1.compareTo(value2) > 0) {
	        throwException(messageCode, fields);
	    }       
	}
	
	/**
	 * Date value1 이 value2 보다  크거나 같은 값이면 입력된 코드 및 필드 값을 갖는 예외를 발생시킨다. 
	 * @param value1
	 * @param value2
	 * @param messageCode
	 * @param fields
	 */
	public static void lt(Date value1, Date value2, String messageCode, String ... fields) {
		if (value1 == null || value2 == null) {
			throwException(messageCode, fields);
		}
		int result = value1.compareTo(value2);
		if (result >= 0) {
			throwException(messageCode, fields);
		}		
	}	
	
}
