package kr.xosoft.xoip.api.biz.email.vo;

import java.util.Date;
import java.util.List;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.biz.site.vo.SiteSmtpVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 이메일 요청 폼 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class EmailTrnsVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * SMTP프로토콜
     */
    private String smtpPrtc;

    /**
     * 서버호스트
     */
    private String svrHost;

    /**
     * 포트번호
     */
    private Integer portNo;

    /**
     * SMTP사용자ID
     */
    private String smtpUsrId;

    /**
     * SMTP사용자비밀번호
     */
    private String smtpUsrPwd;

    /**
     * 인증여부
     */
    private boolean isAtrz;

    /**
     * TLS사용여부
     */
    private boolean isTlsUse;

    /**
     * SSL사용여부
     */
    private boolean isSslUse;    
    
    /**
     * 폼코드
     */
    private String formCd;

    /**
     * 폼번호
     */
    private Integer formNo;

    /**
     * 전송요청일시
     */
    private Date trnsReqDt;

    /**
     * 전송요청회사ID
     */
    private String trnsReqCompId;

    /**
     * 전송요청자ID
     */
    private String trnsReqrId;

    /**
     * 전송자이메일
     */
    private String trnsrEmail;

    /**
     * 전송자명
     */
    private String trnsrNm;

    /**
     * 수신자 이메일 목록 
     */
    private List<String> recpList;

    /**
     * 템플릿변수전문
     */
    private String tmplVarFtxt;    
    
    /**
     * 이메일 제목 
     */
    private String emailSbj;
    
    /**
     * 이메일 내용 
     */
    private String emailCnts;
    
    /**
     * 생성자 
     * @param siteSmtp
     * @param emailForm
     */
    public EmailTrnsVo(SiteSmtpVo siteSmtp) {

        setSmtpPrtc(siteSmtp.getSmtpPrtc());
        setSvrHost(siteSmtp.getSvrHost());
        setPortNo(siteSmtp.getPortNo());
        setSmtpUsrId(siteSmtp.getSmtpUsrId());
        setSmtpUsrPwd(siteSmtp.getSmtpUsrPwd());
        setAtrz("Y".equals(siteSmtp.getAtrzYn()));
        setTlsUse("Y".equals(siteSmtp.getTlsUseYn()));
        setSslUse("Y".equals(siteSmtp.getSslUseYn()));
        setTrnsrEmail(siteSmtp.getTrnsrEmail());
        setTrnsrNm(siteSmtp.getTrnsrNm());
                
    }
    
}
