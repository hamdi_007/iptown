package kr.xosoft.xoip.api.biz.usr.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 사용자이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class UsrHistVo extends UsrVo {
    
    private static final long serialVersionUID = 1L;

    /**
     * 사용자이력일련번호 
     */
    private Integer usrHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public UsrHistVo(String usrId, String histDivCd) {
        setUsrId(usrId);
        setHistDivCd(histDivCd);
    }
}
