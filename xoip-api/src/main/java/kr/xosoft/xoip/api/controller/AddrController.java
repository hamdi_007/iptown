package kr.xosoft.xoip.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.com.service.AddrService;
import kr.xosoft.xoip.api.biz.com.vo.AddrVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;

/**
 * 주소 컨트롤러 
 * @author likejy
 * 
 */
@RestController
public class AddrController {

	@Autowired
	private AddrService addrService;
	
    /**
     * 주소 목록조회 by 검색조건 
     */
    @RequestMapping( value="/api/{version}/addresses", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findAddrPage(
    		@PathVariable("version") final Integer version,
    		final CommonCondition condition) {
    	
    	CommonPage<AddrVo> addrPage = addrService.findAddrPageByCondition(condition);
    	return new ResponseEntity<CommonResult>(new CommonResult("addrPage", addrPage), HttpStatus.OK);
    }
    
}
