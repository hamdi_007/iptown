package kr.xosoft.xoip.api.biz.com.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;

/**
 * 공통코드 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ComCdMapper {

    /**
     * 사용가능한 모든 공통코드 목록 조회 
     */
    public List<ComCdVo> findAllComCdList();
    
}
