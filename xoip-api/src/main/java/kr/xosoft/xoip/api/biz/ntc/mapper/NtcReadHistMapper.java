package kr.xosoft.xoip.api.biz.ntc.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.ntc.vo.NtcReadHistVo;

/**
 * 공지사항조회이력 매퍼 
 * @author hypark
 *
 */
@Repository
public interface NtcReadHistMapper {

    /**
     * 공지사항조회이력 등록 
     */
    public int insertNtcReadHist(NtcReadHistVo ntcReadHist);    
    
}
