package kr.xosoft.xoip.api.biz.comp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.comp.vo.CompFileVo;

/**
 * 회사파일 매퍼 
 * @author likejy
 *
 */
@Repository
public interface CompFileMapper {

    /**
     * 회사파일 등록
     * @param compFile
     * @return
     */
    public int insertCompFile(CompFileVo compFile);
    
    /**
     * 회사파일 수정 by PK
     * @param compFile
     * @return
     */
    public int updateCompFileByKey(CompFileVo compFile);
        
    /**
     * 회사파일 삭제 by PK
     * @param compFile
     * @return
     */
    public int deleteCompFileByKey(CompFileVo compFile);
    
    /**
     * 회사파일 조회 by PK
     * @param siteCd
     * @param compId
     * @param compFileNo
     * @return
     */
    public CompFileVo findCompFileByKey(@Param("siteCd") String siteCd, @Param("compId") String compId, @Param("compFileNo") Integer compFileNo);
    
    /**
     * 회사파일 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countCompFileListByMap(Map<String, Object> map);
    
    /**
     * 회사파일 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<CompFileVo> findCompFileListByMap(Map<String, Object> map);    
    
}
