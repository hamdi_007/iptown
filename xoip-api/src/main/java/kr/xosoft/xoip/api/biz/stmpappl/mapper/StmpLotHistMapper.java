package kr.xosoft.xoip.api.biz.stmpappl.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpLotHistVo;

/**
 * 증지품목이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface StmpLotHistMapper {

    /**
     * 증지품목이력 등록 
     */
    public void insertStmpLotHistAsSelectMaster(StmpLotHistVo stmpLotHist);    
    
}
