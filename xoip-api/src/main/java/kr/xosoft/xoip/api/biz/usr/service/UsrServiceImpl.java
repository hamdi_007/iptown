package kr.xosoft.xoip.api.biz.usr.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.com.service.ComCdService;
import kr.xosoft.xoip.api.biz.comp.service.CompService;
import kr.xosoft.xoip.api.biz.comp.vo.CompVo;
import kr.xosoft.xoip.api.biz.email.service.EmailService;
import kr.xosoft.xoip.api.biz.email.vo.EmailReqFormVo;
import kr.xosoft.xoip.api.biz.usr.mapper.LgnHistMapper;
import kr.xosoft.xoip.api.biz.usr.mapper.PwdChngHistMapper;
import kr.xosoft.xoip.api.biz.usr.mapper.PwdRsetReqMapper;
import kr.xosoft.xoip.api.biz.usr.mapper.UsrHistMapper;
import kr.xosoft.xoip.api.biz.usr.mapper.UsrMapper;
import kr.xosoft.xoip.api.biz.usr.vo.LgnHistVo;
import kr.xosoft.xoip.api.biz.usr.vo.LgnUsrRsltVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdChngFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdChngHistVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdJoinFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetReqFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetReqVo;
import kr.xosoft.xoip.api.biz.usr.vo.TokenFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrCompRsltVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrHistVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrVo;
import kr.xosoft.xoip.api.common.auth.DefaultUserDetails;
import kr.xosoft.xoip.api.common.auth.JwtUtil;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.MessageBean;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.exception.BizException;
import kr.xosoft.xoip.api.common.support.AuthConstant;
import kr.xosoft.xoip.api.common.support.AuthSupport;
import kr.xosoft.xoip.api.common.support.BizSupport;
import kr.xosoft.xoip.api.common.support.StringSupport;
import lombok.extern.slf4j.Slf4j;

/**
 * 사용자 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
@Slf4j
public class UsrServiceImpl implements UsrService {
    
    @Value("${api.xoip.webUrl}")
    private String webUrl;    
    
    @Autowired
    private UsrMapper usrMapper;
    
    @Autowired
    private UsrHistMapper usrHistMapper;
    
    @Autowired
    private LgnHistMapper lgnHistMapper;
    
    @Autowired
    private PwdChngHistMapper pwdChngHistMapper;
    
    @Autowired
    private PwdRsetReqMapper pwdRsetReqMapper;
    
    @Autowired
    private CompService compService;
    
    @Autowired
    private ComCdService comCdService;
    
    @Autowired
    private EmailService emailService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;    
    
    @Autowired
    private JwtUtil jwtUtil;
    
    @Autowired
    private MessageBean messageBean;    
    
    /**
     * 사용자를 등록한다. 
     */
    private UsrVo registUsrWithoutVerification(UsrVo usr) {
        
        // 사용자 등록 
        int cnt = usrMapper.insertUsr(usr);
        Verifier.eq(cnt, 1, "M1000", "word.usr");

        // 사용자 이력 등록 
        UsrHistVo usrHist = new UsrHistVo(usr.getUsrId(), "HD0100");
        usrHistMapper.insertUsrHistAsSelectMaster(usrHist);
        Verifier.notNull(usrHist.getUsrHistSeqno(), "M1000", "word.usrHist");
        
        // 공통코드명 할당
        usr.setUsrStsCdNm(comCdService.findComCdNmByComCd(usr.getUsrStsCd()));
        
        return usr;
    }
    
    /**
     * 라이선서 사용자를 등록한다. 
     */
    @Override 
    public UsrVo registLicrUsr(UsrVo usr) {

        // 비밀번호 입력값 확인
        if (StringUtils.isBlank(usr.getPwd())) {
            Verifier.fail("M1005", "word.pwd");
        }
        if (StringUtils.isBlank(usr.getPwdCnfm())) {
            Verifier.fail("M1005", "word.pwdCnfm");
        }
        Verifier.isEqual(usr.getPwd(), usr.getPwdCnfm(), "M2004");
        
        // 라이선서 회사 조회 확인 
        CompVo fetchedComp = compService.findLicrComp();
        Verifier.isEqual(usr.getCompId(), fetchedComp.getCompId(), "M1004", "word.compId");
        
        // 이메일 중복 확인 
        UsrVo fetchedUsr = findUsrByEmail(usr.getEmail());
        Verifier.isNull(fetchedUsr, "M2000");
        
        // 비밀번호 암호화 
        String rawPwd = usr.getPwd();
        usr.setPwd(passwordEncoder.encode(usr.getPwd()));
        
        // 기본값 할당 
        usr.setUsrStsCd("US0300"); // 정상 
        usr.setJoinDt(new Date());

        // 사용자 등록 
        registUsrWithoutVerification(usr);
        
        // 이메일 전송요청 
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(fetchedComp.getNtnCd());
        String siteWebUrl = BizSupport.parseSiteWebUrl(CommonDataHolder.getCommonData().getSiteCd(), webUrl);
        List<String> recpList = new ArrayList<String>();
        recpList.add(usr.getEmail());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("USER_NAME", usr.getUsrNm());
        varMap.put("LICENSER_COMPANY_NAME", fetchedComp.getCompNm());
        varMap.put("USER_EMAIL", usr.getEmail());
        varMap.put("USER_PASSWORD", rawPwd);
        if (AuthConstant.ROLE_ADMIN.equals(usr.getAuth())) {
            varMap.put("USER_AUTH", messageBean.getMessage("word.admin"));
        } else if (AuthConstant.ROLE_REVIEWER.equals(usr.getAuth())) {
            varMap.put("USER_AUTH", messageBean.getMessage("word.reviewer"));
        } else {
            Verifier.fail("M1004", "auth");
        }
        varMap.put("PROCESS_DATE", DateFormatUtils.format(usr.getJoinDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0102");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);        
        
        return usr;
    }
    
    /**
     * 라이선시 사용자를 등록한다. 
     */
    @Override
    public UsrVo registLiceUsr(UsrVo usr) {
        
        // 비밀번호 입력 불가 
        Verifier.isNull(usr.getPwd(), "M1004", "word.pwd");
        
        // 이메일 중복 확인 
        UsrVo fetchedUsr = findUsrByEmail(usr.getEmail());
        Verifier.isNull(fetchedUsr, "M2000");
        
        // 기본값 할당 
        usr.setUsrStsCd("US0100"); // 신규 
        
        // 사용자 등록 
        return registUsrWithoutVerification(usr);
    }
    
    /**
     * 사용자 정보를 수정한다. 
     */
    private UsrVo modifyUsrWithoutVerification(UsrVo usr) {

        // 사용자 수정 
        int cnt = usrMapper.updateUsrByKey(usr);
        Verifier.eq(cnt, 1, "M1001", "word.usr");

        // 사용자 이력 등록 
        UsrHistVo usrHist = new UsrHistVo(usr.getUsrId(), "HD0200");
        usrHistMapper.insertUsrHistAsSelectMaster(usrHist);
        Verifier.notNull(usrHist.getUsrHistSeqno(), "M1000", "word.usrHist");
        
        // 공통코드명 할당
        usr.setUsrStsCdNm(comCdService.findComCdNmByComCd(usr.getUsrStsCd()));        
        
        return usr;
    }
    
    /**
     * 사용자 기본 정보를 변경한다. 
     */
    @Override
    public UsrVo modifyUsrInfo(UsrVo usr) {
        
        CommonData commonData = CommonDataHolder.getCommonData();

        // 라이선시는 본인 정보만 수정 가능하다. 
        if (AuthSupport.isLicensee(commonData.getAuth())) {
            Verifier.isEqual(usr.getUsrId(), commonData.getUsrId(), "M2001");            
        }
        
        // 사용자 조회 
        UsrVo fetchedUsr = findUsr(usr.getUsrId());
        Verifier.notNull(fetchedUsr, "M1003", "word.usr");
        
        // 권한이 변경된 경우
        if (!fetchedUsr.getAuth().equalsIgnoreCase(usr.getAuth())) {
            // 권한 변경은 관리자만 가능하다.
            Verifier.is(AuthSupport.isAdmin(commonData.getAuth()), "M2002");            
        }
        
        fetchedUsr.setUsrNm(usr.getUsrNm());
        fetchedUsr.setUsrEngNm(usr.getUsrEngNm());
        fetchedUsr.setTelno(usr.getTelno());
        fetchedUsr.setHnpno(usr.getHnpno());
        fetchedUsr.setAuth(usr.getAuth());
        
        // 비고 조회 권한이 없는 경우 비고 값이 null 값으로 전달됨 
        if (usr.getRmk() != null) {
            fetchedUsr.setRmk(usr.getRmk());                        
        }
        
        // 사용자 수정 
        return modifyUsrWithoutVerification(fetchedUsr);
    }
    
    /**
     * 사용자 비밀번호를 변경한다.  
     */
    private void modifyUsrPwdWithoutVerification(String usrId, String encodedPwd, String pwdChngMthdCd) {

        UsrVo usr = new UsrVo();
        usr.setUsrId(usrId);
        usr.setPwd(encodedPwd);
        
        // 비밀번호 변경 
        int cnt = usrMapper.updatePwdByKey(usr);
        Verifier.eq(cnt, 1, "M1001", "word.usr");

        // 비밀번호 변경 이력 등록
        PwdChngHistVo pwdChngHist = new PwdChngHistVo();
        pwdChngHist.setUsrId(usrId);
        pwdChngHist.setPwdChngMthdCd(pwdChngMthdCd);
        int pwdChngHistCnt = pwdChngHistMapper.insertPwdChngHist(pwdChngHist);
        
        Verifier.eq(pwdChngHistCnt, 1, "M1000", "word.pwdChngHist");
    }    
    
    /**
     * 현재 비밀번호를 입력하여 사용자 본인 비밀번호를 변경한다. 
     */
    @Override
    public void modifyUsrPwdWithCurrPwd(PwdChngFormVo pwdChngForm) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 비밀번호 변경은 본인만 가능하다.
        Verifier.isEqual(pwdChngForm.getUsrId(), commonData.getUsrId(), "M2005");
        
        // 입력 비밀번호 일치여부 확인
        Verifier.isEqual(pwdChngForm.getChngPwd(), pwdChngForm.getChngPwdCnfm(), "M2004");
        
        // 현재 비밀번호와 일치하는지여부 확인 
        UsrVo fetchedUsr = findUsr(pwdChngForm.getUsrId());
        if (!passwordEncoder.matches(pwdChngForm.getCurrPwd(), fetchedUsr.getPwd())) {
            Verifier.fail("M2006");
        }

        // 비밀번호 변경 
        modifyUsrPwdWithoutVerification(pwdChngForm.getUsrId(), passwordEncoder.encode(pwdChngForm.getChngPwd()), "UP0100"); 
    }

    /**
     * 이메일 검증값으로 사용자 본인 비밀번호를 변경한다. 
     */
    @Override
    public void modifyUsrPwdWithVrfVal(PwdRsetFormVo pwdRsetForm) {

        // 입력 비밀번호 일치여부 확인
        Verifier.isEqual(pwdRsetForm.getChngPwd(), pwdRsetForm.getChngPwdCnfm(), "M2004");       
        
        PwdRsetReqVo pwdRsetReq = pwdRsetReqMapper.findPwdRsetReqByKey(pwdRsetForm.getSiteCd(), pwdRsetForm.getUsrId(), pwdRsetForm.getRsetReqNo());
        Verifier.notNull(pwdRsetReq, "M2011");  
        Verifier.isEqual(pwdRsetReq.getRsetCmplYn(), "N", "M2012"); 
        Verifier.isEqual(pwdRsetReq.getRsetVrfVal(), pwdRsetForm.getRsetVrfVal(), "M2013"); 
        
        // 검증값 유효기간 확인 (요청일시+2시간)
        Date expiredDate = DateUtils.addHours(pwdRsetReq.getRsetReqDt(), 2);
        Date nowDate = new Date();
        if (nowDate.compareTo(expiredDate) > 0) {
            Verifier.fail("M2014");
        }
        
        // 비밀번호 변경 
        modifyUsrPwdWithoutVerification(pwdRsetForm.getUsrId(), passwordEncoder.encode(pwdRsetForm.getChngPwd()), "UP0200");    

        // 비밀번호 재설정 요청 완료여부 설정 
        pwdRsetReq.setRsetCmplYn("Y");
        pwdRsetReq.setRsetCmplDt(new Date());
        pwdRsetReqMapper.updatePwdRsetReqByKey(pwdRsetReq);
    }
    
    /**
     * 사용자를 초대한다.  
     */
    @Override
    public UsrVo inviteUsr(String usrId) {
                
        // 사용자 조회 
        UsrVo usr = findUsr(usrId);
        Verifier.notNull(usr, "M2015");
        if (!("US0100".equals(usr.getUsrStsCd()) || "US0200".equals(usr.getUsrStsCd()))) {
            Verifier.fail("M2009");
        }
        
        // 검증값 생성 
        String ivtVrfVal = StringSupport.randomString(50);
        log.debug("ivtVrfVal : {}", ivtVrfVal);        
        
        String keyText = new StringBuilder()
            .append("usrId=")
            .append(usr.getUsrId())
            .append("&usrNm=")
            .append(usr.getUsrNm())
            .append("&email=")
            .append(usr.getEmail())
            .append("&ivtVrfVal=")
            .append(ivtVrfVal)
            .toString();
        String encodedKey = null;
        try {
            encodedKey = new String(Base64.encodeBase64(keyText.getBytes("utf-8")));
            log.debug("encodedKey : {}", encodedKey);            
        } catch (UnsupportedEncodingException e) {
            throw new BizException("M2023", e);
        }
        
        // 사용자 초청 값 설정 
        usr.setUsrStsCd("US0200");
        usr.setIvtVrfVal(ivtVrfVal);
        usr.setIvtDt(new Date());
        usr.setIvtrId(CommonDataHolder.getCommonData().getUsrId());
        
        // 사용자 수정 
        modifyUsrWithoutVerification(usr);

        // 이메일 전송요청 
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(usr.getCompId()).getNtnCd());        
        String siteWebUrl = BizSupport.parseSiteWebUrl(CommonDataHolder.getCommonData().getSiteCd(), webUrl);
        List<String> recpList = new ArrayList<String>();
        recpList.add(usr.getEmail());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("USER_NAME", usr.getUsrNm());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", usr.getCompNm());
        varMap.put("USER_EMAIL", usr.getEmail());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(usr.getIvtDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl + "/join/password?k=" + encodedKey);
        varMap.put("MANUAL_LINK", siteWebUrl + "/guide/" + usr.getSiteCd() + "/licensee_guide.pdf");
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0100");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);

        return usr;
    }
    
    /**
     * 초대받은 사용자가 비밀번호를 등록하며 가입한다. 
     */
    @Override
    public UsrVo joinUsr(PwdJoinFormVo pwdJoinForm) {
                
        // 입력 비밀번호 일치여부 확인
        Verifier.isEqual(pwdJoinForm.getPwd(), pwdJoinForm.getPwdCnfm(), "M2004");               
        
        // 사용자 조회 
        UsrVo usr = findUsr(pwdJoinForm.getUsrId());
        
        // 초대받은 라이선시 사용자 인지 확인 
        Verifier.notNull(usr, "M2015");
        Verifier.is(AuthSupport.isLicensee(usr.getAuth()), "M2018");
        Verifier.notEqual(usr.getUsrStsCd(), "US0100", "M2016");
        Verifier.notEqual(usr.getUsrStsCd(), "US0300", "M2017");
        Verifier.notEqual(usr.getUsrStsCd(), "US0400", "M2018");
        Verifier.isEqual(usr.getIvtVrfVal(), pwdJoinForm.getIvtVrfVal(), "M2019");

        // 비밀번호 설정  
        modifyUsrPwdWithoutVerification(pwdJoinForm.getUsrId(), passwordEncoder.encode(pwdJoinForm.getPwd()), "UP0300");            

        // 가입상태 설정 
        usr.setUsrStsCd("US0300");
        usr.setJoinDt(new Date());
        
        // 사용자 수정 
        modifyUsrWithoutVerification(usr);
        
        // 이메일 전송요청
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(usr.getCompId()).getNtnCd());   
        String siteWebUrl = BizSupport.parseSiteWebUrl(CommonDataHolder.getCommonData().getSiteCd(), webUrl);
        List<String> recpList = new ArrayList<String>();
        recpList.add(usr.getEmail());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("USER_NAME", usr.getUsrNm());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", usr.getCompNm());
        varMap.put("USER_EMAIL", usr.getEmail());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(usr.getIvtDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0101");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);
        
        return usr;
    }

//    사용자 탈퇴 기능은 사용하지 않기로 협의 (2021.06.03)
//    /**
//     * 사용자를 탈퇴 처리한다. 
//     */
//    @Override
//    public UsrVo withdrawUsr(String usrId) {
//        
//        // 사용자 본인은 탈퇴처리 불가함 
//        String lgnUsrId = CommonDataHolder.getCommonData().getUsrId();
//        Verifier.notEqual(lgnUsrId, usrId, "M2007");
//        
//        // 사용자 조회 
//        UsrVo usr = findUsr(usrId);
//        Verifier.notEqual(usr.getUsrStsCd(), "US0400", "M2008");
//        
//        // 사용자 탈퇴값 설정  
//        usr.setUsrStsCd("US0400");
//        usr.setWtdrDt(new Date());
//        usr.setWtdrHndlrId(lgnUsrId);
//        
//        // 사용자 수정 
//        return modifyUsrWithoutVerification(usr);
//    }
    
    /**
     * 사용자를 삭제한다. 
     */
    @Override
    public void deleteUsr(String usrId) {

        // 사용자 본인 삭제는 불가함 
        CommonData commonData = CommonDataHolder.getCommonData();
        Verifier.notEqual(usrId, commonData.getUsrId(), "M2022");
        
        // 사용자 삭제 
        UsrVo usr = new UsrVo();
        usr.setUsrId(usrId);
        int cnt = usrMapper.deleteUsrByKey(usr); 
        Verifier.eq(cnt, 1, "M1002", "word.usr");
        
        // 사용자 이력 등록 
        UsrHistVo usrHist = new UsrHistVo(usrId, "HD0300");
        usrHistMapper.insertUsrHistAsSelectMaster(usrHist);
        Verifier.notNull(usrHist.getUsrHistSeqno(), "M1000", "word.usrHist");
    }
    
    /**
     * 사용자를 조회한다. 
     */
    @Override
    public UsrVo findUsr(String usrId) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return usrMapper.findUsrByKey(siteCd, usrId);
    }
    
    /**
     * 이메일로 사용자를 조회한다. 
     */
    @Override
    public UsrVo findUsrByEmail(String email) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return usrMapper.findUsrByEmail(siteCd, email);        
    }
    
    /**
     * 사용자 및 회사 목록 페이지를 조회한다. 
     */
    @Override 
    public CommonPage<UsrCompRsltVo> findUsrCompRsltPage(CommonCondition condition) {
        
        Map<String, Object> map = condition.toMap();
        
        int comTtlCnt = usrMapper.countUsrCompRsltListByMap(map);
        List<UsrCompRsltVo> usrList = usrMapper.findUsrCompRsltListByMap(map);
        
        return new CommonPage<UsrCompRsltVo>(condition, usrList, comTtlCnt);        
    }
    
    /**
     * 핸드폰번호, 사용자이름으로 이메일 목록을 조회한다.  
     */
    @Override
    public List<String> findEmailList(String hnpno, String usrNm) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        List<String> emailList = usrMapper.findEmailListInNormalByHnpnoAndUsrNm(siteCd, hnpno, usrNm);
        
        String email = null;
        int atIdx = 0;
        for (int i = 0; i < emailList.size(); i++) {
            email = emailList.get(i);
            atIdx = email.indexOf("@");
            if (atIdx >= 4) {
                String replaced = email.substring(0, 1) + "*" + email.substring(2, (atIdx-1)) + "*" + email.substring(atIdx);
                emailList.set(i, replaced);
            } 
        }
        return emailList;
    }
    
    /**
     * 회사id로 이메일 목록을 조회한다.  
     */
    @Override
    public List<String> findEmailListByCompId(String compId) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        List<String> emailList = usrMapper.findEmailListInNormalByCompId(siteCd, compId);
       
        return emailList;
    }
    
    /**
     * 사용자를 조회하여 UserDetails 인스턴스를 반환한다. 
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        UsrVo fetchedUsr = findUsrByEmail(username);
        if (fetchedUsr == null) {
            throw new UsernameNotFoundException("email does not exists");
        }
        if (StringUtils.isBlank(fetchedUsr.getPwd())) {
            throw new UsernameNotFoundException("password does not exists");
        }
                
        return DefaultUserDetails.builder()
                .username(fetchedUsr.getEmail())
                .password(fetchedUsr.getPwd())
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialNonExpired(true)
                .enabled("US0300".equals(fetchedUsr.getUsrStsCd()))
                .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList(fetchedUsr.getAuth()))
                .siteCode(fetchedUsr.getSiteCd())
                .companyId(fetchedUsr.getCompId())
                .companyName(fetchedUsr.getCompNm())
                .userId(fetchedUsr.getUsrId())
                .realUserName(fetchedUsr.getUsrNm())
                .build();
    }

    /**
     * 로그인 후처리 
     */
    @Override
    public LgnUsrRsltVo postLogin() {
        
        // LgnUsrRlstVo 생성 
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        DefaultUserDetails user = (DefaultUserDetails)auth.getPrincipal();
        LgnUsrRsltVo lgnUsrRslt = defaultUserDetailsToLgnUsrRslt(user);
        
        // 로그인이력 생성 
        int histCnt = lgnHistMapper.insertLgnHist(lgnUsrRsltTolgnHist(lgnUsrRslt, "UL0100"));
        Verifier.eq(histCnt, 1, "M1000", "word.lgnHist");
        
        return lgnUsrRslt;
    }

    /**
     * 엑세스 토큰을 갱신한다.  
     */
    @Override
    public LgnUsrRsltVo renewToken(TokenFormVo tokenForm) {

        // LgnUsrRsltVo 생성 
        UserDetails parsedUser = jwtUtil.parseToken(tokenForm.getRfshToken());
        DefaultUserDetails user = (DefaultUserDetails)loadUserByUsername(parsedUser.getUsername());
        LgnUsrRsltVo lgnUsrRslt = defaultUserDetailsToLgnUsrRslt(user);
        
        // 로그인이력 생성 
        int histCnt = lgnHistMapper.insertLgnHist(lgnUsrRsltTolgnHist(lgnUsrRslt, "UL0200"));
        Verifier.eq(histCnt, 1, "M1000", "word.lgnHist");
        
        return lgnUsrRslt;
    }

    /**
     * 로그아웃을 처리한다. 
     */
    @Override
    public void logout() {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        // 로그인이력VO 생성 
        LgnHistVo lgnHist = new LgnHistVo();
        lgnHist.setSiteCd(commonData.getSiteCd());
        lgnHist.setUsrId(commonData.getUsrId());
        lgnHist.setLgnDt(new Date());
        lgnHist.setLgnDivCd("UL0300");
        lgnHist.setIpcAddr(commonData.getIpcAddr());        
        
        // 로그인이력 생성 
        int histCnt = lgnHistMapper.insertLgnHist(lgnHist);
        Verifier.eq(histCnt, 1, "M1000", "word.lgnHist");        
    }    
    
    /**
     * DefaultUserDetails 인스턴스를 LgnUsrRsltVo 인스턴스로 매핑하여 반환한다.  
     */
    private LgnUsrRsltVo defaultUserDetailsToLgnUsrRslt(DefaultUserDetails user) {
        
        LgnUsrRsltVo lgnUsrRslt = new LgnUsrRsltVo();
        lgnUsrRslt.setSiteCd(user.getSiteCode());
        lgnUsrRslt.setCompId(user.getCompanyId());
        lgnUsrRslt.setCompNm(user.getCompanyName());
        lgnUsrRslt.setUsrId(user.getUserId());
        lgnUsrRslt.setUsrNm(user.getRealUserName());
        lgnUsrRslt.setEmail(user.getUsername());
        lgnUsrRslt.setAuth(user.getFirstStringAuthority());
        lgnUsrRslt.setAccsToken(jwtUtil.generateToken(user));
        lgnUsrRslt.setRfshToken(jwtUtil.generateRefreshToken(user));   
        return lgnUsrRslt;
    }
    
    /**
     * LgnUsrRsltVo 인스턴스를 LgnHistVo 인스턴스로 매핑하여 반환한다. 
     * @param lgnUsrRslt
     * @return
     */
    private LgnHistVo lgnUsrRsltTolgnHist(LgnUsrRsltVo lgnUsrRslt, String lgnDivCd) {
        
        LgnHistVo lgnHist = new LgnHistVo();
        lgnHist.setSiteCd(lgnUsrRslt.getSiteCd());
        lgnHist.setUsrId(lgnUsrRslt.getUsrId());
        lgnHist.setLgnDt(new Date());
        lgnHist.setLgnDivCd(lgnDivCd);
        lgnHist.setIpcAddr(CommonDataHolder.getCommonData().getIpcAddr());
        return lgnHist;
    }
    
    /**
     * 사용자 초대여부를 조회한다. 
     * @param 
     */
    public String findUsrIvtYn(String usrId) {
        
        UsrVo usr = findUsr(usrId);
        if (usr == null) {
            return "N";
        }
        return "US0200".equals(usr.getUsrStsCd()) ? "Y" : "N";
    }

    /**
     * 비밀번호 재설정 이메일을 발송한다. 
     */
    @Override
    public void processPwdRsetReq(PwdRsetReqFormVo pwdRsetReqForm) {
        
        // 사용자 조회
        UsrVo fetchedUsr = findUsrByEmail(pwdRsetReqForm.getEmail());
        Verifier.notNull(fetchedUsr, "M2010");
        Verifier.isEqual(fetchedUsr.getUsrNm(), pwdRsetReqForm.getUsrNm(), "M2010");
        Verifier.notNull(fetchedUsr.getHnpno(), "M2024");
        String fetchedHnpno = StringUtils.replace(StringUtils.replace(fetchedUsr.getHnpno(), "-", ""), " ", "");
        String inputHnpno = StringUtils.replace(StringUtils.replace(pwdRsetReqForm.getHnpno(), "-", ""), " ", "");
        Verifier.isEqual(fetchedHnpno, inputHnpno, "M2010");
               
        // 검증값 생성 
        String rsetVrfVal = StringSupport.randomString(50);
        log.debug("rsetVrfVal : {}", rsetVrfVal);
        
        // 비밀번호재설정요청 생성 및 등록 
        PwdRsetReqVo pwdRsetReq = new PwdRsetReqVo();
        pwdRsetReq.setUsrId(fetchedUsr.getUsrId());
        pwdRsetReq.setRsetReqDt(new Date());
        pwdRsetReq.setRsetVrfVal(rsetVrfVal);
        pwdRsetReq.setRsetCmplYn("N");
        int pwdRsetReqCnt = pwdRsetReqMapper.insertPwdRsetReq(pwdRsetReq);
        Verifier.eq(pwdRsetReqCnt, 1, "M1000", "word.pwdRsetReq");

        String keyText = new StringBuilder()
                .append("usrId=")
                .append(fetchedUsr.getUsrId())
                .append("&usrNm=")
                .append(fetchedUsr.getUsrNm())
                .append("&email=")
                .append(fetchedUsr.getEmail())
                .append("&rsetReqNo=")
                .append(pwdRsetReq.getRsetReqNo())
                .append("&rsetVrfVal=")
                .append(rsetVrfVal)
                .toString();
        
        String encodedKey = null;
        try {
            encodedKey = new String(Base64.encodeBase64(keyText.getBytes("utf-8")));
            log.debug("encodedKey : {}", encodedKey);                    
        } catch (UnsupportedEncodingException e) {
            throw new BizException("M2023", e);
        }
        
        // 이메일 전송요청 
        String tmplLang = BizSupport.emailTemplateLangFromCompNtnCd(compService.findComp(fetchedUsr.getCompId()).getNtnCd());   
        String siteWebUrl = BizSupport.parseSiteWebUrl(CommonDataHolder.getCommonData().getSiteCd(), webUrl);
        List<String> recpList = new ArrayList<String>();
        recpList.add(fetchedUsr.getEmail());
        
        Map<String, Object> varMap = new HashMap<String, Object>();
        varMap.put("USER_NAME", fetchedUsr.getUsrNm());
        varMap.put("LICENSER_COMPANY_NAME", compService.findLicrComp().getCompNm());
        varMap.put("LICENSEE_COMPANY_NAME", fetchedUsr.getCompNm());
        varMap.put("USER_EMAIL", fetchedUsr.getEmail());
        varMap.put("PROCESS_DATE", DateFormatUtils.format(pwdRsetReq.getRsetReqDt(), "yyyy-MM-dd HH:mm:ss"));
        varMap.put("LINK", siteWebUrl + "/reset/password?k=" + encodedKey);
        
        EmailReqFormVo emailReqForm = new EmailReqFormVo();
        emailReqForm.setFormCd("E0110");
        emailReqForm.setTmplLang(tmplLang);
        emailReqForm.setRecpList(recpList);
        emailReqForm.setVarMap(varMap);
        emailService.requestForSendingEmail(emailReqForm);        
        
    }
    
    /**
     * 사용자 기타 필수 정보가 모두 입력되었는지 확인한다.
     * 라이선시 정보중 대표자명, 대표번호, 전화번호, 핸드폰번호는 라이선서가 처리하는 경우에는 필수 입력항목이 아니지만, 
     * 라이선시가 처리하는 경우에는 필수 입력항목이 된다. 
     * 로그인 시 해당 항목이 모두 입력되어 있는지 검증한다.  
     */
    @Override
    public List<String> checkRequiredUsrInfo(String usrId) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        List<String> list = new ArrayList<String>();
        
        // 라이선서의 경우 확인 대상이 아니다. 
        if (AuthSupport.isLicensor(commonData.getAuth())) {
            return list;
        }
                
        // 사용자+회사 조회 
        UsrCompRsltVo usrComp = usrMapper.findUsrCompRsltByKey(commonData.getSiteCd(), usrId);

        // 기타 필수 정보 확인 
        if (StringUtils.isEmpty(usrComp.getCeoNm())) {
            list.add(messageBean.getMessage("M3008"));
        }
        if (StringUtils.isEmpty(usrComp.getRepTelno())) {
            list.add(messageBean.getMessage("M3009"));
        }
        if (StringUtils.isEmpty(usrComp.getTelno())) {
            list.add(messageBean.getMessage("M2020"));
        }
        if (StringUtils.isEmpty(usrComp.getHnpno())) {
            list.add(messageBean.getMessage("M2021"));
        }
        return list;
    }
    
}
