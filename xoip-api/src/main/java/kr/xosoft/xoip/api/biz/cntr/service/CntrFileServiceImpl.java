package kr.xosoft.xoip.api.biz.cntr.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.cntr.mapper.CntrFileMapper;
import kr.xosoft.xoip.api.biz.cntr.vo.CntrFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * 계약파일 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class CntrFileServiceImpl implements CntrFileService {

    @Autowired
    private CntrFileMapper cntrFileMapper;
    
    /**
     * 계약파일을 등록한다. 
     */
    @Override
    public CntrFileVo registCntrFile(CntrFileVo cntrFile) {

        // 계약파일 등록 
        int cnt = cntrFileMapper.insertCntrFile(cntrFile);
        Verifier.eq(cnt, 1, "M1000", "word.cntrFile");

        return cntrFile;
    }

    /**
     * 계약파일을 수정한다. 
     */
    @Override
    public CntrFileVo modifyCntrFile(CntrFileVo cntrFile) {
        
        // 계약파일 조회 
        CntrFileVo fetchedCntrFile = findCntrFile(cntrFile.getCntrId(), cntrFile.getCntrFileNo());
        Verifier.notNull(fetchedCntrFile, "M1003", "word.cntrFile");
        
        fetchedCntrFile.setCntrFileDivCd(cntrFile.getCntrFileDivCd());
        fetchedCntrFile.setMngFileNm(cntrFile.getMngFileNm());
        
        // 계약 수정 
        int cnt = cntrFileMapper.updateCntrFileByKey(fetchedCntrFile);
        Verifier.eq(cnt, 1, "M1001", "word.cntrFile");

        return fetchedCntrFile;        
    }

    /**
     * 계약파일을 삭제한다. 
     */
    @Override
    public void deleteCntrFile(String cntrId, Integer cntrFileNo) {
        
        // 계약파일 삭제 
        CntrFileVo cntrFile = new CntrFileVo();
        cntrFile.setCntrId(cntrId);
        cntrFile.setCntrFileNo(cntrFileNo);
        int cnt = cntrFileMapper.deleteCntrFileByKey(cntrFile); 
        Verifier.eq(cnt, 1, "M1002", "word.cntrFile");        
    }

    /**
     * 계약파일을 조회한다. 
     */
    @Override
    public CntrFileVo findCntrFile(String cntrId, Integer cntrFileNo) {

        CommonData commonData = CommonDataHolder.getCommonData();
        return cntrFileMapper.findCntrFileByKey(commonData.getSiteCd(), cntrId, cntrFileNo);
    }

    /**
     * 계약파일 목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<CntrFileVo> findCntrFilePage(String cntrId, CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        map.put("cntrId", cntrId);
        
        int comTtlCnt = cntrFileMapper.countCntrFileListByMap(map);
        List<CntrFileVo> cntrList = cntrFileMapper.findCntrFileListByMap(map);
        
        return new CommonPage<CntrFileVo>(condition, cntrList, comTtlCnt);           
    }

}
