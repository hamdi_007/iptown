package kr.xosoft.xoip.api.biz.com.vo;

import java.util.List;
import lombok.Data;

@Data
public class JusoResultVo {
    
    private JusoCommonVo common;
    
    private List<JusoDataVo> juso;
    
}
