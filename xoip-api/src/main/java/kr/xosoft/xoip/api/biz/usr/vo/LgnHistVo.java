package kr.xosoft.xoip.api.biz.usr.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 로그인이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class LgnHistVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 사용자ID
     */
    private String usrId;

    /**
     * 로그인이력번호
     */
    private Integer lgnHistNo;

    /**
     * 로그인일시
     */
    private Date lgnDt;

    /**
     * 로그인구분코드
     */
    private String lgnDivCd;

    /**
     * 인터넷프로토콜주소
     */
    private String ipcAddr;

}
