package kr.xosoft.xoip.api.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import kr.xosoft.xoip.api.common.auth.JwtLoginFilter;
import kr.xosoft.xoip.api.biz.usr.service.UsrService;
import kr.xosoft.xoip.api.common.auth.JwtAuthenticationFilter;

/**
 * 시큐리티 설정 
 * @author likejy
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UsrService usrService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers(HttpMethod.GET, "/api/*/users/email/miss") // 이메일 찾기
            .antMatchers(HttpMethod.POST, "/api/1/users/password/miss") // 비밀번호 재설정 요청
            .antMatchers(HttpMethod.PUT, "/api/1/users/*/password/reset") // 비밀번호 재설정
            .antMatchers(HttpMethod.PUT, "/api/1/users/*/password/join") // 라이선시 사용자 가입 - 비밀번호 설정
            .antMatchers(HttpMethod.GET, "/api/1/users/*/invited") // 라이선시 사용자 초대여부 조회 
            .antMatchers(
                    "/index.html", 
                    "/resources/**", 
                    "/css/**", 
                    "/fonts/**", 
                    "/img/**", 
                    "/js/**", 
                    "/erros");  
    }
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
        
        http.cors().and()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .addFilterAt(jwtLoginFilter(), UsernamePasswordAuthenticationFilter.class)
            .addFilterAt(jwtAuthenticationFilter(), BasicAuthenticationFilter.class)
            .authorizeRequests()
                .anyRequest().authenticated().and()
            .headers().cacheControl();        
    }
    
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(this.usrService);        
        return daoAuthenticationProvider;       
    }    
        
    @Bean
    public JwtLoginFilter jwtLoginFilter() throws Exception {
        JwtLoginFilter filter = new JwtLoginFilter(authenticationManager());
        return filter;
    }
    
    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() throws Exception {
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter(authenticationManager());
        return filter;
    }    
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }    
}
