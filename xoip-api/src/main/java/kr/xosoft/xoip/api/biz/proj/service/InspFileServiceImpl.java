package kr.xosoft.xoip.api.biz.proj.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.proj.mapper.InspFileMapper;
import kr.xosoft.xoip.api.biz.proj.vo.InspFileVo;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * 검수파일 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class InspFileServiceImpl implements InspFileService {

    @Autowired
    private InspFileMapper inspFileMapper;
    
    /**
     * 검수파일을 등록/수정/삭제한다. 
     */
    @Override
    public List<InspFileVo> mergeInspFileList(String projCd, Integer inspNo, List<InspFileVo> inspFileList) {

        List<InspFileVo> createdList = new ArrayList<InspFileVo>();
        List<InspFileVo> updatedList = new ArrayList<InspFileVo>();
        List<InspFileVo> deletedList = findInspFileList(projCd, inspNo);
        
        // 등록/수정/삭제대상 목록 필터링    
        if (inspFileList != null) {
            for (InspFileVo file : inspFileList) {
                // 검수 파일번호가 없는 경우는 신규 등록 파일인 경우임 
                if (file.getInspFileNo() == null) {
                    createdList.add(file);
                    continue;
                }
                int idx = -1;
                for (int i = 0; i < deletedList.size(); i++) {
                    if (deletedList.get(i).getInspFileNo() == file.getInspFileNo()) {
                        idx = i;
                        break;
                    }
                }
                if (idx >= 0) {
                    deletedList.remove(idx);
                    updatedList.add(file);
                } else {
                    createdList.add(file);
                }
            }
        } 
        
        // 검수파일 삭제 
        deleteInspFileList(deletedList);
        
        // 저장된 검수파일 값 수정
        modifyInspFileList(updatedList);
        
        // 신규 검수파일 등록 
        registInspFileList(createdList);
        
        return inspFileList;
    }
    
    /**
     * 검수파일을 등록한다. 
     */
    private void registInspFileList(List<InspFileVo> inspFileList) {
        
        // 검수파일 등록
        if (inspFileList != null) {
            for (InspFileVo inspFile : inspFileList) {
                int cnt = inspFileMapper.insertInspFile(inspFile);
                Verifier.eq(cnt, 1, "M1000", "word.inspFile");
            }
        }
    }

    /**
     * 검수파일을 수정한다. 
     */
    public void modifyInspFileList(List<InspFileVo> inspFileList) {
        
        // 검수파일 수정 
        if (inspFileList != null) {
            for (InspFileVo inspFile : inspFileList) {
                int cnt = inspFileMapper.updateInspFileByKey(inspFile);
                Verifier.eq(cnt, 1, "M1001", "word.inspFile");
            }
        }
    }
    
    /**
     * 검수파일목록을 삭제한다. 
     */
    private void deleteInspFileList(List<InspFileVo> inspFileList) {
        
        // 검수파일 삭제
        if (inspFileList != null) {
            for (InspFileVo inspFile : inspFileList) {
                int cnt = inspFileMapper.deleteInspFileByKey(inspFile); 
                Verifier.eq(cnt, 1, "M1002", "word.inspFile");                        
            }
        }
    }

    /**
     * 검수파일을 삭제한다. 
     */
    @Override
    public void deleteInspFile(String projCd, Integer inspNo, Integer inspFileNo) {

        InspFileVo inspFile = findInspFile(projCd, inspNo, inspFileNo);
        Verifier.notNull(inspFile, "M1003", "word.inspFile");
        
        int cnt = inspFileMapper.deleteInspFileByKey(inspFile); 
        Verifier.eq(cnt, 1, "M1002", "word.inspFile");   
    }    

    /**
     * 검수파일을 조회한다. 
     */
    @Override
    public InspFileVo findInspFile(String projCd, Integer inspNo, Integer inspFileNo) {

        CommonData commonData = CommonDataHolder.getCommonData();
        return inspFileMapper.findInspFileByKey(commonData.getSiteCd(), projCd, inspNo, inspFileNo);
    }

    /**
     * 검수파일 목록 페이지를 조회한다. 
     */
    @Override
    public List<InspFileVo> findInspFileList(String projCd, Integer inspNo) {

        CommonData commonData = CommonDataHolder.getCommonData();
        return inspFileMapper.findInspFileList(commonData.getSiteCd(), projCd, inspNo);
    }

}
