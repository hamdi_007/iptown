package kr.xosoft.xoip.api.biz.com.service;

import java.util.List;
import java.util.Map;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import kr.xosoft.xoip.api.biz.com.vo.SiteComCdVo;

/**
 * 공통코드 서비스
 * @author likejy
 *
 */
public interface ComCdService {

    public Map<String, SiteComCdVo> findAllSiteComCdList();
    
    public String findComCdNmByComCd(String comCd);
    
    public List<ComCdVo> findComCdListByTopComCd(String topComCd, boolean isOnlyAvailable);
    
    public Map<String, Object> findComCdListByCommaSeperatedTopComCd(String commaSeperatedTopComCd, boolean isOnlyAvailable);
	
}
