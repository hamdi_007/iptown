package kr.xosoft.xoip.api.biz.usr.vo;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.parser.AvailableLicensorStringSerializer;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 사용자 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class UsrVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 사용자ID
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=10, max=10, groups=UpdatingGroup.class )
    private String usrId;

    /**
     * 회사ID
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=10, max=10, groups=DefaultGroup.class )
    private String compId;
    
    /**
     * 회사명 
     */
    private String compNm;
    
    /**
     * 이메일
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    @Email( message="chk.email", groups=DefaultGroup.class )    
    private String email;

    /**
     * 사용자명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String usrNm;

    /**
     * 사용자영문명
     */
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String usrEngNm;

    /**
     * 비밀번호
     */
    @JsonProperty( access=Access.WRITE_ONLY )
    @Size( message="chk.size", min=8, max=15, groups=CreatingGroup.class )
    private String pwd;
    
    /**
     * 비밀번호 확인 
     */
    @JsonProperty( access=Access.WRITE_ONLY )
    @Size( message="chk.size", min=8, max=15, groups=CreatingGroup.class )
    private String pwdCnfm;

    /**
     * 전화번호
     */
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )
    private String telno;

    /**
     * 핸드폰번호
     */
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )
    private String hnpno;

    /**
     * 권한
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=50, groups=DefaultGroup.class )
    private String auth;

    /**
     * 사용자상태코드
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=UpdatingGroup.class )
    private String usrStsCd;

    /**
     * 사용자상태코드명
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String usrStsCdNm;    
    
    /**
     * 초대검증값 
     */
    @JsonIgnore
    private String ivtVrfVal;
    
    /**
     * 초대일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonFormat( shape=JsonFormat.Shape.NUMBER )
    private Date ivtDt;

    /**
     * 초대자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String ivtrId;
    
    /**
     * 가입일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonFormat( shape=JsonFormat.Shape.NUMBER )
    private Date joinDt;

    /**
     * 정지일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonFormat( shape=JsonFormat.Shape.NUMBER )
    private Date sspnDt;

    /**
     * 정지처리자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String sspnHndlrId;
    
    /**
     * 탈퇴일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonFormat( shape=JsonFormat.Shape.NUMBER )
    private Date wtdrDt;

    /**
     * 탈퇴처리자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String wtdrHndlrId;
    
    /**
     * 비고
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    @JsonSerialize(using = AvailableLicensorStringSerializer.class)
    private String rmk;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;

}
