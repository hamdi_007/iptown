package kr.xosoft.xoip.api.common.aop;

import java.io.IOException;
import java.util.Iterator;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestTemplateInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        
        if (log.isDebugEnabled()) {
            log.debug("request uri : {}", request.getURI().toString());
            log.debug("request method : {}", request.getMethod().name());
            HttpHeaders headers = request.getHeaders();
            Iterator<String> it = headers.keySet().iterator();
            while(it.hasNext()) {
                String header = it.next();
                log.debug("request header : {} - {}", header, headers.get(header));                
            }
            log.debug("request body : {}", new String(body, "UTF-8"));
        }
        
        ClientHttpResponse response = execution.execute(request, body);

        if (log.isDebugEnabled()) {
            log.debug("statusCode : {} {}", response.getStatusCode().value(), response.getStatusCode().name());
        }
        
        return response;
    }

}
