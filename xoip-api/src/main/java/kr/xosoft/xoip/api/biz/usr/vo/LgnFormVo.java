package kr.xosoft.xoip.api.biz.usr.vo;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 로그인 폼 VO
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class LgnFormVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 이메일 
     */
    private String email;
    
    /**
     * 비밀번호 
     */
    private String pwd;
    
}
