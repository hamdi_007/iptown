package kr.xosoft.xoip.api.biz.comp.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 회사파일 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class CompFileVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 회사ID
     */
    private String compId;

    /**
     * 회사파일번호
     */
    private Integer compFileNo;

    /**
     * 회사파일구분코드
     */
    private String compFileDivCd;
    
    /**
     * 회사파일구분코드명 
     */
    private String compFileDivCdNm;

    /**
     * 원본파일명
     */
    private String orgFileNm;

    /**
     * 관리파일명
     */
    private String mngFileNm;

    /**
     * 물리파일명
     */
    private String pscFileNm;

    /**
     * 파일경로
     */
    private String filePath;

    /**
     * 파일컨텐츠유형
     */
    private String fileCntnTyp;

    /**
     * 파일크기
     */
    private Long fileSize;

    /**
     * 파일확장자명
     */
    private String fileExtNm;

    /**
     * 정렬순번
     */
    private Integer srtOrdno;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;

}
