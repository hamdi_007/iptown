package kr.xosoft.xoip.api.biz.usr.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.usr.vo.UsrHistVo;

/**
 * 사용자이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface UsrHistMapper {

    /**
     * 사용자이력 등록 
     */
    public void insertUsrHistAsSelectMaster(UsrHistVo usrHist);
        
}
