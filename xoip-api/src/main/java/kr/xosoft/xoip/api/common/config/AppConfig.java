package kr.xosoft.xoip.api.common.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.LocalCacheScope;
import org.mybatis.spring.SqlSessionFactoryBean;
//import javax.naming.NamingException;
//import javax.sql.DataSource;
//import org.apache.ibatis.session.LocalCacheScope;
//import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DelegatingDataSource;
import org.springframework.jndi.JndiTemplate;
import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.jdbc.datasource.DataSourceTransactionManager;
//import org.springframework.jdbc.datasource.DelegatingDataSource;
//import org.springframework.jndi.JndiTemplate;
//import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 앱 설정 
 * @author likejy
 *
 */
@Configuration
@ComponentScan( basePackages = { "kr.xosoft.xoip.api" },
				excludeFilters = @Filter({ org.springframework.web.bind.annotation.RestController.class }) )
@MapperScan( basePackages = { "kr.xosoft.xoip.api.biz.**.mapper" }, 
             annotationClass = org.springframework.stereotype.Repository.class )
@EnableTransactionManagement
@EnableAspectJAutoProxy
@PropertySource( "classpath:api.properties" )
public class AppConfig {

    @Bean
    public ObjectMapper objectMapper() {
        
        return new ObjectMapper();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:META-INF/locale/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
        
        AcceptHeaderLocaleResolver resolver = new AcceptHeaderLocaleResolver();
        return resolver;
    }
    
    @Bean
    public DataSource dataSource() throws NamingException {
        
        JndiTemplate jndiTemplate = new JndiTemplate();
        DataSource dataSource = null;
        dataSource = (DataSource) jndiTemplate.lookup("java:comp/env/jdbc/xoip");
        return new DelegatingDataSource(dataSource);
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws NamingException {
        
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactory() throws NamingException {
        
        String[] typeAliasesPackages = {
                "kr.xosoft.xoip.api.biz.cntr.vo",
                "kr.xosoft.xoip.api.biz.com.vo",
                "kr.xosoft.xoip.api.biz.comp.vo",
                "kr.xosoft.xoip.api.biz.email.vo",
                "kr.xosoft.xoip.api.biz.ip.vo",
                "kr.xosoft.xoip.api.biz.prodtyp.vo",
                "kr.xosoft.xoip.api.biz.proj.vo",
                "kr.xosoft.xoip.api.biz.site.vo",
                "kr.xosoft.xoip.api.biz.stmpappl.vo",
                "kr.xosoft.xoip.api.biz.usr.vo",
                "kr.xosoft.xoip.api.biz.faq.vo",
                "kr.xosoft.xoip.api.biz.stats.vo",
                "kr.xosoft.xoip.api.biz.ntc.vo"
        };
        
        // 주의: MyBatis 의 로컬캐시 스콥은 기본값이 SESSION 이며,
        // 이경우 동일 세션에서 동일한 SQL을 반복 실행하는 경우 실제로 쿼리가 재실행 되지 않고, 이전에 사용된 결과셋이 재사용된다.
        // 경우에 따라서 원치 않는 결과를 가져올 수 있으므로, 여기서는 캐시를 사용하지 않도록 설정한다.
        org.apache.ibatis.session.Configuration mybatisConfig = new org.apache.ibatis.session.Configuration();
        mybatisConfig.setMapUnderscoreToCamelCase(true);
        mybatisConfig.setLocalCacheScope(LocalCacheScope.STATEMENT);

        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setTypeAliasesPackage(StringUtils.join(typeAliasesPackages, ","));
        sessionFactoryBean.setConfiguration(mybatisConfig);
        return sessionFactoryBean;
    }
    
}
