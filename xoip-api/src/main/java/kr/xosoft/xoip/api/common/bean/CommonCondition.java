package kr.xosoft.xoip.api.common.bean;

import java.util.HashMap;
import java.util.Map;

import kr.xosoft.xoip.api.common.support.AuthSupport;
import kr.xosoft.xoip.api.common.support.StringSupport;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 공통 검색조건
 * @author likejy
 *
 */
@Data
@Slf4j
public class CommonCondition {
    
    /**
     * 정렬칼럼 
     */
    private String comSrtBy;
    
    /**
     * 역정렬여부
     */
    private Boolean comSrtDesc;

	/**
	 * 페이지 번호 
	 */
	private Integer comPageNo;
	
	/**
	 * 페이지당 아이템 개수 
	 */
	private Integer comItemCntPerPage;
	
	/**
	 * 검색 파라미터 키 
	 */
	private String[] comParamKeys;

	/**
	 * 검색 파라미터 값
	 */
	private String[] comParamVals;

	public CommonCondition() {
		comPageNo = 1;
		comItemCntPerPage = 10;
	}
	
	/**
	 * 검색조건을 map 인스턴스로 변환하여 반환한다. 
	 * @return
	 */
	public Map<String, Object> toMap() {
		
		Map<String, Object> map = new HashMap<String, Object>();
        AuthSupport.assignComLgnCompIdIfLicensee(map);
		map.put("siteCd", CommonDataHolder.getCommonData().getSiteCd());
		map.put("comLang", CommonDataHolder.getCommonData().getLang());
		
	    map.put("comSrtTxt", getComSrtTxt());
		map.put("comPageNo", comPageNo);
		map.put("comItemCntPerPage", comItemCntPerPage);		
		map.put("comItemOffset", getComItemOffset());
		
		if (comParamKeys == null || comParamVals == null) {
			return map;
		}
		if (comParamKeys.length != comParamVals.length) {
			throw new IllegalArgumentException("Parameter keys length (" + comParamKeys.length + ") is different from parameter values length (" + comParamVals.length + ")");
		}
		
		int idx = 0;
		for (String comParamKey : comParamKeys) {
			map.put(comParamKey, comParamVals[idx]);
			idx++;
		}
		
		if (log.isDebugEnabled()) {
			log.debug("commonCondition : {}", this);
			log.debug("map from commonCodition : {}", map);
		}
		return map;
	}
	
	/**
	 * 검색조건을 map 인스턴스로 변환하여 반환한다. 
	 * 단, map 인스턴스에 페이징 처리를 위한 변수는 포함되지 않는다.   
	 * @return
	 */
	public Map<String, Object> toMapWithoutPaging() {
	    
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("siteCd", CommonDataHolder.getCommonData().getSiteCd());
        map.put("comSrtTxt", getComSrtTxt());
        
        if (comParamKeys == null || comParamVals == null) {
            return map;
        }
        if (comParamKeys.length != comParamVals.length) {
            throw new IllegalArgumentException("Parameter keys length (" + comParamKeys.length + ") is different from parameter values length (" + comParamVals.length + ")");
        }
        
        int idx = 0;
        for (String comParamKey : comParamKeys) {
            map.put(comParamKey, comParamVals[idx]);
            idx++;
        }
        
        if (log.isDebugEnabled()) {
            log.debug("commonCondition : {}", this);
            log.debug("map from commonCodition (without paging) : {}", map);
        }
        return map;	    
	}

	/**
	 * 페이징처리를 위한 offset 값을 반환한다. 
	 * @return
	 */
	public Integer getComItemOffset() {
		return comItemCntPerPage * (comPageNo - 1);
	}
	
	/**
	 * 정렬 처리를 위한 키워드 값을 반환한다. 
	 * @return
	 */
	public String getComSrtTxt() {
	    if (comSrtBy == null || comSrtBy.trim().length() == 0 || comSrtDesc == null) {
	        return null;
	    }
	    return StringSupport.camelCaseToUnderScore(comSrtBy) + (comSrtDesc ? " DESC" : " ASC");        
	}

}
