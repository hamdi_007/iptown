package kr.xosoft.xoip.api.biz.proj.service;

import java.util.List;

import kr.xosoft.xoip.api.biz.proj.vo.OpinFileVo;

public interface OpinFileService {

    public List<OpinFileVo> registOrDeleteFileList(String projCd, Integer inspNo, List<OpinFileVo> opinFileList);

    public void deleteOpinFile(String projCd, Integer inspNo, Integer inspFileNo, Integer opinFileNo);
    
    public OpinFileVo findOpinFile(String projCd, Integer inspNo, Integer inspFileNo, Integer opinFileNo);
    
    public List<OpinFileVo> findOpinFileList(String projCd, Integer inspNo);
    
}
