package kr.xosoft.xoip.api.biz.proj.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.parser.AvailableLicensorStringSerializer;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=UpdatingGroup.class )    
    private String projCd;

    /**
     * 프로젝트명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )    
    private String projNm;

    /**
     * 계약ID
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=16, max=16, groups=DefaultGroup.class )    
    private String cntrId;
    
    /**
     * 계약번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private String cntrNo;
    
    /**
     * 회사ID 
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private String compId;
    
    /**
     * 회사코드
     */
    @JsonIgnore
    private String compCd;
    
    /**
     * IPID 
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private String ipId;

    /**
     * IP코드 
     */
    @JsonIgnore
    private String ipCd;
    
    /**
     * 상위상품유형ID
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=16, max=16, groups=DefaultGroup.class )    
    private String topProdTypId;    
    
    /**
     * 상위상품유형명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String topProdTypNm;
    
    /**
     * 상품유형ID
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=16, max=16, groups=DefaultGroup.class )    
    private String prodTypId;

    /**
     * 상품유형명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String prodTypNm;
    
    /**
     * 프로젝트단계코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )        
    private String projStgCd;

    /**
     * 프로젝트단계코드명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projStgCdNm;
    
    /**
     * 프로젝트상태코드
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projStsCd;
    
    /**
     * 프로젝트상태코드명 
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projStsCdNm;

    /**
     * 상품번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )  
    private String prodNo;

    /**
     * 출고계획년월
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=6, groups=DefaultGroup.class )  
    private String rlsPlanYm;

    /**
     * 예상출고금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal estmRlsAmt;

    /**
     * 예상소비자금액
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal estmCnsmAmt;
    
    /**
     * 출고금액율 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private BigDecimal rlsAmtRt;
    
    /**
     * 통화코드 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private String ccCd;

    /**
     * 프로젝트생성일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date projCrtDt;

    /**
     * 프로젝트생성자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projCrtrId;

    /**
     * 프로젝트승인요청일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date projApvReqDt;

    /**
     * 프로젝트승인요청자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projApvReqrId;

    /**
     * 프로젝트승인일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date projApvDt;

    /**
     * 프로젝트승인자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projApvrId;

    /**
     * 프로젝트완료일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date projCmplDt;

    /**
     * 프로젝트완료자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projCmplrId;

    /**
     * 프로젝트반려일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date projRejtDt;

    /**
     * 프로젝트반려자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projRejtrId;

    /**
     * 프로젝트취소일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    private Date projCnclDt;

    /**
     * 프로젝트취소자ID
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String projCnclrId;

    /**
     * 비고
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    @JsonSerialize( using = AvailableLicensorStringSerializer.class )    
    private String rmk;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;

    /**
     * 상품판매국가코드 목록 
     */
    @NotEmpty( message="chk.notEmpty", groups=DefaultGroup.class )
    @JsonInclude( JsonInclude.Include.NON_NULL )
    private List<String> prodSaleNtnCdList;
    
    /**
     * 상품판매채널코드 목록 
     */
    @NotEmpty( message="chk.notEmpty", groups=DefaultGroup.class )
    @JsonInclude( JsonInclude.Include.NON_NULL )
    private List<String> prodSaleChnlCdList;
    
    /**
     * 상품지원언어코드 목록 
     */
    @NotEmpty( message="chk.notEmpty", groups=DefaultGroup.class )
    @JsonInclude( JsonInclude.Include.NON_NULL )
    private List<String> prodSprtLangCdList;
    
    /**
     * 상품대상연령코드 목록 
     */
    @NotEmpty( message="chk.notEmpty", groups=DefaultGroup.class )
    @JsonInclude( JsonInclude.Include.NON_NULL )
    private List<String> prodTgtAgeCdList;
    
    /**
     * 상품대상성별코드 목록 
     */
    @NotEmpty( message="chk.notEmpty", groups=DefaultGroup.class )
    @JsonInclude( JsonInclude.Include.NON_NULL )
    private List<String> prodTgtSexCdList;    
        
}
