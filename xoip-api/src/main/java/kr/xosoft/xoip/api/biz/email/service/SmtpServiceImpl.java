package kr.xosoft.xoip.api.biz.email.service;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import kr.xosoft.xoip.api.biz.email.vo.EmailTrnsHistVo;
import kr.xosoft.xoip.api.biz.email.vo.EmailTrnsVo;

/**
 * SMTP 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
@Slf4j
public class SmtpServiceImpl implements SmtpService {

    @Autowired
    private EmailService emailService;

    @Async
    @Override
    public void sendEmail(EmailTrnsVo emailTrns) {

        EmailTrnsHistVo emailTrnsHist = new EmailTrnsHistVo(emailTrns);
        
        Properties props = new Properties();
        props.put("mail.transport.protocol", emailTrns.getSmtpPrtc());
        props.put("mail.smtp.port", emailTrns.getPortNo()); 
        props.put("mail.smtp.starttls.enable", emailTrns.isTlsUse());
        props.put("mail.smtp.auth", emailTrns.isAtrz());
        props.put("mail.smtp.ssl.enable", emailTrns.isSslUse());       
        props.put("mail.smtp.connectiontimeout", 3000);
        props.put("mail.smtp.timeout", 5000);
        
        Session session = Session.getDefaultInstance(props);
        Transport transport = null;
        try {
            
            transport = session.getTransport();
            transport.connect(emailTrns.getSvrHost(), emailTrns.getPortNo(), emailTrns.getSmtpUsrId(), emailTrns.getSmtpUsrPwd());

            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(emailTrns.getTrnsrEmail(), emailTrns.getTrnsrNm()));
            for (String recpEmail : emailTrns.getRecpList()) {
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recpEmail));                
            }

            msg.setSubject(emailTrns.getEmailSbj());
            msg.setContent(emailTrns.getEmailCnts(), "text/html;charset=euc-kr");
            
            transport.sendMessage(msg, msg.getAllRecipients());
            emailTrnsHist.setTrnsStsCd("TS0100");

        } catch (Exception e) {
            log.warn("Failed to send email : {}", emailTrns);
            if (log.isInfoEnabled()) {
                log.info(ExceptionUtils.getStackTrace(e));                
            } else {
                log.warn(e.toString());
            }
            emailTrnsHist.setTrnsStsCd("TS0200");
            emailTrnsHist.setErrCnts(e.getMessage() != null && e.getMessage().length() > 2000 ? e.getMessage().substring(0, 2000) : e.getMessage());
        } finally {
            try { if (transport != null) transport.close(); } catch (MessagingException ignore) {}
            emailTrnsHist.setTrnsEndDt(new Date());   
            // 이메일 전송 이력 등록 
            emailService.registEmailTrnsHist(emailTrnsHist);
        }        
    }
    
}
