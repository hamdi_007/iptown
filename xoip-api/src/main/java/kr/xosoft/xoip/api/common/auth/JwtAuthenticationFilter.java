package kr.xosoft.xoip.api.common.auth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.MessageBean;
import lombok.extern.slf4j.Slf4j;

/**
 * JWT 인증 필터 
 * @author likejy
 *
 */
@Component
@Slf4j
public class JwtAuthenticationFilter extends BasicAuthenticationFilter {

	private static final String AUTHORIZATION_KEY = "Authorization";
	
	private static final String TOKEN_PREFIX = "Bearer ";
		
	@Autowired
	JwtUtil jwtUtil;
	
    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private MessageBean messageBean;   	
	
    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    /**
     * 인증 처리 
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        log.debug("attempt authorization : {}", request.getRequestURL());
        
        try {

            // 요청헤더에서 authorization 문자열 값을 읽는다 
            String authorizationString = request.getHeader(AUTHORIZATION_KEY);

            // authorization 문자열 값이 규격에 맞지 않는 경우 리턴 - 이후 Spring security에서 처리 
            if(authorizationString == null || !authorizationString.startsWith(TOKEN_PREFIX)) {
                if (request.getRequestURI().endsWith("/auth/login") && SecurityContextHolder.getContext().getAuthentication() != null) {
                    chain.doFilter(request, response);
                    return;
                } else {
                    throw new AuthenticationCredentialsNotFoundException("token is not found");                    
                } 
            }

            // 인증 토큰 파싱  
            String token = authorizationString.substring(7); 
            UserDetails userDetails = null;
            try {
                userDetails = jwtUtil.parseToken(token);                    
            } catch (ExpiredJwtException e) {
                throw new CredentialsExpiredException("token is expired", e);                    
            } catch (JwtException e) {
                throw new BadCredentialsException("failed to parse token", e);
            }
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            
            // 인증내용 시큐리티 컨텍스트에 저장 
            SecurityContextHolder.getContext().setAuthentication(authentication);
                
        } catch (AuthenticationException e) {
            SecurityContextHolder.clearContext();
            onUnsuccessfulAuthentication(request, response, e);
            return;
        }
        
        // Continue filter execution
        chain.doFilter(request, response);
        
    }
    
    /**
     * 인증 처리 실패 
     */
    @Override
    protected void onUnsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException  {
        
        int status = 0;
        CommonResult result = new CommonResult();
        if (exception instanceof AuthenticationCredentialsNotFoundException) {
            status = HttpStatus.UNAUTHORIZED.value();            
            result.setErrorCode("E4011");
        } else if (exception instanceof CredentialsExpiredException) {
            status = HttpStatus.UNAUTHORIZED.value();            
            result.setErrorCode("E4012");
        } else if (exception instanceof BadCredentialsException) {
            status = HttpStatus.UNAUTHORIZED.value();
            result.setErrorCode("E4013");
        } else {
            status = HttpStatus.FORBIDDEN.value();            
            result.setErrorCode("E4030");
        }
        result.setErrorMessage(messageBean.getMessage(result.getErrorCode()));
        
        response.setCharacterEncoding("UTF-8");
        response.setStatus(status);
        response.getWriter().write(objectMapper.writeValueAsString(result));        

    }    

}
