package kr.xosoft.xoip.api.biz.ip.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.biz.cntr.service.CntrService;
import kr.xosoft.xoip.api.biz.ip.mapper.IpHistMapper;
import kr.xosoft.xoip.api.biz.ip.mapper.IpMapper;
import kr.xosoft.xoip.api.biz.ip.vo.IpHistVo;
import kr.xosoft.xoip.api.biz.ip.vo.IpVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;

/**
 * IP 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class IpServiceImpl implements IpService {

    @Autowired
    private CntrService cntrService;
    
    @Autowired
    private IpMapper ipMapper;
    
    @Autowired
    private IpHistMapper ipHistMapper;
    
    /**
     * IP를 등록한다. 
     */
    @Override
    public IpVo registIp(IpVo ip) {
        
        // IP코드 중복 확인
        IpVo fetchedIp = findIpByIpCd(ip.getIpCd());
        Verifier.isNull(fetchedIp, "M4000");
        
        // IP 등록 
        int cnt = ipMapper.insertIp(ip);
        Verifier.eq(cnt, 1, "M1000", "word.ip");

        // IP 이력 등록 
        IpHistVo ipHist = new IpHistVo(ip.getIpId(), "HD0100");
        ipHistMapper.insertIpHistAsSelectMaster(ipHist);
        Verifier.notNull(ipHist.getIpHistSeqno(), "M1000", "word.ipHist");
        
        return ip;
    }

    /**
     * IP를 수정한다. 
     */
    @Override
    public IpVo modifyIp(IpVo ip) {
        
        // IP 조회 
        IpVo fetchedIp = findIp(ip.getIpId());
        Verifier.notNull(fetchedIp, "M1003", "word.ip");
        
        fetchedIp.setIpNm(ip.getIpNm());
        fetchedIp.setIpEngNm(ip.getIpEngNm());
        fetchedIp.setUseYn(ip.getUseYn());
        fetchedIp.setSrtOrdno(ip.getSrtOrdno());
        fetchedIp.setDtlDesc(ip.getDtlDesc());
        fetchedIp.setEngDtlDesc(ip.getEngDtlDesc());
        
        // IP 수정 
        int cnt = ipMapper.updateIpByKey(fetchedIp);
        Verifier.eq(cnt, 1, "M1001", "word.ip");

        // IP 이력 등록 
        IpHistVo ipHist = new IpHistVo(ip.getIpId(), "HD0200");
        ipHistMapper.insertIpHistAsSelectMaster(ipHist);
        Verifier.notNull(ipHist.getIpHistSeqno(), "M1000", "word.ipHist");
        
        return ip;        
    }

    /**
     * IP를 삭제한다. 
     */
    @Override
    public void deleteIp(String ipId) {
        
        // 등록된 계약이 존재하는 경우 IP 삭제 불가 
        String existYn = cntrService.findCntrExistYnByIpId(ipId);
        Verifier.isEqual(existYn, "N", "M4001");        
        
        // IP 삭제 
        IpVo ip = new IpVo();
        ip.setIpId(ipId);
        int cnt = ipMapper.deleteIpByKey(ip); 
        Verifier.eq(cnt, 1, "M1002", "word.ip");
        
        // IP 이력 등록 
        IpHistVo ipHist = new IpHistVo(ipId, "HD0300");
        ipHistMapper.insertIpHistAsSelectMaster(ipHist);
        Verifier.notNull(ipHist.getIpHistSeqno(), "M1000", "word.ipHist");        
        
    }

    /**
     * IP를 조회한다. 
     */
    @Override
    public IpVo findIp(String ipId) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return ipMapper.findIpByKey(siteCd, ipId);
    }

    /**
     * IP코드로 IP를 조회한다. 
     */
    public IpVo findIpByIpCd(String ipCd) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return ipMapper.findIpByIpCd(siteCd, ipCd);        
    }
    
    /**
     * IP목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<IpVo> findIpPage(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        
        int comTtlCnt = ipMapper.countIpListByMap(map);
        List<IpVo> ipList = ipMapper.findIpListByMap(map);
        
        return new CommonPage<IpVo>(condition, ipList, comTtlCnt);           
    }

}
