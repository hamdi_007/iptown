package kr.xosoft.xoip.api.biz.ntc.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.biz.ntc.mapper.NtcHistMapper;
import kr.xosoft.xoip.api.biz.ntc.mapper.NtcMapper;
import kr.xosoft.xoip.api.biz.ntc.mapper.NtcReadHistMapper;
import kr.xosoft.xoip.api.biz.ntc.vo.NtcHistVo;
import kr.xosoft.xoip.api.biz.ntc.vo.NtcReadHistVo;
import kr.xosoft.xoip.api.biz.ntc.vo.NtcVo;

/**
 * 공지사항 서비스 Impl
 * @author hypark
 *
 */
@Transactional
@Service
public class NtcServiceImpl implements NtcService {
	
    @Autowired
    private NtcMapper ntcMapper;
    
    @Autowired
    private NtcHistMapper ntcHistMapper;
    
    @Autowired
    private NtcReadHistMapper ntcReadHistMapper;
    
    /**
     * 공지사항을 등록한다. 
     */
    @Override
	public NtcVo registNtc(NtcVo ntc) {
		 
		// 공지사항 등록 
        int cnt = ntcMapper.insertNtc(ntc);
        Verifier.eq(cnt, 1, "M1000", "word.ntc");

        ntc.setRegDt(ntc.getModDt());
        ntc.setReadCnt(0);
        
        // 공지사항 이력 등록 
        NtcHistVo ntcHist = new NtcHistVo(ntc.getNtcNo(), "HD0100");
        ntcHistMapper.insertNtcHistAsSelectMaster(ntcHist);
        Verifier.notNull(ntcHist.getNtcHistSeqno(), "M1000", "word.ntcHist");
        
        return ntc;
        
	}

	/**
     * 공지사항을 수정한다. 
     */
	@Override
	public NtcVo modifyNtc(NtcVo ntc) {
		
		// 공지사항 조회 
		NtcVo fetchedNtc = findNtc(ntc.getNtcNo());
        Verifier.notNull(fetchedNtc, "M1003", "word.ntc");
        
        fetchedNtc.setLangVal(ntc.getLangVal());
        fetchedNtc.setNtcSbj(ntc.getNtcSbj());
        fetchedNtc.setNtcCnts(ntc.getNtcCnts());
        fetchedNtc.setHeadFixYn(ntc.getHeadFixYn());
        fetchedNtc.setPostYn(ntc.getPostYn());
        
        // 공지사항 수정 
        int cnt = ntcMapper.updateNtcByKey(fetchedNtc);
        Verifier.eq(cnt, 1, "M1001", "word.ntc");
        
        // 공지사항 이력 등록 
        NtcHistVo ntcHist = new NtcHistVo(ntc.getNtcNo(), "HD0200");
        ntcHistMapper.insertNtcHistAsSelectMaster(ntcHist);
        Verifier.notNull(ntcHist.getNtcHistSeqno(), "M1000", "word.ntcHist");        
        
        return fetchedNtc;    
	}

	/**
     * 공지사항을 삭제한다. 
     */
	@Override
	public void deleteNtc(Integer ntcNo) {
		
		// 공지사항 삭제 
        NtcVo ntc = new NtcVo();
        ntc.setNtcNo(ntcNo);
        int cnt = ntcMapper.deleteNtcByKey(ntc); 
        Verifier.eq(cnt, 1, "M1002", "word.ntc");
         
        // 공지사항 이력 등록 
        NtcHistVo ntcHist = new NtcHistVo(ntc.getNtcNo(), "HD0300");
        ntcHistMapper.insertNtcHistAsSelectMaster(ntcHist);
        Verifier.notNull(ntcHist.getNtcHistSeqno(), "M1000", "word.ntcHist");      
        	
	}
	
	/**
     * 공지사항을 조회한다. 
     */
	@Override
	public NtcVo findNtc(Integer ntcNo) {
		
		CommonData commonData = CommonDataHolder.getCommonData();
		String siteCd = commonData.getSiteCd();
		String usrId = commonData.getUsrId();
        NtcVo ntc = ntcMapper.findNtcByKey(siteCd, ntcNo);
        
        //라이선시일 경우 조회수 변경 처리
        if (commonData.getAuth().equals("ROLE_LICENSEE") && ntc != null) {
        	NtcReadHistVo ntcReadHist = new NtcReadHistVo(ntc.getNtcNo(), usrId);
        	int readCnt = ntcReadHistMapper.insertNtcReadHist(ntcReadHist);
        	if (readCnt == 1) {
        		int cnt = ntcMapper.updateNtcCntByKey(ntc);
        		Verifier.eq(cnt, 1, "M1000", "word.ntcReadHist");
        	}
        }
        
        return ntc;
        
	}
	
	/**
     * 공지사항 목록 페이지를 조회한다. 
     */
	@Override
	public CommonPage<NtcVo> findNtcPage(CommonCondition condition) {
		
		Map<String, Object> map = condition.toMap();

        int comTtlCnt = ntcMapper.countNtcListByMap(map);
        List<NtcVo> ntcList = ntcMapper.findNtcListByMap(map);
        
        return new CommonPage<NtcVo>(condition, ntcList, comTtlCnt);   
        
	}
	
	/**
     * 라이선시용 공지사항 목록 페이지를 조회한다. 
     */
	@Override
	public CommonPage<NtcVo> findLiceNtcPage(CommonCondition condition) {
		
		Map<String, Object> map = condition.toMap();

        int comTtlCnt = ntcMapper.countLiceNtcListByMap(map);
        List<NtcVo> liceNtcList = ntcMapper.findLiceNtcListByMap(map);
        
        return new CommonPage<NtcVo>(condition, liceNtcList, comTtlCnt);  
        
	}
	
}
