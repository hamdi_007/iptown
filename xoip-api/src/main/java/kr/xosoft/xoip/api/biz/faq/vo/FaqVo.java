package kr.xosoft.xoip.api.biz.faq.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * FAQ VO 
 * @author hypark
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class FaqVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    
    /**
     *  FAQ 번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )    
    private Integer faqNo;

    /**
     * 언어값
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=2, groups=DefaultGroup.class )
    private String langVal;
    
    /**
     * FAQ구분코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )
    private String faqDivCd;

    /**
     * FAQ구분코드명
     */
    @JsonProperty( access=Access.READ_ONLY )
    private String faqDivCdNm;    
    
    /**
     * 질문
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=500, groups=DefaultGroup.class )
    private String qstn;
    
    /**
     * 답변 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String ansr;
    
    /**
     * 게시여부
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Pattern( message="chk.yn", regexp="(^[YN]{1}$)", groups=DefaultGroup.class )        
    private String postYn;
    
    /**
     * 등록일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonFormat( shape=JsonFormat.Shape.NUMBER )
    private Date regDt;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
}
