package kr.xosoft.xoip.api.biz.proj.service;

import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspFileUnionVo;
import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProjDsgnInspUnionVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface DsgnInspService {
    
    public DsgnInspFileUnionVo registDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion);
        
    public DsgnInspFileUnionVo modifyDsgnInspFileUnionInDraft(DsgnInspFileUnionVo dsgnInspUnion);

    public DsgnInspFileUnionVo requestDsgnInspFileUnionForApproval(DsgnInspFileUnionVo dsgnInspUnion);
    
    public DsgnInspFileUnionVo applyDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion);
    
    public DsgnInspFileUnionVo approveWithChangeDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion);
    
    public DsgnInspFileUnionVo approveDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion);
    
    public DsgnInspFileUnionVo rejectDsgnInspFileUnion(DsgnInspFileUnionVo dsgnInspUnion);
    
    public void deleteDsgnInsp(String projCd, Integer inspNo);
    
    public ProjDsgnInspUnionVo findProjDsgnInspUnion(String projCd, Integer inspNo);
    
    public DsgnInspVo findLocalizedDsgnInsp(String projCd, Integer inspNo);
    
    public CommonPage<DsgnInspVo> findLocalizedDsgnInspPage(String projCd, CommonCondition condition);
    
    public String findInspStgCdForNextStage(String projCd);
    
}
