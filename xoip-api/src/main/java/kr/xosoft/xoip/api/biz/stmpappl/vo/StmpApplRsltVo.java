package kr.xosoft.xoip.api.biz.stmpappl.vo;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 증지신청 조회결과 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StmpApplRsltVo extends StmpApplVo {
    
    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트명 
     */
    private String projNm;

    /**
     * IPID
     */
    private String ipId;
    
    /**
     * IP 코드 
     */
    private String ipCd;
    
    /**
     * IP명 
     */
    private String ipNm;
    
    /**
     * 라이선시회사ID
     */
    private String liceCompId;
    
    /**
     * 라이선시회사코드 
     */
    private String liceCompCd;
    
    /**
     * 라이선시회사명 
     */
    private String liceCompNm;
    
    /**
     * 계약ID
     */
    private String cntrId;
    
    /**
     * 계약번호 
     */
    private String cntrNo;
    
    /**
     * 계약명 
     */
    private String cntrNm;
    
    /**
     * 상위상품유형ID
     */
    private String topProdTypId;
    
    /**
     * 상위상품유형코드 
     */
    private String topProdTypCd;
    
    /**
     * 상위상품유형명 
     */
    private String topProdTypNm;
    
    /**
     * 상품유형ID
     */
    private String prodTypId;
    
    /**
     * 상품유형코드 
     */
    private String prodTypCd;
    
    /**
     * 상품유형명 
     */
    private String prodTypNm;
    
    /**
     * 프로젝트생성일시 
     */
    private Date projCrtDt;
    
    /**
     * 프로젝트승인요청일시 
     */
    private Date projApvReqDt;
    
    /**
     * 프로젝트승인일시 
     */
    private Date projApvDt;
    
    /**
     * 프로젝트완료일시 
     */
    private Date projCmplDt;
    
    /**
     * 첨부파일유무 
     */
    private String fileDelYn;
    
    /**
     * 첨부파일유무 
     */
    private String stmpConf;
        
        
    
    
}
