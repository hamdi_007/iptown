package kr.xosoft.xoip.api.biz.proj.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 검수파일 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class OpinFileVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 회사ID 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=10, max=10, groups=DefaultGroup.class )
    private String compId;    
    
    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=DefaultGroup.class )     
    private String projCd;

    /**
     * 검수번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer inspNo;

    /**
     * 검수파일번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Integer inspFileNo;
    
    /**
     * 의견파일번호 
     */
    private Integer opinFileNo;

    /**
     * 원본파일명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=300, groups=DefaultGroup.class )
    private String orgFileNm;

    /**
     * 관리파일명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=300, groups=DefaultGroup.class )
    private String mngFileNm;

    /**
     * 물리파일명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=300, groups=DefaultGroup.class )
    private String pscFileNm;

    /**
     * 파일경로
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=500, groups=DefaultGroup.class )
    private String filePath;
    
    /**
     * 썸네일소파일명
     */
    @Size( message="chk.sizeMax", max=300, groups=DefaultGroup.class )
    private String tumbSmFileNm;
    
    /**
     * 썸네일중파일명
     */
    @Size( message="chk.sizeMax", max=300, groups=DefaultGroup.class )
    private String tumbMdFileNm;    

    /**
     * 파일컨텐츠유형
     */
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String fileCntnTyp;

    /**
     * 파일크기
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    private Long fileSize;

    /**
     * 파일확장자명
     */
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String fileExtNm;

    /**
     * 정렬순번
     */
    private Integer srtOrdno;
    
    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
}
