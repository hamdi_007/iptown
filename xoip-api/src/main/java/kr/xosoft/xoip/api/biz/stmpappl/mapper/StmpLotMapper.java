package kr.xosoft.xoip.api.biz.stmpappl.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpLotVo;

/**
 * 증지품목 매퍼 
 * @author likejy
 *
 */
@Repository
public interface StmpLotMapper {

    /** 
     * 증지품목 등록
     * @param stmpLot
     * @return
     */
    public int insertStmpLot(StmpLotVo stmpLot);
    
    /**
     * 증지품목 수정 by PK
     * @param stmpLot
     * @return
     */
    public int updateStmpLotByKey(StmpLotVo stmpLot);
        
    /**
     * 증지품목 삭제 by PK
     * @param stmpLot
     * @return
     */
    public int deleteStmpLotByKey(StmpLotVo stmpLot);
    
    /**
     * 증지품목 조회 by PK
     * @param siteCd
     * @param projCd
     * @param stmpLotNo
     * @return
     */
    public StmpLotVo findStmpLotByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("stmpApplNo") Integer stmpApplNo, @Param("stmpLotNo") Integer stmpLotNo);
   
    /**
     * 증지품목 목록조회 by 증지신청 PK
     * @param map
     * @return
     */
    public List<StmpLotVo> findStmpLotListByStmpApplKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("stmpApplNo") Integer stmpApplNo);        
        
}
