package kr.xosoft.xoip.api.biz.stmpappl.service;

import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplUnionVo;
import kr.xosoft.xoip.api.biz.stmpappl.vo.StmpApplVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.file.buffer.BufferData;

public interface StmpApplService {
    
    public StmpApplUnionVo initStmpApplUnion(String projCd);
    
    public StmpApplUnionVo registStmpApplUnion(StmpApplUnionVo stmpApplUnion);
        
    public StmpApplUnionVo modifyStmpApplUnion(StmpApplUnionVo stmpApplUnion);

    public void deleteStmpAppl(String projCd, Integer stmpApplNo);
    
    public void updateConfirmStmpAppl(String projCd, Integer stmpApplNo);
    
    public StmpApplVo findStmpApplVo(String projCd, Integer stmpApplNo);
    
    public StmpApplUnionVo findStmpApplUnionVo(String projCd, Integer stmpApplNo);
    
    public CommonPage<StmpApplVo> findStmpApplRsltPage(CommonCondition condition);
    
    public BufferData makePrdcRptExcelData(String projCd, Integer stmpApplNo);
    
    public BufferData makeStmpApplRptExcelData(String projCd, Integer stmpApplNo);
    
}
