package kr.xosoft.xoip.api.biz.faq.service;

import java.util.List;

import kr.xosoft.xoip.api.biz.faq.vo.FaqVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface FaqService {

    public FaqVo registFaq(FaqVo faq);
        
    public FaqVo modifyFaq(FaqVo faq);
    
    public void deleteFaq(Integer faqNo);
    
    public FaqVo findFaq(Integer faqNo);
    
    public CommonPage<FaqVo> findFaqPage(CommonCondition condition);    
    
    public List<FaqVo> findFaqDivCdList(CommonCondition condition);

	public CommonPage<FaqVo> findLiceFaqPage(CommonCondition condition);
  
}
