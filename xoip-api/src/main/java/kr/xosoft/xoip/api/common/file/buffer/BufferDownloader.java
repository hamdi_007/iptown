package kr.xosoft.xoip.api.common.file.buffer;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 메모리내 바이트 데이터를 파일로 다운로드 처리한다.  
 * @author likejy
 *
 */
@Component
@Slf4j
public class BufferDownloader {

    public ResponseEntity<Resource> downloadBuffer(BufferData data) {
        
        // 헤더 설정 
        // 모든브라우저에서 한글 파일명이 안깨지게 하려면, 아래와 같이 설정해야 한다.  
        String encFileNm = null;
        try {
            encFileNm = URLEncoder.encode(data.getFileNm(),"UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            log.warn("failed to encode file name : {}", e.toString());
            encFileNm = data.getFileNm();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + encFileNm + ";filename*=UTF-8''" + encFileNm);
        
        // 미디어 타입 설정 
        MediaType mediaType = null;
        if (data.getFileCntnTyp() == null) {
            mediaType = MediaType.parseMediaType("application/octet-stream");
        } else {
            mediaType = MediaType.parseMediaType(data.getFileCntnTyp());
        }
        
        // 리소스 변환 및 리턴  
        InputStreamResource resource = new InputStreamResource(new ByteArrayInputStream(data.getBuffer()));
        return ResponseEntity.ok()
                .headers(headers)
                .contentType(mediaType)
                .contentLength(data.getBufferLength())
                .body(resource);                
    }
    
}
