package kr.xosoft.xoip.api.biz.email.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.email.vo.EmailFormVo;

/**
 * 이메일 폼 매퍼 
 * @author likejy
 *
 */
@Repository
public interface EmailFormMapper {

    public EmailFormVo findCurrentLocalizedEmailFormByKey(@Param("siteCd") String siteCd, @Param("formCd") String formCd, @Param("tmplLang") String tmplLang);
    
}
