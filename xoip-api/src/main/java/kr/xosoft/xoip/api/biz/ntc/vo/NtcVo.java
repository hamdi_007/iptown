package kr.xosoft.xoip.api.biz.ntc.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 공지사항 VO 
 * @author hypark
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class NtcVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    
    /**
     *  공지사항 번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )    
    private Integer ntcNo;

    /**
     * 언어값
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=2, groups=DefaultGroup.class )
    private String langVal;
    
    /**
     * 공지사항제목
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String ntcSbj;
    
    /**
     * 공지사항내용 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String ntcCnts;
    
    /**
     * 상단고정여부
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Pattern( message="chk.yn", regexp="(^[YN]{1}$)", groups=DefaultGroup.class )        
    private String headFixYn;
    
    /**
     * 게시여부
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Pattern( message="chk.yn", regexp="(^[YN]{1}$)", groups=DefaultGroup.class )        
    private String postYn;
    
    /**
     *  조회개수
     */  
    private Integer readCnt;
    
    /**
     * 등록일시
     */
    @JsonProperty( access=Access.READ_ONLY )
    @JsonFormat( shape=JsonFormat.Shape.NUMBER )
    private Date regDt;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
   
    
}
