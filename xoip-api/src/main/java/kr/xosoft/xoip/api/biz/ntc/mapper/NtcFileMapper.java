package kr.xosoft.xoip.api.biz.ntc.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.ntc.vo.NtcFileVo;

/**
 * 공지사항 파일 매퍼 
 * @author hypark
 *
 */
@Repository
public interface NtcFileMapper {

    /**
     * 공지사항 파일 등록
     * @param ntcFile
     * @return
     */
    public int insertNtcFile(NtcFileVo ntcFile);
        
    /**
     * 공지사항 파일 삭제 by PK
     * @param ntcFile
     * @return
     */
    public int deleteNtcFileByKey(NtcFileVo ntcFile);
    
    /**
     * 공지사항 파일 조회 by PK
     * @param siteCd
     * @param ntcNo
     * @param ntcFileNo
     * @return
     */
    public NtcFileVo findNtcFileByKey(@Param("siteCd") String siteCd, @Param("ntcNo") Integer ntcNo, @Param("ntcFileNo") Integer ntcFileNo);
    
    /**
     * 공지사항 파일 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countNtcFileListByMap(Map<String, Object> map);
    
    /**
     * 공지사항 파일 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<NtcFileVo> findNtcFileListByMap(Map<String, Object> map);    
    
}
