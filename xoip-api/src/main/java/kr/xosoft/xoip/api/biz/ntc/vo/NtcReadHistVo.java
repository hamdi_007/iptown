package kr.xosoft.xoip.api.biz.ntc.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 공지사항조회이력 VO 
 * @author hypark
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class NtcReadHistVo extends NtcVo {

	private static final long serialVersionUID = 1L;

    /**
     * 공지사항 번호
     */
    private Integer ntcNo;
    
    /**
     * 조회자ID
     */
    private String readrId;
    
    /**
     * 생성자 
     */
    public NtcReadHistVo(Integer ntcNo, String readrId) {
		setNtcNo(ntcNo);
		setReadrId(readrId);
	}
    
    
    
}
