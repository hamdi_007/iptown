package kr.xosoft.xoip.api.biz.usr.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.usr.vo.LgnHistVo;

/**
 * 로그인이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface LgnHistMapper {

    /**
     * 로그인이력 등록 
     */
    public int insertLgnHist(LgnHistVo lgnHist);
        
}
