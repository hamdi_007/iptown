package kr.xosoft.xoip.api.biz.proj.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 프로젝트이름변경폼 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ProjNmChgFormVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=DefaultGroup.class )    
    private String projCd;
    
    /**
     * 프로젝트명 
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )  
    private String projNm;
    
    /**
     * 이력사유내용
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String histRsnCnts;
    
}
