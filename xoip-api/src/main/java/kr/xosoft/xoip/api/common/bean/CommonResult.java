package kr.xosoft.xoip.api.common.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * 공통전문
 * @author likejy
 *
 */
public class CommonResult {

    /**
     * 에러코드 
     */
    private String errorCode;
    
    /**
     * 에러메시지 
     */
    private String errorMessage;
    
    /**
     * 결과 맵 
     */
    private Map<String, Object> result;
    
    /**
     * 생성자 
     */
    public CommonResult() {
        result = new HashMap<String, Object>();
    }
    
    /**
     * 생성자 
     * @param key
     * @param o
     */
    public CommonResult(String key, Object o) {
        this();
        result.put(key, o);
    }
    
    /**
     * 생성자
     * @param data
     */
    public CommonResult(Map<String, Object> result) {
        this();
        this.result = result;
    }
    
    /**
     * 결과 map 을 리턴한다. 
     * @return
     */
    public Map<String, Object> getResult() {
        return result;
    }
    
    /**
     * 결과 map 에 key 값을 키로 갖는 객체 o 를 할당한다. 
     * @param key
     * @param o
     * @return
     */
    public CommonResult putObject(String key, Object o) {
        result.put(key, o);
        return this;
    }
    
    /**
     * 결과 map 에서 key 값을 키로 갖는 객체를 리턴한다. 
     * @param key
     * @return
     */
    public Object getObject(String key) {
        return result.get(key);
    }
    
    /**
     * 에러코드로 errorCode 를 할당한다.
     * @param errorCode
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    
    /**
     * 에러코드를 리턴한다. 
     * @return
     */
    public String getErrorCode() {
        return errorCode;
    }
    
    /**
     * 에러메세지로 errorMessage 를 할당한다. 
     * @param errorMessage
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    /**
     * 에러메세지를 리턴한다. 
     * @return
     */
    public String getErrorMessage() {
        return errorMessage;
    }
        
}
