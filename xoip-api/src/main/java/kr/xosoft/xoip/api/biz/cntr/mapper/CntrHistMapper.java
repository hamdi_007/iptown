package kr.xosoft.xoip.api.biz.cntr.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.cntr.vo.CntrHistVo;

/**
 * 계약이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface CntrHistMapper {

    /**
     * 계약이력 등록 
     */
    public void insertCntrHistAsSelectMaster(CntrHistVo cntrHist);    
    
}
