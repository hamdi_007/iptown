package kr.xosoft.xoip.api.biz.cntr.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.support.StringSupport;
import kr.xosoft.xoip.api.biz.cntr.mapper.CntrHistMapper;
import kr.xosoft.xoip.api.biz.cntr.mapper.CntrMapper;
import kr.xosoft.xoip.api.biz.cntr.vo.CntrHistVo;
import kr.xosoft.xoip.api.biz.cntr.vo.CntrVo;
import kr.xosoft.xoip.api.biz.comp.service.CompService;
import kr.xosoft.xoip.api.biz.comp.vo.CompVo;
import kr.xosoft.xoip.api.biz.ip.service.IpService;
import kr.xosoft.xoip.api.biz.ip.vo.IpVo;
import kr.xosoft.xoip.api.biz.proj.service.ProjService;

/**
 * 계약 서비스 Impl
 * @author likejy
 *
 */
@Transactional
@Service
public class CntrServiceImpl implements CntrService {

    @Autowired
    private CompService compService;
    
    @Autowired
    private IpService ipService;
    
    @Autowired
    private ProjService projService;
    
    @Autowired
    private CntrMapper cntrMapper;
    
    @Autowired
    private CntrHistMapper cntrHistMapper;
    
    /**
     * 계약을 등록한다. 
     */
    @Override
    public CntrVo registCntr(CntrVo cntr) {
        
        // 회사 조회 
        CompVo comp = compService.findComp(cntr.getCompId());
        Verifier.notNull(comp, "M1003", "word.comp");
        
        // IP 조회 
        IpVo ip = ipService.findIp(cntr.getIpId());
        Verifier.notNull(ip, "M1003", "word.ip");
        Verifier.isEqual(ip.getUseYn(), "Y", "M5000");
        
        // 계약종료일이 계약시작일보다 빠르거나 같은 경우 Exception 발생
        if (!StringUtils.isBlank(cntr.getCntrStrYmd()) && !StringUtils.isBlank(cntr.getCntrEndYmd())) {
            Date strDate = StringSupport.parseDate(cntr.getCntrStrYmd(), "yyyyMMdd");
            Date endDate = StringSupport.parseDate(cntr.getCntrEndYmd(), "yyyyMMdd");
			Verifier.lt(strDate, endDate, "chk.prd");
        }
        // 계약 등록 
        int cnt = cntrMapper.insertCntr(cntr);
        Verifier.eq(cnt, 1, "M1000", "word.cntr");

        // 계약 이력 등록 
        CntrHistVo cntrHist = new CntrHistVo(cntr.getCntrId(), "HD0100");
        cntrHistMapper.insertCntrHistAsSelectMaster(cntrHist);
        Verifier.notNull(cntrHist.getCntrHistSeqno(), "M1000", "word.cntrHist");
        
        return cntr;
    }

    /**
     * 계약을 수정한다. 
     */
    @Override
    public CntrVo modifyCntr(CntrVo cntr) {
        
        // 계약 조회 
        CntrVo fetchedCntr = findLocalizedCntr(cntr.getCntrId());
        Verifier.notNull(fetchedCntr, "M1003", "word.cntr");
        
        fetchedCntr.setCntrNm(cntr.getCntrNm());
        fetchedCntr.setCntrStrYmd(cntr.getCntrStrYmd());
        fetchedCntr.setCntrEndYmd(cntr.getCntrEndYmd());
        fetchedCntr.setCntrYmd(cntr.getCntrYmd());
        fetchedCntr.setCntrVldYmd(cntr.getCntrVldYmd());
        
        // 비고 조회 권한이 없는 경우 비고 값이 null 값으로 전달됨         
        if (cntr.getRmk() != null) {
            fetchedCntr.setRmk(cntr.getRmk());            
        }
        
        // 계약종료일이 계약시작일보다 빠르거나 같은 경우 Exception 발생
        if (!StringUtils.isBlank(cntr.getCntrStrYmd()) && !StringUtils.isBlank(cntr.getCntrEndYmd())) {
            Date strDate = StringSupport.parseDate(cntr.getCntrStrYmd(), "yyyyMMdd");
            Date endDate = StringSupport.parseDate(cntr.getCntrEndYmd(), "yyyyMMdd");
            Verifier.lt(strDate, endDate, "chk.prd");
        }        

        // 계약 수정 
        int cnt = cntrMapper.updateCntrByKey(fetchedCntr);
        Verifier.eq(cnt, 1, "M1001", "word.cntr");

        // 계약 이력 등록 
        CntrHistVo cntrHist = new CntrHistVo(cntr.getCntrId(), "HD0200");
        cntrHistMapper.insertCntrHistAsSelectMaster(cntrHist);
        Verifier.notNull(cntrHist.getCntrHistSeqno(), "M1000", "word.cntrHist");
        
        return cntr;        
    }

    /**
     * 계약을 삭제한다. 
     */
    @Override
    public void deleteCntr(String cntrId) {
        
        // 등록된 프로젝트가 존재하는 경우 계약 삭제 불가 
        String existYn = projService.findProjExistYnByCntrId(cntrId);
        Verifier.isEqual(existYn, "N", "M5000");
        
        // 계약 삭제 
        CntrVo cntr = new CntrVo();
        cntr.setCntrId(cntrId);
        int cnt = cntrMapper.deleteCntrByKey(cntr); 
        Verifier.eq(cnt, 1, "M1002", "word.cntr");
        
        // 계약 이력 등록 
        CntrHistVo cntrHist = new CntrHistVo(cntrId, "HD0300");
        cntrHistMapper.insertCntrHistAsSelectMaster(cntrHist);
        Verifier.notNull(cntrHist.getCntrHistSeqno(), "M1000", "word.cntrHist");        
        
    }

    /**
     * 계약을 조회한다. 
     */
    @Override
    public CntrVo findLocalizedCntr(String cntrId) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return cntrMapper.findLocalizedCntrByKey(commonData.getSiteCd(), cntrId, commonData.getLang());
    }
    
    /**
     * 현재 시점에 유효한 계약을 조회한다.  
     */
    public CntrVo findValidCntr(String cntrId) {
        
        CntrVo cntr = findLocalizedCntr(cntrId);
        Verifier.notNull(cntr, "M1003", "word.cntr");
        
        // 현재일 
        String currYmd = DateFormatUtils.format(new Date(), "yyyyMMdd");
        
        // 계약 시작일 체크 : 현재일이 계약 시작일과 동일하거나 이후 이어야 함  
        if (cntr.getCntrStrYmd() != null && cntr.getCntrStrYmd().length() == 8) {
            Verifier.ge(currYmd.compareTo(cntr.getCntrStrYmd()), 0, "M5001");
        }
        // 계약 종료일 / 유효일 체크 : 현재일이 계약 종료일(유효일)과 동일하거나 이전 이어야 함 
        if (cntr.getCntrEndYmd() != null && cntr.getCntrEndYmd().length() == 8) {
            // 현재일이 계약 종료일 이후인 경우 추가적으로 계약 유효일과 비교 후 판단한다. 
            if (currYmd.compareTo(cntr.getCntrEndYmd()) > 0) {
                if (cntr.getCntrVldYmd() != null && cntr.getCntrVldYmd().length() == 8) {
                    Verifier.le(currYmd.compareTo(cntr.getCntrVldYmd()), 0, "M5002");                                    
                }
            }
        }
        
        return cntr;
    }
    
    /**
     * 계약번호로 계약을 조회한다. 
     */
    @Override
    public CntrVo findCntrByCntrNo(String cntrNo) {
        
        String siteCd = CommonDataHolder.getCommonData().getSiteCd();
        return cntrMapper.findCntrByCntrNo(siteCd, cntrNo);
    }    

    /**
     * 계약목록 페이지를 조회한다. 
     */
    @Override
    public CommonPage<CntrVo> findLocalizedCntrPage(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        
        int comTtlCnt = cntrMapper.countCntrListByMap(map);
        List<CntrVo> cntrList = cntrMapper.findLocalizedCntrListByMap(map);
        
        return new CommonPage<CntrVo>(condition, cntrList, comTtlCnt);           
    }

    /**
     * 회사ID로 작성된 계약의 존재여부를 조회한다. 
     */
    public String findCntrExistYnByCompId(String compId) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return cntrMapper.findCntrExistYnByCompId(commonData.getSiteCd(), compId);
    }
    
    /**
     * IPID로 작성된 계약의 존재여부를 조회한다. 
     */
    public String findCntrExistYnByIpId(String ipId) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        return cntrMapper.findCntrExistYnByIpId(commonData.getSiteCd(), ipId);
    } 
    
}
