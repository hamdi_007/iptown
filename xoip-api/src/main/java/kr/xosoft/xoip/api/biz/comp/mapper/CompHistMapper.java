package kr.xosoft.xoip.api.biz.comp.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.comp.vo.CompHistVo;

/**
 * 회사이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface CompHistMapper {

    /**
     * 회사이력 등록 
     */
    public void insertCompHistAsSelectMaster(CompHistVo compHist);
        
}
