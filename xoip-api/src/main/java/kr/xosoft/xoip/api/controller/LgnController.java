package kr.xosoft.xoip.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.usr.service.UsrService;
import kr.xosoft.xoip.api.biz.usr.vo.LgnUsrRsltVo;
import kr.xosoft.xoip.api.biz.usr.vo.TokenFormVo;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 로그인 Controller
 * @author likejy
 *
 */
@RestController
public class LgnController {
    
    @Autowired
    private UsrService usrService;

    /**
     * 로그인
     */
    @RequestMapping( value="/api/{version}/auth/login", method = RequestMethod.POST )
    public ResponseEntity<CommonResult> postLogin(
            @PathVariable("version") final Integer version) {
        
        LgnUsrRsltVo lgnUsrRslt = usrService.postLogin();
        List<String> requiredList = usrService.checkRequiredUsrInfo(lgnUsrRslt.getUsrId());
        
        CommonResult result = new CommonResult("lgnUsrRslt", lgnUsrRslt);
        result.putObject("requiredList", requiredList);
        return new ResponseEntity<CommonResult>(result, HttpStatus.OK);
    }
    
    /**
     * 로그아웃 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")    
    @RequestMapping( value="/api/{version}/auth/logout", method = RequestMethod.DELETE )
    public ResponseEntity<CommonResult> logout(
            @PathVariable("version") final Integer version) {
        
        usrService.logout();
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 토큰 갱신 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")    
    @RequestMapping( value="/api/{version}/auth/tokens", method = RequestMethod.PUT )
    public ResponseEntity<CommonResult> renewToken(
            @PathVariable("version") final Integer version,
            @Validated(UpdatingGroup.class) @RequestBody final TokenFormVo tokenForm) {
        
        LgnUsrRsltVo lgnUsrRslt = usrService.renewToken(tokenForm);
        return new ResponseEntity<CommonResult>(new CommonResult("lgnUsrRslt", lgnUsrRslt), HttpStatus.OK);
    }

}
