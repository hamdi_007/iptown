package kr.xosoft.xoip.api.biz.usr.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 비밀번호 재설정 요청폼 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class PwdRsetReqVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 사용자ID
     */
    private String usrId;

    /**
     * 재설정요청번호
     */
    private Integer rsetReqNo;

    /**
     * 재설정요청일시
     */
    private Date rsetReqDt;

    /**
     * 재설정검증값
     */
    private String rsetVrfVal;

    /**
     * 재설정완료여부
     */
    private String rsetCmplYn;

    /**
     * 재설정완료일시
     */
    private Date rsetCmplDt;    
    
}
