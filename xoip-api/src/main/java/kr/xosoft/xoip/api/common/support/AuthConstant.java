package kr.xosoft.xoip.api.common.support;

/**
 * 권한 
 * @author likejy
 *
 */
public class AuthConstant {

    public static String ROLE_ADMIN = "ROLE_ADMIN";
    public static String ROLE_REVIEWER = "ROLE_REVIEWER";
    public static String ROLE_LICENSEE = "ROLE_LICENSEE";
    
}
