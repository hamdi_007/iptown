package kr.xosoft.xoip.api.biz.comp.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 회사 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class CompVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    
    /**
     * 회사ID
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    @Size( message="chk.sizeSame", min=10, max=10, groups=UpdatingGroup.class )
    private String compId;    
    
    /**
     * 회사코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=3, max=3, groups=DefaultGroup.class )
    private String compCd;

    /**
     * 회사명
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String compNm;
    
    /**
     * 회사구분코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )
    private String compDivCd;

    /**
     * 국가코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )
    private String ntnCd;

    /**
     * 사업자등록번호
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Pattern( message="chk.bizno", regexp="(^[0-9]{10}$)", groups=DefaultGroup.class )
    private String bizno;

    /**
     * 주소
     */
    @Size( message="chk.sizeMax", max=500, groups=DefaultGroup.class )
    private String addr;

    /**
     * 상세주소
     */
    @Size( message="chk.sizeMax", max=500, groups=DefaultGroup.class )
    private String dtlAddr;

    /**
     * 우편번호
     */
    @Size( message="chk.sizeMax", max=8, groups=DefaultGroup.class )
    private String postNo;

    /**
     * 대표자명
     */
    @Size( message="chk.sizeMax", max=100, groups=DefaultGroup.class )
    private String ceoNm;

    /**
     * 대표전화번호
     */
    @Size( message="chk.sizeMax", max=20, groups=DefaultGroup.class )
    private String repTelno;

    /**
     * 비고
     */
    @Size( message="chk.sizeMax", max=2000, groups=DefaultGroup.class )
    private String rmk;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;    
    
    /**
     * 회사 수
     */
    @JsonIgnore
    private String compCnt;
    
    
    
}
