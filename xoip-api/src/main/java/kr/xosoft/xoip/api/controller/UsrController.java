package kr.xosoft.xoip.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.usr.service.UsrService;
import kr.xosoft.xoip.api.biz.usr.vo.PwdChngFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdJoinFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.PwdRsetReqFormVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrCompRsltVo;
import kr.xosoft.xoip.api.biz.usr.vo.UsrVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.support.AuthSupport;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * 사용자 Controller 
 * @author likejy
 *
 */
@RestController
public class UsrController {

    @Autowired
    private UsrService usrService;
    
    /**
     * 사용자를 등록한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/users", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registLicensorUsr(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final UsrVo usr) {
        
        UsrVo fetchedUsr = null;
        if (AuthSupport.isLicensor(usr.getAuth())) {
            fetchedUsr = usrService.registLicrUsr(usr);            
        } else {
            fetchedUsr = usrService.registLiceUsr(usr);
        }
        return new ResponseEntity<CommonResult>(new CommonResult("usr", fetchedUsr), HttpStatus.OK);
    }   
    
    /**
     * 사용자 기본 정보를 수정한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/users/{usrId}/info", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyUsrInfo(
            @PathVariable("version") final Integer version,
            @PathVariable("usrId") final String usrId,
            @Validated(UpdatingGroup.class) @RequestBody final UsrVo usr) {
        
        Verifier.isEqual(usrId, usr.getUsrId(), "M1004", "word.usrId");
        UsrVo fetchedUsr = usrService.modifyUsrInfo(usr);
        return new ResponseEntity<CommonResult>(new CommonResult("usr", fetchedUsr), HttpStatus.OK);
    }        
    
    /**
     * 사용자 비밀번호를 수정한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/users/{usrId}/password", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyUsrPwdWithCurrPwd(
            @PathVariable("version") final Integer version,
            @PathVariable("usrId") final String usrId,
            @Validated(UpdatingGroup.class) @RequestBody final PwdChngFormVo pwdChngForm) {
        
        Verifier.isEqual(usrId, pwdChngForm.getUsrId(), "M1004", "word.usrId");
        usrService.modifyUsrPwdWithCurrPwd(pwdChngForm);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }            
    
    /**
     * 사용자 비밀번호를 재설정한다. 
     */
    @RequestMapping( value="/api/{version}/users/{usrId}/password/reset", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyUsrPwdForReset(
            @PathVariable("version") final Integer version,
            @PathVariable("usrId") final String usrId,
            @Validated(UpdatingGroup.class) @RequestBody final PwdRsetFormVo pwdRsetForm) {
        
        Verifier.isEqual(usrId, pwdRsetForm.getUsrId(), "M1004", "word.usrId");
        usrService.modifyUsrPwdWithVrfVal(pwdRsetForm);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }     
    
    /**
     * 라이선시 사용자 가입을 위해 비밀번호를 설정한다. 
     */
    @RequestMapping( value="/api/{version}/users/{usrId}/password/join", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyUsrPwdForJoin(
            @PathVariable("version") final Integer version,
            @PathVariable("usrId") final String usrId,
            @Validated(UpdatingGroup.class) @RequestBody final PwdJoinFormVo pwdJoinForm) {
        
        Verifier.isEqual(usrId, pwdJoinForm.getUsrId(), "M1004", "word.usrId");
        usrService.joinUsr(pwdJoinForm);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }          
    

    /**
     * 사용자에게 초청 이메일을 발송한다.   
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/users/{usrId}/invitation", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> inviteUsr(
            @PathVariable("version") final Integer version,
            @PathVariable("usrId") final String usrId) {
        
        UsrVo fetchedUsr = usrService.inviteUsr(usrId);
        return new ResponseEntity<CommonResult>(new CommonResult("usr", fetchedUsr), HttpStatus.OK);
    }         

//    탈퇴기능은 사용하지 않기로 협의 (2021.06.03)     
//    /**
//     * 사용자를 탈퇴 처리한다.  
//     */
//    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
//    @RequestMapping( value="/api/{version}/users/{usrId}/withdrawal", method=RequestMethod.PUT )
//    public ResponseEntity<CommonResult> withdrawUsr(
//            @PathVariable("version") final Integer version,
//            @PathVariable("usrId") final String usrId) {
//        
//        UsrVo fetchedUsr = usrService.withdrawUsr(usrId);
//        return new ResponseEntity<CommonResult>(new CommonResult("usr", fetchedUsr), HttpStatus.OK);
//    }            
    
    /**
     * 사용자를 삭제한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER')")
    @RequestMapping( value="/api/{version}/users/{usrId}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteUsr(
            @PathVariable("version") final Integer version,
            @PathVariable("usrId") final String usrId) {
        
        usrService.deleteUsr(usrId);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * 사용자를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/users/{usrId}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findUsr(
            @PathVariable("version") final Integer version,
            @PathVariable("usrId") final String usrId) {
        
        UsrVo fetchedUsr = usrService.findUsr(usrId);
        return new ResponseEntity<CommonResult>(new CommonResult("usr", fetchedUsr), HttpStatus.OK);
    }
    
    /**
     * 이메일로 사용자를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/users/email", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findUsrByEmail(
            @PathVariable("version") final Integer version,
            @RequestParam( value="email", required=true ) final String email) {
        
        UsrVo fetchedUsr = usrService.findUsrByEmail(email);
        return new ResponseEntity<CommonResult>(new CommonResult("usr", fetchedUsr), HttpStatus.OK);
    }    
    
    /**
     * 사용자 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/users", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findUsrPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<UsrCompRsltVo> fetchedUsrCompPage = usrService.findUsrCompRsltPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("usrCompPage", fetchedUsrCompPage), HttpStatus.OK);
    }

    /**
     * 사용자 이메일 목록을 조회한다. 
     * @param version
     * @param hnpno
     * @param usrNm
     * @return
     */
    @RequestMapping( value="/api/{version}/users/email/miss", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findEmailList(
            @PathVariable("version") final Integer version,
            @RequestParam( value="hnpno", required=true ) final String hnpno,
            @RequestParam( value="usrNm", required=true ) final String usrNm) {

        List<String> fetchedList = usrService.findEmailList(hnpno, usrNm);
        return new ResponseEntity<CommonResult>(new CommonResult("emailList", fetchedList), HttpStatus.OK);
    }

    /**
     * 사용자 초대 여부를 조회한다. 
     * @param version
     * @param usrId
     * @return
     */
    @RequestMapping( value="/api/{version}/users/{usrId}/invited", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findUsrIvtYn(
            @PathVariable("version") final Integer version,
            @PathVariable( value="usrId", required=true ) final String usrId) {
        
        String ivtYn = usrService.findUsrIvtYn(usrId);
        return new ResponseEntity<CommonResult>(new CommonResult("ivtYn", ivtYn), HttpStatus.OK);
    }
    
    /**
     * 사용자 비밀번호 재설정 이메일 전송을 요청한다. 
     * @param version
     * @param pwdRsetReqForm
     * @return
     */
    @RequestMapping( value="/api/{version}/users/password/miss", method=RequestMethod.POST )    
    public ResponseEntity<CommonResult> registPwdRsetReq(
            @PathVariable("version") final Integer version,
            @Validated @RequestBody final PwdRsetReqFormVo pwdRsetReqForm) {
    
        usrService.processPwdRsetReq(pwdRsetReqForm);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }    
    
    /**
     * 라이선시 사용자 기타 필수입력 항목중 미입력 항목을 확인한다.  
     * @param version
     * @param usrId
     * @return
     */
    @RequestMapping( value="/api/{version}/users/{usrId}/check", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> checkRequiredUsrInfo(
            @PathVariable("version") final Integer version,
            @PathVariable( value="usrId", required=true ) final String usrId) {
        
        List<String> requiredList = usrService.checkRequiredUsrInfo(usrId);
        return new ResponseEntity<CommonResult>(new CommonResult("requiredList", requiredList), HttpStatus.OK);
    }    
    
}
