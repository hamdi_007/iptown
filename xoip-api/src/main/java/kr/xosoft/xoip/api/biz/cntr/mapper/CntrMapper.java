package kr.xosoft.xoip.api.biz.cntr.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.cntr.vo.CntrVo;

/**
 * 계약 매퍼 
 * @author likejy
 *
 */
@Repository
public interface CntrMapper {

   /**
    * 계약 등록
    * @param cntr
    * @return
    */
   public int insertCntr(CntrVo cntr);
   
   /**
    * 계약 수정 by PK
    * @param cntr
    * @return
    */
   public int updateCntrByKey(CntrVo cntr);
       
   /**
    * 계약 삭제 by PK
    * @param cntr
    * @return
    */
   public int deleteCntrByKey(CntrVo cntr);
   
   /**
    * 계약 조회 by PK
    * @param siteCd
    * @param cntrId
    * @return
    */
   public CntrVo findLocalizedCntrByKey(@Param("siteCd") String siteCd, @Param("cntrId") String cntrId, @Param("comLang") String comLang);
   
   /**
    * 계약 조회 by 계약번호 
    * @param siteCd
    * @param cntrNo
    * @return
    */
   public CntrVo findCntrByCntrNo(@Param("siteCd") String siteCd, @Param("cntrNo") String cntrNo);   
   
   /**
    * 계약 목록 건수조회 by 검색조건
    * @param map
    * @return
    */
   public int countCntrListByMap(Map<String, Object> map);
   
   /**
    * 계약 목록조회 by 검색조건
    * @param map
    * @return
    */
   public List<CntrVo> findLocalizedCntrListByMap(Map<String, Object> map);    
   
   /**
    * 계약 존재여부 조회 by 회사ID 
    * @param compId
    * @return
    */
   public String findCntrExistYnByCompId(@Param("siteCd") String siteCd, @Param("compId") String compId);
   
   /**
    * 계약 존재여부 조회 by IPID 
    * @param ipId
    * @return
    */
   public String findCntrExistYnByIpId(@Param("siteCd") String siteCd, @Param("ipId") String ipId);   
   
}
