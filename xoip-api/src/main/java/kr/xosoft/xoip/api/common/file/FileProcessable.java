package kr.xosoft.xoip.api.common.file;

import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;

/**
 * 파일 처리 VO 인터페이스  
 * @author likejy
 *
 */
public interface FileProcessable {

    /**
     * 업로드 처리중인 multipart file 인스턴스를 리턴 
     * @return
     */
    public MultipartFile getMultipartFile();
    
    /**
     * 다운로드 처리중인 FileVo 인스턴스를 리턴 
     * @return
     */
    public BaseVo getFileVo();
        
    /**
     * 파일명을 제외한 디렉토리 전체 경로를 리턴 
     * @return
     */
    public String getCanonicalDirPath();
    
    /**
     * 파일구분코드를 리턴 
     * @return
     */
    public String getFileDivCd();

    /**
     * 관리파일명을 리턴 
     * @return
     */
    public String getMngFileNm();
    
    /**
     * 베이스패스를 리턴 
     * @return
     */
    public String getBasePath();
    
    /**
     * 파일 전체 경로를 리턴 
     * @return
     */
    public String getCanonicalFilePath();
    
    /**
     * 썸네일중 전체 경로를 리턴 
     * @return
     */
    public String getCanonicalTumbMdPath();
    
    /**
     * 썸네일소 전체 경로를 리턴 
     * @return
     */
    public String getCanonicalTumbSmPath();
    
    /**
     * 파일 컨텐츠타입을 리턴 
     * @return
     */
    public String getFileCntnTyp();
    
    /**
     * 파일 사이즈를 리턴 
     * @return
     */
    public Long getFileSize();
    
    /**
     * 파일 확장자명을 리턴 
     * @return
     */
    public String getFileExtNm();
    
    /**
     * 파일구분코드 필수입력값 여부를 리턴 
     * @return
     */
    public boolean isFileDivCdRequired();
    
    /**
     * 미리보기 가능여부를 리턴 
     * @return
     */
    public boolean isAvailableForPreview();
    
    /**
     * 썸네일 생성여부를 리턴 
     * @return
     */
    public boolean shouldCreateThumbnail();
    
}
