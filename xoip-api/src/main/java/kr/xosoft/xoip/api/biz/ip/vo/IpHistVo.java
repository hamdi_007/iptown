package kr.xosoft.xoip.api.biz.ip.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * IP이력 VO 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class IpHistVo extends IpVo {

    private static final long serialVersionUID = 1L;

    /**
     * IP이력일련번호 
     */
    private Integer ipHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public IpHistVo(String ipId, String histDivCd) {
        setIpId(ipId);
        setHistDivCd(histDivCd);
    }        
    
}
