package kr.xosoft.xoip.api.biz.faq.service;

import kr.xosoft.xoip.api.biz.faq.vo.FaqFileVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

public interface FaqFileService {

    public FaqFileVo registFaqFile(FaqFileVo faqFile);
    
    public void deleteFaqFile(Integer faqNo, Integer faqFileNo);
    
    public FaqFileVo findFaqFile(Integer faqNo, Integer faqFileNo);
    
    public CommonPage<FaqFileVo> findFaqFilePage(Integer faqNo, CommonCondition condition);
    
}
