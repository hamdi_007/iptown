package kr.xosoft.xoip.api.common.config;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 비동기 스레드풀 설정 
 * @author likejy
 *
 */
@Configuration
@EnableAsync
public class AsyncConfig {

    @Value("${api.executor.corePoolSize}")
    private Integer corePoolSize;

    @Value("${api.executor.maxPoolSize}")
    private Integer maxPoolSize;
    
    @Value("${api.executor.queueCapacity}")
    private Integer queueCapacity;
        
    @Bean
    public Executor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(corePoolSize);
        taskExecutor.setMaxPoolSize(maxPoolSize);
        taskExecutor.setQueueCapacity(queueCapacity);
        taskExecutor.setThreadNamePrefix("Executor-");
        taskExecutor.initialize();
        return taskExecutor;
    }    
    
}
