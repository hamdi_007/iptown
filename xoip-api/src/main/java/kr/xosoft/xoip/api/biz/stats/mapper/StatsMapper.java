package kr.xosoft.xoip.api.biz.stats.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.stats.vo.StatsVo;

/**
 * 통계 매퍼 
 * @author leejaejun
 *
 */
@Repository
public interface StatsMapper {

   /**
    * 통계 목록 건수조회 by ip, comp, prodTyp
    * @param map
    * @return
    */
   public int countStatsListByIpCompProdTyp(Map<String, Object> map);
   
   /**
    * 통계 목록 조회 by ip, comp, prodTyp
    * @param map
    * @return
    */
   public List<StatsVo> findStatsListByIpCompProdTyp(Map<String, Object> map);
   
   /**
    * 통계 목록 건수조회 by ip, comp
    * @param map
    * @return
    */
   public int countStatsListByIpComp(Map<String, Object> map);
   
   /**
    * 통계 목록 조회 by ip, comp
    * @param map
    * @return
    */
   public List<StatsVo> findStatsListByIpComp(Map<String, Object> map);
   
   /**
    * 통계 목록 건수조회 by ip, prodTyp
    * @param map
    * @return
    */
   public int countStatsListByIpProdTyp(Map<String, Object> map);
   
   /**
    * 통계 목록 조회 by ip, prodTyp
    * @param map
    * @return
    */
   public List<StatsVo> findStatsListByIpProdTyp(Map<String, Object> map);
   
   /**
    * 통계 목록 건수조회 by ip
    * @param map
    * @return
    */
   public int countStatsListByIp(Map<String, Object> map);
   
   /**
    * 통계 목록 조회 by ip
    * @param map
    * @return
    */
   public List<StatsVo> findStatsListByIp(Map<String, Object> map);
   
}
