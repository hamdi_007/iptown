package kr.xosoft.xoip.api.biz.com.service;

import kr.xosoft.xoip.api.biz.com.vo.AddrVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;

/**
 * 주소 서비스
 * @author likejy
 *
 */
public interface AddrService {

	public CommonPage<AddrVo> findAddrPageByCondition(CommonCondition condition);
	
}
