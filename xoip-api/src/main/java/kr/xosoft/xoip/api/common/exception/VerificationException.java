package kr.xosoft.xoip.api.common.exception;

/**
 * 값 검증 예외 
 * @author likejy
 *
 */
public class VerificationException extends BizException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 생성자 
	 */
	public VerificationException() {
		super();
	}
	
	/**
	 * 생성자 
	 * @param code
	 */
	public VerificationException(String code) {
		super(code);
	}
	
	/**
	 * 생성자 
	 * @param code
	 * @param args
	 */
	public VerificationException(String code, String[] args) {
		super(code, args);
	}
	
	/**
	 * 생성자 
	 * @param code
	 * @param args
	 * @param cause
	 */
	public VerificationException(String code, String[] args, Throwable cause) {
		super(code, args, cause);
	}
	
	/**
	 * 생성자 
	 * @param code
	 * @param cause
	 */
	public VerificationException(String code, Throwable cause) {
		super(code, cause);
	}
	
	/**
	 * 생성자
	 * @param cause
	 */
	public VerificationException(Throwable cause) {
		super(cause);
	}		

}
