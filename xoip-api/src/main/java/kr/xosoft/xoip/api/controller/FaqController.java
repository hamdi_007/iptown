package kr.xosoft.xoip.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.xosoft.xoip.api.biz.faq.service.FaqService;
import kr.xosoft.xoip.api.biz.faq.vo.FaqVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.CommonResult;
import kr.xosoft.xoip.api.common.bean.Verifier;
import kr.xosoft.xoip.api.common.validator.CreatingGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;

/**
 * FAQ Controller 
 * @author hypark
 *
 */
@RestController
public class FaqController {

    @Autowired
    private FaqService faqService;
    
    /**
     * FAQ를 등록한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/faq", method=RequestMethod.POST )
    public ResponseEntity<CommonResult> registFaq(
            @PathVariable("version") final Integer version,
            @Validated(CreatingGroup.class) @RequestBody final FaqVo faq) {
        
        FaqVo fetchedFaq = faqService.registFaq(faq);        
        return new ResponseEntity<CommonResult>(new CommonResult("faq", fetchedFaq), HttpStatus.OK);
    }   
    
    /**
     * FAQ를 수정한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/faq/{faqNo}", method=RequestMethod.PUT )
    public ResponseEntity<CommonResult> modifyFaq(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo,
            @Validated(UpdatingGroup.class) @RequestBody final FaqVo faq) {
        
        Verifier.isEqual(faqNo, faq.getFaqNo(), "M1004", "word.faqNo");
        FaqVo fetchedFaq = faqService.modifyFaq(faq);
        return new ResponseEntity<CommonResult>(new CommonResult("faq", fetchedFaq), HttpStatus.OK);
    }        
    
    /**
     * FAQ를 삭제한다. 
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping( value="/api/{version}/faq/{faqNo}", method=RequestMethod.DELETE )
    public ResponseEntity<CommonResult> deleteFaq(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo) {
        
    	faqService.deleteFaq(faqNo);
        return new ResponseEntity<CommonResult>(new CommonResult(), HttpStatus.OK);
    }
    
    /**
     * FAQ를 조회한다. 
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/faq/{faqNo}", method=RequestMethod.GET )
    public ResponseEntity<CommonResult> findFaq(
            @PathVariable("version") final Integer version,
            @PathVariable("faqNo") final Integer faqNo) {
        
        FaqVo fetchedFaq = faqService.findFaq(faqNo);
        return new ResponseEntity<CommonResult>(new CommonResult("faq", fetchedFaq), HttpStatus.OK);
    }
    
    /**
     * FAQ 구분코드를 조회한다.  
     */
    @PreAuthorize("hasAnyRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/faq/code", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findLiceFaqDivCd(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
    	List<FaqVo> faqDivCdList = faqService.findFaqDivCdList(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("faqDivCdList", faqDivCdList), HttpStatus.OK);
    }
       
    /**
     * FAQ 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'REVIEWER', 'LICENSEE')")
    @RequestMapping( value="/api/{version}/faq", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findFaqPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<FaqVo> fetchedFaqPage = faqService.findFaqPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("faqPage", fetchedFaqPage), HttpStatus.OK);
    }
    
    /**
     * 라이선시용 FAQ 목록을 조회한다.  
     */
    @PreAuthorize("hasAnyRole('LICENSEE')")
    @RequestMapping( value="/api/{version}/licefaq", method=RequestMethod.GET ) 
    public ResponseEntity<CommonResult> findLiceFaqPage(
            @PathVariable("version") final Integer version,
            final CommonCondition condition) {
        
        CommonPage<FaqVo> fetchedLiceFaqPage = faqService.findLiceFaqPage(condition); 
        return new ResponseEntity<CommonResult>(new CommonResult("liceFaqPage", fetchedLiceFaqPage), HttpStatus.OK);
    }  
    
}
