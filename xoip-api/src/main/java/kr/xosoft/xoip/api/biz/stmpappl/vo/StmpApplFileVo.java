package kr.xosoft.xoip.api.biz.stmpappl.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.validator.DefaultGroup;
import kr.xosoft.xoip.api.common.validator.UpdatingGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 증지파일 VO 
 * @author luna1819
 *
 */

@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class StmpApplFileVo extends BaseVo{
	
	private static final long serialVersionUID = 1L;
	
	/**
     * 프로젝트코드
     */
    @NotNull( message="chk.notNull", groups=DefaultGroup.class )
    @Size( message="chk.sizeSame", min=22, max=22, groups=DefaultGroup.class )            
    private String projCd;

    /**
     * 증지신청번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private Integer stmpApplNo;

    /**
     * 계약파일번호
     */
    @NotNull( message="chk.notNull", groups=UpdatingGroup.class )
    private Integer stmpFileNo;
    
    
    /**
     * 원본파일명
     */
    private String orgFileNm;

    /**
     * 관리파일명
     */
    private String mngFileNm;

    /**
     * 물리파일명
     */
    private String pscFileNm;

    /**
     * 파일경로
     */
    private String filePath;

    /**
     * 파일컨텐츠유형
     */
    private String fileCntnTyp;

    /**
     * 파일크기
     */
    private Long fileSize;

    /**
     * 파일확장자명
     */
    private String fileExtNm;

    /**
     * 정렬순번
     */
    private Integer srtOrdno;

    /**
     * 삭제여부
     */
    @JsonIgnore
    private String delYn;

    /**
     * 삭제일시
     */
    @JsonIgnore
    private Date delDt;

    /**
     * 삭제자ID
     */
    @JsonIgnore
    private String delrId;
    
	
	
}
