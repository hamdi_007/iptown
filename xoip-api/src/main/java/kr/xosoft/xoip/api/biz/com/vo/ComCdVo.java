package kr.xosoft.xoip.api.biz.com.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 공통코드 VO
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class ComCdVo extends BaseVo {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 공통코드
	 */
	private String comCd;

	/**
	 * 공통코드명
	 */
	private String comCdNm;

    /**
     * 공통코드영문명
     */
    private String comCdEngNm;

    /**
     * 상위사이트코드
     */
    @JsonIgnore
    private String topSiteCd;
    
	/**
	 * 상위공통코드
	 */
    @JsonProperty( access=Access.WRITE_ONLY )
	private String topComCd;

	/**
	 * 코드값1
	 */
	@JsonInclude( JsonInclude.Include.NON_NULL )	
	private String cdVal1;

	/**
	 * 코드값2
	 */
	@JsonInclude( JsonInclude.Include.NON_NULL )
	private String cdVal2;

	/**
	 * 코드값3
	 */
	@JsonInclude( JsonInclude.Include.NON_NULL )
	private String cdVal3;

	/**
	 * 코드값4
	 */
	@JsonInclude( JsonInclude.Include.NON_NULL )
	private String cdVal4;

	/**
	 * 코드값5
	 */
	@JsonInclude( JsonInclude.Include.NON_NULL )
	private String cdVal5;
	
	/**
	 * 사용여부 
	 */
	private String useYn;

    /**
     * 정렬순번
     */
	@JsonProperty( access=Access.READ_ONLY )
    private Integer srtOrdno;

}
