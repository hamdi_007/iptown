package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.DsgnInspVo;

/**
 * 디자인검수 매퍼 
 * @author likejy
 *
 */
@Repository
public interface DsgnInspMapper {

    /**
     * 디자인검수 등록
     * @param dsgnInsp
     * @return
     */
    public int insertDsgnInsp(DsgnInspVo dsgnInsp);
    
    /**
     * 디자인검수 수정 by PK
     * @param dsgnInsp
     * @return
     */
    public int updateDsgnInspByKey(DsgnInspVo dsgnInsp);
        
    /**
     * 디자인검수 삭제 by PK
     * @param dsgnInsp
     * @return
     */
    public int deleteDsgnInspByKey(DsgnInspVo dsgnInsp);
    
    /**
     * 디자인검수 조회 by PK
     * @param siteCd
     * @param projCd
     * @param inspNo
     * @return
     */
    public DsgnInspVo findLocalizedDsgnInspByKey(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("inspNo") Integer inspNo, @Param("comLang") String comLang);
   
    /**
     * 디자인검수 목록 건수조회 by 검색조건
     * @param map
     * @return
     */
    public int countDsgnInspListByMap(Map<String, Object> map);
    
    /**
     * 디자인검수 목록조회 by 검색조건
     * @param map
     * @return
     */
    public List<DsgnInspVo> findLocalizedDsgnInspListByMap(Map<String, Object> map);        
    
    /**
     * 작성중이거나 승인요청중인 디자인검수 건수 조회 
     * @param siteCd
     * @param projCd
     * @return
     */
    public int countOnGoingDsgnInspByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
    
    /**
     * 다음 검수단계 코드를 조회 
     * @param siteCd
     * @param projCd
     * @return
     */
    public String findInspStgCdForNextStageByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
    
}
