package kr.xosoft.xoip.api.biz.usr.vo;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 로그인 사용자 VO
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class LgnUsrRsltVo extends BaseVo {
    
    private static final long serialVersionUID = 1L;
    
    /**
     * 회사ID 
     */
    private String compId;
    
    /**
     * 회사명
     */
    private String compNm;
    
    /**
     * 사용자ID
     */
    private String usrId;
    
    /**
     * 사용자명 
     */
    private String usrNm;
    
    /**
     * 이메일 
     */
    private String email;
    
    /**
     * 권한 
     */
    private String auth;
    
    /**
     * 엑세스 토큰 
     */
    private String accsToken;
    
    /**
     * 갱신 토큰 
     */
    private String rfshToken;

}
