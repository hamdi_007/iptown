package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdTgtSexVo;

/**
 * 상품대상성별 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProdTgtSexMapper {

    /**
     * 상품대상성별 등록
     * @param prodTgtSex
     * @return
     */
    public int insertProdTgtSex(ProdTgtSexVo prodTgtSex);
    
    /**
     * 상품대상성별 삭제 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */
    public int deleteProdTgtSexByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
 
    /**
     * 성별코드 목록조회 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */    
    public List<String> findSexCdListByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
    
    /**
     * 성별코드 목록조회 by 프로젝트코드
     * @param siteCd
     * @param projCd
     * @return
     */
    public List<ComCdVo> findLocalizedComCdListForSexCdByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("comLang") String comLang);
    
}
