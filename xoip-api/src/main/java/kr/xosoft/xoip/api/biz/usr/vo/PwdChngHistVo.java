package kr.xosoft.xoip.api.biz.usr.vo;

import java.util.Date;

import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 비밀번호 변경 이력 
 * @author likejy
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class PwdChngHistVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 사용자ID
     */
    private String usrId;

    /**
     * 비밀번호변경번호
     */
    private Integer pwdChngNo;

    /**
     * 비밀번호변경일시
     */
    private Date pwdChngDt;

    /**
     * 비밀번호변경방법코드
     */
    private String pwdChngMthdCd;

}
