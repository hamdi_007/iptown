package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdSaleNtnVo;

/**
 * 상품판매국가 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProdSaleNtnMapper {

    /**
     * 상품판매국가 등록
     * @param prodSaleNtn
     * @return
     */
    public int insertProdSaleNtn(ProdSaleNtnVo prodSaleNtn);
    
    /**
     * 상품판매국가 삭제 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */
    public int deleteProdSaleNtnByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
        
    /**
     * 국가코드값 목록조회 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */    
    public List<String> findNtnCdListByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
 
    /**
     * 국가코드 목록조회 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */
    public List<ComCdVo> findLocalizedComCdListForNtnCdByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("comLang") String comLang);
}
