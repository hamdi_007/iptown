package kr.xosoft.xoip.api.biz.ntc.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 공지사항이력 VO 
 * @author hypark
 *
 */
@Data
@EqualsAndHashCode( callSuper=true )
@ToString( callSuper=true )
public class NtcHistVo extends NtcVo {

    private static final long serialVersionUID = 1L;

    /**
     * 공지사항이력일련번호
     */
    private Integer ntcHistSeqno;
    
    /**
     * 이력구분코드 
     */
    private String histDivCd;

    /**
     * 생성자 
     */
    public NtcHistVo(Integer ntcNo, String histDivCd) {
		setNtcNo(ntcNo);
		setHistDivCd(histDivCd);
	}
    
}
