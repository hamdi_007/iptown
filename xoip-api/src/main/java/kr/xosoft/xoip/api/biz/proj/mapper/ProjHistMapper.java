package kr.xosoft.xoip.api.biz.proj.mapper;

import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.proj.vo.ProjHistVo;

/**
 * 프로젝트이력 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProjHistMapper {

    /**
     * 프로젝트이력 등록 
     */
    public void insertProjHistAsSelectMaster(ProjHistVo projHist);    
    
}
