package kr.xosoft.xoip.api.common.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 메시지 빈 
 * @author likejy
 *
 */
@Component
@Slf4j
public class MessageBean {

    @Autowired
    private MessageSource messageSource;    
    
    /**
     * 메세지코드로 등록된 메세지를 조회한다. 
     */
    public String getMessage(String messageCode) {
        return getMessage(messageCode, null);
    }
    
    /**
     * 메세지코드 및 메세지인자값으로 등록된 메세지를 조회한다. 
     */
    public String getMessage(String messageCode, String[] messageArguments) {
        if (messageCode == null) {
            return null;
        }
        String message = null;
        try {
            message = messageSource.getMessage(messageCode, messageArguments, LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException ignore) {
            log.warn("Message code ({}) does not exists in message property file.", messageCode);
        }        
        if (message == null) {
            return messageCode;
        } else {
            return message;
        }
    }   
    
}
