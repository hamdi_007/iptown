package kr.xosoft.xoip.api.common.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * 공통데이터 
 * @author likejy
 *
 */
@Getter
@Builder
@ToString
public class CommonData {

    /**
     * 사용자ID
     */
    private String usrId;

    /**
     * 사용자명
     */
    private String usrNm;

    /**
     * 이메일
     */
    private String email;
    
    /**
     * 사이트코드
     */
    private String siteCd;

    /**
     * 회사ID
     */
    private String compId;

    /**
     * 회사명
     */
    private String compNm;

    /**
     * IP주소
     */
    private String ipcAddr;

    /**
     * 사용자권한
     */
    private String auth;
    
    /**
     * 브라우저 설정 언어코드 (ko, en 만 지원)
     */
    private String lang;

}
