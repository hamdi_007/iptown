package kr.xosoft.xoip.api.biz.proj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import kr.xosoft.xoip.api.biz.com.vo.ComCdVo;
import kr.xosoft.xoip.api.biz.proj.vo.ProdTgtAgeVo;

/**
 * 상품대상연령 매퍼 
 * @author likejy
 *
 */
@Repository
public interface ProdTgtAgeMapper {

    /**
     * 상품대상연령 등록
     * @param prodTgtAge
     * @return
     */
    public int insertProdTgtAge(ProdTgtAgeVo prodTgtAge);
    
    /**
     * 상품대상연령 삭제 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */
    public int deleteProdTgtAgeByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
        
    /**
     * 연령코드값 목록조회 by 프로젝트코드 
     * @param siteCd
     * @param projCd
     * @return
     */    
    public List<String> findAgeCdListByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd);
    
    /**
     * 연령코드 목록조회 by 프로젝트코드
     * @param siteCd
     * @param projCd
     * @return
     */
    public List<ComCdVo> findLocalizedComCdListForAgeCdByProjCd(@Param("siteCd") String siteCd, @Param("projCd") String projCd, @Param("comLang") String comLang);
    
}
