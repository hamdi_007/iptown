package kr.xosoft.xoip.api.common.file;

import java.text.Normalizer;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.multipart.MultipartFile;

import kr.xosoft.xoip.api.biz.cntr.vo.CntrFileVo;
import kr.xosoft.xoip.api.biz.com.vo.BaseVo;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;

/**
 * 계약파일 Wrapper
 * @author likejy
 *
 */
public class CntrFileWrapper implements FileProcessable {

    private static final String SUB_DIR_NM = "contract";
    
    private MultipartFile mFile;
        
    private CntrFileVo fileVo;
    
    private String basePath;
    
    private String canonicalDirPath;
    
    private String canonicalFilePath;
    
    public CntrFileWrapper(MultipartFile mFile, String cntrId, String cntrFileDivCd, String basePath) {
        
        CommonData commonData = CommonDataHolder.getCommonData();
        
        this.mFile = mFile;
        this.fileVo = new CntrFileVo();
        this.basePath = basePath;

        // 맥에서는 한글을 조합형(NFD)를 사용하고, 윈도우에서는 완성형(NFC)를 사용한다. 
        // 따라서 맥에서 한글 파일명을 전송하는 경우 MultipartFile.getOriginalFilename() 메소드는 조합형 한글을 반환하며 DB에 저장될 때에도 조합형으로 저장되게 된다.
        // 이 경우에 DB에 저장된 값(파일명)을 조회시 정상적으로 조회되지 않는다. 
        // 따라서, 이 문제 해결을 위해 입력된 원본 파일명을 항상 완성형(NFC)로 변환하여 사용한다. 
        String originalFilename = Normalizer.normalize(mFile.getOriginalFilename(), Normalizer.Form.NFC);        
        String ext = FilenameUtils.getExtension(originalFilename);
        String pysicalFilename = RandomStringUtils.randomAlphabetic(6) + "_" + System.currentTimeMillis() + "." + ext;
        String filePath = "/" + commonData.getSiteCd() + "/" + SUB_DIR_NM + "/" + cntrId + "/" + pysicalFilename;
        
        fileVo.setCntrId(cntrId);
        fileVo.setCntrFileDivCd(cntrFileDivCd);
        fileVo.setOrgFileNm(originalFilename);
        fileVo.setMngFileNm(originalFilename);
        fileVo.setPscFileNm(pysicalFilename);
        fileVo.setFilePath(filePath);
        fileVo.setFileCntnTyp(mFile.getContentType());
        fileVo.setFileSize(mFile.getSize());        
        fileVo.setFileExtNm(ext);
        
        initPath(fileVo, basePath);        
    }
    
    public CntrFileWrapper(CntrFileVo cntrFile, String basePath) {
        this.fileVo = cntrFile;
        this.basePath = basePath;
        if (cntrFile != null) {
            initPath(cntrFile, basePath);
        }
    }
    
    private void initPath(CntrFileVo cntrFile, String basePath) {
        String tempPath = FilenameUtils.concat(basePath, cntrFile.getSiteCd());
        tempPath = FilenameUtils.concat(tempPath, SUB_DIR_NM);
        tempPath = FilenameUtils.concat(tempPath, cntrFile.getCntrId());
        this.canonicalDirPath = tempPath;
        this.canonicalFilePath = FilenameUtils.concat(canonicalDirPath, cntrFile.getPscFileNm());        
    }

    @Override
    public MultipartFile getMultipartFile() {
        return mFile;
    }
    
    @Override
    public BaseVo getFileVo() {
        return fileVo;
    }
    
    @Override
    public String getBasePath() {
        return basePath;
    }
    
    @Override
    public String getCanonicalDirPath() {
        return canonicalDirPath;
    }

    @Override
    public String getFileDivCd() {
        return fileVo.getCntrFileDivCd();
    }
    
    @Override
    public String getMngFileNm() {
        return fileVo.getMngFileNm();
    }

    @Override
    public String getCanonicalFilePath() {
        return canonicalFilePath;
    }
    
    @Override
    public String getCanonicalTumbMdPath() {
        return null;
    }
    
    @Override
    public String getCanonicalTumbSmPath() {
        return null;
    }
    
    @Override
    public String getFileCntnTyp() {
        return fileVo.getFileCntnTyp();
    }

    @Override
    public Long getFileSize() {
        return fileVo.getFileSize();
    }
    
    @Override
    public String getFileExtNm() {
        return fileVo.getFileExtNm();
    }
    
    @Override
    public boolean isFileDivCdRequired() {
        return true;
    }
    
    @Override
    public boolean isAvailableForPreview() {
        if (fileVo.getFileExtNm() == null) {
            return false;
        }
        String extNm = fileVo.getFileExtNm().toLowerCase();
        if ("png".equals(extNm) || "jpg".equals(extNm) || "jpeg".equals(extNm) || "pdf".equals(extNm)) {
            return true;
        } else {
            return false;
        }
    }    
    
    @Override
    public boolean shouldCreateThumbnail() {
        return false;
    }

}
