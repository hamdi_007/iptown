package kr.xosoft.xoip.api.biz.stats.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import kr.xosoft.xoip.api.biz.stats.mapper.StatsMapper;
import kr.xosoft.xoip.api.biz.stats.vo.StatsVo;
import kr.xosoft.xoip.api.common.bean.CommonCondition;
import kr.xosoft.xoip.api.common.bean.CommonData;
import kr.xosoft.xoip.api.common.bean.CommonDataHolder;
import kr.xosoft.xoip.api.common.bean.CommonPage;
import kr.xosoft.xoip.api.common.bean.MessageBean;
import kr.xosoft.xoip.api.common.exception.BizException;
import kr.xosoft.xoip.api.common.file.buffer.BufferData;
import lombok.extern.slf4j.Slf4j;

/**
 * 통계 서비스 Impl
 * @author leejaejun
 *
 */
@Transactional
@Service
@Slf4j
public class StatsServiceImpl implements StatsService {

	private static final String STATS_RPT_FILE_NM = "stats_rpt.xlsx";
	
	@Value("${api.template.basePath}")
    private String templateBasePath;
	
	@Autowired
    private StatsMapper statsMapper;
	
	@Autowired
    private MessageBean messageBean;
	
    /**
     * 통계 목록 페이지를 조회한다.
     */
    @Override
    public CommonPage<StatsVo> findStatsPage(CommonCondition condition) {

        Map<String, Object> map = condition.toMap();
        List<StatsVo> statsList = null;
        int comTtlCnt = 0;
        
        if(map.get("CMD").equals("IPCOMPPRODTYP")) {
        	comTtlCnt = statsMapper.countStatsListByIpCompProdTyp(map);
            statsList = statsMapper.findStatsListByIpCompProdTyp(map);
        } else if(map.get("CMD").equals("IPCOMP")) {
        	comTtlCnt = statsMapper.countStatsListByIpComp(map);
            statsList = statsMapper.findStatsListByIpComp(map);
        } else if(map.get("CMD").equals("IPPRODTYP")) {
        	comTtlCnt = statsMapper.countStatsListByIpProdTyp(map);
            statsList = statsMapper.findStatsListByIpProdTyp(map);
        } else if(map.get("CMD").equals("IP")) {
        	comTtlCnt = statsMapper.countStatsListByIp(map);
            statsList = statsMapper.findStatsListByIp(map);
        }
      
        return new CommonPage<StatsVo>(condition, statsList, comTtlCnt);           
    }
    
    /**
     * 통계 엑셀 데이터를 생성한다.  
     */
    @Override
    public BufferData makeStatsRptExcelData(CommonCondition condition) {

    	CommonPage<StatsVo> fetchedStatsPage = findStatsPage(condition);
    	
        FileInputStream fis = null;
        ByteArrayOutputStream bos = null;
        XSSFWorkbook workbook = null;
        
        try {
            
            File file = new File(getTemplateFilePath(STATS_RPT_FILE_NM));
            fis = new FileInputStream(file);
            workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowIdx = 0;
            int cellIdx = 0;
            
            CellStyle boderStyle = workbook.createCellStyle();
            boderStyle.setBorderLeft(BorderStyle.THIN);
            boderStyle.setBorderTop(BorderStyle.THIN);
            boderStyle.setBorderRight(BorderStyle.THIN);
            boderStyle.setBorderBottom(BorderStyle.THIN);               
            
            rowIdx = 4;
            XSSFRow row = null;
            XSSFCell cell = null;
            for (StatsVo stats : fetchedStatsPage.getList()) {
                cellIdx = 1;
                row = sheet.getRow(rowIdx);
                if (row == null) {
                	row = sheet.createRow(rowIdx);
                }
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getIpCd());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getIpNm());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getCompNm());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getProdTypNm());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getMaxCnsmAmt().intValue());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getMinCnsmAmt().intValue());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getAvgCnsmAmt().intValue());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getMaxRlsAmt().intValue());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getMinRlsAmt().intValue());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getAvgRlsAmt().intValue());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getPrdcCnt().intValue());
                cell = row.createCell(cellIdx++);
                cell.setCellStyle(boderStyle);
                cell.setCellValue(stats.getRyltAmt().intValue());
           
                rowIdx++;
            }
            
            // BufferData 생성 및 리턴 
            String fileNm = messageBean.getMessage("word.statsRptFileNmPrefix") + ".xlsx";
            bos = new ByteArrayOutputStream();
            workbook.write(bos);
            return new BufferData(bos.toByteArray(), fileNm, BufferData.CONTENT_TYPE_XLSX);
            
        } catch (FileNotFoundException e) {
            throw new BizException("M7022", e);
        } catch (IOException e) {
            throw new BizException("M7023", e);
        } finally {
            try { if (workbook != null) workbook.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
            try { if (fis != null) fis.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
            try { if (bos != null) bos.close(); } catch (Exception ignore) { log.warn(ExceptionUtils.getStackTrace(ignore)); }
        }
    }
    
    private String getTemplateFilePath(String fileNm) {
        CommonData commonData = CommonDataHolder.getCommonData();
        String templateFilePath = FilenameUtils.concat(templateBasePath, commonData.getSiteCd());
        templateFilePath = FilenameUtils.concat(templateFilePath, fileNm);
        return templateFilePath;
    }
    
}
