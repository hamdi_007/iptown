UPDATE xoip.com_cd
SET com_cd_eng_nm='0-3 Years'
WHERE site_cd='CJE' AND com_cd='SA0010';

UPDATE xoip.com_cd
SET com_cd_eng_nm='4-6 Years'
WHERE site_cd='CJE' AND com_cd='SA0020';

UPDATE xoip.com_cd
SET com_cd_eng_nm='7-9 Years'
WHERE site_cd='CJE' AND com_cd='SA0030';

UPDATE xoip.com_cd
SET com_cd_eng_nm='10-13 Years'
WHERE site_cd='CJE' AND com_cd='SA0040';

UPDATE xoip.com_cd
SET com_cd_eng_nm='14-19 Years'
WHERE site_cd='CJE' AND com_cd='SA0050';

UPDATE xoip.com_cd
SET com_cd_eng_nm='20-29 Years'
WHERE site_cd='CJE' AND com_cd='SA0060';

UPDATE xoip.com_cd
SET com_cd_eng_nm='30-39 Years'
WHERE site_cd='CJE' AND com_cd='SA0070';

UPDATE xoip.com_cd
SET com_cd_eng_nm='40-49 Years'
WHERE site_cd='CJE' AND com_cd='SA0080';

UPDATE xoip.com_cd
SET com_cd_eng_nm='50-59 Years'
WHERE site_cd='CJE' AND com_cd='SA0090';

UPDATE xoip.com_cd
SET com_cd_eng_nm='60+ Years'
WHERE site_cd='CJE' AND com_cd='SA0100';

commit;
