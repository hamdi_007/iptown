ALTER TABLE xoip.site_smtp ADD head_tmpl_eng text NULL COMMENT '상단템플릿영문';
ALTER TABLE xoip.site_smtp ADD foot_tmpl_eng text NULL COMMENT '하단템플릿영문';

ALTER TABLE xoip.site_smtp_hist ADD head_tmpl_eng text NULL COMMENT '상단템플릿영문';
ALTER TABLE xoip.site_smtp_hist ADD foot_tmpl_eng text NULL COMMENT '하단템플릿영문';

ALTER TABLE xoip.email_form ADD sbj_tmpl_eng varchar(500) NULL COMMENT '제목템플릿영문';
ALTER TABLE xoip.email_form ADD cnts_tmpl_eng text NULL COMMENT '내용템플릿영문';

ALTER TABLE xoip.email_form_hist ADD sbj_tmpl_eng varchar(500) NULL COMMENT '제목템플릿영문';
ALTER TABLE xoip.email_form_hist ADD cnts_tmpl_eng text NULL COMMENT '내용템플릿영문';

