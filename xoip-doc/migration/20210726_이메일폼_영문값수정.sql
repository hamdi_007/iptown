update email_form set cnts_tmpl = '#{USER_NAME}님 안녕하세요.<br/>
#{LICENSER_COMPANY_NAME} 의 초대로 #{SERVICE_NAME} 회원으로 초청 되었습니다.<br/>
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 로그인 이메일 : #{USER_EMAIL}<br/>
* 초대 일시 : #{PROCESS_DATE}<br/>
<br/>
1. 가이드 다운로드<br>
: #{SERVICE_NAME} 가이드를 다운 받으신 후 가이드에 따라 사용 부탁드립니다.<br/>
<a href="#{MANUAL_LINK}" target="_blank">라이선시 사용 가이드 다운받기</a>
<br/><br/>
2. 주의 사항<br/>
: 로그인 이메일은 라이선시 마다 공용으로 쓸 한개의 메일로만 가입이 가능하며, 비밀번호 역시 공동으로 사용할 수 있게끔 지정 부탁드립니다.<br/>
: 매뉴얼의 경우 #{LICENSER_COMPANY_NAME} 내부의 디자이너가 별도의 메일로 전달 드리고 있습니다. 매뉴얼은 CJENM의 중요한 IP자산으로, 저작권 침해로 이어질 수 있는 외부유출 등 취급에 각별한 주의를 부탁드립니다.(스토리, 귀신, 캐릭터 노출 등)<br/>
<br/>
#{SERVICE_NAME}을 이용하시려면, 아래 링크를 클릭하시고 회원 가입을 완료해 주세요.<br/>
<a href="#{LINK}" target="_blank">회원 가입하기</a><br/>
<br/>
감사합니다.<br/>
',
sbj_tmpl_eng = '[#{SERVICE_NAME}] You are invited to become a member.',
cnts_tmpl_eng = 'Hello, #{USER_NAME}.<br/>
You were invited as a member of #{SERVICE_NAME} by invitation from #{LICENSER_COMPANY_NAME}.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Invitation Date : #{PROCESS_DATE}<br/>
<br/>
1. Download guide<br>
: After downloading #{SERVICE_NAME} guide, please use it according to the guide.<br/>
<a href="#{MANUAL_LINK}" target="_blank">Download the Licensee User Guide</a>
<br/><br/>
2. CAUTION<br/>
: Login e-mail can be registered with only one e-mail for common use per licensee. And you should specify a password so that it can also be used jointly.<br/>
: The manual is delivered by e-mail by designer #{LICENSER_COMPANY_NAME}.<br/>
The manual is an important IP asset of CJENM, so please pay special attention to handling it, such as leaks that may lead to copyright infringement. (Story, ghost, character exposure, etc.)<br/>
<br/>
To use #{SERVICE_NAME}, please click the link below and complete sign up.<br/>
<a href="#{LINK}" target="_blank">Sign up</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0100'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] Membership registration is complete.',
cnts_tmpl_eng = 'Hello, #{USER_NAME}.<br/>
We warmly welcome you to join #{SERVICE_NAME}.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Invitation date: #{PROCESS_DATE}<br/>
<br/>
To use #{SERVICE_NAME}, please click the link below and sign in.<br/>
<a href="#{LINK}" target="_blank">Sign in</a>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0101'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] You are registered as a licenser user.',
cnts_tmpl_eng = 'Hello, #{USER_NAME}.<br/>
You are registered as a licenser user of the service.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Login password : #{USER_PASSWORD}<br/>
* Authority : #{USER_AUTH}<br/>
* Registration date : #{PROCESS_DATE}<br/>
<br/>
You should change a password for security.<br/>
To change a password, please click the link below and sign in<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0102'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] Password reset guide.',
cnts_tmpl_eng = 'Hello, #{USER_NAME}.<br/>
#{SERVICE_NAME} has requested a login password reset. If you are not the person who requested the password reset, please ignore this message.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Request Date : #{PROCESS_DATE}<br/>
<br/>
To reset your password, click the link below.<br/>
<a href="#{LINK}" target="_blank">Reset your password</a>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0110'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] #{PROJECT_NAME} is registered.',
cnts_tmpl_eng = 'Hello, #{USER_NAME}.<br/>
The project has been successfully created in #{SERVICE_NAME}.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Creation Date : #{PROCESS_DATE}<br/>
<br/>
Please request approval of the project created in #{SERVICE_NAME} so that the design inspector in charge of #{LICENSER_COMPANY_NAME} can approve the project and proceed with the design inspection.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0201'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] #{PROJECT_NAME} has been requested for approval.',
cnts_tmpl_eng = 'Hello, #{USER_NAME}.<br/>
The request for approval of the project below has been normally received by #{SERVICE_NAME}.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Approval Date : #{PROCESS_DATE}<br/>
<br/>
According to the received schedule, the design inspector of #{LICENSER_COMPANY_NAME} will check the project and application for design inspection. You can check the progress of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0210'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] #{PROJECT_NAME} has been approved.',
cnts_tmpl_eng = 'Hello, #{LICENSEE_COMPANY_NAME}.<br/>
The project below registered in #{SERVICE_NAME} has been approved.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Approval Date : #{PROCESS_DATE}<br/>
<br/>
The design inspector of #{LICENSER_COMPANY_NAME} will write design review comments, and you can check the progress of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0220'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] #{PROJECT_NAME} has been rejected.',
cnts_tmpl_eng = 'Hello, #{LICENSEE_COMPANY_NAME}.<br/>
Unfortunately, we would like to inform you that the following project registered to #{SERVICE_NAME} has been rejected.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Return Date : #{PROCESS_DATE}<br/>
<br/>
You can check the project\'s rejection history at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0230'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] The design feedback of the project has been registered.',
cnts_tmpl_eng = 'Hello, #{LICENSEE_COMPANY_NAME}.<br/>
Design review comments have been registered for the following projects registered in #{SERVICE_NAME}.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Inspection Date : #{PROCESS_DATE}<br/>
<br/>
you can check the design review comments of the project in #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0240'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] #{PROJECT_NAME} has been canceled.',
cnts_tmpl_eng = 'Hello, #{LICENSEE_COMPANY_NAME}.<br/>
The project in #{SERVICE_NAME} has been cancelled.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Cancellation Date : #{PROCESS_DATE}<br/>
<br/>
You can check the cancellation history of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0250'
  and form_no = 1;

update email_form set sbj_tmpl_eng = '[#{SERVICE_NAME}] #{PROJECT_NAME} is complete.',
cnts_tmpl_eng = 'Hello, #{LICENSEE_COMPANY_NAME}.<br/>
The project in #{SERVICE_NAME} has been completed.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Completion Date : #{PROCESS_DATE}<br/>
<br/>
You can check the completion history of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'
where site_cd = 'CJE'
  and form_cd = 'E0260'
  and form_no = 1;