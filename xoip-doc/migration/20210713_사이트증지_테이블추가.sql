-- 사이트증지
DROP TABLE IF EXISTS site_stmp RESTRICT;

-- 사이트증지
CREATE TABLE site_stmp (
    site_cd                  VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    licr_chrgr_nm            VARCHAR(100)  NOT NULL COMMENT '라이선서담당자명', -- 라이선서담당자명
    licr_chrgr_telno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자전화번호', -- 라이선서담당자전화번호
    licr_chrgr_hnpno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자핸드폰번호', -- 라이선서담당자핸드폰번호
    licr_chrgr_email         VARCHAR(100)  NOT NULL COMMENT '라이선서담당자이메일', -- 라이선서담당자이메일
    stmp_mnfc_comp_nm        VARCHAR(100)  NOT NULL COMMENT '증지제작회사명', -- 증지제작회사명
    stmp_mnfc_chrgr_nm       VARCHAR(100)  NOT NULL COMMENT '증지제작담당자명', -- 증지제작담당자명
    stmp_mnfc_chrgr_telno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자전화번호', -- 증지제작담당자전화번호
    stmp_mnfc_chrgr_hnpno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자핸드폰번호', -- 증지제작담당자핸드폰번호
    stmp_mnfc_chrgr_email    VARCHAR(100)  NOT NULL COMMENT '증지제작담당자이메일', -- 증지제작담당자이메일
    stmp_mnfc_comp_acct_info VARCHAR(100)  NOT NULL COMMENT '증지제작회사계좌정보', -- 증지제작회사계좌정보
    cc_cd                    VARCHAR(8)    NOT NULL COMMENT '통화코드', -- 통화코드
    stmp_std_amt             DECIMAL(18,2) NOT NULL COMMENT '증지기준금액', -- 증지기준금액
    reg_dt                   TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id                  VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt                   TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id                  VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사이트증지';

-- 사이트증지
ALTER TABLE site_stmp
    ADD CONSTRAINT PK_site_stmp -- 사이트증지 기본키
        PRIMARY KEY (
            site_cd -- 사이트코드
        );
        
INSERT INTO site_stmp VALUES ('CJE', '박한나', '02-371-9341', '010-6803-7024', 'hanna.park7@cj.net', '(주)선경홀로그램', '김태훈', '02-3397-3738', '010-8214-0107', 'jintaixun0107@skhologram.co.kr', '신한은행 140-002-300150 (주)선경홀로그램', 'CCKRW0', 4.5, NOW(), 'SYSTEM', NOW(), 'SYSTEM');
COMMIT;
        