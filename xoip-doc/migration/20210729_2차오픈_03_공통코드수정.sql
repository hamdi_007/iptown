INSERT INTO com_cd VALUES ('CJE', 'RS0000', '로열티기준코드', 'Royalty Standard Code', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 0, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'RS0100', '소비자가', 'Retail Price', 'CJE', 'RS0000', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'RS0200', '출고가', 'Release Price', 'CJE', 'RS0000', NULL, NULL, NULL, NULL, NULL, 'Y', 2, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');

INSERT INTO com_cd VALUES ('CJE', 'FD0000', 'FAQ구분코드', 'FAQ Division', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 0, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'FD0100', '회원', 'User', 'CJE', 'FD0000', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'FD0200', 'IP', 'IP', 'CJE', 'FD0000', NULL, NULL, NULL, NULL, NULL, 'Y', 2, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'FD0300', '계약', 'Contract', 'CJE', 'FD0000', NULL, NULL, NULL, NULL, NULL, 'Y', 3, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'FD0400', '프로젝트', 'Project', 'CJE', 'FD0000', NULL, NULL, NULL, NULL, NULL, 'Y', 4, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'FD0500', '증지신청', 'Stamp Apply', 'CJE', 'FD0000', NULL, NULL, NULL, NULL, NULL, 'Y', 5, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');

INSERT INTO com_cd VALUES ('CJE', 'TS0000', '전송상태코드', 'Transfer Status', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', 0, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'TS0100', '성공', 'Success', 'CJE', 'TS0000', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');
INSERT INTO com_cd VALUES ('CJE', 'TS0200', '실패', 'Fail', 'CJE', 'TS0000', NULL, NULL, NULL, NULL, NULL, 'Y', 2, 'N', NULL, NULL, now(), 'SYSTEM', now(), 'SYSTEM');

UPDATE xoip.com_cd
SET com_cd_eng_nm='0-3 Years'
WHERE site_cd='CJE' AND com_cd='SA0010';

UPDATE xoip.com_cd
SET com_cd_eng_nm='4-6 Years'
WHERE site_cd='CJE' AND com_cd='SA0020';

UPDATE xoip.com_cd
SET com_cd_eng_nm='7-9 Years'
WHERE site_cd='CJE' AND com_cd='SA0030';

UPDATE xoip.com_cd
SET com_cd_eng_nm='10-13 Years'
WHERE site_cd='CJE' AND com_cd='SA0040';

UPDATE xoip.com_cd
SET com_cd_eng_nm='14-19 Years'
WHERE site_cd='CJE' AND com_cd='SA0050';

UPDATE xoip.com_cd
SET com_cd_eng_nm='20-29 Years'
WHERE site_cd='CJE' AND com_cd='SA0060';

UPDATE xoip.com_cd
SET com_cd_eng_nm='30-39 Years'
WHERE site_cd='CJE' AND com_cd='SA0070';

UPDATE xoip.com_cd
SET com_cd_eng_nm='40-49 Years'
WHERE site_cd='CJE' AND com_cd='SA0080';

UPDATE xoip.com_cd
SET com_cd_eng_nm='50-59 Years'
WHERE site_cd='CJE' AND com_cd='SA0090';

UPDATE xoip.com_cd
SET com_cd_eng_nm='60+ Years'
WHERE site_cd='CJE' AND com_cd='SA0100';

update com_cd set com_cd_nm = '포르투갈어' where site_cd = 'CJE' and com_cd = 'SLPT00';
update com_cd set com_cd_nm = '포르투갈', com_cd_eng_nm = 'Portugal' where site_cd = 'CJE' and com_cd = 'SNPT00';
update com_cd set com_cd_nm = '포르투갈', com_cd_eng_nm = 'Portugal' where site_cd = 'CJE' and com_cd = 'STPT00';

/* 공통코드 이력 (신규등록건) */
insert into com_cd_hist (hist_div_cd, site_cd, com_cd, com_cd_nm, com_cd_eng_nm, top_site_cd, top_com_cd, cd_val_1, cd_val_2, cd_val_3, cd_val_4, cd_val_5, use_yn, srt_ordno, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id) 
select 'HD0100', site_cd, com_cd, com_cd_nm, com_cd_eng_nm, top_site_cd, top_com_cd, cd_val_1, cd_val_2, cd_val_3, cd_val_4, cd_val_5, use_yn, srt_ordno, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id
  from com_cd
 where site_cd = 'CJE' 
   and (com_cd like 'RS%' or com_cd like 'FD%' or com_cd like 'TS%');

/* 공통코드 이력 (수정건) */
insert into com_cd_hist (hist_div_cd, site_cd, com_cd, com_cd_nm, com_cd_eng_nm, top_site_cd, top_com_cd, cd_val_1, cd_val_2, cd_val_3, cd_val_4, cd_val_5, use_yn, srt_ordno, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id) 
select 'HD0200', site_cd, com_cd, com_cd_nm, com_cd_eng_nm, top_site_cd, top_com_cd, cd_val_1, cd_val_2, cd_val_3, cd_val_4, cd_val_5, use_yn, srt_ordno, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id
  from com_cd
 where site_cd = 'CJE'
   and com_cd in ('SA0010', 'SA0020', 'SA0030', 'SA0040', 'SA0050', 'SA0060', 'SA0070', 'SA0080', 'SA0090', 'SA0100', 'SLPT00', 'SNPT00', 'STPT00');

commit;

