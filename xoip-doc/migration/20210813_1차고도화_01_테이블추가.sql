DROP TABLE IF EXISTS stmp_appl_file RESTRICT;
-- 증지신청파일
CREATE TABLE  stmp_appl_file (
    stmp_file_no  int unsigned    not null,
    stmp_appl_no  int unsigned    not null,
    site_cd       varchar(3)      not null,
    proj_cd       varchar(50)     not null,
    org_file_nm   varchar(300)    not null,
    mng_file_nm   varchar(300)    not null,
    psc_file_nm   varchar(300)    not null,
    file_path     varchar(300)    not null,
    file_cntn_typ varchar(100)    null,
    file_size     bigint unsigned not null,
    file_ext_nm   varchar(100)    null,
    srt_ordno     int unsigned    not null,
    del_yn        char            not null,
    del_dt        timestamp       null,
    delr_id       varchar(10)     null,
    reg_dt        timestamp       not null,
    regr_id       varchar(10)     not null,
    mod_dt        timestamp       not null,
    modr_id       varchar(10)     not null
)
comment '증지신청파일';

-- 증지신청파일
ALTER TABLE stmp_appl_file
    ADD CONSTRAINT PK_stmp_appl_file -- 공지사항 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            proj_cd,   -- 프로젝트코드
            stmp_appl_no,   -- 증지신청번호
            stmp_file_no   -- 증지신청파일번호
        );

