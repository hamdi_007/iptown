update email_form set cnts_tmpl = '#{USER_NAME}님 안녕하세요. <br/>
#{LICENSER_COMPANY_NAME} 의 초대로 #{SERVICE_NAME} 회원으로 초청 되었습니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 로그인 이메일 : #{USER_EMAIL}<br/>
* 초대 일시 : #{PROCESS_DATE}<br/>
<br/>
1. 가이드 다운로드<br>
: #{SERVICE_NAME} 가이드를 다운 받으신 후 가이드에 따라 사용 부탁드립니다.<br/>
<a href="#{MANUAL_LINK}" target="_blank">라이선시 사용 가이드 다운받기</a>
<br/><br/>
2. 주의 사항<br/>
: 로그인 이메일은 라이선시 마다 공용으로 쓸 한개의 메일로만 가입이 가능하며, 비밀번호 역시 공동으로 사용할 수 있게끔 지정 부탁드립니다.<br/>
: 매뉴얼의 경우 #{LICENSER_COMPANY_NAME} 내부의 디자이너가 별도의 메일로 전달 드리고 있습니다. 매뉴얼은 CJENM의 중요한 IP자산으로, 저작권 침해로 이어질 수 있는 외부유출 등 취급에 각별한 주의를 부탁드립니다.(스토리, 귀신, 캐릭터 노출 등)<br/>
<br/>
#{SERVICE_NAME}을 이용하시려면, 아래 링크를 클릭하시고 회원 가입을 완료해 주세요.<br/>  
<a href="#{LINK}" target="_blank">회원 가입하기</a><br/>
<br/>
감사합니다.<br/>
' 
where site_cd = 'CJE'
  and form_cd = 'E0100'
  and form_no = 1;