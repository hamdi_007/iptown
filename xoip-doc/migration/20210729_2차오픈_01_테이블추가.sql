-- 공지사항
DROP TABLE IF EXISTS ntc RESTRICT;

-- 공지사항이력
DROP TABLE IF EXISTS ntc_hist RESTRICT;

-- 공지사항조회이력
DROP TABLE IF EXISTS ntc_read_hist RESTRICT;

-- 공지사항파일
DROP TABLE IF EXISTS ntc_file RESTRICT;

-- FAQ
DROP TABLE IF EXISTS faq RESTRICT;

-- FAQ파일
DROP TABLE IF EXISTS faq_file RESTRICT;

-- FAQ이력
DROP TABLE IF EXISTS faq_hist RESTRICT;

-- 공지사항
CREATE TABLE ntc (
    site_cd     VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no      INT UNSIGNED  NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    lang_val    VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    ntc_sbj     VARCHAR(100)  NOT NULL COMMENT '공지사항제목', -- 공지사항제목
    ntc_cnts    VARCHAR(2000) NOT NULL COMMENT '공지사항내용', -- 공지사항내용
    head_fix_yn CHAR(1)       NOT NULL COMMENT '상단고정여부', -- 상단고정여부
    post_yn     CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    read_cnt    INT           NOT NULL COMMENT '조회개수', -- 조회개수
    del_yn      CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt      TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id     VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt      TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id     VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt      TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id     VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공지사항';

-- 공지사항
ALTER TABLE ntc
    ADD CONSTRAINT PK_ntc -- 공지사항 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            ntc_no   -- 공지사항번호
        );

-- 공지사항이력
CREATE TABLE ntc_hist (
    ntc_hist_seqno INT UNSIGNED  NOT NULL COMMENT '공지사항이력일련번호', -- 공지사항이력일련번호
    hist_div_cd    VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd        VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no         INT UNSIGNED  NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    lang_val       VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    ntc_sbj        VARCHAR(100)  NOT NULL COMMENT '공지사항제목', -- 공지사항제목
    ntc_cnts       VARCHAR(2000) NOT NULL COMMENT '공지사항내용', -- 공지사항내용
    head_fix_yn    CHAR(1)       NOT NULL COMMENT '상단고정여부', -- 상단고정여부
    post_yn        CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    read_cnt       INT           NOT NULL COMMENT '조회개수', -- 조회개수
    del_yn         CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt         TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id        VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt         TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id        VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt         TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id        VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공지사항이력';

-- 공지사항이력
ALTER TABLE ntc_hist
    ADD CONSTRAINT PK_ntc_hist -- 공지사항이력 기본키
        PRIMARY KEY (
            ntc_hist_seqno -- 공지사항이력일련번호
        );

ALTER TABLE ntc_hist
    MODIFY COLUMN ntc_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '공지사항이력일련번호';

ALTER TABLE ntc_hist
    AUTO_INCREMENT = 1;

-- 공지사항조회이력
CREATE TABLE ntc_read_hist (
    site_cd  VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no   INT UNSIGNED NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    readr_id VARCHAR(10)  NOT NULL COMMENT '조회자ID', -- 조회자ID
    read_dt  TIMESTAMP    NOT NULL COMMENT '조회일시', -- 조회일시
    reg_dt   TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id  VARCHAR(10)  NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '공지사항조회이력';

-- 공지사항조회이력
ALTER TABLE ntc_read_hist
    ADD CONSTRAINT PK_ntc_read_hist -- 공지사항조회이력 기본키
        PRIMARY KEY (
            site_cd,  -- 사이트코드
            ntc_no,   -- 공지사항번호
            readr_id  -- 조회자ID
        );

-- 공지사항파일
CREATE TABLE ntc_file (
    site_cd       VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no        INT UNSIGNED    NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    ntc_file_no   INT UNSIGNED    NOT NULL COMMENT '공지사항파일번호', -- 공지사항파일번호
    org_file_nm   VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm   VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm   VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    file_path     VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size     BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm   VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno     INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn        CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt        TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id       VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt        TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id       VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt        TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id       VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공지사항파일';

-- 공지사항파일
ALTER TABLE ntc_file
    ADD CONSTRAINT PK_ntc_file -- 공지사항파일 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            ntc_no,      -- 공지사항번호
            ntc_file_no  -- 공지사항파일번호
        );

-- FAQ
CREATE TABLE faq (
    site_cd    VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    faq_no     INT UNSIGNED  NOT NULL COMMENT 'FAQ번호', -- FAQ번호
    lang_val   VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    faq_div_cd VARCHAR(8)    NOT NULL COMMENT 'FAQ구분코드', -- FAQ구분코드
    qstn       VARCHAR(500)  NOT NULL COMMENT '질문', -- 질문
    ansr       VARCHAR(2000) NOT NULL COMMENT '답변', -- 답변
    post_yn    CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    del_yn     CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt     TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id    VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt     TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id    VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt     TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id    VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'FAQ';

-- FAQ
ALTER TABLE faq
    ADD CONSTRAINT PK_faq -- FAQ 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            faq_no   -- FAQ번호
        );

-- FAQ파일
CREATE TABLE faq_file (
    site_cd       VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    faq_no        INT UNSIGNED    NOT NULL COMMENT 'FAQ번호', -- FAQ번호
    faq_file_no   INT UNSIGNED    NOT NULL COMMENT 'FAQ파일번호', -- FAQ파일번호
    org_file_nm   VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm   VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm   VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    file_path     VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size     BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm   VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno     INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn        CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt        TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id       VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt        TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id       VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt        TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id       VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'FAQ파일';

-- FAQ파일
ALTER TABLE faq_file
    ADD CONSTRAINT PK_faq_file -- FAQ파일 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            faq_no,      -- FAQ번호
            faq_file_no  -- FAQ파일번호
        );

-- FAQ이력
CREATE TABLE faq_hist (
    faq_hist_seqno INT UNSIGNED  NOT NULL COMMENT 'FAQ이력일련번호', -- FAQ이력일련번호
    hist_div_cd    VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd        VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    faq_no         INT UNSIGNED  NOT NULL COMMENT 'FAQ번호', -- FAQ번호
    lang_val       VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    faq_div_cd     VARCHAR(8)    NOT NULL COMMENT 'FAQ구분코드', -- FAQ구분코드
    qstn           VARCHAR(500)  NOT NULL COMMENT '질문', -- 질문
    ansr           VARCHAR(2000) NOT NULL COMMENT '답변', -- 답변
    post_yn        CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    del_yn         CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt         TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id        VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt         TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id        VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt         TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id        VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'FAQ이력';

-- FAQ이력
ALTER TABLE faq_hist
    ADD CONSTRAINT PK_faq_hist -- FAQ이력 기본키
        PRIMARY KEY (
            faq_hist_seqno -- FAQ이력일련번호
        );

ALTER TABLE faq_hist
    MODIFY COLUMN faq_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'FAQ이력일련번호';

ALTER TABLE faq_hist
    AUTO_INCREMENT = 1;




    
-- 마크업
DROP TABLE IF EXISTS mark RESTRICT;

-- 마크업의견
DROP TABLE IF EXISTS mark_opin RESTRICT;

-- 증지신청
DROP TABLE IF EXISTS stmp_appl RESTRICT;

-- 증지품목
DROP TABLE IF EXISTS stmp_lot RESTRICT;

-- 증지신청이력
DROP TABLE IF EXISTS stmp_appl_hist RESTRICT;

-- 증지품목이력
DROP TABLE IF EXISTS stmp_lot_hist RESTRICT;

-- 마크업
CREATE TABLE mark (
    site_cd      VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd      VARCHAR(22)  NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    insp_no      INT UNSIGNED NOT NULL COMMENT '검수번호', -- 검수번호
    insp_file_no INT UNSIGNED NOT NULL COMMENT '검수파일번호', -- 검수파일번호
    mark_no      INT UNSIGNED NOT NULL COMMENT '마크업번호', -- 마크업번호
    mark_meta    MEDIUMTEXT   NOT NULL COMMENT '마크업메타', -- 마크업메타
    del_yn       CHAR(1)      NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt       TIMESTAMP    NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id      VARCHAR(10)  NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt       TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10)  NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt       TIMESTAMP    NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id      VARCHAR(10)  NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '마크업';

-- 마크업
ALTER TABLE mark
    ADD CONSTRAINT PK_mark -- 마크업 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            insp_no,      -- 검수번호
            insp_file_no, -- 검수파일번호
            mark_no       -- 마크업번호
        );

-- 마크업의견
CREATE TABLE mark_opin (
    site_cd        VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd        VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    insp_no        INT UNSIGNED  NOT NULL COMMENT '검수번호', -- 검수번호
    insp_file_no   INT UNSIGNED  NOT NULL COMMENT '검수파일번호', -- 검수파일번호
    mark_no        INT UNSIGNED  NOT NULL COMMENT '마크업번호', -- 마크업번호
    mark_opin_no   INT UNSIGNED  NOT NULL COMMENT '마크업의견번호', -- 마크업의견번호
    clr_val        VARCHAR(50)   NOT NULL COMMENT '색상값', -- 색상값
    mark_opin_cnts VARCHAR(2000) NOT NULL COMMENT '마크업의견내용', -- 마크업의견내용
    del_yn         CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt         TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id        VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt         TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id        VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt         TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id        VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '마크업의견';

-- 마크업의견
ALTER TABLE mark_opin
    ADD CONSTRAINT PK_mark_opin -- 마크업의견 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            insp_no,      -- 검수번호
            insp_file_no, -- 검수파일번호
            mark_no,      -- 마크업번호
            mark_opin_no  -- 마크업의견번호
        );

-- 증지신청
CREATE TABLE stmp_appl (
    site_cd                  VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd                  VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    stmp_appl_no             INT UNSIGNED  NOT NULL COMMENT '증지신청번호', -- 증지신청번호
    stmp_appl_dt             TIMESTAMP     NOT NULL COMMENT '증지신청일시', -- 증지신청일시
    stmp_applr_nm            VARCHAR(100)  NOT NULL COMMENT '증지신청자명', -- 증지신청자명
    stmp_applr_telno         VARCHAR(20)   NOT NULL COMMENT '증지신청자전화번호', -- 증지신청자전화번호
    stmp_applr_hnpno         VARCHAR(20)   NOT NULL COMMENT '증지신청자핸드폰번호', -- 증지신청자핸드폰번호
    stmp_applr_email         VARCHAR(100)  NOT NULL COMMENT '증지신청자이메일', -- 증지신청자이메일
    licr_chrgr_nm            VARCHAR(100)  NOT NULL COMMENT '라이선서담당자명', -- 라이선서담당자명
    licr_chrgr_telno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자전화번호', -- 라이선서담당자전화번호
    licr_chrgr_hnpno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자핸드폰번호', -- 라이선서담당자핸드폰번호
    licr_chrgr_email         VARCHAR(100)  NOT NULL COMMENT '라이선서담당자이메일', -- 라이선서담당자이메일
    stmp_recp_nm             VARCHAR(100)  NOT NULL COMMENT '증지수신자명', -- 증지수신자명
    stmp_recp_telno          VARCHAR(20)   NOT NULL COMMENT '증지수신자전화번호', -- 증지수신자전화번호
    stmp_recp_addr           VARCHAR(500)  NOT NULL COMMENT '증지수신자주소', -- 증지수신자주소
    stmp_recp_dtl_addr       VARCHAR(500)  NOT NULL COMMENT '증지수신자상세주소', -- 증지수신자상세주소
    stmp_recp_post_no        VARCHAR(8)    NOT NULL COMMENT '증지수신자우편번호', -- 증지수신자우편번호
    bill_email               VARCHAR(100)  NOT NULL COMMENT '계산서이메일', -- 계산서이메일
    stmp_mnfc_comp_nm        VARCHAR(100)  NOT NULL COMMENT '증지제작회사명', -- 증지제작회사명
    stmp_mnfc_chrgr_nm       VARCHAR(100)  NOT NULL COMMENT '증지제작담당자명', -- 증지제작담당자명
    stmp_mnfc_chrgr_telno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자전화번호', -- 증지제작담당자전화번호
    stmp_mnfc_chrgr_hnpno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자핸드폰번호', -- 증지제작담당자핸드폰번호
    stmp_mnfc_chrgr_email    VARCHAR(100)  NOT NULL COMMENT '증지제작담당자이메일', -- 증지제작담당자이메일
    stmp_mnfc_comp_acct_info VARCHAR(100)  NOT NULL COMMENT '증지제작회사계좌정보', -- 증지제작회사계좌정보
    cc_cd                    VARCHAR(8)    NOT NULL COMMENT '통화코드', -- 통화코드
    stmp_std_amt             DECIMAL(18,2) NOT NULL COMMENT '증지기준금액', -- 증지기준금액
    prdc_cnt_sum             INT           NOT NULL COMMENT '생산개수합계', -- 생산개수합계
    rylt_amt_sum             DECIMAL(18,2) NOT NULL COMMENT '로열티금액합계', -- 로열티금액합계
    stmp_amt_sum             DECIMAL(18,2) NOT NULL COMMENT '증지금액합계', -- 증지금액합계
    stmp_vat_sum             DECIMAL(18,2) NOT NULL COMMENT '증지부가세합계', -- 증지부가세합계
    dps_tgt_amt_sum          DECIMAL(18,2) NOT NULL COMMENT '입금대상금액합계', -- 입금대상금액합계
    stmp_applr_sign_val      MEDIUMTEXT    NOT NULL COMMENT '증지신청자서명값', -- 증지신청자서명값
    del_yn                   CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt                   TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id                  VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt                   TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id                  VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt                   TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id                  VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '증지신청';

-- 증지신청
ALTER TABLE stmp_appl
    ADD CONSTRAINT PK_stmp_appl -- 증지신청 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            stmp_appl_no  -- 증지신청번호
        );

-- 증지품목
CREATE TABLE stmp_lot (
    site_cd      VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd      VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    stmp_appl_no INT UNSIGNED  NOT NULL COMMENT '증지신청번호', -- 증지신청번호
    lot_no       INT UNSIGNED  NOT NULL COMMENT '품목번호', -- 품목번호
    lot_nm       VARCHAR(100)  NOT NULL COMMENT '품목명', -- 품목명
    cnsm_amt     DECIMAL(18,2) NOT NULL COMMENT '소비자금액', -- 소비자금액
    rls_amt      DECIMAL(18,2) NOT NULL COMMENT '출고금액', -- 출고금액
    rylt_std_cd  VARCHAR(8)    NOT NULL COMMENT '로열티기준코드', -- 로열티기준코드
    rylt_rt      DECIMAL(13,5) NOT NULL COMMENT '로열티율', -- 로열티율
    prdc_cnt     INT           NOT NULL COMMENT '생산개수', -- 생산개수
    rylt_amt     DECIMAL(18,2) NOT NULL COMMENT '로열티금액', -- 로열티금액
    stmp_knd_val VARCHAR(10)   NOT NULL COMMENT '증지종류값', -- 증지종류값
    stmp_amt     DECIMAL(18,2) NOT NULL COMMENT '증지금액', -- 증지금액
    del_yn       CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt       TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id      VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt       TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt       TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id      VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '증지품목';

-- 증지품목
ALTER TABLE stmp_lot
    ADD CONSTRAINT PK_stmp_lot -- 증지품목 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            stmp_appl_no, -- 증지신청번호
            lot_no        -- 품목번호
        );

-- 증지신청이력
CREATE TABLE stmp_appl_hist (
    stmp_appl_hist_seqno     INT UNSIGNED  NOT NULL COMMENT '증지신청이력일련번호', -- 증지신청이력일련번호
    hist_div_cd              VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd                  VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd                  VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    stmp_appl_no             INT UNSIGNED  NOT NULL COMMENT '증지신청번호', -- 증지신청번호
    stmp_appl_dt             TIMESTAMP     NOT NULL COMMENT '증지신청일시', -- 증지신청일시
    stmp_applr_nm            VARCHAR(100)  NOT NULL COMMENT '증지신청자명', -- 증지신청자명
    stmp_applr_telno         VARCHAR(20)   NOT NULL COMMENT '증지신청자전화번호', -- 증지신청자전화번호
    stmp_applr_hnpno         VARCHAR(20)   NOT NULL COMMENT '증지신청자핸드폰번호', -- 증지신청자핸드폰번호
    stmp_applr_email         VARCHAR(100)  NOT NULL COMMENT '증지신청자이메일', -- 증지신청자이메일
    licr_chrgr_nm            VARCHAR(100)  NOT NULL COMMENT '라이선서담당자명', -- 라이선서담당자명
    licr_chrgr_telno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자전화번호', -- 라이선서담당자전화번호
    licr_chrgr_hnpno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자핸드폰번호', -- 라이선서담당자핸드폰번호
    licr_chrgr_email         VARCHAR(100)  NOT NULL COMMENT '라이선서담당자이메일', -- 라이선서담당자이메일
    stmp_recp_nm             VARCHAR(100)  NOT NULL COMMENT '증지수신자명', -- 증지수신자명
    stmp_recp_telno          VARCHAR(20)   NOT NULL COMMENT '증지수신자전화번호', -- 증지수신자전화번호
    stmp_recp_addr           VARCHAR(500)  NOT NULL COMMENT '증지수신자주소', -- 증지수신자주소
    stmp_recp_dtl_addr       VARCHAR(500)  NOT NULL COMMENT '증지수신자상세주소', -- 증지수신자상세주소
    stmp_recp_post_no        VARCHAR(8)    NOT NULL COMMENT '증지수신자우편번호', -- 증지수신자우편번호
    bill_email               VARCHAR(100)  NOT NULL COMMENT '계산서이메일', -- 계산서이메일
    stmp_mnfc_comp_nm        VARCHAR(100)  NOT NULL COMMENT '증지제작회사명', -- 증지제작회사명
    stmp_mnfc_chrgr_nm       VARCHAR(100)  NOT NULL COMMENT '증지제작담당자명', -- 증지제작담당자명
    stmp_mnfc_chrgr_telno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자전화번호', -- 증지제작담당자전화번호
    stmp_mnfc_chrgr_hnpno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자핸드폰번호', -- 증지제작담당자핸드폰번호
    stmp_mnfc_chrgr_email    VARCHAR(100)  NOT NULL COMMENT '증지제작담당자이메일', -- 증지제작담당자이메일
    stmp_mnfc_comp_acct_info VARCHAR(100)  NOT NULL COMMENT '증지제작회사계좌정보', -- 증지제작회사계좌정보
    cc_cd                    VARCHAR(8)    NOT NULL COMMENT '통화코드', -- 통화코드
    stmp_std_amt             DECIMAL(18,2) NOT NULL COMMENT '증지기준금액', -- 증지기준금액
    prdc_cnt_sum             INT           NOT NULL COMMENT '생산개수합계', -- 생산개수합계
    rylt_amt_sum             DECIMAL(18,2) NOT NULL COMMENT '로열티금액합계', -- 로열티금액합계
    stmp_amt_sum             DECIMAL(18,2) NOT NULL COMMENT '증지금액합계', -- 증지금액합계
    stmp_vat_sum             DECIMAL(18,2) NOT NULL COMMENT '증지부가세합계', -- 증지부가세합계
    dps_tgt_amt_sum          DECIMAL(18,2) NOT NULL COMMENT '입금대상금액합계', -- 입금대상금액합계
    stmp_applr_sign_val      MEDIUMTEXT    NOT NULL COMMENT '증지신청자서명값', -- 증지신청자서명값
    del_yn                   CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt                   TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id                  VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt                   TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id                  VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt                   TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id                  VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '증지신청이력';

-- 증지신청이력
ALTER TABLE stmp_appl_hist
    ADD CONSTRAINT PK_stmp_appl_hist -- 증지신청이력 기본키
        PRIMARY KEY (
            stmp_appl_hist_seqno -- 증지신청이력일련번호
        );

ALTER TABLE stmp_appl_hist
    MODIFY COLUMN stmp_appl_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '증지신청이력일련번호';

ALTER TABLE stmp_appl_hist
    AUTO_INCREMENT = 1;

-- 증지품목이력
CREATE TABLE stmp_lot_hist (
    stmp_lot_hist_seqno INT UNSIGNED  NOT NULL COMMENT '증지품목이력이련번호', -- 증지품목이력이련번호
    hist_div_cd         VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd             VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd             VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    stmp_appl_no        INT UNSIGNED  NOT NULL COMMENT '증지신청번호', -- 증지신청번호
    lot_no              INT UNSIGNED  NOT NULL COMMENT '품목번호', -- 품목번호
    lot_nm              VARCHAR(100)  NOT NULL COMMENT '품목명', -- 품목명
    cnsm_amt            DECIMAL(18,2) NOT NULL COMMENT '소비자금액', -- 소비자금액
    rls_amt             DECIMAL(18,2) NOT NULL COMMENT '출고금액', -- 출고금액
    rylt_std_cd         VARCHAR(8)    NOT NULL COMMENT '로열티기준코드', -- 로열티기준코드
    rylt_rt             DECIMAL(13,5) NOT NULL COMMENT '로열티율', -- 로열티율
    prdc_cnt            INT           NOT NULL COMMENT '생산개수', -- 생산개수
    rylt_amt            DECIMAL(18,2) NOT NULL COMMENT '로열티금액', -- 로열티금액
    stmp_knd_val        VARCHAR(10)   NOT NULL COMMENT '증지종류값', -- 증지종류값
    stmp_amt            DECIMAL(18,2) NOT NULL COMMENT '증지금액', -- 증지금액
    del_yn              CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt              TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id             VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt              TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id             VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt              TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id             VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '증지품목이력';

-- 증지품목이력
ALTER TABLE stmp_lot_hist
    ADD CONSTRAINT PK_stmp_lot_hist -- 증지품목이력 기본키
        PRIMARY KEY (
            stmp_lot_hist_seqno -- 증지품목이력이련번호
        );

ALTER TABLE stmp_lot_hist
    MODIFY COLUMN stmp_lot_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '증지품목이력이련번호';

ALTER TABLE stmp_lot_hist
    AUTO_INCREMENT = 1;




-- 사이트증지
DROP TABLE IF EXISTS site_stmp RESTRICT;

-- 사이트증지
CREATE TABLE site_stmp (
    site_cd                  VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    licr_chrgr_nm            VARCHAR(100)  NOT NULL COMMENT '라이선서담당자명', -- 라이선서담당자명
    licr_chrgr_telno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자전화번호', -- 라이선서담당자전화번호
    licr_chrgr_hnpno         VARCHAR(20)   NOT NULL COMMENT '라이선서담당자핸드폰번호', -- 라이선서담당자핸드폰번호
    licr_chrgr_email         VARCHAR(100)  NOT NULL COMMENT '라이선서담당자이메일', -- 라이선서담당자이메일
    stmp_mnfc_comp_nm        VARCHAR(100)  NOT NULL COMMENT '증지제작회사명', -- 증지제작회사명
    stmp_mnfc_chrgr_nm       VARCHAR(100)  NOT NULL COMMENT '증지제작담당자명', -- 증지제작담당자명
    stmp_mnfc_chrgr_telno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자전화번호', -- 증지제작담당자전화번호
    stmp_mnfc_chrgr_hnpno    VARCHAR(20)   NOT NULL COMMENT '증지제작담당자핸드폰번호', -- 증지제작담당자핸드폰번호
    stmp_mnfc_chrgr_email    VARCHAR(100)  NOT NULL COMMENT '증지제작담당자이메일', -- 증지제작담당자이메일
    stmp_mnfc_comp_acct_info VARCHAR(100)  NOT NULL COMMENT '증지제작회사계좌정보', -- 증지제작회사계좌정보
    cc_cd                    VARCHAR(8)    NOT NULL COMMENT '통화코드', -- 통화코드
    stmp_std_amt             DECIMAL(18,2) NOT NULL COMMENT '증지기준금액', -- 증지기준금액
    reg_dt                   TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id                  VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt                   TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id                  VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사이트증지';

-- 사이트증지
ALTER TABLE site_stmp
    ADD CONSTRAINT PK_site_stmp -- 사이트증지 기본키
        PRIMARY KEY (
            site_cd -- 사이트코드
        );
        
INSERT INTO site_stmp VALUES ('CJE', '박한나', '02-371-9341', '010-6803-7024', 'hanna.park7@cj.net', '(주)선경홀로그램', '김태훈', '02-3397-3738', '010-8214-0107', 'jintaixun0107@skhologram.co.kr', '신한은행 140-002-300150 (주)선경홀로그램', 'CCKRW0', 4.5, NOW(), 'SYSTEM', NOW(), 'SYSTEM');
COMMIT;
                