-- 공지사항
DROP TABLE IF EXISTS ntc RESTRICT;

-- 공지사항이력
DROP TABLE IF EXISTS ntc_hist RESTRICT;

-- 공지사항조회이력
DROP TABLE IF EXISTS ntc_read_hist RESTRICT;

-- 공지사항파일
DROP TABLE IF EXISTS ntc_file RESTRICT;

-- FAQ
DROP TABLE IF EXISTS faq RESTRICT;

-- FAQ파일
DROP TABLE IF EXISTS faq_file RESTRICT;

-- FAQ이력
DROP TABLE IF EXISTS faq_hist RESTRICT;

-- 공지사항
CREATE TABLE ntc (
    site_cd     VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no      INT UNSIGNED  NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    lang_val    VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    ntc_sbj     VARCHAR(100)  NOT NULL COMMENT '공지사항제목', -- 공지사항제목
    ntc_cnts    VARCHAR(2000) NOT NULL COMMENT '공지사항내용', -- 공지사항내용
    head_fix_yn CHAR(1)       NOT NULL COMMENT '상단고정여부', -- 상단고정여부
    post_yn     CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    read_cnt    INT           NOT NULL COMMENT '조회개수', -- 조회개수
    del_yn      CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt      TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id     VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt      TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id     VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt      TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id     VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공지사항';

-- 공지사항
ALTER TABLE ntc
    ADD CONSTRAINT PK_ntc -- 공지사항 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            ntc_no   -- 공지사항번호
        );

-- 공지사항이력
CREATE TABLE ntc_hist (
    ntc_hist_seqno INT UNSIGNED  NOT NULL COMMENT '공지사항이력일련번호', -- 공지사항이력일련번호
    hist_div_cd    VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd        VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no         INT UNSIGNED  NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    lang_val       VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    ntc_sbj        VARCHAR(100)  NOT NULL COMMENT '공지사항제목', -- 공지사항제목
    ntc_cnts       VARCHAR(2000) NOT NULL COMMENT '공지사항내용', -- 공지사항내용
    head_fix_yn    CHAR(1)       NOT NULL COMMENT '상단고정여부', -- 상단고정여부
    post_yn        CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    read_cnt       INT           NOT NULL COMMENT '조회개수', -- 조회개수
    del_yn         CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt         TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id        VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt         TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id        VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt         TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id        VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공지사항이력';

-- 공지사항이력
ALTER TABLE ntc_hist
    ADD CONSTRAINT PK_ntc_hist -- 공지사항이력 기본키
        PRIMARY KEY (
            ntc_hist_seqno -- 공지사항이력일련번호
        );

ALTER TABLE ntc_hist
    MODIFY COLUMN ntc_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '공지사항이력일련번호';

ALTER TABLE ntc_hist
    AUTO_INCREMENT = 1;

-- 공지사항조회이력
CREATE TABLE ntc_read_hist (
    site_cd  VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no   INT UNSIGNED NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    readr_id VARCHAR(10)  NOT NULL COMMENT '조회자ID', -- 조회자ID
    read_dt  TIMESTAMP    NOT NULL COMMENT '조회일시', -- 조회일시
    reg_dt   TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id  VARCHAR(10)  NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '공지사항조회이력';

-- 공지사항조회이력
ALTER TABLE ntc_read_hist
    ADD CONSTRAINT PK_ntc_read_hist -- 공지사항조회이력 기본키
        PRIMARY KEY (
            site_cd,  -- 사이트코드
            ntc_no,   -- 공지사항번호
            readr_id  -- 조회자ID
        );

-- 공지사항파일
CREATE TABLE ntc_file (
    site_cd       VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    ntc_no        INT UNSIGNED    NOT NULL COMMENT '공지사항번호', -- 공지사항번호
    ntc_file_no   INT UNSIGNED    NOT NULL COMMENT '공지사항파일번호', -- 공지사항파일번호
    org_file_nm   VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm   VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm   VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    file_path     VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size     BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm   VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno     INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn        CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt        TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id       VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt        TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id       VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt        TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id       VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공지사항파일';

-- 공지사항파일
ALTER TABLE ntc_file
    ADD CONSTRAINT PK_ntc_file -- 공지사항파일 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            ntc_no,      -- 공지사항번호
            ntc_file_no  -- 공지사항파일번호
        );

-- FAQ
CREATE TABLE faq (
    site_cd    VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    faq_no     INT UNSIGNED  NOT NULL COMMENT 'FAQ번호', -- FAQ번호
    lang_val   VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    faq_div_cd VARCHAR(8)    NOT NULL COMMENT 'FAQ구분코드', -- FAQ구분코드
    qstn       VARCHAR(500)  NOT NULL COMMENT '질문', -- 질문
    ansr       VARCHAR(2000) NOT NULL COMMENT '답변', -- 답변
    post_yn    CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    del_yn     CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt     TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id    VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt     TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id    VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt     TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id    VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'FAQ';

-- FAQ
ALTER TABLE faq
    ADD CONSTRAINT PK_faq -- FAQ 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            faq_no   -- FAQ번호
        );

-- FAQ파일
CREATE TABLE faq_file (
    site_cd       VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    faq_no        INT UNSIGNED    NOT NULL COMMENT 'FAQ번호', -- FAQ번호
    faq_file_no   INT UNSIGNED    NOT NULL COMMENT 'FAQ파일번호', -- FAQ파일번호
    org_file_nm   VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm   VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm   VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    file_path     VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size     BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm   VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno     INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn        CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt        TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id       VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt        TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id       VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt        TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id       VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'FAQ파일';

-- FAQ파일
ALTER TABLE faq_file
    ADD CONSTRAINT PK_faq_file -- FAQ파일 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            faq_no,      -- FAQ번호
            faq_file_no  -- FAQ파일번호
        );

-- FAQ이력
CREATE TABLE faq_hist (
    faq_hist_seqno INT UNSIGNED  NOT NULL COMMENT 'FAQ이력일련번호', -- FAQ이력일련번호
    hist_div_cd    VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd        VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    faq_no         INT UNSIGNED  NOT NULL COMMENT 'FAQ번호', -- FAQ번호
    lang_val       VARCHAR(2)    NOT NULL COMMENT '언어값', -- 언어값
    faq_div_cd     VARCHAR(8)    NOT NULL COMMENT 'FAQ구분코드', -- FAQ구분코드
    qstn           VARCHAR(500)  NOT NULL COMMENT '질문', -- 질문
    ansr           VARCHAR(2000) NOT NULL COMMENT '답변', -- 답변
    post_yn        CHAR(1)       NOT NULL COMMENT '게시여부', -- 게시여부
    del_yn         CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt         TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id        VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt         TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id        VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt         TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id        VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'FAQ이력';

-- FAQ이력
ALTER TABLE faq_hist
    ADD CONSTRAINT PK_faq_hist -- FAQ이력 기본키
        PRIMARY KEY (
            faq_hist_seqno -- FAQ이력일련번호
        );

ALTER TABLE faq_hist
    MODIFY COLUMN faq_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'FAQ이력일련번호';

ALTER TABLE faq_hist
    AUTO_INCREMENT = 1;
    
    