update site_smtp
   set head_tmpl_eng = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>IPTOWN</title>
</head>
<body>
    <!-- OUTERMOST CONTAINER TABLE -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="font-family: \'dotum\', sans-serif;font-size: 12px;line-height: 20px;letter-spacing: -0.01em;color: #000;">
        <tr>
            <td>
                <!-- 700px CONTENTS CONTAINER TABLE -->
                <table border="0" cellpadding="0" cellspacing="0" width="700" class="table-wrapper" align="center" style="padding-top: 31px;padding-bottom: 30px;padding-left: 29px;padding-right: 29px;border: 1px solid #d6d6d6;">
                    <tr>
                        <td>
                            <!-- HEADER TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-header" style="border-bottom: 1px solid #d6d6d6;padding-bottom: 20px;">
                                <tr>
                                    <td valign="top">
                                        <img src="#{WEB_URL}/images/logo_#{SITE_CD}.png" alt="LOGO" width="106" height="53" />
                                    </td>
                                    <td valign="top" style="text-align: right;">
                                        <img src="#{WEB_URL}/images/logo_IP.png" alt="IPTOWN" width="106" height="53" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- CONTENTS TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-content" style="min-height: 300px;padding-top: 30px;padding-bottom: 30px;">
                                <tr>
                                    <td valign="top">
',
       foot_tmpl_eng = '
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- FOOTER TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-footer" style="background: #ededed;padding-top: 20px;padding-right: 20px;padding-bottom: 18px;padding-left: 20px;text-align: center;color: #333;">
                                <tr>
                                    <td valign="top">
                                        This mail is <b><font color="#0971ce">only sent automatically</font></b> when you sign up for IPTOWN.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
'
 where site_cd = 'CJE';