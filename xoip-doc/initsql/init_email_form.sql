-- truncate all 
truncate table email_form;
truncate table email_form_hist;

-- E0100
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0100', 1, '라이선시 회원 초대 알림', '20210101', '99991231', '[#{SERVICE_NAME}] 회원으로 초청 되었습니다.', '[#{SERVICE_NAME}] You are invited to become a member.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'#{USER_NAME}님 안녕하세요.<br/>
#{LICENSER_COMPANY_NAME} 의 초대로 #{SERVICE_NAME} 회원으로 초청 되었습니다.<br/>
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 로그인 이메일 : #{USER_EMAIL}<br/>
* 초대 일시 : #{PROCESS_DATE}<br/>
<br/>
1. 가이드 다운로드<br>
: #{SERVICE_NAME} 가이드를 다운 받으신 후 가이드에 따라 사용 부탁드립니다.<br/>
<a href="#{MANUAL_LINK}" target="_blank">라이선시 사용 가이드 다운받기</a>
<br/><br/>
2. 주의 사항<br/>
: 로그인 이메일은 라이선시 마다 공용으로 쓸 한개의 메일로만 가입이 가능하며, 비밀번호 역시 공동으로 사용할 수 있게끔 지정 부탁드립니다.<br/>
: 매뉴얼의 경우 #{LICENSER_COMPANY_NAME} 내부의 디자이너가 별도의 메일로 전달 드리고 있습니다.<br/> 
매뉴얼은 CJENM의 중요한 IP자산으로, 저작권 침해로 이어질 수 있는 외부유출 등 취급에 각별한 주의를 부탁드립니다.(스토리, 귀신, 캐릭터 노출 등)<br/>
<br/>
#{SERVICE_NAME}을 이용하시려면, 아래 링크를 클릭하시고 회원 가입을 완료해 주세요.<br/>
<a href="#{LINK}" target="_blank">회원 가입하기</a><br/>
<br/>
감사합니다.<br/>
',
'Hello, #{USER_NAME}.<br/>
You were invited as a member of #{SERVICE_NAME} by invitation from #{LICENSER_COMPANY_NAME}.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Invitation Date : #{PROCESS_DATE}<br/>
<br/>
1. Download guide<br>
: After downloading #{SERVICE_NAME} guide, please use it according to the guide.<br/>
<a href="#{MANUAL_LINK}" target="_blank">Download the Licensee User Guide</a>
<br/><br/>
2. CAUTION<br/>
: Login e-mail can be registered with only one e-mail for common use per licensee. And you should specify a password so that it can also be used jointly.<br/>
: The manual is delivered by e-mail by designer #{LICENSER_COMPANY_NAME}.<br/> 
The manual is an important IP asset of CJENM, so please pay special attention to handling it, such as leaks that may lead to copyright infringement. (Story, ghost, character exposure, etc.)<br/>
<br/>
To use #{SERVICE_NAME}, please click the link below and complete sign up.<br/> 
<a href="#{LINK}" target="_blank">Sign up</a><br/>
<br/>
Thank you.<br/>
');

-- E0101
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0101', 1, '라이선시 회원 가입 환영', '20210101', '99991231', '[#{SERVICE_NAME}] 회원가입이 완료 되었습니다.', '[#{SERVICE_NAME}] Membership registration is complete.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{USER_NAME}님 안녕하세요.<br/>
#{SERVICE_NAME} 가입을 진심으로 환영합니다.<br/>
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 로그인 이메일 : #{USER_EMAIL}<br/>
* 가입일시 : #{PROCESS_DATE}<br/>
<br/>
아래 링크를 클릭하여 #{SERVICE_NAME}에 로그인 하시면 서비스를 정상적으로 이용 하실 수 있습니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{USER_NAME}<br/>
We warmly welcome you to join #{SERVICE_NAME}.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Invitation date: #{PROCESS_DATE}<br/>
<br/>
To use #{SERVICE_NAME}, please click the link below and sign in.<br/>
<a href="#{LINK}" target="_blank">Sign in</a>
<br/>
Thank you.<br/>
'); 

-- E0102
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0102', 1, '라이선시 사용자 등록 완료', '20210101', '99991231', '[#{SERVICE_NAME}] 라이선서 사용자로 등록 되었습니다.', '[#{SERVICE_NAME}] You are registered as a licenser user.', null, '[#{SERVICE_NAME}] You are registered as a licensor user.', 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{USER_NAME}님 안녕하세요.<br/>
#{SERVICE_NAME}의 라이선서 사용자로 등록 되었습니다.<br/>
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 로그인 이메일 : #{USER_EMAIL}<br/>
* 로그인 비밀번호 : #{USER_PASSWORD}<br/>
* 권한 : #{USER_AUTH}<br/>
* 등록일시 : #{PROCESS_DATE}<br/>
<br/>
아래 링크를 클릭하여 #{SERVICE_NAME}에 로그인 후 발급된 비밀번호를 변경 하시고 서비스를 이용 하시기 바랍니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{USER_NAME}.<br/>
You are registered as a licenser user of the service.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Login password : #{USER_PASSWORD}<br/>
* Authority : #{USER_AUTH}<br/>
* Registration date : #{PROCESS_DATE}<br/>
<br/>
You should change a password for security.<br/>
To change a password, please click the link below and sign in<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
'); 

-- E0110
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0110', 1, '비밀번호 재설정 안내', '20210101', '99991231', '[#{SERVICE_NAME}] 비밀번호 재설정 안내', '[#{SERVICE_NAME}] Password reset guide.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{USER_NAME}님 안녕하세요.<br/>
#{SERVICE_NAME}에 로그인 비밀번호 재설정이 요청 되었습니다. 만일 비밀번호 재설정을 요청한 사람이 본인이 아니라면 이 메시지를 무시해 주세요.<br/>
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 로그인 이메일 : #{USER_EMAIL}<br/>
* 요청 일시 : #{PROCESS_DATE}<br/>
<br/>
비밀번호를 재설정 하시려면, 아래 링크를 클릭하세요.<br/>
<a href="#{LINK}" target="_blank">비밀번호 재설정하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{USER_NAME}.<br/> 
#{SERVICE_NAME} has requested a login password reset. If you are not the person who requested the password reset, please ignore this message.<br/> 
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Login e-mail : #{USER_EMAIL}<br/>
* Request Date : #{PROCESS_DATE}<br/>
<br/>
To reset your password, click the link below.<br/>
<a href="#{LINK}" target="_blank">Reset your password</a>
<br/>
Thank you.<br/>
');

-- E0201
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0201', 1, '프로젝트 생성 안내', '20210101', '99991231', '[#{SERVICE_NAME}] #{PROJECT_NAME}이(가) 등록 되었습니다.', '[#{SERVICE_NAME}] #{PROJECT_NAME} is registered.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{USER_NAME}님 안녕하세요.<br/> 
#{SERVICE_NAME}에 성공적으로 프로젝트가 생성되었습니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 프로젝트 코드 : #{PROJECT_CODE}<br/>
* 프로젝트명 : #{PROJECT_NAME}<br/>
* 생성일시 : #{PROCESS_DATE}<br/>
<br/>
#{LICENSER_COMPANY_NAME}의 디자인 검수 담당자가 프로젝트 승인 및 디자인 검수를 진행할 수 있도록 #{SERVICE_NAME}에서 생성한 프로젝트를 승인요청 해주세요.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{USER_NAME}.<br/> 
The project has been successfully created in #{SERVICE_NAME}.<br/> 
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Creation Date : #{PROCESS_DATE}<br/>
<br/>
Please request approval of the project created in #{SERVICE_NAME} so that the design inspector in charge of #{LICENSER_COMPANY_NAME} can approve the project and proceed with the design inspection.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
');

-- E0210
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0210', 1, '프로젝트 승인 요청 안내', '20210101', '99991231', '[#{SERVICE_NAME}] #{PROJECT_NAME}이(가) 승인 요청 되었습니다.', '[#{SERVICE_NAME}] #{PROJECT_NAME} has been requested for approval.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{USER_NAME}님 안녕하세요.<br/> 
#{SERVICE_NAME}에 아래 프로젝트의 승인 요청이 정상적으로 접수 되었습니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 프로젝트 코드 : #{PROJECT_CODE}<br/>
* 프로젝트명 : #{PROJECT_NAME}<br/>
* 승인요청 일시 : #{PROCESS_DATE}<br/>
<br/>
접수된 일정에 따라서 #{LICENSER_COMPANY_NAME}의 디자인 검수 담당자가 프로젝트 및 디자인 검수 신청 내용을 확인 합니다. #{SERVICE_NAME}에서 프로젝트의 진행 상황을 확인하실 수 있습니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/> 
',
'
Hello, #{USER_NAME}<br/> 
The request for approval of the project below has been normally received by #{SERVICE_NAME}.<br/> 
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Approval Date : #{PROCESS_DATE}<br/>
<br/>
According to the received schedule, the design inspector of #{LICENSER_COMPANY_NAME} will check the project and application for design inspection. You can check the progress of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
');

-- E0220
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0220', 1, '프로젝트 승인 안내', '20210101', '99991231', '[#{SERVICE_NAME}] #{PROJECT_NAME}이(가) 승인 되었습니다.', '[#{SERVICE_NAME}] #{PROJECT_NAME} has been approved.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{LICENSEE_COMPANY_NAME}님 안녕하세요.<br/> 
#{SERVICE_NAME}에 등록하신 아래 프로젝트가 승인 되었습니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 프로젝트 코드 : #{PROJECT_CODE}<br/>
* 프로젝트명 : #{PROJECT_NAME}<br/>
* 승인 일시 : #{PROCESS_DATE}<br/>
<br/>
#{LICENSER_COMPANY_NAME}의 디자인 검수 담당자가 디자인 검수 의견을 작성할  예정이며, #{SERVICE_NAME}에서 프로젝트의 진행 상황을 확인하실 수 있습니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{LICENSEE_COMPANY_NAME}.<br/> 
The project below registered in #{SERVICE_NAME} has been approved.<br/> 
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Approval Date : #{PROCESS_DATE}<br/>
<br/>
The design inspector of #{LICENSER_COMPANY_NAME} will write design review comments, and you can check the progress of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
');

-- E0230
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0230', 1, '프로젝트 반려 안내', '20210101', '99991231', '[#{SERVICE_NAME}] #{PROJECT_NAME}이(가) 반려 되었습니다.', '[#{SERVICE_NAME}] #{PROJECT_NAME} has been rejected.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{LICENSEE_COMPANY_NAME}님 안녕하세요.<br/> 
안타깝게도 #{SERVICE_NAME}에 등록하신 아래 프로젝트가 반려 되었음을 알려드립니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 프로젝트 코드 : #{PROJECT_CODE}<br/>
* 프로젝트명 : #{PROJECT_NAME}<br/>
* 반려 일시 : #{PROCESS_DATE}<br/>
<br/>
#{SERVICE_NAME}에서 프로젝트의 반려 내역을 확인하실 수 있습니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{LICENSEE_COMPANY_NAME}.<br/> 
Unfortunately, we would like to inform you that the following project registered to #{SERVICE_NAME} has been rejected.<br/> 
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Return Date : #{PROCESS_DATE}<br/>
<br/>
You can check the project\'s rejection history at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
');

-- E0240
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0240', 1, '수정 프로젝트 안내', '20210101', '99991231', '[#{SERVICE_NAME}] #{PROJECT_NAME}의 디자인 검수 의견이 등록 되었습니다.', '[#{SERVICE_NAME}] The design feedback of the project has been registered.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{LICENSEE_COMPANY_NAME}님 안녕하세요.<br/> 
#{SERVICE_NAME}에 등록하신 아래 프로젝트에 디자인 검수 의견이 등록 되었습니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 프로젝트 코드 : #{PROJECT_CODE}<br/>
* 프로젝트명 : #{PROJECT_NAME}<br/>
* 검수 일시 : #{PROCESS_DATE}<br/>
<br/>
#{SERVICE_NAME}에서 프로젝트의 디자인 검수 의견을 확인하실 수 있습니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{LICENSEE_COMPANY_NAME}.<br/> 
Design review comments have been registered for the following projects registered in #{SERVICE_NAME}.<br/> 
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Inspection Date : #{PROCESS_DATE}<br/>
<br/>
you can check the design review comments of the project in #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/> 
');

-- E0250
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0250', 1, '취소 프로젝트 안내', '20210101', '99991231', '[#{SERVICE_NAME}] #{PROJECT_NAME}이(가) 취소 되었습니다.', '[#{SERVICE_NAME}] #{PROJECT_NAME} has been canceled.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{LICENSEE_COMPANY_NAME}님 안녕하세요.<br/> 
#{SERVICE_NAME}에 등록하신 아래 프로젝트가 취소 되었습니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 프로젝트 코드 : #{PROJECT_CODE}<br/>
* 프로젝트명 : #{PROJECT_NAME}<br/>
* 취소 일시 : #{PROCESS_DATE}<br/>
<br/>
#{SERVICE_NAME}에서 프로젝트의 취소 내역을 확인하실 수 있습니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/>
',
'
Hello, #{LICENSEE_COMPANY_NAME}<br/> 
The project in #{SERVICE_NAME} has been canceled.<br/>
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Cancellation Date : #{PROCESS_DATE}<br/>
<br/>
You can check the cancellation history of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a>
<br/>
Thank you.<br/> 
');

-- E0260
insert into email_form (site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, sbj_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id, cnts_tmpl, cnts_tmpl_eng)
values ('CJE', 'E0260', 1, '완료 프로젝트 안내', '20210101', '99991231', '[#{SERVICE_NAME}] #{PROJECT_NAME}이(가) 완료 되었습니다.', '[#{SERVICE_NAME}] #{PROJECT_NAME} is complete.', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM', 
'
#{LICENSEE_COMPANY_NAME}님 안녕하세요.<br/> 
#{SERVICE_NAME}에 등록하신 아래 프로젝트가 완료 되었습니다.<br/> 
<br/>
* 라이선서 : #{LICENSER_COMPANY_NAME}<br/>
* 소속회사 : #{LICENSEE_COMPANY_NAME}<br/>
* 프로젝트 코드 : #{PROJECT_CODE}<br/>
* 프로젝트명 : #{PROJECT_NAME}<br/>
* 완료 일시 : #{PROCESS_DATE}<br/>
<br/>
#{SERVICE_NAME}에서 프로젝트의 완료 내역을 확인하실 수 있습니다.<br/>
<a href="#{LINK}" target="_blank">로그인 하기</a><br/>
<br/>
감사합니다.<br/> 
',
'
Hello, #{LICENSEE_COMPANY_NAME}.<br/> 
The project in #{SERVICE_NAME} has been completed.<br/> 
<br/>
* Licenser : #{LICENSER_COMPANY_NAME}<br/>
* Company : #{LICENSEE_COMPANY_NAME}<br/>
* Project Code : #{PROJECT_CODE}<br/>
* Project Name : #{PROJECT_NAME}<br/>
* Completion Date : #{PROCESS_DATE}<br/>
<br/>
You can check the completion history of the project at #{SERVICE_NAME}.<br/>
<a href="#{LINK}" target="_blank">Sign in</a><br/>
<br/>
Thank you.<br/>
');

-- email_form_hist
insert into email_form_hist(hist_div_cd, site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, cnts_tmpl, sbj_tmpl_eng, cnts_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id)
select 'HD0100', site_cd, form_cd, form_no, form_nm, aply_str_ymd, aply_end_ymd, sbj_tmpl, cnts_tmpl, sbj_tmpl_eng, cnts_tmpl_eng, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id
  from email_form 
 where 1=1;
 