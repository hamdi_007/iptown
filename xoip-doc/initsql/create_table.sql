-- 사이트
DROP TABLE IF EXISTS site RESTRICT;

-- 회사
DROP TABLE IF EXISTS comp RESTRICT;

-- 사용자
DROP TABLE IF EXISTS usr RESTRICT;

-- IP
DROP TABLE IF EXISTS ip RESTRICT;

-- 계약
DROP TABLE IF EXISTS cntr RESTRICT;

-- 프로젝트
DROP TABLE IF EXISTS proj RESTRICT;

-- 상품유형
DROP TABLE IF EXISTS prod_typ RESTRICT;

-- 디자인검수
DROP TABLE IF EXISTS dsgn_insp RESTRICT;

-- 프로젝트변경이력
DROP TABLE IF EXISTS proj_chg_hist RESTRICT;

-- 상품판매국가
DROP TABLE IF EXISTS prod_sale_ntn RESTRICT;

-- 상품판매채널
DROP TABLE IF EXISTS prod_sale_chnl RESTRICT;

-- 상품지원언어
DROP TABLE IF EXISTS prod_sprt_lang RESTRICT;

-- 상품대상연령
DROP TABLE IF EXISTS prod_tgt_age RESTRICT;

-- 회사파일
DROP TABLE IF EXISTS comp_file RESTRICT;

-- 계약파일
DROP TABLE IF EXISTS cntr_file RESTRICT;

-- 검수파일
DROP TABLE IF EXISTS insp_file RESTRICT;

-- 로그인이력
DROP TABLE IF EXISTS lgn_hist RESTRICT;

-- 비밀번호변경이력
DROP TABLE IF EXISTS pwd_chng_hist RESTRICT;

-- 비밀번호재설정요청
DROP TABLE IF EXISTS pwd_rset_req RESTRICT;

-- 사용자이력
DROP TABLE IF EXISTS usr_hist RESTRICT;

-- 회사이력
DROP TABLE IF EXISTS comp_hist RESTRICT;

-- 상품유형이력
DROP TABLE IF EXISTS prod_typ_hist RESTRICT;

-- IP이력
DROP TABLE IF EXISTS ip_hist RESTRICT;

-- 계약이력
DROP TABLE IF EXISTS cntr_hist RESTRICT;

-- 프로젝트이력
DROP TABLE IF EXISTS proj_hist RESTRICT;

-- 디자인검수이력
DROP TABLE IF EXISTS dsgn_insp_hist RESTRICT;

-- 사이트이력
DROP TABLE IF EXISTS site_hist RESTRICT;

-- 공통코드
DROP TABLE IF EXISTS com_cd RESTRICT;

-- 공통코드이력
DROP TABLE IF EXISTS com_cd_hist RESTRICT;

-- 사이트SMTP
DROP TABLE IF EXISTS site_smtp RESTRICT;

-- 이메일폼
DROP TABLE IF EXISTS email_form RESTRICT;

-- 이메일전송이력
DROP TABLE IF EXISTS email_trns_hist RESTRICT;

-- 이메일폼이력
DROP TABLE IF EXISTS email_form_hist RESTRICT;

-- 사이트SMTP이력
DROP TABLE IF EXISTS site_smtp_hist RESTRICT;

-- 상품대상성별
DROP TABLE IF EXISTS prod_tgt_sex RESTRICT;

-- 의견파일
DROP TABLE IF EXISTS opin_file RESTRICT;

-- 사이트
CREATE TABLE site (
    site_cd VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    site_nm VARCHAR(100)  NOT NULL COMMENT '사이트명', -- 사이트명
    use_yn  CHAR(1)       NOT NULL COMMENT '사용여부', -- 사용여부
    rmk     VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn  CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt  TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt  TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt  TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사이트';

-- 사이트
ALTER TABLE site
    ADD CONSTRAINT PK_site -- 사이트 기본키
        PRIMARY KEY (
            site_cd -- 사이트코드
        );

-- 회사
CREATE TABLE comp (
    site_cd     VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    comp_id     VARCHAR(10)   NOT NULL COMMENT '회사ID', -- 회사ID
    comp_cd     VARCHAR(3)    NOT NULL COMMENT '회사코드', -- 회사코드
    comp_nm     VARCHAR(100)  NOT NULL COMMENT '회사명', -- 회사명
    comp_div_cd VARCHAR(8)    NOT NULL COMMENT '회사구분코드', -- 회사구분코드
    ntn_cd      VARCHAR(8)    NOT NULL COMMENT '국가코드', -- 국가코드
    bizno       VARCHAR(10)   NOT NULL COMMENT '사업자등록번호', -- 사업자등록번호
    addr        VARCHAR(500)  NULL     COMMENT '주소', -- 주소
    dtl_addr    VARCHAR(500)  NULL     COMMENT '상세주소', -- 상세주소
    post_no     VARCHAR(8)    NULL     COMMENT '우편번호', -- 우편번호
    ceo_nm      VARCHAR(100)  NULL     COMMENT '대표자명', -- 대표자명
    rep_telno   VARCHAR(20)   NULL     COMMENT '대표전화번호', -- 대표전화번호
    rmk         VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn      CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt      TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id     VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt      TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id     VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt      TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id     VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '회사';

-- 회사
ALTER TABLE comp
    ADD CONSTRAINT PK_comp -- 회사 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            comp_id  -- 회사ID
        );

-- 사용자
CREATE TABLE usr (
    site_cd       VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    usr_id        VARCHAR(10)   NOT NULL COMMENT '사용자ID', -- 사용자ID
    comp_id       VARCHAR(10)   NOT NULL COMMENT '회사ID', -- 회사ID
    email         VARCHAR(100)  NOT NULL COMMENT '이메일', -- 이메일
    usr_nm        VARCHAR(100)  NOT NULL COMMENT '사용자명', -- 사용자명
    usr_eng_nm    VARCHAR(100)  NULL     COMMENT '사용자영문명', -- 사용자영문명
    pwd           VARCHAR(500)  NULL     COMMENT '비밀번호', -- 비밀번호
    telno         VARCHAR(20)   NULL     COMMENT '전화번호', -- 전화번호
    hnpno         VARCHAR(20)   NULL     COMMENT '핸드폰번호', -- 핸드폰번호
    auth          VARCHAR(50)   NOT NULL COMMENT '권한', -- 권한
    usr_sts_cd    VARCHAR(8)    NOT NULL COMMENT '사용자상태코드', -- 사용자상태코드
    ivt_vrf_val   VARCHAR(50)   NULL     COMMENT '초대검증값', -- 초대검증값
    ivt_dt        TIMESTAMP     NULL     COMMENT '초대일시', -- 초대일시
    ivtr_id       VARCHAR(10)   NULL     COMMENT '초대자ID', -- 초대자ID
    join_dt       TIMESTAMP     NULL     COMMENT '가입일시', -- 가입일시
    wtdr_dt       TIMESTAMP     NULL     COMMENT '탈퇴일시', -- 탈퇴일시
    wtdr_hndlr_id VARCHAR(10)   NULL     COMMENT '탈퇴처리자ID', -- 탈퇴처리자ID
    rmk           VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn        CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt        TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id       VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt        TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id       VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt        TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id       VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사용자';

-- 사용자
ALTER TABLE usr
    ADD CONSTRAINT PK_usr -- 사용자 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            usr_id   -- 사용자ID
        );

-- IP
CREATE TABLE ip (
    site_cd      VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    ip_id        VARCHAR(10)   NOT NULL COMMENT 'IPID', -- IPID
    ip_cd        VARCHAR(3)    NOT NULL COMMENT 'IP코드', -- IP코드
    ip_nm        VARCHAR(100)  NOT NULL COMMENT 'IP명', -- IP명
    ip_eng_nm    VARCHAR(100)  NOT NULL COMMENT 'IP영문명', -- IP영문명
    use_yn       CHAR(1)       NOT NULL COMMENT '사용여부', -- 사용여부
    srt_ordno    INT UNSIGNED  NOT NULL COMMENT '정렬순번', -- 정렬순번
    dtl_desc     VARCHAR(2000) NULL     COMMENT '상세설명', -- 상세설명
    eng_dtl_desc VARCHAR(2000) NULL     COMMENT '영문상세설명', -- 영문상세설명
    del_yn       CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt       TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id      VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt       TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt       TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id      VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'IP';

-- IP
ALTER TABLE ip
    ADD CONSTRAINT PK_ip -- IP 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            ip_id    -- IPID
        );

-- 계약
CREATE TABLE cntr (
    site_cd      VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    cntr_id      VARCHAR(16)   NOT NULL COMMENT '계약ID', -- 계약ID
    cntr_no      VARCHAR(50)   NOT NULL COMMENT '계약번호', -- 계약번호
    cntr_nm      VARCHAR(100)  NULL     COMMENT '계약명', -- 계약명
    comp_id      VARCHAR(10)   NOT NULL COMMENT '회사ID', -- 회사ID
    ip_id        VARCHAR(10)   NOT NULL COMMENT 'IPID', -- IPID
    cntr_ymd     VARCHAR(8)    NULL     COMMENT '계약년월일', -- 계약년월일
    cntr_str_ymd VARCHAR(8)    NULL     COMMENT '계약시작년월일', -- 계약시작년월일
    cntr_end_ymd VARCHAR(8)    NULL     COMMENT '계약종료년월일', -- 계약종료년월일
    cntr_vld_ymd VARCHAR(8)    NULL     COMMENT '계약유효년월일', -- 계약유효년월일
    rmk          VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn       CHAR(1)       NULL     COMMENT '삭제여부', -- 삭제여부
    del_dt       TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id      VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt       TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt       TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id      VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '계약';

-- 계약
ALTER TABLE cntr
    ADD CONSTRAINT PK_cntr -- 계약 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            cntr_id  -- 계약ID
        );

-- 프로젝트
CREATE TABLE proj (
    site_cd          VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd          VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    proj_nm          VARCHAR(100)  NOT NULL COMMENT '프로젝트명', -- 프로젝트명
    cntr_id          VARCHAR(16)   NOT NULL COMMENT '계약ID', -- 계약ID
    top_prod_typ_id  VARCHAR(16)   NOT NULL COMMENT '상위상품유형ID', -- 상위상품유형ID
    prod_typ_id      VARCHAR(16)   NOT NULL COMMENT '상품유형ID', -- 상품유형ID
    proj_stg_cd      VARCHAR(8)    NOT NULL COMMENT '프로젝트단계코드', -- 프로젝트단계코드
    proj_sts_cd      VARCHAR(8)    NOT NULL COMMENT '프로젝트상태코드', -- 프로젝트상태코드
    prod_no          VARCHAR(100)  NULL     COMMENT '상품번호', -- 상품번호
    rls_plan_ym      VARCHAR(6)    NULL     COMMENT '출고계획년월', -- 출고계획년월
    estm_rls_amt     DECIMAL(18,2) NULL     COMMENT '예상출고금액', -- 예상출고금액
    estm_cnsm_amt    DECIMAL(18,2) NULL     COMMENT '예상소비자금액', -- 예상소비자금액
    rls_amt_rt       DECIMAL(13,5) NULL     COMMENT '출고금액율', -- 출고금액율
    cc_cd            VARCHAR(8)    NULL     COMMENT '통화코드', -- 통화코드
    proj_crt_dt      TIMESTAMP     NOT NULL COMMENT '프로젝트생성일시', -- 프로젝트생성일시
    proj_crtr_id     VARCHAR(10)   NOT NULL COMMENT '프로젝트생성자ID', -- 프로젝트생성자ID
    proj_apv_req_dt  TIMESTAMP     NULL     COMMENT '프로젝트승인요청일시', -- 프로젝트승인요청일시
    proj_apv_reqr_id VARCHAR(10)   NULL     COMMENT '프로젝트승인요청자ID', -- 프로젝트승인요청자ID
    proj_apv_dt      TIMESTAMP     NULL     COMMENT '프로젝트승인일시', -- 프로젝트승인일시
    proj_apvr_id     VARCHAR(10)   NULL     COMMENT '프로젝트승인자ID', -- 프로젝트승인자ID
    proj_cmpl_dt     TIMESTAMP     NULL     COMMENT '프로젝트완료일시', -- 프로젝트완료일시
    proj_cmplr_id    VARCHAR(10)   NULL     COMMENT '프로젝트완료자ID', -- 프로젝트완료자ID
    proj_rejt_dt     TIMESTAMP     NULL     COMMENT '프로젝트반려일시', -- 프로젝트반려일시
    proj_rejtr_id    VARCHAR(10)   NULL     COMMENT '프로젝트반려자ID', -- 프로젝트반려자ID
    proj_cncl_dt     TIMESTAMP     NULL     COMMENT '프로젝트취소일시', -- 프로젝트취소일시
    proj_cnclr_id    VARCHAR(10)   NULL     COMMENT '프로젝트취소자ID', -- 프로젝트취소자ID
    rmk              VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn           CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt           TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id          VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt           TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id          VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt           TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id          VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '프로젝트';

-- 프로젝트
ALTER TABLE proj
    ADD CONSTRAINT PK_proj -- 프로젝트 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            proj_cd  -- 프로젝트코드
        );

-- 상품유형
CREATE TABLE prod_typ (
    site_cd         VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    prod_typ_id     VARCHAR(16)   NOT NULL COMMENT '상품유형ID', -- 상품유형ID
    prod_typ_cd     VARCHAR(6)    NOT NULL COMMENT '상품유형코드', -- 상품유형코드
    prod_typ_nm     VARCHAR(100)  NOT NULL COMMENT '상품유형명', -- 상품유형명
    prod_typ_eng_nm VARCHAR(100)  NULL     COMMENT '상품유형영문명', -- 상품유형영문명
    prod_typ_lv     INT UNSIGNED  NOT NULL COMMENT '상품유형레벨', -- 상품유형레벨
    top_site_cd     VARCHAR(3)    NULL     COMMENT '상위사이트코드', -- 상위사이트코드
    top_prod_typ_id VARCHAR(16)   NULL     COMMENT '상위상품유형ID', -- 상위상품유형ID
    use_yn          CHAR(1)       NOT NULL COMMENT '사용여부', -- 사용여부
    srt_ordno       INT UNSIGNED  NOT NULL COMMENT '정렬순번', -- 정렬순번
    dtl_desc        VARCHAR(2000) NULL     COMMENT '상세설명', -- 상세설명
    eng_dtl_desc    VARCHAR(2000) NULL     COMMENT '영문상세설명', -- 영문상세설명
    del_yn          CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt          TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id         VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt          TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id         VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt          TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id         VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '상품유형';

-- 상품유형
ALTER TABLE prod_typ
    ADD CONSTRAINT PK_prod_typ -- 상품유형 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            prod_typ_id  -- 상품유형ID
        );

-- 디자인검수
CREATE TABLE dsgn_insp (
    site_cd         VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd         VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    insp_no         INT UNSIGNED  NOT NULL COMMENT '검수번호', -- 검수번호
    insp_stg_cd     VARCHAR(8)    NOT NULL COMMENT '검수단계코드', -- 검수단계코드
    nxt_insp_stg_cd VARCHAR(8)    NULL     COMMENT '다음검수단계코드', -- 다음검수단계코드
    insp_sts_cd     VARCHAR(8)    NOT NULL COMMENT '검수상태코드', -- 검수상태코드
    insp_appl_dt    TIMESTAMP     NULL     COMMENT '검수신청일시', -- 검수신청일시
    insp_applr_id   VARCHAR(10)   NULL     COMMENT '검수신청자ID', -- 검수신청자ID
    insp_appl_cnts  VARCHAR(2000) NULL     COMMENT '검수신청내용', -- 검수신청내용
    insp_dt         TIMESTAMP     NULL     COMMENT '검수일시', -- 검수일시
    inspr_id        VARCHAR(10)   NULL     COMMENT '검수자ID', -- 검수자ID
    insp_opin_cnts  VARCHAR(2000) NULL     COMMENT '검수의견내용', -- 검수의견내용
    del_yn          CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt          TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id         VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt          TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id         VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt          TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id         VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '디자인검수';

-- 디자인검수
ALTER TABLE dsgn_insp
    ADD CONSTRAINT PK_dsgn_insp -- 디자인검수 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            proj_cd, -- 프로젝트코드
            insp_no  -- 검수번호
        );

-- 프로젝트변경이력
CREATE TABLE proj_chg_hist (
    site_cd          VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd          VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    proj_hist_no     INT UNSIGNED  NOT NULL COMMENT '프로젝트이력번호', -- 프로젝트이력번호
    proj_hist_div_cd VARCHAR(8)    NOT NULL COMMENT '프로젝트이력구분코드', -- 프로젝트이력구분코드
    bef_val          VARCHAR(100)  NULL     COMMENT '이전값', -- 이전값
    aft_val          VARCHAR(100)  NULL     COMMENT '이후값', -- 이후값
    hist_rsn_cnts    VARCHAR(2000) NULL     COMMENT '이력사유내용', -- 이력사유내용
    reg_dt           TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id          VARCHAR(10)   NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '프로젝트변경이력';

-- 프로젝트변경이력
ALTER TABLE proj_chg_hist
    ADD CONSTRAINT PK_proj_chg_hist -- 프로젝트변경이력 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            proj_hist_no  -- 프로젝트이력번호
        );

-- 상품판매국가
CREATE TABLE prod_sale_ntn (
    site_cd VARCHAR(3)  NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd VARCHAR(22) NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    ntn_cd  VARCHAR(8)  NOT NULL COMMENT '국가코드', -- 국가코드
    reg_dt  TIMESTAMP   NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id VARCHAR(10) NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '상품판매국가';

-- 상품판매국가
ALTER TABLE prod_sale_ntn
    ADD CONSTRAINT PK_prod_sale_ntn -- 상품판매국가 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            proj_cd, -- 프로젝트코드
            ntn_cd   -- 국가코드
        );

-- 상품판매채널
CREATE TABLE prod_sale_chnl (
    site_cd      VARCHAR(3)  NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd      VARCHAR(22) NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    sale_chnl_cd VARCHAR(8)  NOT NULL COMMENT '판매채널코드', -- 판매채널코드
    reg_dt       TIMESTAMP   NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10) NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '상품판매채널';

-- 상품판매채널
ALTER TABLE prod_sale_chnl
    ADD CONSTRAINT PK_prod_sale_chnl -- 상품판매채널 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            sale_chnl_cd  -- 판매채널코드
        );

-- 상품지원언어
CREATE TABLE prod_sprt_lang (
    site_cd VARCHAR(3)  NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd VARCHAR(22) NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    lang_cd VARCHAR(8)  NOT NULL COMMENT '언어코드', -- 언어코드
    reg_dt  TIMESTAMP   NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id VARCHAR(10) NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '상품지원언어';

-- 상품지원언어
ALTER TABLE prod_sprt_lang
    ADD CONSTRAINT PK_prod_sprt_lang -- 상품지원언어 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            proj_cd, -- 프로젝트코드
            lang_cd  -- 언어코드
        );

-- 상품대상연령
CREATE TABLE prod_tgt_age (
    site_cd VARCHAR(3)  NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd VARCHAR(22) NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    age_cd  VARCHAR(8)  NOT NULL COMMENT '연령코드', -- 연령코드
    reg_dt  TIMESTAMP   NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id VARCHAR(10) NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '상품대상연령';

-- 상품대상연령
ALTER TABLE prod_tgt_age
    ADD CONSTRAINT PK_prod_tgt_age -- 상품대상연령 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            proj_cd, -- 프로젝트코드
            age_cd   -- 연령코드
        );

-- 회사파일
CREATE TABLE comp_file (
    site_cd          VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    comp_id          VARCHAR(10)     NOT NULL COMMENT '회사ID', -- 회사ID
    comp_file_no     INT UNSIGNED    NOT NULL COMMENT '회사파일번호', -- 회사파일번호
    comp_file_div_cd VARCHAR(8)      NOT NULL COMMENT '회사파일구분코드', -- 회사파일구분코드
    org_file_nm      VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm      VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm      VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    file_path        VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ    VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size        BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm      VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno        INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn           CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt           TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id          VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt           TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id          VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt           TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id          VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '회사파일';

-- 회사파일
ALTER TABLE comp_file
    ADD CONSTRAINT PK_comp_file -- 회사파일 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            comp_id,      -- 회사ID
            comp_file_no  -- 회사파일번호
        );

-- 계약파일
CREATE TABLE cntr_file (
    site_cd          VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    cntr_id          VARCHAR(16)     NOT NULL COMMENT '계약ID', -- 계약ID
    cntr_file_no     INT UNSIGNED    NOT NULL COMMENT '계약파일번호', -- 계약파일번호
    cntr_file_div_cd VARCHAR(8)      NOT NULL COMMENT '계약파일구분코드', -- 계약파일구분코드
    org_file_nm      VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm      VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm      VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    file_path        VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ    VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size        BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm      VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno        INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn           CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt           TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id          VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt           TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id          VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt           TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id          VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '계약파일';

-- 계약파일
ALTER TABLE cntr_file
    ADD CONSTRAINT PK_cntr_file -- 계약파일 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            cntr_id,      -- 계약ID
            cntr_file_no  -- 계약파일번호
        );

-- 검수파일
CREATE TABLE insp_file (
    site_cd          VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd          VARCHAR(22)     NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    insp_no          INT UNSIGNED    NOT NULL COMMENT '검수번호', -- 검수번호
    insp_file_no     INT UNSIGNED    NOT NULL COMMENT '검수파일번호', -- 검수파일번호
    insp_file_div_cd VARCHAR(8)      NOT NULL COMMENT '검수파일구분코드', -- 검수파일구분코드
    org_file_nm      VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm      VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm      VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    tumb_sm_file_nm  VARCHAR(300)    NULL     COMMENT '썸네일소파일명', -- 썸네일소파일명
    tumb_md_file_nm  VARCHAR(300)    NULL     COMMENT '썸네일중파일명', -- 썸네일중파일명
    file_path        VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ    VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size        BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm      VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno        INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    insp_appl_cnts   VARCHAR(2000)   NULL     COMMENT '검수신청내용', -- 검수신청내용
    insp_opin_cnts   VARCHAR(2000)   NULL     COMMENT '검수의견내용', -- 검수의견내용
    del_yn           CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt           TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id          VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt           TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id          VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt           TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id          VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '검수파일';

-- 검수파일
ALTER TABLE insp_file
    ADD CONSTRAINT PK_insp_file -- 검수파일 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            insp_no,      -- 검수번호
            insp_file_no  -- 검수파일번호
        );

-- 로그인이력
CREATE TABLE lgn_hist (
    site_cd     VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    usr_id      VARCHAR(10)  NOT NULL COMMENT '사용자ID', -- 사용자ID
    lgn_hist_no INT UNSIGNED NOT NULL COMMENT '로그인이력번호', -- 로그인이력번호
    lgn_dt      TIMESTAMP    NOT NULL COMMENT '로그인일시', -- 로그인일시
    lgn_div_cd  VARCHAR(8)   NOT NULL COMMENT '로그인구분코드', -- 로그인구분코드
    ipc_addr    VARCHAR(50)  NULL     COMMENT '인터넷프로토콜주소', -- 인터넷프로토콜주소
    reg_dt      TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id     VARCHAR(10)  NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '로그인이력';

-- 로그인이력
ALTER TABLE lgn_hist
    ADD CONSTRAINT PK_lgn_hist -- 로그인이력 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            usr_id,      -- 사용자ID
            lgn_hist_no  -- 로그인이력번호
        );

-- 비밀번호변경이력
CREATE TABLE pwd_chng_hist (
    site_cd          VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    usr_id           VARCHAR(10)  NOT NULL COMMENT '사용자ID', -- 사용자ID
    pwd_chng_no      INT UNSIGNED NOT NULL COMMENT '비밀번호변경번호', -- 비밀번호변경번호
    pwd_chng_dt      TIMESTAMP    NOT NULL COMMENT '비밀번호변경일시', -- 비밀번호변경일시
    pwd_chng_mthd_cd VARCHAR(8)   NOT NULL COMMENT '비밀번호변경방법코드', -- 비밀번호변경방법코드
    reg_dt           TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id          VARCHAR(10)  NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '비밀번호변경이력';

-- 비밀번호변경이력
ALTER TABLE pwd_chng_hist
    ADD CONSTRAINT PK_pwd_chng_hist -- 비밀번호변경이력 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            usr_id,      -- 사용자ID
            pwd_chng_no  -- 비밀번호변경번호
        );

-- 비밀번호재설정요청
CREATE TABLE pwd_rset_req (
    site_cd      VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    usr_id       VARCHAR(10)  NOT NULL COMMENT '사용자ID', -- 사용자ID
    rset_req_no  INT UNSIGNED NOT NULL COMMENT '재설정요청번호', -- 재설정요청번호
    rset_req_dt  TIMESTAMP    NOT NULL COMMENT '재설정요청일시', -- 재설정요청일시
    rset_vrf_val VARCHAR(50)  NOT NULL COMMENT '재설정검증값', -- 재설정검증값
    rset_cmpl_yn CHAR(1)      NOT NULL COMMENT '재설정완료여부', -- 재설정완료여부
    rset_cmpl_dt TIMESTAMP    NULL     COMMENT '재설정완료일시', -- 재설정완료일시
    reg_dt       TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10)  NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt       TIMESTAMP    NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id      VARCHAR(10)  NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '비밀번호재설정요청';

-- 비밀번호재설정요청
ALTER TABLE pwd_rset_req
    ADD CONSTRAINT PK_pwd_rset_req -- 비밀번호재설정요청 기본키
        PRIMARY KEY (
            site_cd,     -- 사이트코드
            usr_id,      -- 사용자ID
            rset_req_no  -- 재설정요청번호
        );

-- 사용자이력
CREATE TABLE usr_hist (
    usr_hist_seqno INT UNSIGNED  NOT NULL COMMENT '사용자이력일련번호', -- 사용자이력일련번호
    hist_div_cd    VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd        VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    usr_id         VARCHAR(10)   NOT NULL COMMENT '사용자ID', -- 사용자ID
    comp_id        VARCHAR(10)   NOT NULL COMMENT '회사ID', -- 회사ID
    email          VARCHAR(100)  NOT NULL COMMENT '이메일', -- 이메일
    usr_nm         VARCHAR(100)  NOT NULL COMMENT '사용자명', -- 사용자명
    usr_eng_nm     VARCHAR(100)  NULL     COMMENT '사용자영문명', -- 사용자영문명
    pwd            VARCHAR(500)  NULL     COMMENT '비밀번호', -- 비밀번호
    telno          VARCHAR(20)   NULL     COMMENT '전화번호', -- 전화번호
    hnpno          VARCHAR(20)   NULL     COMMENT '핸드폰번호', -- 핸드폰번호
    auth           VARCHAR(50)   NOT NULL COMMENT '권한', -- 권한
    usr_sts_cd     VARCHAR(8)    NOT NULL COMMENT '사용자상태코드', -- 사용자상태코드
    ivt_vrf_val    VARCHAR(50)   NULL     COMMENT '초대검증값', -- 초대검증값
    ivt_dt         TIMESTAMP     NULL     COMMENT '초대일시', -- 초대일시
    ivtr_id        VARCHAR(10)   NULL     COMMENT '초대자ID', -- 초대자ID
    join_dt        TIMESTAMP     NULL     COMMENT '가입일시', -- 가입일시
    wtdr_dt        TIMESTAMP     NULL     COMMENT '탈퇴일시', -- 탈퇴일시
    wtdr_hndlr_id  VARCHAR(10)   NULL     COMMENT '탈퇴처리자ID', -- 탈퇴처리자ID
    rmk            VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn         CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt         TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id        VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt         TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id        VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt         TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id        VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사용자이력';

-- 사용자이력
ALTER TABLE usr_hist
    ADD CONSTRAINT PK_usr_hist -- 사용자이력 기본키
        PRIMARY KEY (
            usr_hist_seqno -- 사용자이력일련번호
        );

ALTER TABLE usr_hist
    MODIFY COLUMN usr_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '사용자이력일련번호';

ALTER TABLE usr_hist
    AUTO_INCREMENT = 1;

-- 회사이력
CREATE TABLE comp_hist (
    comp_hist_seqno INT UNSIGNED  NOT NULL COMMENT '회사이력일련번호', -- 회사이력일련번호
    hist_div_cd     VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd         VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    comp_id         VARCHAR(10)   NOT NULL COMMENT '회사ID', -- 회사ID
    comp_cd         VARCHAR(3)    NOT NULL COMMENT '회사코드', -- 회사코드
    comp_nm         VARCHAR(100)  NOT NULL COMMENT '회사명', -- 회사명
    comp_div_cd     VARCHAR(8)    NOT NULL COMMENT '회사구분코드', -- 회사구분코드
    ntn_cd          VARCHAR(8)    NOT NULL COMMENT '국가코드', -- 국가코드
    bizno           VARCHAR(10)   NULL     COMMENT '사업자등록번호', -- 사업자등록번호
    addr            VARCHAR(500)  NULL     COMMENT '주소', -- 주소
    dtl_addr        VARCHAR(500)  NULL     COMMENT '상세주소', -- 상세주소
    post_no         VARCHAR(8)    NULL     COMMENT '우편번호', -- 우편번호
    ceo_nm          VARCHAR(100)  NULL     COMMENT '대표자명', -- 대표자명
    rep_telno       VARCHAR(20)   NULL     COMMENT '대표전화번호', -- 대표전화번호
    rmk             VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn          CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt          TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id         VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt          TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id         VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt          TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id         VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '회사이력';

-- 회사이력
ALTER TABLE comp_hist
    ADD CONSTRAINT PK_comp_hist -- 회사이력 기본키
        PRIMARY KEY (
            comp_hist_seqno -- 회사이력일련번호
        );

ALTER TABLE comp_hist
    MODIFY COLUMN comp_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '회사이력일련번호';

ALTER TABLE comp_hist
    AUTO_INCREMENT = 1;

-- 상품유형이력
CREATE TABLE prod_typ_hist (
    prod_typ_hist_seqno INT UNSIGNED  NOT NULL COMMENT '상품유형이력일련번호', -- 상품유형이력일련번호
    hist_div_cd         VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd             VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    prod_typ_id         VARCHAR(16)   NOT NULL COMMENT '상품유형ID', -- 상품유형ID
    prod_typ_cd         VARCHAR(6)    NOT NULL COMMENT '상품유형코드', -- 상품유형코드
    prod_typ_nm         VARCHAR(100)  NOT NULL COMMENT '상품유형명', -- 상품유형명
    prod_typ_eng_nm     VARCHAR(100)  NOT NULL COMMENT '상품유형영문명', -- 상품유형영문명
    prod_typ_lv         INT UNSIGNED  NOT NULL COMMENT '상품유형레벨', -- 상품유형레벨
    top_site_cd         VARCHAR(3)    NULL     COMMENT '상위사이트코드', -- 상위사이트코드
    top_prod_typ_id     VARCHAR(16)   NULL     COMMENT '상위상품유형ID', -- 상위상품유형ID
    use_yn              CHAR(1)       NOT NULL COMMENT '사용여부', -- 사용여부
    srt_ordno           INT UNSIGNED  NOT NULL COMMENT '정렬순번', -- 정렬순번
    dtl_desc            VARCHAR(2000) NULL     COMMENT '상세설명', -- 상세설명
    eng_dtl_desc        VARCHAR(2000) NULL     COMMENT '영문상세설명', -- 영문상세설명
    del_yn              CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt              TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id             VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt              TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id             VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt              TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id             VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '상품유형이력';

-- 상품유형이력
ALTER TABLE prod_typ_hist
    ADD CONSTRAINT PK_prod_typ_hist -- 상품유형이력 기본키
        PRIMARY KEY (
            prod_typ_hist_seqno -- 상품유형이력일련번호
        );

ALTER TABLE prod_typ_hist
    MODIFY COLUMN prod_typ_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '상품유형이력일련번호';

ALTER TABLE prod_typ_hist
    AUTO_INCREMENT = 1;

-- IP이력
CREATE TABLE ip_hist (
    ip_hist_seqno INT UNSIGNED  NOT NULL COMMENT 'IP이력일련번호', -- IP이력일련번호
    hist_div_cd   VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd       VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    ip_id         VARCHAR(10)   NOT NULL COMMENT 'IPID', -- IPID
    ip_cd         VARCHAR(3)    NOT NULL COMMENT 'IP코드', -- IP코드
    ip_nm         VARCHAR(100)  NOT NULL COMMENT 'IP명', -- IP명
    ip_eng_nm     VARCHAR(100)  NOT NULL COMMENT 'IP영문명', -- IP영문명
    use_yn        CHAR(1)       NOT NULL COMMENT '사용여부', -- 사용여부
    srt_ordno     INT UNSIGNED  NOT NULL COMMENT '정렬순번', -- 정렬순번
    dtl_desc      VARCHAR(2000) NULL     COMMENT '상세설명', -- 상세설명
    eng_dtl_desc  VARCHAR(2000) NULL     COMMENT '영문상세설명', -- 영문상세설명
    del_yn        CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt        TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id       VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt        TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id       VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt        TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id       VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT 'IP이력';

-- IP이력
ALTER TABLE ip_hist
    ADD CONSTRAINT PK_ip_hist -- IP이력 기본키
        PRIMARY KEY (
            ip_hist_seqno -- IP이력일련번호
        );

ALTER TABLE ip_hist
    MODIFY COLUMN ip_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'IP이력일련번호';

ALTER TABLE ip_hist
    AUTO_INCREMENT = 1;

-- 계약이력
CREATE TABLE cntr_hist (
    cntr_hist_seqno INT UNSIGNED  NOT NULL COMMENT '계약이력일련번호', -- 계약이력일련번호
    hist_div_cd     VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd         VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    cntr_id         VARCHAR(16)   NOT NULL COMMENT '계약ID', -- 계약ID
    cntr_no         VARCHAR(50)   NOT NULL COMMENT '계약번호', -- 계약번호
    cntr_nm         VARCHAR(100)  NULL     COMMENT '계약명', -- 계약명
    comp_id         VARCHAR(10)   NOT NULL COMMENT '회사ID', -- 회사ID
    ip_id           VARCHAR(10)   NOT NULL COMMENT 'IPID', -- IPID
    cntr_ymd        VARCHAR(8)    NULL     COMMENT '계약년월일', -- 계약년월일
    cntr_str_ymd    VARCHAR(8)    NULL     COMMENT '계약시작년월일', -- 계약시작년월일
    cntr_end_ymd    VARCHAR(8)    NULL     COMMENT '계약종료년월일', -- 계약종료년월일
    cntr_vld_ymd    VARCHAR(8)    NULL     COMMENT '계약유효년월일', -- 계약유효년월일
    rmk             VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn          CHAR(1)       NULL     COMMENT '삭제여부', -- 삭제여부
    del_dt          TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id         VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt          TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id         VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt          TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id         VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '계약이력';

-- 계약이력
ALTER TABLE cntr_hist
    ADD CONSTRAINT PK_cntr_hist -- 계약이력 기본키
        PRIMARY KEY (
            cntr_hist_seqno -- 계약이력일련번호
        );

ALTER TABLE cntr_hist
    MODIFY COLUMN cntr_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '계약이력일련번호';

ALTER TABLE cntr_hist
    AUTO_INCREMENT = 1;

-- 프로젝트이력
CREATE TABLE proj_hist (
    proj_hist_seqno  INT UNSIGNED  NOT NULL COMMENT '프로젝트이력일련번호', -- 프로젝트이력일련번호
    hist_div_cd      VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd          VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd          VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    proj_nm          VARCHAR(100)  NOT NULL COMMENT '프로젝트명', -- 프로젝트명
    cntr_id          VARCHAR(16)   NOT NULL COMMENT '계약ID', -- 계약ID
    top_prod_typ_id  VARCHAR(16)   NOT NULL COMMENT '상위상품유형ID', -- 상위상품유형ID
    prod_typ_id      VARCHAR(16)   NOT NULL COMMENT '상품유형ID', -- 상품유형ID
    proj_stg_cd      VARCHAR(8)    NOT NULL COMMENT '프로젝트단계코드', -- 프로젝트단계코드
    proj_sts_cd      VARCHAR(8)    NOT NULL COMMENT '프로젝트상태코드', -- 프로젝트상태코드
    prod_no          VARCHAR(100)  NULL     COMMENT '상품번호', -- 상품번호
    rls_plan_ym      VARCHAR(6)    NULL     COMMENT '출고계획년월', -- 출고계획년월
    estm_rls_amt     DECIMAL(18,2) NULL     COMMENT '예상출고금액', -- 예상출고금액
    estm_cnsm_amt    DECIMAL(18,2) NULL     COMMENT '예상소비자금액', -- 예상소비자금액
    rls_amt_rt       DECIMAL(13,5) NULL     COMMENT '출고금액율', -- 출고금액율
    cc_cd            VARCHAR(8)    NULL     COMMENT '통화코드', -- 통화코드
    proj_crt_dt      TIMESTAMP     NOT NULL COMMENT '프로젝트생성일시', -- 프로젝트생성일시
    proj_crtr_id     VARCHAR(10)   NOT NULL COMMENT '프로젝트생성자ID', -- 프로젝트생성자ID
    proj_apv_req_dt  TIMESTAMP     NULL     COMMENT '프로젝트승인요청일시', -- 프로젝트승인요청일시
    proj_apv_reqr_id VARCHAR(10)   NULL     COMMENT '프로젝트승인요청자ID', -- 프로젝트승인요청자ID
    proj_apv_dt      TIMESTAMP     NULL     COMMENT '프로젝트승인일시', -- 프로젝트승인일시
    proj_apvr_id     VARCHAR(10)   NULL     COMMENT '프로젝트승인자ID', -- 프로젝트승인자ID
    proj_cmpl_dt     TIMESTAMP     NULL     COMMENT '프로젝트완료일시', -- 프로젝트완료일시
    proj_cmplr_id    VARCHAR(10)   NULL     COMMENT '프로젝트완료자ID', -- 프로젝트완료자ID
    proj_rejt_dt     TIMESTAMP     NULL     COMMENT '프로젝트반려일시', -- 프로젝트반려일시
    proj_rejtr_id    VARCHAR(10)   NULL     COMMENT '프로젝트반려자ID', -- 프로젝트반려자ID
    proj_cncl_dt     TIMESTAMP     NULL     COMMENT '프로젝트취소일시', -- 프로젝트취소일시
    proj_cnclr_id    VARCHAR(10)   NULL     COMMENT '프로젝트취소자ID', -- 프로젝트취소자ID
    rmk              VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn           CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt           TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id          VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt           TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id          VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt           TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id          VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '프로젝트이력';

-- 프로젝트이력
ALTER TABLE proj_hist
    ADD CONSTRAINT PK_proj_hist -- 프로젝트이력 기본키
        PRIMARY KEY (
            proj_hist_seqno -- 프로젝트이력일련번호
        );

ALTER TABLE proj_hist
    MODIFY COLUMN proj_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '프로젝트이력일련번호';

ALTER TABLE proj_hist
    AUTO_INCREMENT = 1;

-- 디자인검수이력
CREATE TABLE dsgn_insp_hist (
    insp_hist_seqno INT UNSIGNED  NOT NULL COMMENT '검수이력일련번호', -- 검수이력일련번호
    hist_div_cd     VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd         VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd         VARCHAR(22)   NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    insp_no         INT UNSIGNED  NOT NULL COMMENT '검수번호', -- 검수번호
    insp_stg_cd     VARCHAR(8)    NOT NULL COMMENT '검수단계코드', -- 검수단계코드
    nxt_insp_stg_cd VARCHAR(8)    NULL     COMMENT '다음검수단계코드', -- 다음검수단계코드
    insp_sts_cd     VARCHAR(8)    NOT NULL COMMENT '검수상태코드', -- 검수상태코드
    insp_appl_dt    TIMESTAMP     NULL     COMMENT '검수신청일시', -- 검수신청일시
    insp_applr_id   VARCHAR(10)   NULL     COMMENT '검수신청자ID', -- 검수신청자ID
    insp_appl_cnts  VARCHAR(2000) NULL     COMMENT '검수신청내용', -- 검수신청내용
    insp_dt         TIMESTAMP     NULL     COMMENT '검수일시', -- 검수일시
    inspr_id        VARCHAR(10)   NULL     COMMENT '검수자ID', -- 검수자ID
    insp_opin_cnts  VARCHAR(2000) NULL     COMMENT '검수의견내용', -- 검수의견내용
    del_yn          CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt          TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id         VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt          TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id         VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt          TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id         VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '디자인검수이력';

-- 디자인검수이력
ALTER TABLE dsgn_insp_hist
    ADD CONSTRAINT PK_dsgn_insp_hist -- 디자인검수이력 기본키
        PRIMARY KEY (
            insp_hist_seqno -- 검수이력일련번호
        );

ALTER TABLE dsgn_insp_hist
    MODIFY COLUMN insp_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '검수이력일련번호';

ALTER TABLE dsgn_insp_hist
    AUTO_INCREMENT = 1;

-- 사이트이력
CREATE TABLE site_hist (
    site_hist_seqno INT UNSIGNED  NOT NULL COMMENT '사이트이력일련번호', -- 사이트이력일련번호
    hist_div_cd     VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd         VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    site_nm         VARCHAR(100)  NOT NULL COMMENT '사이트명', -- 사이트명
    use_yn          CHAR(1)       NOT NULL COMMENT '사용여부', -- 사용여부
    rmk             VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn          CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt          TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id         VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt          TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id         VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt          TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id         VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사이트이력';

-- 사이트이력
ALTER TABLE site_hist
    ADD CONSTRAINT PK_site_hist -- 사이트이력 기본키
        PRIMARY KEY (
            site_hist_seqno -- 사이트이력일련번호
        );

ALTER TABLE site_hist
    MODIFY COLUMN site_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '사이트이력일련번호';

ALTER TABLE site_hist
    AUTO_INCREMENT = 1;

-- 공통코드
CREATE TABLE com_cd (
    site_cd       VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    com_cd        VARCHAR(8)   NOT NULL COMMENT '공통코드', -- 공통코드
    com_cd_nm     VARCHAR(100) NOT NULL COMMENT '공통코드명', -- 공통코드명
    com_cd_eng_nm VARCHAR(100) NOT NULL COMMENT '공통코드영문명', -- 공통코드영문명
    top_site_cd   VARCHAR(3)   NULL     COMMENT '상위사이트코드', -- 상위사이트코드
    top_com_cd    VARCHAR(8)   NULL     COMMENT '상위공통코드', -- 상위공통코드
    cd_val_1      VARCHAR(100) NULL     COMMENT '코드값1', -- 코드값1
    cd_val_2      VARCHAR(100) NULL     COMMENT '코드값2', -- 코드값2
    cd_val_3      VARCHAR(100) NULL     COMMENT '코드값3', -- 코드값3
    cd_val_4      VARCHAR(100) NULL     COMMENT '코드값4', -- 코드값4
    cd_val_5      VARCHAR(100) NULL     COMMENT '코드값5', -- 코드값5
    use_yn        CHAR(1)      NOT NULL COMMENT '사용여부', -- 사용여부
    srt_ordno     INT UNSIGNED NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn        CHAR(1)      NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt        TIMESTAMP    NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id       VARCHAR(10)  NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt        TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id       VARCHAR(10)  NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt        TIMESTAMP    NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id       VARCHAR(10)  NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공통코드';

-- 공통코드
ALTER TABLE com_cd
    ADD CONSTRAINT PK_com_cd -- 공통코드 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            com_cd   -- 공통코드
        );

-- 공통코드이력
CREATE TABLE com_cd_hist (
    com_cd_hist_seqno INT UNSIGNED NOT NULL COMMENT '공통코드이력일련번호', -- 공통코드이력일련번호
    hist_div_cd       VARCHAR(8)   NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd           VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    com_cd            VARCHAR(8)   NOT NULL COMMENT '공통코드', -- 공통코드
    com_cd_nm         VARCHAR(100) NOT NULL COMMENT '공통코드명', -- 공통코드명
    com_cd_eng_nm     VARCHAR(100) NOT NULL COMMENT '공통코드영문명', -- 공통코드영문명
    top_site_cd       VARCHAR(3)   NULL     COMMENT '상위사이트코드', -- 상위사이트코드
    top_com_cd        VARCHAR(8)   NULL     COMMENT '상위공통코드', -- 상위공통코드
    cd_val_1          VARCHAR(100) NULL     COMMENT '코드값1', -- 코드값1
    cd_val_2          VARCHAR(100) NULL     COMMENT '코드값2', -- 코드값2
    cd_val_3          VARCHAR(100) NULL     COMMENT '코드값3', -- 코드값3
    cd_val_4          VARCHAR(100) NULL     COMMENT '코드값4', -- 코드값4
    cd_val_5          VARCHAR(100) NULL     COMMENT '코드값5', -- 코드값5
    use_yn            CHAR(1)      NOT NULL COMMENT '사용여부', -- 사용여부
    srt_ordno         INT UNSIGNED NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn            CHAR(1)      NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt            TIMESTAMP    NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id           VARCHAR(10)  NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt            TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id           VARCHAR(10)  NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt            TIMESTAMP    NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id           VARCHAR(10)  NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '공통코드이력';

-- 공통코드이력
ALTER TABLE com_cd_hist
    ADD CONSTRAINT PK_com_cd_hist -- 공통코드이력 기본키
        PRIMARY KEY (
            com_cd_hist_seqno -- 공통코드이력일련번호
        );

ALTER TABLE com_cd_hist
    MODIFY COLUMN com_cd_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '공통코드이력일련번호';

ALTER TABLE com_cd_hist
    AUTO_INCREMENT = 1;

-- 사이트SMTP
CREATE TABLE site_smtp (
    site_cd      VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    smtp_prtc    VARCHAR(10)  NOT NULL COMMENT 'SMTP프로토콜', -- SMTP프로토콜
    svr_host     VARCHAR(100) NOT NULL COMMENT '서버호스트', -- 서버호스트
    port_no      INT UNSIGNED NOT NULL COMMENT '포트번호', -- 포트번호
    smtp_usr_id  VARCHAR(100) NOT NULL COMMENT 'SMTP사용자ID', -- SMTP사용자ID
    smtp_usr_pwd VARCHAR(500) NOT NULL COMMENT 'SMTP사용자비밀번호', -- SMTP사용자비밀번호
    trnsr_email  VARCHAR(100) NOT NULL COMMENT '전송자이메일', -- 전송자이메일
    trnsr_nm     VARCHAR(100) NOT NULL COMMENT '전송자명', -- 전송자명
    smtp_use_yn  CHAR(1)      NOT NULL COMMENT 'SMTP사용여부', -- SMTP사용여부
    atrz_yn      CHAR(1)      NOT NULL COMMENT '인증여부', -- 인증여부
    tls_use_yn   CHAR(1)      NOT NULL COMMENT 'TLS사용여부', -- TLS사용여부
    ssl_use_yn   CHAR(1)      NOT NULL COMMENT 'SSL사용여부', -- SSL사용여부
    head_tmpl    TEXT         NULL     COMMENT '상단템플릿', -- 상단템플릿
    foot_tmpl    TEXT         NULL     COMMENT '하단템플릿', -- 하단템플릿
    reg_dt       TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10)  NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt       TIMESTAMP    NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id      VARCHAR(10)  NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사이트SMTP';

-- 사이트SMTP
ALTER TABLE site_smtp
    ADD CONSTRAINT PK_site_smtp -- 사이트SMTP 기본키
        PRIMARY KEY (
            site_cd -- 사이트코드
        );

-- 이메일폼
CREATE TABLE email_form (
    site_cd      VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    form_cd      VARCHAR(5)    NOT NULL COMMENT '폼코드', -- 폼코드
    form_no      INT UNSIGNED  NOT NULL COMMENT '폼번호', -- 폼번호
    form_nm      VARCHAR(100)  NOT NULL COMMENT '폼명', -- 폼명
    aply_str_ymd VARCHAR(8)    NOT NULL COMMENT '적용시작년월일', -- 적용시작년월일
    aply_end_ymd VARCHAR(8)    NOT NULL COMMENT '적용종료년월일', -- 적용종료년월일
    sbj_tmpl     VARCHAR(500)  NOT NULL COMMENT '제목템플릿', -- 제목템플릿
    cnts_tmpl    TEXT          NOT NULL COMMENT '내용템플릿', -- 내용템플릿
    rmk          VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn       CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt       TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id      VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt       TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id      VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt       TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id      VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '이메일폼';

-- 이메일폼
ALTER TABLE email_form
    ADD CONSTRAINT PK_email_form -- 이메일폼 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            form_cd, -- 폼코드
            form_no  -- 폼번호
        );

-- 이메일전송이력
CREATE TABLE email_trns_hist (
    email_trns_hist_seqno INT UNSIGNED  NOT NULL COMMENT '이메일전송이력일련번호', -- 이메일전송이력일련번호
    site_cd               VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    form_cd               VARCHAR(5)    NOT NULL COMMENT '폼코드', -- 폼코드
    form_no               INT UNSIGNED  NOT NULL COMMENT '폼번호', -- 폼번호
    trns_sts_cd           VARCHAR(8)    NOT NULL COMMENT '전송상태코드', -- 전송상태코드
    trns_req_dt           TIMESTAMP     NOT NULL COMMENT '전송요청일시', -- 전송요청일시
    trns_end_dt           TIMESTAMP     NULL     COMMENT '전송종료일시', -- 전송종료일시
    trns_req_comp_id      VARCHAR(10)   NULL     COMMENT '전송요청회사ID', -- 전송요청회사ID
    trns_reqr_id          VARCHAR(10)   NULL     COMMENT '전송요청자ID', -- 전송요청자ID
    trnsr_email           VARCHAR(100)  NOT NULL COMMENT '전송자이메일', -- 전송자이메일
    trnsr_nm              VARCHAR(100)  NOT NULL COMMENT '전송자명', -- 전송자명
    ttl_recp              TEXT          NOT NULL COMMENT '전체수신자', -- 전체수신자
    tmpl_var_ftxt         TEXT          NULL     COMMENT '템플릿변수전문', -- 템플릿변수전문
    err_cnts              VARCHAR(2000) NULL     COMMENT '오류내용', -- 오류내용
    reg_dt                TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id               VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt                TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id               VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '이메일전송이력';

-- 이메일전송이력
ALTER TABLE email_trns_hist
    ADD CONSTRAINT PK_email_trns_hist -- 이메일전송이력 기본키
        PRIMARY KEY (
            email_trns_hist_seqno -- 이메일전송이력일련번호
        );

ALTER TABLE email_trns_hist
    MODIFY COLUMN email_trns_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '이메일전송이력일련번호';

ALTER TABLE email_trns_hist
    AUTO_INCREMENT = 1;

-- 이메일폼이력
CREATE TABLE email_form_hist (
    email_form_hist_seqno INT UNSIGNED  NOT NULL COMMENT '이메일폼이력일련번호', -- 이메일폼이력일련번호
    hist_div_cd           VARCHAR(8)    NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd               VARCHAR(3)    NOT NULL COMMENT '사이트코드', -- 사이트코드
    form_cd               VARCHAR(5)    NOT NULL COMMENT '폼코드', -- 폼코드
    form_no               INT UNSIGNED  NOT NULL COMMENT '폼번호', -- 폼번호
    form_nm               VARCHAR(100)  NOT NULL COMMENT '폼명', -- 폼명
    aply_str_ymd          VARCHAR(8)    NOT NULL COMMENT '적용시작년월일', -- 적용시작년월일
    aply_end_ymd          VARCHAR(8)    NOT NULL COMMENT '적용종료년월일', -- 적용종료년월일
    sbj_tmpl              VARCHAR(500)  NOT NULL COMMENT '제목템플릿', -- 제목템플릿
    cnts_tmpl             TEXT          NOT NULL COMMENT '내용템플릿', -- 내용템플릿
    rmk                   VARCHAR(2000) NULL     COMMENT '비고', -- 비고
    del_yn                CHAR(1)       NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt                TIMESTAMP     NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id               VARCHAR(10)   NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt                TIMESTAMP     NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id               VARCHAR(10)   NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt                TIMESTAMP     NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id               VARCHAR(10)   NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '이메일폼이력';

-- 이메일폼이력
ALTER TABLE email_form_hist
    ADD CONSTRAINT PK_email_form_hist -- 이메일폼이력 기본키
        PRIMARY KEY (
            email_form_hist_seqno -- 이메일폼이력일련번호
        );

ALTER TABLE email_form_hist
    MODIFY COLUMN email_form_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '이메일폼이력일련번호';

ALTER TABLE email_form_hist
    AUTO_INCREMENT = 1;

-- 사이트SMTP이력
CREATE TABLE site_smtp_hist (
    site_smtp_hist_seqno INT UNSIGNED NOT NULL COMMENT '사이트SMTP이력일련번호', -- 사이트SMTP이력일련번호
    hist_div_cd          VARCHAR(8)   NOT NULL COMMENT '이력구분코드', -- 이력구분코드
    site_cd              VARCHAR(3)   NOT NULL COMMENT '사이트코드', -- 사이트코드
    smtp_prtc            VARCHAR(10)  NOT NULL COMMENT 'SMTP프로토콜', -- SMTP프로토콜
    svr_host             VARCHAR(100) NOT NULL COMMENT '서버호스트', -- 서버호스트
    port_no              INT UNSIGNED NOT NULL COMMENT '포트번호', -- 포트번호
    smtp_usr_id          VARCHAR(100) NOT NULL COMMENT 'SMTP사용자ID', -- SMTP사용자ID
    smtp_usr_pwd         VARCHAR(500) NOT NULL COMMENT 'SMTP사용자비밀번호', -- SMTP사용자비밀번호
    trnsr_email          VARCHAR(100) NOT NULL COMMENT '전송자이메일', -- 전송자이메일
    trnsr_nm             VARCHAR(100) NOT NULL COMMENT '전송자명', -- 전송자명
    smtp_use_yn          CHAR(1)      NOT NULL COMMENT 'SMTP사용여부', -- SMTP사용여부
    atrz_yn              CHAR(1)      NOT NULL COMMENT '인증여부', -- 인증여부
    tls_use_yn           CHAR(1)      NOT NULL COMMENT 'TLS사용여부', -- TLS사용여부
    ssl_use_yn           CHAR(1)      NOT NULL COMMENT 'SSL사용여부', -- SSL사용여부
    head_tmpl            TEXT         NULL     COMMENT '상단템플릿', -- 상단템플릿
    foot_tmpl            TEXT         NULL     COMMENT '하단템플릿', -- 하단템플릿
    reg_dt               TIMESTAMP    NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id              VARCHAR(10)  NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt               TIMESTAMP    NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id              VARCHAR(10)  NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '사이트SMTP이력';

-- 사이트SMTP이력
ALTER TABLE site_smtp_hist
    ADD CONSTRAINT PK_site_smtp_hist -- 사이트SMTP이력 기본키
        PRIMARY KEY (
            site_smtp_hist_seqno -- 사이트SMTP이력일련번호
        );

ALTER TABLE site_smtp_hist
    MODIFY COLUMN site_smtp_hist_seqno INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '사이트SMTP이력일련번호';

ALTER TABLE site_smtp_hist
    AUTO_INCREMENT = 1;

-- 상품대상성별
CREATE TABLE prod_tgt_sex (
    site_cd VARCHAR(3)  NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd VARCHAR(22) NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    sex_cd  VARCHAR(8)  NOT NULL COMMENT '성별코드', -- 성별코드
    reg_dt  TIMESTAMP   NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id VARCHAR(10) NOT NULL COMMENT '등록자ID' -- 등록자ID
)
COMMENT '상품대상성별';

-- 상품대상성별
ALTER TABLE prod_tgt_sex
    ADD CONSTRAINT PK_prod_tgt_sex -- 상품대상성별 기본키
        PRIMARY KEY (
            site_cd, -- 사이트코드
            proj_cd, -- 프로젝트코드
            sex_cd   -- 성별코드
        );

-- 의견파일
CREATE TABLE opin_file (
    site_cd         VARCHAR(3)      NOT NULL COMMENT '사이트코드', -- 사이트코드
    proj_cd         VARCHAR(22)     NOT NULL COMMENT '프로젝트코드', -- 프로젝트코드
    insp_no         INT UNSIGNED    NOT NULL COMMENT '검수번호', -- 검수번호
    insp_file_no    INT UNSIGNED    NOT NULL COMMENT '검수파일번호', -- 검수파일번호
    opin_file_no    INT UNSIGNED    NOT NULL COMMENT '의견파일번호', -- 의견파일번호
    org_file_nm     VARCHAR(300)    NOT NULL COMMENT '원본파일명', -- 원본파일명
    mng_file_nm     VARCHAR(300)    NOT NULL COMMENT '관리파일명', -- 관리파일명
    psc_file_nm     VARCHAR(300)    NOT NULL COMMENT '물리파일명', -- 물리파일명
    tumb_sm_file_nm VARCHAR(300)    NULL     COMMENT '썸네일소파일명', -- 썸네일소파일명
    tumb_md_file_nm VARCHAR(300)    NULL     COMMENT '썸네일중파일명', -- 썸네일중파일명
    file_path       VARCHAR(500)    NOT NULL COMMENT '파일경로', -- 파일경로
    file_cntn_typ   VARCHAR(100)    NULL     COMMENT '파일컨텐츠유형', -- 파일컨텐츠유형
    file_size       BIGINT UNSIGNED NOT NULL COMMENT '파일크기', -- 파일크기
    file_ext_nm     VARCHAR(100)    NULL     COMMENT '파일확장자명', -- 파일확장자명
    srt_ordno       INT UNSIGNED    NOT NULL COMMENT '정렬순번', -- 정렬순번
    del_yn          CHAR(1)         NOT NULL COMMENT '삭제여부', -- 삭제여부
    del_dt          TIMESTAMP       NULL     COMMENT '삭제일시', -- 삭제일시
    delr_id         VARCHAR(10)     NULL     COMMENT '삭제자ID', -- 삭제자ID
    reg_dt          TIMESTAMP       NOT NULL COMMENT '등록일시', -- 등록일시
    regr_id         VARCHAR(10)     NOT NULL COMMENT '등록자ID', -- 등록자ID
    mod_dt          TIMESTAMP       NOT NULL COMMENT '수정일시', -- 수정일시
    modr_id         VARCHAR(10)     NOT NULL COMMENT '수정자ID' -- 수정자ID
)
COMMENT '의견파일';

-- 의견파일
ALTER TABLE opin_file
    ADD CONSTRAINT PK_opin_file -- 의견파일 기본키
        PRIMARY KEY (
            site_cd,      -- 사이트코드
            proj_cd,      -- 프로젝트코드
            insp_no,      -- 검수번호
            insp_file_no, -- 검수파일번호
            opin_file_no  -- 의견파일번호
        );