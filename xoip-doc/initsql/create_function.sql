/* fn_get_com_cd_nm */

DELIMITER $$
DROP FUNCTION IF EXISTS fn_get_com_cd_nm $$ 
CREATE FUNCTION fn_get_com_cd_nm ( inSiteCd VARCHAR(3), inComCd VARCHAR(10) ) 
RETURNS VARCHAR(100)
BEGIN
    DECLARE outComCdNm VARCHAR(100);

    SELECT com_cd_nm
      INTO outComCdNm
      FROM com_cd 
     WHERE site_cd = inSiteCd
       AND com_cd = inComCd
       AND del_yn = 'N';  

     RETURN outComCdNm;
END $$

/* fn_get_lc_com_cd_nm */

DELIMITER $$
DROP FUNCTION IF EXISTS fn_get_lc_com_cd_nm $$ 
CREATE FUNCTION fn_get_lc_com_cd_nm ( inSiteCd VARCHAR(3), inComCd VARCHAR(10), inLang VARCHAR(2) ) 
RETURNS VARCHAR(100)
BEGIN
    DECLARE outComCdNm VARCHAR(100);

    SELECT CASE WHEN inLang = 'ko' THEN com_cd_nm
                ELSE com_cd_eng_nm
            END com_cd_nm
      INTO outComCdNm
      FROM com_cd 
     WHERE site_cd = inSiteCd
       AND com_cd = inComCd
       AND del_yn = 'N';  

     RETURN outComCdNm;
END $$
