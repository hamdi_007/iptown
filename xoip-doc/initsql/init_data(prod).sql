/* 공통코드 이력 */
insert into com_cd_hist (hist_div_cd, site_cd, com_cd, com_cd_nm, com_cd_eng_nm, top_site_cd, top_com_cd, cd_val_1, cd_val_2, cd_val_3, cd_val_4, cd_val_5, use_yn, srt_ordno, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id) 
select 'HD0100', site_cd, com_cd, com_cd_nm, com_cd_eng_nm, top_site_cd, top_com_cd, cd_val_1, cd_val_2, cd_val_3, cd_val_4, cd_val_5, use_yn, srt_ordno, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id
  from com_cd
 where 1=1;
 
/* 사이트 */
insert into site (site_cd, site_nm, site_eng_nm, use_yn, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id) values ('CJE', 'CJENM LABS', 'CJENM LABS', 'Y', null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM');

/* 사이트이력 */
insert into site_hist (hist_div_cd, site_cd, site_nm, site_eng_nm, use_yn, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id)
select 'HD0100', site_cd, site_nm, site_eng_nm, use_yn, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id
  from site
 where 1=1;

/* 회사 */
insert into comp (site_cd, comp_id, comp_cd, comp_nm, comp_div_cd, ntn_cd, bizno, addr, dtl_addr, post_no, ceo_nm, rep_telno, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id)
values ('CJE', 'COCJE10000', 'CJE', 'CJENM', 'CD0100', 'SNKR00', '1234512345', null, null, null, null, null, null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM');

/* 회사이력 */
insert into comp_hist (hist_div_cd, site_cd, comp_id, comp_cd, comp_nm, comp_div_cd, ntn_cd, bizno, addr, dtl_addr, post_no, ceo_nm, rep_telno, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id)
select 'HD0100', site_cd, comp_id, comp_cd, comp_nm, comp_div_cd, ntn_cd, bizno, addr, dtl_addr, post_no, ceo_nm, rep_telno, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id
  from comp 
 where 1=1;
 
/* 사용자 */
insert into usr (site_cd, usr_id, comp_id, email, usr_nm, usr_eng_nm, pwd, telno, hnpno, auth, usr_sts_cd, ivt_dt, ivtr_id, join_dt, wtdr_dt, wtdr_hndlr_id, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id)
values ('CJE', 'CJE1000000', 'COCJE10000', 'nr.kong@cj.net', '공나래', 'NR Kong', '$2a$10$aN3DIDUZ4YZgzie4twZIV.n.FQVPZNjU6rDxxmSIi/ZBBDkLYMp1m', null, null, 'ROLE_ADMIN', 'US0300', null, null, now(), null, null, null, 'N', null, null, now(), 'SYSTEM', now(), 'SYSTEM');

/* 사용자이력 */
insert into usr_hist (hist_div_cd, site_cd, usr_id, comp_id, email, usr_nm, usr_eng_nm, pwd, telno, hnpno, auth, usr_sts_cd, ivt_dt, ivtr_id, join_dt, wtdr_dt, wtdr_hndlr_id, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id)
select 'HD0100', site_cd, usr_id, comp_id, email, usr_nm, usr_eng_nm, pwd, telno, hnpno, auth, usr_sts_cd, ivt_dt, ivtr_id, join_dt, wtdr_dt, wtdr_hndlr_id, rmk, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id
  from usr 
 where 1=1;
 
/* 상품유형 이력 */
insert into prod_typ_hist (hist_div_cd, site_cd, prod_typ_id, prod_typ_cd, prod_typ_nm, prod_typ_eng_nm, prod_typ_lv, top_site_cd, top_prod_typ_id, use_yn, srt_ordno, dtl_desc, eng_dtl_desc, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id) 
select 'HD0100', site_cd, prod_typ_id, prod_typ_cd, prod_typ_nm, prod_typ_eng_nm, prod_typ_lv, top_site_cd, top_prod_typ_id, use_yn, srt_ordno, dtl_desc, eng_dtl_desc, del_yn, del_dt, delr_id, reg_dt, regr_id, mod_dt, modr_id 
  from prod_typ
 where 1=1;
  