/* 사이트 SMTP */
insert into site_smtp (site_cd, smtp_prtc, svr_host, port_no, smtp_usr_id, smtp_usr_pwd, trnsr_email, trnsr_nm, smtp_use_yn, atrz_yn, tls_use_yn, ssl_use_yn, head_tmpl, foot_tmpl, reg_dt, regr_id, mod_dt, modr_id)
values ('CJE', 'smtps', 'email-smtp.ap-northeast-2.amazonaws.com', 465, 'AKIAQ7IOSCUSX6SIV6LF', 'BBlTrPDD15L3JRsr664gmgfU5HdlnQqKWKE78vDgTci+', 'nr.kong@cj.net', 'CJENM', 'Y', 'Y', 'Y', 'Y', null, null, now(), 'SYSTEM', now(), 'SYSTEM');

/* 템플릿 수정 */
update site_smtp
   set head_tmpl = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>IPTOWN</title>
</head>
<body>
    <!-- OUTERMOST CONTAINER TABLE -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="font-family: \'dotum\', sans-serif;font-size: 12px;line-height: 20px;letter-spacing: -0.01em;color: #000;">
        <tr>
            <td>
                <!-- 700px CONTENTS CONTAINER TABLE -->
                <table border="0" cellpadding="0" cellspacing="0" width="700" class="table-wrapper" align="center" style="padding-top: 31px;padding-bottom: 30px;padding-left: 29px;padding-right: 29px;border: 1px solid #d6d6d6;">
                    <tr>
                        <td>
                            <!-- HEADER TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-header" style="border-bottom: 1px solid #d6d6d6;padding-bottom: 20px;">
                                <tr>
                                    <td valign="top">
                                        <img src="#{WEB_URL}/images/logo_#{SITE_CD}.png" alt="LOGO" width="106" height="53" />
                                    </td>
                                    <td valign="top" style="text-align: right;">
                                        <img src="#{WEB_URL}/images/logo_IP.png" alt="IPTOWN" width="106" height="53" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- CONTENTS TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-content" style="min-height: 300px;padding-top: 30px;padding-bottom: 30px;">
                                <tr>
                                    <td valign="top">
',
       head_tmpl_eng = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>IPTOWN</title>
</head>
<body>
    <!-- OUTERMOST CONTAINER TABLE -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="font-family: \'dotum\', sans-serif;font-size: 12px;line-height: 20px;letter-spacing: -0.01em;color: #000;">
        <tr>
            <td>
                <!-- 700px CONTENTS CONTAINER TABLE -->
                <table border="0" cellpadding="0" cellspacing="0" width="700" class="table-wrapper" align="center" style="padding-top: 31px;padding-bottom: 30px;padding-left: 29px;padding-right: 29px;border: 1px solid #d6d6d6;">
                    <tr>
                        <td>
                            <!-- HEADER TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-header" style="border-bottom: 1px solid #d6d6d6;padding-bottom: 20px;">
                                <tr>
                                    <td valign="top">
                                        <img src="#{WEB_URL}/images/logo_#{SITE_CD}.png" alt="LOGO" width="106" height="53" />
                                    </td>
                                    <td valign="top" style="text-align: right;">
                                        <img src="#{WEB_URL}/images/logo_IP.png" alt="IPTOWN" width="106" height="53" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- CONTENTS TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-content" style="min-height: 300px;padding-top: 30px;padding-bottom: 30px;">
                                <tr>
                                    <td valign="top">
',
       foot_tmpl = '
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- FOOTER TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-footer" style="background: #ededed;padding-top: 20px;padding-right: 20px;padding-bottom: 18px;padding-left: 20px;text-align: center;color: #333;">
                                <tr>
                                    <td valign="top">
                                        본 메일은 IPTOWN 가입시 자동 발송되는 <b><font color="#0971ce">발송전용</font></b> 메일입니다.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
',
       foot_tmpl_eng = '
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- FOOTER TABLE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-footer" style="background: #ededed;padding-top: 20px;padding-right: 20px;padding-bottom: 18px;padding-left: 20px;text-align: center;color: #333;">
                                <tr>
                                    <td valign="top">
                                        This mail is <b><font color="#0971ce">only sent automatically</font></b> when you sign up for IPTOWN.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
'
 where site_cd = 'CJE';

/* 사이트 SMTP 이력 */
insert into site_smtp_hist (hist_div_cd, site_cd, smtp_prtc, svr_host, port_no, smtp_usr_id, smtp_usr_pwd, trnsr_email, trnsr_nm, smtp_use_yn, atrz_yn, tls_use_yn, ssl_use_yn, head_tmpl, foot_tmpl, reg_dt, regr_id, mod_dt, modr_id)
select 'HD0100', site_cd, smtp_prtc, svr_host, port_no, smtp_usr_id, smtp_usr_pwd, trnsr_email, trnsr_nm, smtp_use_yn, atrz_yn, tls_use_yn, ssl_use_yn, head_tmpl, foot_tmpl, reg_dt, regr_id, mod_dt, modr_id
  from site_smtp
 where 1=1;

